package bluedot.spectrum.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bluedot.spectrum.bean.User;
import bluedot.spectrum.dao.cfg.SqlResult;
import bluedot.spectrum.dao.cfg.SqlResult.SqlData;
import bluedot.spectrum.exception.DaoException;
import bluedot.spectrum.utils.BeanUtils;
import bluedot.spectrum.utils.DBUtils;
import bluedot.spectrum.utils.TxQueryRunner;

/**
 * 
 * @author longshu 2016年8月22日
 */
public class BaseDaoTest implements BaseDao {
	private QueryRunner qr = new TxQueryRunner();

	Logger logger = Logger.getLogger(BaseDaoTest.class);
	Map<String, Object> params;
	private int premarykey;
	User user = new User();

	@Before
	public void init() {
		// params = new HashMap<String, Object>();

		// user.setRoleID(Role.SUPERADMIN);
		user.setPassword("00000000");
		user.setEmail("夏天的冬天的邮箱");
		user.setUsername("lalalal");
	}

	@After
	public void after() {
		logger.debug(params);
	}

	// 得到自增主键值
	public int getPremarykeyValue() {
		return premarykey;
	}

	@Override
	public int add(String view, Map<String, Object> params) throws DaoException {
		SqlData sqlData = SqlResult.getSqlData(view, params);
		String sql = sqlData.getSql();
		try {
			DBUtils.beginTransaction();// 开启事务
			int count = qr.update(sql, sqlData.getParList().toArray());
			String sql1 = "SELECT LAST_INSERT_ID()";
			Number key = qr.query(sql1, new ScalarHandler<Number>());
			if (key != null)
				premarykey = key.intValue();
			System.out.println("----->" + premarykey);
			DBUtils.commitTransaction();// 提交事务
			return count;
		} catch (SQLException e) {
			try {
				DBUtils.rollbackTransaction();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} // 回滚事务
			logger.error(e.getMessage(), e);
			throw new DaoException(e);
		}
	}

	@Override
	public int delete(String view, Map<String, Object> params) throws DaoException {
		return update(view, params);
	}

	@Override
	public int update(String view, Map<String, Object> params) throws DaoException {
		SqlData sqlData = SqlResult.getSqlData(view, params);
		String sql = sqlData.getSql();
		logger.debug(sql);
		try {
			return qr.update(sql, sqlData.getParList().toArray());
		} catch (SQLException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public List<Map<String, Object>> query(String view, Map<String, Object> params) throws DaoException {
		SqlData sqlData = SqlResult.getSqlData(view, params);
		String sql = sqlData.getSql();
		/*Object[] par1 = sqlData.getParList().toArray();
		sqlData = sqlData.ChangeByConditions("where email=?");
		String sql2 = sqlData.getSql();
		Object[] par2 = sqlData.getParList().toArray();
		System.out.println(sql);
		System.out.println("参数：");
		for(Object o1:par1){
			System.out.println(o1);
		}
		System.out.println("使用外传where替换方法，where子句： where email=?");
		System.out.println("替换后：");
		System.out.println(sql2);
		System.out.println("参数：");
		for(Object o2:par2){
			System.out.println(o2);
		}*/
		List<Map<String, Object>> list = null;
		try {
			list = qr.query(sql, new MapListHandler(), sqlData.getParList().toArray());
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		return list;
	}

	@Override
	public List<Map<String, Object>> query(String view, Map<String, Object> params, String strWhere)
			throws DaoException {
		SqlData sqlData = SqlResult.getSqlData(view, params);
		sqlData.ChangeByConditions(strWhere);
		String sql = sqlData.getSql();
		logger.debug("-------------------->" + sql);
		List<Map<String, Object>> list = null;
		try {
			list = qr.query(sql, new MapListHandler(), sqlData.getParList().toArray());
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		return list;
	}

	/**
	 * Test method for {@link bluedot.spectrum.dao.BaseDao#add(java.lang.String, java.util.Map)}.
	 * @throws DaoException 
	 */
	@Test
	public void testAdd() throws DaoException {
		params = BeanUtils.beanToMap(user);
		System.out.println(user);
		int r = add("addUser", params);
		logger.debug(r);
	}

	/**
	 * Test method for {@link bluedot.spectrum.dao.BaseDao#delete(java.lang.String, java.util.Map)}.
	 * @throws DaoException 
	 */
	@Test
	public void testDelete() throws DaoException {
		user.setUserID(3);
		params = BeanUtils.beanToMap(user);
		int r = delete("deleteUser", params);
		logger.debug(r);
	}

	/**
	 * Test method for {@link bluedot.spectrum.dao.BaseDao#update(java.lang.String, java.util.Map)}.
	 * @throws DaoException 
	 */
	@Test
	public void testUpdate() throws DaoException {
		user.setEmail("修改了");
		user.setUserID(2);
		//params = BeanUtils.beanToMap(user);
		Map param = new HashMap<String,Object>();
		param.put("userID", 22);
		param.put("favoriteID",  33);
		param.put("favoriteName", "xingss");
		int r = update("updateFavorite", param);
		logger.debug(r);
	}

	/**
	 * Test method for {@link bluedot.spectrum.dao.BaseDao#query(java.lang.String, java.util.Map)}.
	 * @throws DaoException 
	 */
	@Test
	public void testQuery() throws DaoException {
		user.setEmail("夏天的冬天的邮箱");
		params = BeanUtils.beanToMap(user);
		List<Map<String, Object>> list = query("test", params, "limit 0,20");
		logger.debug(list);
	}

	@Override
	public void transaction() {
	}

	@Override
	public void commit() {
	}

	@Override
	public void rollback() {
	}
    @Test
    public void getTest(){
    	SqlData sqlData;
    
		try {
	      Map<String, Object> params=new HashMap<String,Object>();
	      params.put("reportID", 1);
			sqlData = SqlResult.getSqlData("queryViewReport", params);
			String sql = sqlData.getSql();
			System.out.println("sql="+sql);
		} catch (DaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
    	
    }
}
