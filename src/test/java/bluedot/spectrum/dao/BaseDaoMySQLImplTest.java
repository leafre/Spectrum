package bluedot.spectrum.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bluedot.spectrum.bean.Role;
import bluedot.spectrum.bean.User;
import bluedot.spectrum.exception.DaoException;
import bluedot.spectrum.utils.BeanUtils;

/**
 * 
 * @author longshu 2016年8月22日
 */
public class BaseDaoMySQLImplTest {
	Logger logger = Logger.getLogger(BaseDaoTest.class);
	BaseDao dao = new BaseDaoMySQLImpl();
	Map<String, Object> params;
	User user = new User();

	@Before
	public void init() {
		user.setRoleID(Role.SUPERADMIN);
		user.setUsername("隔壁老王");
		user.setPassword("00000000");
		user.setEmail("123@q.com");
	}

	@After
	public void after() {
		logger.debug(params);
	}

	/**
	 * Test method for {@link bluedot.spectrum.dao.BaseDaoMySQLImpl#add(java.lang.String, java.util.Map)}.
	 * @throws DaoException 
	 */
	@Test
	public void testAdd() throws DaoException {
		params = BeanUtils.beanToMap(user);
		int r = dao.add("addUser", params);
		logger.debug(r);
	}

	/**
	 * Test method for {@link bluedot.spectrum.dao.BaseDaoMySQLImpl#delete(java.lang.String, java.util.Map)}.
	 * @throws DaoException 
	 */
	@Test
	public void testDelete() throws DaoException {
		user.setUserID(3);
		params = BeanUtils.beanToMap(user);
		int r = dao.delete("deleteUser", params);
		logger.debug(r);
	}

	/**
	 * Test method for {@link bluedot.spectrum.dao.BaseDaoMySQLImpl#update(java.lang.String, java.util.Map)}.
	 * @throws DaoException 
	 */
	@Test
	public void testUpdate() throws DaoException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("spectrumID", "123");
		params.put("spectrumName", "123");
		params.put("description", "123");
		int r = dao.update("modifySpectrum", params);
		logger.debug(r);
	}

	/**
	 * Test method for {@link bluedot.spectrum.dao.BaseDaoMySQLImpl#query(java.lang.String, java.util.Map)}.
	 * @throws DaoException 
	 */
	@Test
	public void testQuery() throws DaoException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("spectrumTypeID", 123);
		params.put("detectedID", 123);
		//params.put("pluginID", 123);
		//列'ID'在字段列表中重复，其实就是两张表有相同的字段，但是使用时表字段的名称前没有加表名，导致指代不明
		List<Map<String, Object>> listMap = dao.query("queryPluginID", params);
		logger.debug(listMap);
	}

}
