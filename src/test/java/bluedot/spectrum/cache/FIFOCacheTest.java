package bluedot.spectrum.cache;

import org.junit.Test;

/**
 * 
 * @author longshu 2016年9月18日
 */
public class FIFOCacheTest {

	Cache<Integer, Object> cache = new FIFOCache<Integer, Object>(2, 1000);

	@Test
	public void test() throws InterruptedException {
		cache.put(1, 1);
		cache.put(2, 2, 500);
		cache.put(3, 3);

		System.out.println("isFull : " + cache.isFull());
		System.out.println("get(1):" + cache.get(1));
		System.out.println("contains(1) : " + cache.contains(1));
		Thread.sleep(600);
		System.out.println("contains(2) : " + cache.get(2));
		System.out.println("contains(3) : " + cache.get(3));
	}

}
