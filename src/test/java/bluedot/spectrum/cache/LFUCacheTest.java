package bluedot.spectrum.cache;

import org.junit.Test;

/**
 * 
 * @author longshu 2016年9月18日
 */
public class LFUCacheTest {

	Cache<Integer, Object> cache = new LFUCache<Integer, Object>(2, 1000);

	@Test
	public void test() throws InterruptedException {
		cache.put(1, 1);
		cache.get(1);
		
		cache.put(2, 2, 500);
		Thread.sleep(600);

		cache.put(3, 3);
		cache.get(3);
		cache.get(3);
		cache.get(3);
		cache.put(4, 4);

		System.out.println("isFull : " + cache.isFull());
		System.out.println("get(1):" + cache.get(1));// 最少使用
		System.out.println("get(2):" + cache.get(2));// 过期
		System.out.println("get(3):" + cache.get(3));
		System.out.println("get(4):" + cache.get(4));
	}

}
