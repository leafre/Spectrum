package bluedot.spectrum.web.core;

import org.junit.Test;

/**
 * 
 * @author longshu 2016年9月29日
 */
public class ServiceMapperTest {

	/**
	 * Test method for {@link bluedot.spectrum.web.core.ServiceMapper#getServiceConfig(java.lang.String)}.
	 */
	@Test
	public void testGetServiceConfig() {
		ServiceConfig serviceConfig = ServiceMapper.getServiceConfig("login");
		System.out.println(serviceConfig);
	}

	/**
	 * Test method for {@link bluedot.spectrum.web.core.ServiceMapper#reload()}.
	 */
	@Test
	public void testReload() {
		ServiceMapper.reload();
	}

	/**
	 * Test method for {@link bluedot.spectrum.web.core.ServiceMapper#parser()}.
	 */
	@Test
	public void testParser() {
		ServiceMapper.preParser();
	}

}
