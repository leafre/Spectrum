package bluedot.spectrum.web.core;

import org.junit.Test;

/**
 * 
 * @author longshu 2016年8月25日
 */
public class FSConfigTest {

	static {
		FSConfig.init("test");
	}

	/**
	 * Test method for {@link bluedot.spectrum.web.core.FSConfig#userFavorite()}.
	 */
	@Test
	public void testUserFavorite() {
		System.out.println(FSConfig.userFavorite());
	}

	/**
	 * Test method for {@link bluedot.spectrum.web.core.FSConfig#standSpectrum()}.
	 */
	@Test
	public void testStandSpectrum() {
		System.out.println(FSConfig.standSpectrum());
	}

	/**
	 * Test method for {@link bluedot.spectrum.web.core.FSConfig#application()}.
	 */
	@Test
	public void testApplicationDir() {
		System.out.println(FSConfig.application());
	}

	/**
	 * Test method for {@link bluedot.spectrum.web.core.FSConfig#backup()}.
	 */
	@Test
	public void testBackup() {
		System.out.println(FSConfig.backup());
	}

	/**
	 * Test method for {@link bluedot.spectrum.web.core.FSConfig#temp()}.
	 */
	@Test
	public void testTemp() {
		System.out.println(FSConfig.temp());
	}

	/**
	 * Test method for {@link bluedot.spectrum.web.core.FSConfig#template()}.
	 */
	@Test
	public void testTemplate() {
		System.out.println(FSConfig.template());
	}
	
}
