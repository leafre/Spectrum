package bluedot.spectrum.utils.mail;

import java.io.IOException;

/**
 * 
 * @author longshu 2016年9月2日
 */
public class SendEmailUtilsTest {

	public static void main(String[] args) throws IOException {
		SendEmailUtils.init(null);
		String subject = SendEmailUtils.getSubject("test_subject", "349759857@qq.com");
		String content = SendEmailUtils.getContent("test_content", "349759857@qq.com", "测试内容");
		Mail mail = SendEmailUtils.getMail("349759857@qq.com", subject, content);
		SendEmailUtils.sendMail(mail);
	}

}