package bluedot.spectrum.utils;

import static bluedot.spectrum.utils.StringUtils.*;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * 
 * @author longshu 2016年8月21日
 */
public class StringUtilsTest {

	/**
	 * Test method for {@link bluedot.spectrum.utils.StringUtils#arrayIsEmpty(java.lang.CharSequence[])}.
	 */
	@Test
	public void testArrayIsEmpty() {
		assertEquals(true,arrayIsEmpty(new String[] {}));
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.StringUtils#isEmpty(java.lang.CharSequence)}.
	 */
	@Test
	public void testIsEmpty() {
		assertEquals(false,isEmpty("test"));
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.StringUtils#isAnyEmpty(java.lang.CharSequence[])}.
	 */
	@Test
	public void testIsAnyEmpty() {
		assertEquals(true,isAnyEmpty("1", "2", null));
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.StringUtils#isBlank(java.lang.CharSequence)}.
	 */
	@Test
	public void testIsBlank() {
		assertEquals(true, isBlank("     "));
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.StringUtils#isAnyBlank(java.lang.CharSequence[])}.
	 */
	@Test
	public void testIsAnyBlank() {
		assertEquals(true, isAnyBlank(" ",null));
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.StringUtils#trim(java.lang.String)}.
	 */
	@Test
	public void testTrim() {
		assertEquals("trim", trim("  trim  "));
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.StringUtils#trimToNull(java.lang.String)}.
	 */
	@Test
	public void testTrimToNull() {
		assertEquals(null, trimToNull("     "));
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.StringUtils#trimToEmpty(java.lang.String)}.
	 */
	@Test
	public void testTrimToEmpty() {
		assertEquals(EMPTY, trimToEmpty("     "));
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.StringUtils#startsWith(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testStartsWith() {
		assertEquals(true, startsWith("123", "1"));
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.StringUtils#startsWithAny(java.lang.String, java.lang.String[])}.
	 */
	@Test
	public void testStartsWithAny() {
		assertEquals(true, startsWithAny("asvcs", "c","a"));
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.StringUtils#endsWith(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testEndsWith() {
		assertEquals(true, endsWith("1234abc", "bc"));
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.StringUtils#endsWithAny(java.lang.String, java.lang.String[])}.
	 */
	@Test
	public void testEndsWithAny() {
		assertEquals(true, endsWithAny("qwert", "a","1","rt"));
	}

}
