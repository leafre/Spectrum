package bluedot.spectrum.utils;

import java.io.IOException;

import org.junit.Test;

/**
 * 
 * @author longshu 2016年10月12日
 */
public class Base64ImageTest {

	/**
	 * Test method for {@link bluedot.spectrum.utils.Base64Image#toImage()}.
	 * @throws IOException 
	 */
	@Test
	public void testToImage() throws IOException {
		Base64Image image = new Base64Image(Base64Image.toBase64Data("test/base64.jpg"));
		image.toImage("test/base64Img.jpg");
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.Base64Image#toBase64Data()}.
	 * @throws IOException 
	 */
	@Test
	public void testToBase64Data() throws IOException {
		String data = Base64Image.toBase64Data("test/base64.jpg");
		System.out.println(data);
	}

}
