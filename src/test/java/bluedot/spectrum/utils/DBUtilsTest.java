package bluedot.spectrum.utils;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * 
 * @author longshu 2016年8月17日
 */
public class DBUtilsTest {
	Logger logger = Logger.getLogger(DBUtilsTest.class);

	/**
	 * Test method for {@link bluedot.spectrum.utils.DBUtils#getDataSource()}.
	 */
	@Test
	public void testGetDataSource() {
		DataSource ds = DBUtils.getDataSource();
		logger.info("DataSource:" + ds);
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.DBUtils#getConnection()}.
	 * {@link bluedot.spectrum.utils.DBUtils#releaseConnection(java.sql.Connection)}.
	 * @throws SQLException 
	 */
	@Test
	public void testGetConnection() throws SQLException {
		Connection connection = DBUtils.getConnection();
		logger.info("Connection:" + connection);
		DBUtils.releaseConnection(connection);
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.DBUtils#beginTransaction()}.
	 * {@link bluedot.spectrum.utils.DBUtils#commitTransaction()}.
	 * {@link bluedot.spectrum.utils.DBUtils#rollbackTransaction()}.
	 * @throws SQLException 
	 */
	@Test
	public void testTransaction() throws SQLException {
		boolean success = true;

		DBUtils.beginTransaction();
		logger.info("beginTransaction");
		if (success) {
			DBUtils.commitTransaction();
			logger.info("commitTransaction");
		} else {
			DBUtils.rollbackTransaction();
			logger.info("rollbackTransaction");
		}
		System.out.println("====================");
		success = false;
		DBUtils.beginTransaction();
		logger.info("beginTransaction");
		if (success) {
			DBUtils.commitTransaction();
			logger.info("commitTransaction");
		} else {
			DBUtils.rollbackTransaction();
			logger.info("rollbackTransaction");
		}
	}

}
