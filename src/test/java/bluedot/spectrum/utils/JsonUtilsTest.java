package bluedot.spectrum.utils;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import bluedot.spectrum.api.Point;
import bluedot.spectrum.api.SpectrumFile;
import bluedot.spectrum.bean.Backup;
import bluedot.spectrum.bean.Recover;

/**
 * 
 * @author longshu 2016年8月24日
 */
public class JsonUtilsTest {

	/**
	 * Test method for {@link bluedot.spectrum.utils.JsonUtils#Gson()}.
	 */
	@Test
	public void testGson() {
		List<Point> list = new ArrayList<Point>();
		list.add(new Point(123, 321.2));
		list.add(new Point(124, 322.3));
		list.add(new Point(125, 324.5));
		list.add(new Point(126, 327.7));
		list.add(new Point(127, 325.3));
		SpectrumFile file = new SpectrumFile("敌敌畏", "拉曼", list);
		file.setExtInfo("time", "2016-08-25");
		file.setPeaks(list);
		String json = JsonUtils.Gson(true).toJson(file);
		System.out.println(json);
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.JsonUtils#toJson(java.lang.Object, java.lang.String)}.
	 */
	@Test
	public void testToJson() {
		Backup backup = new Backup(12, "备份", "备份内容", "我的备份", 1234567890L);
		Recover recover = new Recover(12, backup, 1231233545, "内容", "系统恢复");
		String json = JsonUtils.Gson(false).toJson(recover);
		System.out.println(json);
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.JsonUtils#fromJson(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testFromJson() {
		Backup backup = new Backup(12, "备份", "备份内容", "我的备份", 1234567890L);
		Recover recover = new Recover(12, backup, 1231233545, "内容", "系统恢复");
		String json = JsonUtils.Gson(false).toJson(recover);
		Recover recover2 = JsonUtils.fromJson(json, Recover.class.getName());
		System.out.println(JsonUtils.Gson(true).toJson(recover2));
	}

	@Test
	public void testFromJsonFile() throws Exception {
		String path = SpectrumFile.class.getClassLoader().getResource("spectrum.file.json").getPath();
		SpectrumFile spectrumFile = JsonUtils.Gson(false).fromJson(new FileReader(path), SpectrumFile.class);
		String json = JsonUtils.Gson(true).toJson(spectrumFile, SpectrumFile.class);
		System.out.println(json);
	}
}
