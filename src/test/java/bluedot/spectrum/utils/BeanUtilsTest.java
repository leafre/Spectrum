package bluedot.spectrum.utils;

import java.util.Map;

import org.junit.Test;

import bluedot.spectrum.bean.Backup;

/**
 * 
 * @author longshu 2016年8月21日
 */
public class BeanUtilsTest {
	Backup bak = new Backup(1, "我的备份", "好多", "我的好多备份", System.currentTimeMillis());

	/**
	 * Test method for {@link bluedot.spectrum.utils.BeanUtils#beanToMap(java.lang.Object)}.
	 */
	@Test
	public void testBeanToMap() {
		Map<String, Object> beanToMap = BeanUtils.beanToMap(bak);
		System.out.println(beanToMap);
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.BeanUtils#mapToBean(java.util.Map, java.lang.Class)}.
	 */
	@Test
	public void testMapToBean() {
		Map<String, Object> beanToMap = BeanUtils.beanToMap(bak);
		Backup bean = BeanUtils.mapToBean(beanToMap, Backup.class);
		System.out.println(bean);
	}


}
