package bluedot.spectrum.utils;

import org.junit.Test;

/**
 * 
 * @author longshu 2016年8月25日
 */
public class FileUtilsTest {

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#getUUIDFilename(java.lang.String, boolean)}.
	 */
	@Test
	public void testGetUUIDFilenameStringBoolean() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#getUUIDFilename(java.lang.String, boolean, java.util.Date)}.
	 */
	@Test
	public void testGetUUIDFilenameStringBooleanDate() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#makeDirName(java.lang.String[])}.
	 */
	@Test
	public void testMakeDirName() {
		FileUtils.makeDirName("test", "1", "2");
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#delete(java.util.List)}.
	 */
	@Test
	public void testDeleteListOfString() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#delete(java.lang.String)}.
	 */
	@Test
	public void testDeleteString() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#getTempDirectoryPath()}.
	 */
	@Test
	public void testGetTempDirectoryPath() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#getTempDirectory()}.
	 */
	@Test
	public void testGetTempDirectory() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#Utils()}.
	 */
	@Test
	public void testUtils() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#copyFile(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testCopyFile() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#copyFileToDirectory(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testCopyFileToDirectory() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#copyDirectory(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testCopyDirectory() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#copyDirectoryToDirectory(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testCopyDirectoryToDirectory() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#copyInputStreamToFile(java.io.InputStream, java.lang.String)}.
	 */
	@Test
	public void testCopyInputStreamToFile() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#moveDirectory(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testMoveDirectory() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#moveDirectoryToDirectory(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testMoveDirectoryToDirectory() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#moveFile(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testMoveFile() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#moveFileToDirectory(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testMoveFileToDirectory() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#sizeOf(java.lang.String)}.
	 */
	@Test
	public void testSizeOf() {
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.FileUtils#sizeOfDirectory(java.lang.String)}.
	 */
	@Test
	public void testSizeOfDirectory() {

	}

}
