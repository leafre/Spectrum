package bluedot.spectrum.utils;

import org.junit.Test;

/**
 * 
 * @author longshu 2016年8月17日
 */
public class EncryptDecryptTest {

	/**
	 * Test method for {@link bluedot.spectrum.utils.EncryptDecrypt#SHA1Bit(byte[])}.
	 */
	@Test
	public void testSHA1Bit() {
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.EncryptDecrypt#SHA1(java.lang.String)}.
	 */
	@Test
	public void testSHA1() {
		String sha1 = EncryptDecrypt.SHA1("longshu");
		System.out.println("SHA1: " + sha1);
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.EncryptDecrypt#MD5Bit(byte[])}.
	 */
	@Test
	public void testMD5Bit() {
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.EncryptDecrypt#MD5(java.lang.String)}.
	 */
	@Test
	public void testMD5() {
		String md5 = EncryptDecrypt.MD5("longshu");
		System.out.println("MD5: " + md5);
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.EncryptDecrypt#encodeBASE64(java.lang.String)}.
	 */
	@Test
	public void testBASE64() {
		String base64 = EncryptDecrypt.encodeBASE64("longshu");
		System.out.println("encodeBASE64: " + base64);
		System.out.println("decodeBASE64: " + EncryptDecrypt.decodeBASE64(base64));
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.EncryptDecrypt#encryptBitAES(byte[], java.lang.String)}.
	 */
	@Test
	public void testBitAES() {
	}


	/**
	 * Test method for {@link bluedot.spectrum.utils.EncryptDecrypt#decryptAES(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testAES() {
		String aes = EncryptDecrypt.encryptAES("longshu", "longshu");
		System.out.println("encodeBASE64: " + aes);
		System.out.println("decodeBASE64: " + EncryptDecrypt.decryptAES(aes, "longshu"));
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.EncryptDecrypt#byte2HexStr(byte[])}.
	 */
	@Test
	public void testByte2HexStr() {
		byte[] bytes = new byte[] { 8, 18 };
		String hex = EncryptDecrypt.byte2HexStr(bytes);
		System.out.println("hex:" + hex);
	}

	/**
	 * Test method for {@link bluedot.spectrum.utils.EncryptDecrypt#hexStr2Bytes(java.lang.String)}.
	 */
	@Test
	public void testHexStr2Bytes() {
		byte[] bytes = EncryptDecrypt.hexStr2Bytes("0812");
		for (byte b : bytes) {
			System.out.println("byte:" + b);
		}
	}

}
