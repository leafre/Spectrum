package bluedot.spectrum.utils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

/**
 * 
 * @author 刘驭洲 2016年9月20日
 */
public class CreateWordUtilsTest {

	@Test
	public void test() {
		Map<String,Object>dataMap = new HashMap<String,Object>();
		Map<String,Object>ext = new HashMap<String,Object>();
		setData(dataMap,ext);
		try {
			new CreateWordUtils().createWord(dataMap, ext);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(new File(""+ext.get("destPath"),""+ext.get("spectrumName")+"_"+ext.get("saveTime")+".doc").exists());
	}
	
	public void setData(Map<String,Object>dataMap,Map<String,Object>ext){
		dataMap.put("time", "38257398725837528735");
		dataMap.put("spectrumID", "3948290384091");
		dataMap.put("spectrumType", "拉曼光谱");
		dataMap.put("hardwareName", "ksdjfklsjlafjsdjk");
		dataMap.put("resolutionRate","2.3");
		dataMap.put("detectedName", "苹果");
		dataMap.put("unit", "cm/s");
		dataMap.put("contentName", "维生素C");
		dataMap.put("concentration", "234");
		dataMap.put("standards", "100");
		dataMap.put("standardLine", "322");
		dataMap.put("value", "40");
		
		ext.put("templateName", "report.ftl");
		ext.put("templatePath", "src/main/webapp/WEB-INF/data");
		ext.put("destPath", "test/"+"ewrwer"+"_"+342342314+".doc");
	}
}
