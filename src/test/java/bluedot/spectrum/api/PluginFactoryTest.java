package bluedot.spectrum.api;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import bluedot.spectrum.api.impl.NormalizationPlugin;
import bluedot.spectrum.utils.JsonUtils;

/**
 * 
 * @author longshu 2016年8月25日
 */
@SuppressWarnings("unchecked")
public class PluginFactoryTest {
	static SpectrumFile spectrumFile;

	@BeforeClass
	public static void init() throws JsonSyntaxException, JsonIOException, FileNotFoundException {
		String path = SpectrumFile.class.getClassLoader().getResource("spectrum.file.json").getPath();
		spectrumFile = JsonUtils.Gson(false).fromJson(new FileReader(path), SpectrumFile.class);
	}

	/**
	 * Test method for {@link bluedot.spectrum.api.PluginFactory#loadPlugin(java.lang.String)}.
	 * @throws PluginApiException 
	 */
	@Test
	public void testLoadPluginString() throws PluginApiException {
		// 平滑处理
		String jarPath = PluginApi.class.getClassLoader().getResource("testPlugin.zip").getPath();
		PluginApi plugin = PluginFactory.loadPlugin(jarPath);

		List<Point> data = spectrumFile.getPoints();
		List<Point> points = (List<Point>) plugin.execute(data, null);
		for (Point point : points) {
			System.out.println(point);
		}
	}

	/**
	 * Test method for {@link bluedot.spectrum.api.PluginFactory#loadPlugin(java.io.File)}.
	 */
	@Ignore
	public void testLoadPluginFile() {
		
	}

	/**
	 * Test method for {@link bluedot.spectrum.api.PluginFactory#getPluginByName(java.lang.String)}.
	 */
	@Ignore
	public void testGetPluginByName() {
	}

	/**
	 * Test method for {@link bluedot.spectrum.api.PluginFactory#getPlugin(java.lang.Class)}.
	 * @throws PluginApiException 
	 */
	@Test
	public void testGetPlugin() throws PluginApiException {
		PluginApi plugin = PluginFactory.getPlugin(NormalizationPlugin.class);
		List<Point> points = (List<Point>) plugin.execute(spectrumFile.getPoints(), null);
		// pretreatment(spectrumFile.getPoints(),// null);
		for (Point point : points) {
			System.out.println(point);
		}
	}

}
