package bluedot.spectrum.api.impl;

import java.io.FileReader;
import java.util.List;

import org.junit.Test;

import bluedot.spectrum.api.PluginFactory;
import bluedot.spectrum.api.Point;
import bluedot.spectrum.api.SpectrumFile;
import bluedot.spectrum.utils.JsonUtils;

public class PeakSeekingPluginTest {

	PeakSeekingPlugin peakSeekingPlugin;

	@Test
	public void testExecute() {
	}

	@Test
	public void testGetPluginType() {
	}

	@Test
	public void testAnalysis() throws Exception {
		peakSeekingPlugin = PluginFactory.getPlugin(PeakSeekingPlugin.class);

		String path = SpectrumFile.class.getClassLoader().getResource("spectrum.file.json").getPath();
		SpectrumFile spectrumFile = JsonUtils.Gson(false).fromJson(new FileReader(path), SpectrumFile.class);
		// List<Point> data = spectrumFile.getPoints();
		/*for (Point point : data) {
			System.out.println(point);
		}*/
		SpectrumFile spectrum = peakSeekingPlugin.handleData(spectrumFile, null);
		List<Point> peaks = spectrum.getPeaks();
		if (peaks.isEmpty()) {
			System.out.println("没有峰点");
		}
		for (Point point : peaks) {
			System.out.println(point);
		}
		System.out.println(peaks.size());
	}

}
