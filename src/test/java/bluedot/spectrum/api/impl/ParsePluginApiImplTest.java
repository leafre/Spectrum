package bluedot.spectrum.api.impl;

import java.io.FileNotFoundException;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import bluedot.spectrum.api.PluginApiException;
import bluedot.spectrum.api.PluginFactory;
import bluedot.spectrum.api.SpectrumFile;

/**
 * 
 * @author longshu 2016年8月25日
 */
public class ParsePluginApiImplTest {
	static String path;
	static ParsePluginApiImpl plugin;

	@BeforeClass
	public static void init() throws JsonSyntaxException, JsonIOException, FileNotFoundException, PluginApiException {
		path = SpectrumFile.class.getClassLoader().getResource("spectrum.file.json").getPath();
		plugin = PluginFactory.getPlugin(ParsePluginApiImpl.class);
		System.out.println(path);
	}

	/**
	 * Test method for {@link bluedot.spectrum.api.impl.ParsePluginApiImpl#execute(java.lang.Object, java.util.Map)}.
	 * @throws PluginApiException 
	 */
	@Test
	public void testExecute() throws PluginApiException {
		SpectrumFile spectrumFile = (SpectrumFile) plugin.execute(path, null);
		System.out.println(spectrumFile);
	}

	/**
	 * Test method for {@link bluedot.spectrum.api.impl.ParsePluginApiImpl#parse(java.lang.String, java.util.Map)}.
	 * @throws PluginApiException 
	 */
	@Test
	@Ignore
	public void testParse() throws PluginApiException {
		SpectrumFile spectrumFile = plugin.parse(path, null);
		System.out.println(spectrumFile);
	}

}
