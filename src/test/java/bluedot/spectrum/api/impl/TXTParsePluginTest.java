package bluedot.spectrum.api.impl;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import bluedot.spectrum.api.PluginApiException;
import bluedot.spectrum.api.PluginFactory;
import bluedot.spectrum.api.PluginType;
import bluedot.spectrum.api.SpectrumFile;

/**
 * 
 * @author longshu 2016年9月22日
 */
public class TXTParsePluginTest {

	TXTParsePlugin plugin;

	@Before
	public void init() {
		try {
			plugin = PluginFactory.getPlugin(TXTParsePlugin.class);
		} catch (PluginApiException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test method for {@link bluedot.spectrum.api.impl.TXTParsePlugin#execute(java.lang.Object, java.util.Map)}.
	 */
	@Test
	public void testExecute() {
	}

	/**
	 * Test method for {@link bluedot.spectrum.api.impl.TXTParsePlugin#getPluginType()}.
	 */
	@Test
	public void testGetPluginType() {
		assertEquals(plugin.getPluginType(), PluginType.SPECTRUM_PARSE);
	}

	/**
	 * Test method for {@link bluedot.spectrum.api.impl.TXTParsePlugin#parse(java.lang.String, java.util.Map)}.
	 * @throws PluginApiException 
	 */
	@Test
	public void testParse() throws PluginApiException {
		String path = new File("test", "textSpectrum3.txt").getAbsolutePath();
		SpectrumFile spectrumFile = plugin.parse(path, null);
		System.out.println(spectrumFile);
	}

}
