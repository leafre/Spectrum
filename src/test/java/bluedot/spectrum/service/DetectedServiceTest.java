/**
 * 
 */
package bluedot.spectrum.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import bluedot.spectrum.bean.DetectedContent;
import bluedot.spectrum.bean.DetectedObject;
import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.utils.BeanUtils;

/**
 * @author YiJie
 * @date 2016年9月1日
 */
public class DetectedServiceTest {

	/**
	 * Test method for
	 * {@link bluedot.spectrum.service.DetectedService#addDetectedObject(java.util.Map, java.util.Map)}
	 * .
	 */
	@Test
	public void testAddDetectedObject() {
		DetectedObject det = new DetectedObject();
		det.setDetectedID("RC-001-001");
		det.setCategoryID(1);
		det.setChineseName("中文名");
		det.setEnglishName("english");
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		params.put("detectedID", new String[] { "RC-001-003" });
		params.put("categoryID", new Integer[] { 1 });// 被检测物类别
		params.put("chineseName", new String[] { "中文名" });
		params.put("englishName", new String[] { "english" });
		params.put("contentID", new Integer[] { 7, 8 });// 一组检测内容ID
		params.put("standards", new String[] { "GB/T 30942-2014", "GB/T 30942-2016" });
		params.put("standardLine", new Double[] { 70.00, 8.15 });
		params.put("value", new Double[] { 0.05, 0.20 });
		params.put("unit", new String[] { "kg", "ml" });
		Map<String, Object> data = new HashMap<String, Object>();

		try {
			new DetectedService("addDetectedObject", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test method for
	 * {@link bluedot.spectrum.service.DetectedService#modifyDetectedObject(java.util.Map, java.util.Map)}
	 * .
	 */
	@Test
	public void testModifyDetectedObject() {
		QueryService q = new QueryService();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("detectedID", new String("RC-001-002"));
		List<Map<String, Object>> l = q.queryList("queryDetectedObject", params);
		Map<String, Object> m = l.get(0);
		DetectedObject d = BeanUtils.mapToBean(m, DetectedObject.class);
		d.setCategoryID(2);

		Map<String, Object> data = new HashMap<String, Object>();

		Map<String, Object[]> p = new HashMap<String, Object[]>();
		p.put("detectedID", new String[] { d.getDetectedID() });
		p.put("categoryID", new Integer[] { d.getCategoryID() });// 被检测物类别
		p.put("chineseName", new String[] { d.getChineseName() });
		p.put("englishName", new String[] { d.getEnglishName() });
		try {
			new DetectedService("modifyDetectedObject", p, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test method for
	 * {@link bluedot.spectrum.service.DetectedService#deleteDetectedObject(java.util.Map, java.util.Map)}
	 * .
	 */
	@Test
	public void testDeleteDetectedObject() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		params.put("detectedID", new String[] { "RC-001-002" });
		Map<String, Object> data = new HashMap<String, Object>();
		try {
			new DetectedService("deleteDetectedObject", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test method for
	 * {@link bluedot.spectrum.service.DetectedService#addDetectedContent(java.util.Map, java.util.Map)}
	 * .
	 */
	@Test
	public void testAddDetectedContent() {
		// DetectedContent det = new DetectedContent();
		// det.setChineseName("中文名");
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		params.put("chineseName", new String[] { "中文名" });
		Map<String, Object> data = new HashMap<String, Object>();
		try {
			new DetectedService("addDetectedContent", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test method for
	 * {@link bluedot.spectrum.service.DetectedService#modifyDetectedContent(java.util.Map, java.util.Map)}
	 * .
	 */
	@Test
	public void testModifyDetectedContent() {
		QueryService q = new QueryService();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("contentID", 21);
		List<Map<String, Object>> l = q.queryList("queryDetectedContent", params);
		Map<String, Object> m = l.get(0);
		DetectedContent d = BeanUtils.mapToBean(m, DetectedContent.class);
		d.setChemicalName("test");

		Map<String, Object> data = new HashMap<String, Object>();

		Map<String, Object[]> p = new HashMap<String, Object[]>();
		p.put("contentID", new Integer[] { d.getContentID() });
		p.put("categoryID", new Integer[] { 1 });// 被检测物类别 p.put("chineseName",
													// new String[] {
													// d.getChineseName() });
		p.put("englishName", new String[] { d.getEnglishName() });
		p.put("chemicalName", new String[] { d.getChemicalName() });
		try {
			new DetectedService("modifyDetectedContent", p, data);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Test method for
	 * {@link bluedot.spectrum.service.DetectedService#deleteDetectedContent(java.util.Map, java.util.Map)}
	 * .
	 */
	@Test
	public void testDeleteDetectedContent() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		params.put("contentID", new Integer[] { 18 });
		Map<String, Object> data = new HashMap<String, Object>();
		try {
			new DetectedService("deleteDetectedContent", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

}
