package bluedot.spectrum.service;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;

import bluedot.spectrum.bean.AccountState;
import bluedot.spectrum.bean.Role;
import bluedot.spectrum.bean.User;
import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.utils.VerifyCode;
import bluedot.spectrum.utils.mail.SendEmailUtils;
import bluedot.spectrum.web.core.FSConfig;

public class UserServiceTest {

	private UserService userService;

	public UserServiceTest() {
		
		FSConfig.init(new File("test").getAbsolutePath());
		try {
			
			SendEmailUtils.init(null);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		userService = new UserService();
	}

	@Test
	public void testIsRepeat() {
		
		System.out.println(userService.isRepeat("4","我的收藏4"));
	}
	
	/**
	 * 高老师
	 */
	@Test
	public void registerTest() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		params.put("username", new String[] { "ssss222" });
		params.put("roleID", new String[] { Role.LAB + "" });
		params.put("email", new String[] { "19102674763@qq.com" });
		params.put("tel", new String[] { "18279180262" });
		params.put("password", new String[] { "gz22111" });
		params.put(VerifyCode.KEY_CODE, new String[] { "123" });
		params.put("verifyCode", new String[] { "123" });
        params.put("IDNumber", new String[]{"350127199304033217"});
    	params.put("addresse",new String[]{"sdfsfdsfs"}) ;
		params.put("realName", new String[]{"ss"});
		params.put("gender", new String[]{"保密"});
		params.put("insitute",new String[]{"sfdsfdsfsfsf"});
		params.put("position",new String[]{"sfdsfdsfsfsf"});
		params.put("detail",new String[]{"sfdsfdsfsfsf"});
		params.put("file",new String[]{"sfdsfdsfsfsf"});
		try {
			UserService us = new UserService("register", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetNowDate() {
		
		Calendar thawTime = new GregorianCalendar();
		thawTime.add(Calendar.MINUTE, 20);
		System.out.println(thawTime.get(Calendar.DAY_OF_MONTH));
	}
	
	@Test
	public void testGetNowDatetime() {
//		System.out.println(userService.getNowDatetime());
		String ss="sss";
		
	}

	
	@Test
	/**
	 * 高老师
	 */
	public void loginTest() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		// params.put("account", new String[] { "sssss" });
		// params.put("roleID", new String[] { Role.USER+"" });
		params.put("account", new String[] { "1910267763@qq.com" });
		// params.put("tel", new String[] { "18279180762" });
		params.put("password", new String[] { "gz22111" });
		params.put(VerifyCode.KEY_CODE, new String[] { "123" });
		params.put("verifyCode", new String[] { "123" });
		try {
			UserService us = new UserService("login", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}

	}

	
	
	/**
	 * 付大石
	 */
	@Test
	public void testApplyCancelAdmin() {

		// 构造测试数据
		Map<String, String[]> params = new HashMap<String, String[]>();
		params.put("userID", new String[] { "1" });

		Map<String, Object> data = new HashMap<String, Object>();

//		开始测试
		printRedirectPage(userService.applyCancelAdmin(params, data));
		printData(data);
	}

	/**
	 * 付大石
	 */
	@Test
	public void testApplyRole() {
		// 构造测试数据
		Map<String, String[]> params = new HashMap<String, String[]>();
		params.put("applyRoleID", new String[] { "2" });
		params.put("applyFile", new String[] { "2" });
		params.put("userID", new String[] { "2" });
		params.put("birthday", new String[] { "0" });
		params.put("photo", new String[] { "photo" });
		params.put("gender", new String[] { "男" });
		params.put("tel", new String[] { "15083825093" });
		params.put("position", new String[] { "position" });
		params.put("institute", new String[] { "institute" });
		params.put("detail", new String[] { "detail" });
		params.put("address", new String[] { "address" });
		Map<String, Object> data = new HashMap<String, Object>();

		// 开始测试
		printRedirectPage(userService.applyRole(params, data));
		printData(data);
	}

	/**
	 * 付大石
	 */
	@Test
	public void testApplyThaw() {
		// 构造测试数据
		Map<String, String[]> params = new HashMap<String, String[]>();
		params.put("reason", new String[] { "qwe" });
		params.put("userID", new String[] { "1" });
		Map<String, Object> data = new HashMap<String, Object>();

		// 开始测试
		printRedirectPage(userService.applyThaw(params, data));
		printData(data);
	}

	/**
	 * @author 付大石
	 */
	@Test
	public void testExamineRoleApplication() {
		
		// 构造测试数据
		Map<String, String[]> params = new HashMap<String, String[]>();
		params.put("state", new String[] { "拒绝" });
		params.put("applicationID", new String[] { "3" });
		params.put("reason", new String[] { "qwe" });
		params.put("applyRoleID", new String[] { "2" });
		params.put("userID", new String[] { "1" });
		params.put("email",new String[]{"15083825093@163.com"});
		params.put("session_username",new String[]{"付大石"});
		params.put("username",new String[]{"付磊"});
		Map<String, Object> data = new HashMap<String, Object>();

		// 开始测试
		printRedirectPage(userService.examineRoleApplication(params, data));
		printData(data);
	}

	/**
	 * @author
	 */
	@Test
	public void testExamineThawApplication() {

		// 构造测试数据
		Map<String, String[]> params = new HashMap<String, String[]>();
		params.put("view", new String[] { "examineThawApplication" });
		params.put("state", new String[] { "通过" });
		params.put("applicationID", new String[] { "1" });
		params.put("reason", new String[] { "qwe" });
		params.put("thawTimeString", new String[] { "1" });
		params.put("userID", new String[] { "1" });
		Map<String, Object> data = new HashMap<String, Object>();

		// 开始测试
		printRedirectPage(userService.examineThawApplication(params, data));
		printData(data);
	}

	/**
	 * @author 付大石
	 */
	@Test
	public void testThawUser() {

		// 构造数据
		User user = new User();
		user.setUserID(1);

		// 开始测试
		// userService.thawUser(user);
	}

	/**
	 * @author 付大石
	 */
	@Test
	public void testModifyUserInfo() {

		// 构造测试参数
		Map<String, String[]> params = new HashMap<String, String[]>();
		params.put("session_userID", new String[] { "1" });
		params.put("birthday", new String[] { "0" });
		params.put("photo", new String[] { "photo" });
		params.put("gender", new String[] { "女" });
		params.put("tel", new String[] { "15083825091" });
		params.put("position", new String[] { "position" });
		params.put("institute", new String[] { "institute" });
		params.put("detail", new String[] { "detail" });
		params.put("address", new String[] { "address" });
		Map<String, Object> data = new HashMap<String, Object>();

		// 开始测试
		 printRedirectPage(userService.modifyUserInfo(params,data));
		printData(data);
	}

	/**
	 * 付大石
	 */
	@Test
	public void testFrozen() {

		// 构造测试数据
		User user = new User();
		user.setUserID(1);
		user.setThawTime(1L);
		user.setState(AccountState.FROZEN);

		// 开始测试
//		 userService.frozen(user,"密码连续输入错误超过5次");
	}

	/**
	 * @author 付大石
	 */
	@Test
	public void testModifyFavoriteName() {
		
		//构造测试参数
		Map<String, String[]> params = new HashMap<String, String[]>();
		params.put("session_userID", new String[] { "1" });
		params.put("session_email",new String[]{"710106701@qq.com"});
		params.put("favoriteID", new String[] { "1" });
		params.put("newFavoriteName", new String[] { "我的收藏1" });
		Map<String, Object> data = new HashMap<String, Object>();
		
		//开始测试
		printRedirectPage(userService.modifyFavoriteName(params,data));
		printData(data);
	}
	
	/**
	 * @author 付大石
	 */
	@Test
	public void testCreateFavorite() {

		//构造测试参数
		Map<String, String[]> params = new HashMap<String, String[]>();
		params.put("session_userID", new String[] { "4" });
		params.put("session_email",new String[]{"testuser@qq.com"});
		params.put("favoriteName", new String[] { "我的收藏4" });
		Map<String, Object> data = new HashMap<String, Object>();
		
		//开始测试
		printRedirectPage(userService.createFavorite(params,data));
		printData(data);
		
	}
	
	@Test
	public void testCreateFavorite2() {
		
		User user = new User();
		user.setEmail("15083825093@qq.com");
		user.setUserID(2);

		//开始测试
		//System.out.println(userService.createFavorite(user,"我的收藏"));
	}
	
	
	/**
	 * @author 付大石
	 */
	@Test
	public void testModifyPassword() {
		
		// 构造测试参数
		Map<String, String[]> params = new HashMap<String, String[]>();
		params.put("session_userID", new String[] { "1" });
		params.put("oldPassword", new String[] { "123123qwe12" });
		params.put("newPassword", new String[] { "123123qwe123" });
		params.put("confirmPassword", new String[] { "123123qwe123" });
		params.put("countString", new String[] { "1" });
		params.put("verifyCode", new String[] { "123456" });
		params.put("session_verifycode", new String[] { "123456" });
		params.put("session_email", new String[] { "710106701@qq.com" });
		params.put("session_username", new String[] { "Clive" });
		Map<String, Object> data = new HashMap<String, Object>();

		// 开始测试
		printRedirectPage(userService.modifyPassword(params,data));
		printData(data);
	}

	/**
	 * @author 付大石
	 */
	@Test
	public void testFindPassword() {

		// 构造测试数据
		Map<String, String[]> params = new HashMap<String, String[]>();
		params.put("email", new String[] { "710106701@qq.com" });
		params.put("password", new String[] { "123456789" });
		params.put("verifyCode", new String[] { "486847" });
		params.put("session_verifycode", new String[] { "486847" });
		Map<String, Object> data = new HashMap<String, Object>();

		// 开始测试
		 printRedirectPage(userService.findPassword(params,data));
		printData(data);
	}

	
	
	@Test
	public void testBackupFavorite() {
		
//		userService.backupFavorite("qwe");
	}
	
	@Test
	public void testDeleteFavorite() {
		
		Map<String, String[]> params = new HashMap<String, String[]>();
		params.put("favoriteID", new String[]{"1"});
		Map<String, Object> data = new HashMap<String, Object>();
		
		// 开始测试
		printRedirectPage(userService.deleteFavorite(params,data));
		printData(data);
	}
	
	public void printRedirectPage(String page) {

		System.out.println(page);
	}

	public void printData(Map<String, Object> data) {

		Set<Entry<String, Object>> set = data.entrySet();
		Iterator<Entry<String, Object>> iterator = set.iterator();
		while (iterator.hasNext()) {

			Entry<String, Object> entry = iterator.next();
			System.out.println(entry.getKey() + "---" + entry.getValue());
		}
	}
}