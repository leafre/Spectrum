package bluedot.spectrum.service;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import bluedot.spectrum.api.PluginType;
import bluedot.spectrum.bean.ApplicationState;
import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.web.core.FSConfig;

public class PluginServiceTest {

	@BeforeClass
	public static void init() {
		FSConfig.init("test");
	}

	/**
	 * {@link bluedot.spectrum.service.PluginService#PluginService(String, java.util.Map, java.util.Map)}
	 */
	@Test
	public void testUploadPlugin(){
		Map<String,Object[]>params = new HashMap<String,Object[]>();
		Map<String,Object>data = new HashMap<String,Object>();
		set("uploadPlugin",params);
		try {
			new PluginService("uploadPlugin",params,data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}
	
	@Test
	public void testDeletePlugin(){
		Map<String,Object[]>params = new HashMap<String,Object[]>();
		Map<String,Object>data = new HashMap<String,Object>();
		set("deletePlugin",params);
		try {
			new PluginService("deletePlugin",params,data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}
	
	@Test
	public void testDownloadPlugin(){
		Map<String,Object[]>params = new HashMap<String,Object[]>();
		Map<String,Object>data = new HashMap<String,Object>();
		set("downloadPlugin",params);
		try {
			new PluginService("downloadPlugin",params,data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}
	
	@Test
	public void testAuditPlugin(){
		Map<String,Object[]>params = new HashMap<String,Object[]>();
		Map<String,Object>data = new HashMap<String,Object>();
		set("auditPlugin",params);
		try {
			new PluginService("auditPlugin",params,data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}
	
	@Test
	public void testModifyPlugin(){
		Map<String,Object[]>params = new HashMap<String,Object[]>();
		Map<String,Object>data = new HashMap<String,Object>();
		set("modifyPlugin",params);
		try {
			new PluginService("modifyPlugin",params,data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}
	
	private void set(String view,Map<String,Object[]>params){
		if(view.equals("uploadPlugin")){
			params.put("pluginID", new String[]{""});
			params.put("session_userID", new String[]{"6"});
			params.put("pluginType", new String[]{PluginType.PRETREAMENT});
			params.put("description", new String[]{"dfgsdgs"});
			params.put("pluginName", new String[]{"fdg"});
			params.put("version", new String[]{"sg"});
			params.put("codePath", new String[]{new File(FSConfig.temp(),"jkdjskflhkd.jsp").getAbsolutePath()});
			params.put("jarPath", new String[]{new File(FSConfig.temp(),"jkdlhkd.jsp").getAbsolutePath()});
			params.put("state", new String[]{ApplicationState.WAIT});
			params.put("pluginMainIDs", new String[]{"10"});
			params.put("pluginSubIDs", new String[]{"12",});
			params.put("hardwareIDs", new String[]{"2","3"});
			params.put("spectrumTypeIDs0", new String[]{"1","2"});
			params.put("spectrumTypeIDs1", new String[]{"1"});
			params.put("detectedObjectIDs", new String[]{"1","2","3","4"});
			params.put("contentIDs0", new String[]{"1","3"});
			params.put("contentIDs1", new String[]{"4"});
			params.put("contentIDs2", new String[]{"2","4","5"});
			params.put("contentIDs3", new String[]{"2","6"});
		}
		if(view.equals("downloadPlugin")){
			params.put("pluginID", new String[]{"10"});
		}
		if(view.equals("auditPlugin")){
			params.put("pluginID",new String[]{"1"});
			params.put("state", new String[]{ApplicationState.PASS});
		}
		if(view.equals("deletePlugin")){
			params.put("pluginID",new String[]{"1"});
		}
		if(view.equals("modifyPlugin")){
			params.put("pluginID",new String[]{"1"});
			params.put("pluginType", new String[]{PluginType.PRETREAMENT});
			params.put("pluginName", new String[]{"fdgdd"});
			params.put("description", new String[]{"fdsfs"});
			params.put("version", new String[]{"sgss"});
			params.put("pluginMainIDs", new String[]{"10"});
			params.put("pluginSubIDs", new String[]{"13",});
			params.put("hardwareIDs", new String[]{"2","3"});
			params.put("spectrumTypeIDs0", new String[]{"1","2"});
			params.put("spectrumTypeIDs1", new String[]{"1"});
			params.put("detectedObjectIDs", new String[]{"1","2","3","4"});
			params.put("contentIDs0", new String[]{"1","3"});
			params.put("contentIDs1", new String[]{"4"});
			params.put("contentIDs2", new String[]{"2","4","5"});
			params.put("contentIDs3", new String[]{"2","6"});
		}
	}
}
