package bluedot.spectrum.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.web.core.ServiceMapper;

public class HardwareServiceTest {

	@BeforeClass
	public static void init() {
		ServiceMapper.parser();
	}

	/**
	 * {@link bluedot.spectrum.service.HardwareService#HardwareService(String, java.util.Map, java.util.Map)}
	 */
	@Test
	public void testAddHardware() {
		Logger logger = Logger.getLogger(HardwareService.class);
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		set("addHardware", params);
		logger.debug("到这里了");
		System.out.println(params);
		try {
			new HardwareService("addHardware", params, data);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
		}
		System.out.println((params == null) + "   " + data.toString());
	}

	@Test
	public void testModifyHardware() {
		Logger logger = Logger.getLogger(HardwareService.class);
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		set("modifyHardware", params);
		System.out.println(params);
		try {
			new HardwareService("modifyHardware", params, data);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
		}
		System.out.println((params == null) + "   " + data.toString());

	}

	@Test
	public void testdDeleteHardware() {
		Logger logger = Logger.getLogger(HardwareService.class);
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		set("deleteHardware", params);
		System.out.println(params);
		try {
			new HardwareService("deleteHardware", params, data);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
		}
		System.out.println((params == null) + "   " + data.toString());
	}

	public void set(String view, Map<String, Object[]> params) {
		if (view.equals("addHardware")) {
			params.put("hardwareID", new Object[] { "fkjdksljf" });
			params.put("hardwareName", new Object[] { "dkfjkljfliajfiljaiojfknkjdhfjksdhf" });
			params.put("spectrumFileTypeIDs", new Object[] { 1 });
			params.put("spectrumTypeIDs", new Object[] { 1 });
		} else if (view.equals("modifyHardware")) {
			params.put("hardwareID", new Object[] { "2" });
			params.put("hardwareName", new Object[] { "iknkjdhfjksdhf" });
			params.put("spectrumFileTypeIDs", new Object[] { 1, 2 });
			params.put("spectrumTypeIDs", new Object[] { 1 });
		} else {
			params.put("hardwareID", new Object[] { "1" });
		}
	}
}
