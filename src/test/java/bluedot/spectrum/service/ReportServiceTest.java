package bluedot.spectrum.service;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.web.core.FSConfig;
import bluedot.spectrum.web.core.ServiceMapper;

public class ReportServiceTest {
	
	@BeforeClass
	public static void init() {
		ServiceMapper.preParser();
		FSConfig.init(new File("test").getAbsolutePath());// 工程目录下的test目录
	}
	
	@Test
	public void testGetReport() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		params.put("pluginID", new String[]{"1"});
		params.put("spectrumID",new String[]{"1234567890"});
		try {
			new ReportService("getReport", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}

	@Test
	public void testCollectReport() {
		Map<String,Object[]>params = new HashMap<String,Object[]>();
		Map<String,Object>data = new HashMap<String,Object>();
		params.put("spectrumID", new String[]{"0100200001000001"});
		params.put("contentID", new String[]{"2"});
		params.put("detectedID", new String[]{"1"});
		params.put("favoriteID", new String[]{"1"});
		params.put("concentration", new String[]{"3.4"});
		params.put("standards", new String[]{"GB/T 30942-2014"});
		params.put("hardwareName", new String[]{"dfhjka"});
		params.put("value", new String[]{"32.2"});
		params.put("standardLine", new String[]{"2.3"});
		params.put("unit", new String[]{"mol/L"});
		params.put("contentChineseName", new String[]{"fjdsk"});
		params.put("detectedChineseName", new String[]{"fdsjk"});
		try {
			new SpectrumService("collectReport", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}

	@Test
	public void testDeleteReport() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		//params.put("spectrumID", new String[] {"1233","12525252","12313131"}); //三个光谱都不存在
		params.put("spectrumID", new String[] {"1234567890","12313131","1234567891"}); //三个存在两个
		Map<String, Object> data = new HashMap<String, Object>();
		try {
			new SpectrumService("deleteReport", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}

}
