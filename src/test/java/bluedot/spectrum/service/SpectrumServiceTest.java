package bluedot.spectrum.service;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.web.core.FSConfig;
import bluedot.spectrum.web.core.ServiceMapper;

/**
 * SpectrumServiceTest
 */
public class SpectrumServiceTest {

	@BeforeClass
	public static void init() {
		ServiceMapper.preParser();
		FSConfig.init(new File("test").getAbsolutePath());// 工程目录下的test目录
	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#pretreatment(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testPretreatment() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#collectSpectrum(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testCollectSpectrum() {
		Logger logger = Logger.getLogger(HardwareService.class);
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		/*params.put("spectrumID", new String[]{"2"});
		params.put("favoriteID", new String[]{"1"});
		params.put("session_email",new String[]{"123@qq.com"});*/
		params.put("spectrumName", new String[] { "小麦光谱" });
		params.put("description", new String[] { "小麦光谱" });
		params.put("favoriteID", new String[] { "1" });
		params.put("session_userID", new String[] { "6" });
		params.put("hardwareID", new String[] { "2" });
		params.put("spectrumTypeID", new String[] { "1" });
		params.put("spectrumFileTypeID", new String[] { "1" });
		params.put("detectedID", new String[] { "1" });
		params.put("contentIDs", new String[] { "1" });
		params.put("spectrumFile", new String[] { "test_spectrum.json" });
		System.out.println(params.toString());
		try {
			new SpectrumService("collectSpectrum", params, data);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
		}
		System.out.println((params == null) + " , " + data.toString());
	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#deleteSpectrum(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testDeleteSpectrum() {
		Logger logger = Logger.getLogger(HardwareService.class);
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		params.put("spectrumID", new String[] { "1234567890" });
		params.put("favoriteID", new String[] { "1" });
		params.put("email", new String[] { "testuser@qq.com" });
		System.out.println(params);
		try {
			new SpectrumService("deleteSpectrum", params, data);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
		}
		System.out.println((params == null) + "   " + data.toString());
	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#swapCoordinate(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testSwapCoordinate() {

	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#frequencyStandardization(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testFrequencyStandardization() {

	}

	/**
	 * @author 海松 2016-09-18
	 * 差谱测试
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#substractSpectrum(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testSubstractSpectrum() {
		// params封装谱图数据，差谱A和被差谱B
		Map<String, String[]> params = new HashMap<String, String[]>();
		params.put("spectrumID", new String[] { "1", "2" });// 光谱编号
		params.put("pluginID", new String[] { "2" });// 差谱算法插件ID
		// 对应的sqlConfig.xml operate值
		params.put("operate", new String[] { "queryPluginById" });
		// data封装提示信息
		Map<String, Object> data = new HashMap<String, Object>();

		// 调用差谱Service
		SpectrumService s = new SpectrumService();
		s.substractSpectrum(params, data);
		//输出后台处理信息
		System.out.println("后台处理信息:"+data.get("substractSpectrum_msg"));
	
	}


	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#multiplySpectrum(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testMultiplySpectrum() {

	}

	/**
	 * 测试算术谱图
	 * 
	 * @author 付大石
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#arithmeticSpectrum(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testArithmeticSpectrum() {
		// 构造测试数据
		Map<String, String[]> paramMap = new HashMap<String, String[]>();
		paramMap.put("formula", new String[] { "A-B" });
		paramMap.put("spectrumID", new String[]{ "1234567890", "1234567891" });
		paramMap.put("pluginID", new String[] { "1" });
		Map<String, Object> data = new HashMap<String, Object>();

		// 开始测试
		SpectrumService service = new SpectrumService();
		System.out.println(service.arithmeticSpectrum(paramMap, data));
		System.out.println("---");
		printData(data);
	}

	void printData(Map<String, Object> data) {
		Set<Entry<String, Object>> set = data.entrySet();
		Iterator<Entry<String, Object>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Entry<String, Object> entry = iterator.next();
			System.out.println(entry.getKey() + "---" + entry.getValue());
		}
	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#measureNoise(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testMeasureNoise() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		params.put("spectrumID", new String[] { "1234567890" });
		params.put("sectionLeft", new String[] { "2000" });
		params.put("sectionRight", new String[] { "1000" });
		try {
			new SpectrumService("measureNoise", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#measureAverageValue(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testMeasureAverageValue() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		params.put("spectrumID", new String[] { "1234567890" });
		params.put("sectionLeft", new String[] { "2000" });
		params.put("sectionRight", new String[] { "1000" });
		try {
			new SpectrumService("measureAverageValue", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#derivativeSpectrum(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testDerivativeSpectrum() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		params.put("spectrumID", new String[] { "1234567890" });
		params.put("orderNumber", new String[] { "1" });
		try {
			new SpectrumService("derivativeSpectrum", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#getPeaks(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testGetPeaks() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		params.put("spectrumID", new String[] { "1234567890" });
		params.put("valueY", new String[] { "10000" });
		params.put("sensitiveValue", new String[] { "50" });
		try {
			new SpectrumService("getPeaks", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#smoothSpectrum(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testSmoothSpectrum() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		params.put("spectrumID", new String[] { "1234567890" });
		try {
			new SpectrumService("smoothSpectrum", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#calibrationSpectrum(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testCalibrationSpectrum() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		params.put("spectrumID", new String[] { "1234567890" });
		
		try {
			new SpectrumService("calibrationSpectrum", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#modifySpectrum(java.util.Map, java.util.Map)}.
	 */
	
	@Test
	public void testModifySpectrum() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		params.put("spectrumID", new String[] {"1234567890"});
		params.put("spectrumName", new String[] {"newName"});
		params.put("spectruDescription", new String[] {"newDescription"});
		try {
			new SpectrumService("modifySpectrum", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#auditSpectrum(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testAuditSpectrum() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		params.put("spectrumID", new String[] { "1234567890" });
		params.put("spectrumName", new String[] { "小麦光谱" });
		params.put("userID", new String[] { "4" });
		params.put("hardwareID", new Integer[] { 1 });
		params.put("spectrumTypeID", new Integer[] { 1 });
		params.put("spectrumFileTypeID", new Integer[] { 1 });
		params.put("detectedID", new Integer[] { 1 });
		params.put("contentsID", new Integer[] { 1 });
		params.put("spectrumFile", new String[] { "testuser@qq.com\testfavorite\test_Spectrum.json" });
		params.put("ispass", new String[] { "拒绝" });
		params.put("reason", new String[] { "您的光谱还存在一些问题，不能予与通过" });
		Map<String, Object> data = new HashMap<String, Object>();
		try {
			new SpectrumService("auditSpectrum", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#submitSpectrum(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testSubmitSpectrum() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		params.put("spectrumID", new String[] { "1234567892" });
		Map<String, Object> data = new HashMap<String, Object>();
		try {
			new SpectrumService("submitSpectrum", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}

	/**
	 * Test method for {@link bluedot.spectrum.service.SpectrumService#uploadSpectrum(java.util.Map, java.util.Map)}.
	 */
	@Test
	public void testUploadSpectrum() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		params.put("spectrumFile", new String[] { "\\textSpectrum1.txt" });
		params.put("spectrumName", new String[] { "小麦光谱" });
		params.put("userID", new String[]{ "1" });
		params.put("hardwareID", new Integer[] { 1 });
		params.put("spectrumTypeID", new Integer[] { 1 });
		params.put("spectrumFileTypeID", new Integer[] { 1 });
		params.put("detectedID", new Integer[] { 1 });
		params.put("contentsIDs", new Integer[] { 1 });
		Map<String, Object> data = new HashMap<String, Object>();
		try {
			new SpectrumService("uploadSpectrum", params, data);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
	}

}
