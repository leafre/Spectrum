package bluedot.spectrum.service;


import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.web.core.FSConfig;

public class MaintainServiceTest {

	@BeforeClass
	public static void init() {
		FSConfig.init("test");
	}
	
	/**
	 * {@link bluedot.spectrum.service.MaintainService#MaintainService(String, java.util.Map, java.util.Map)}
	 */
	@Test
	public void testBackup(){
		Logger logger =Logger.getLogger(HardwareService.class);
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		set("backup",params);
		logger.debug("到这里了");
		System.out.println(params);
		try {
			new MaintainService("backup", params, data);
		} catch (ServiceException e) {
			logger.error(e.getMessage(),e);
		}
		System.out.println((params==null)+"   " +data.toString());
	}
	
	@Test
	public void testRecover(){
		Logger logger =Logger.getLogger(HardwareService.class);
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		Map<String, Object> data = new HashMap<String, Object>();
		set("recover",params);
		System.out.println(params);
		try {
			new MaintainService("recover",params,data);
		} catch (ServiceException e) {
			logger.error(e.getMessage(),e);
		}
		System.out.println((params==null)+"   " +data.toString());
	}
	
	public void set(String view,Map<String,Object[]>params){
		if(view.equals("backup")){
			params.put("backupName",new Object[]{"dkfjkljfliajksdhf"});
		    params.put("content",new Object[]{"userPluginDir,pluginDir"});
		    params.put("description", new Object[]{""});
		}
		if(view.equals("recover")){
			params.put("backupID", new Object[]{"21"});
			params.put("content",new Object[]{"userPluginDir,pluginDir"});
			params.put("description", new Object[]{""});
		}
	}
}
