package bluedot.spectrum.service;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import bluedot.spectrum.bean.Log;
import bluedot.spectrum.bean.Log.LogLevel;
import bluedot.spectrum.bean.Log.LogType;
import bluedot.spectrum.cache.Cache;
import bluedot.spectrum.cache.LFUCache;
import bluedot.spectrum.dao.BaseDao;
import bluedot.spectrum.dao.DaoFactory;
import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.utils.BeanUtils;
import bluedot.spectrum.web.core.Service;
import bluedot.spectrum.web.core.ServiceMapper;
import bluedot.spectrum.web.core.ServiceResultAdvice;

/**
 * 所有业务类的抽取
 */
public abstract class ServiceSupport extends Service implements ServiceResultAdvice {
	protected static Logger logger = Logger.getLogger(ServiceSupport.class);// 代码的日志
	protected static LogService logService = new LogService();// 项目系统的日志
	protected static QueryService queryService = new QueryService();// 查询业务
	protected BaseDao baseDao = DaoFactory.getDao();
	// K: Service类名 V: 所有业务方法
	private static Cache<String, Method[]> serviceMethodCache = new LFUCache<String, Method[]>(0);
	protected Method[] methods;
	protected String view;
	protected String result; // service.xml的<result>的name

	protected ServiceSupport() {
	}

	protected ServiceSupport(String view, final Map<String, Object[]> params, Map<String, Object> data)
			throws ServiceException {
		super(view, params, data);
		this.view = view;
		// 缓存中获取
		methods = serviceMethodCache.get(this.getClass().getName());
		if (null == methods) {
			// 获取子类的所有方法
			methods = this.getClass().getDeclaredMethods();
			// 获取子类的所有业务方法
			List<Method> ms = new LinkedList<Method>();
			for (Method method : methods) {
				if (isServiceMethod(method)) {
					ms.add(method);
				}
			}
			// 覆盖原有
			methods = new Method[ms.size()];
			methods = ms.toArray(methods);
			// 保存缓存
			serviceMethodCache.put(this.getClass().getName(), methods);
		}
		logger.debug("view:" + view + ",params size:" + params.size());
		if (logger.isDebugEnabled() && !params.isEmpty())
			showParams(params);
	}

	/**
	 * 子类执行业务逻辑
	 * @param view
	 * @param params
	 * @param data
	 * @throws ServiceException
	 */
	protected void execute(String view, Map<String, Object[]> params, Map<String, Object> data)
			throws ServiceException {
		/*
		 * 内部调用方法完成业务
		 */
		Method method = getMethod(ServiceMapper.getServiceConfig(view).getMethodName());
		if (null == method) {
			String msg = this.getClass().getSimpleName() + "没有该[" + view + "]方法";
			logger.info(msg);
			throw new ServiceException(msg);
		}
		try {
			result = (String) method.invoke(this, params, data);// 完成业务
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * 获取子类的要执行的业务方法
	 * @param methodName
	 * @return
	 */
	private Method getMethod(String methodName) {
		for (int i = 0; i < methods.length; i++) {
			if (methods[i].getName().equals(methodName)) {// 比较方法名
				return methods[i];
			}
		}
		return null;
	}

	/**
	 * 是否是业务方法
	 * @param method
	 * @return
	 */
	private boolean isServiceMethod(Method method) {
		// 权限为默认权限,返回值为String
		if (0 == method.getModifiers() && String.class.equals(method.getReturnType())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * T类型为Object[]
	 * 获取Map<String, Object[]>中指定key和Object[]中index位置的数据
	 * @param params
	 * @param key Map的key
	 * @param index Object[]中的位置
	 * @return
	 */
	protected <T> Object getParamsValue(Map<String, T> params, String key, int index) {
		T value = params.get(key);
		if (null == value) {
			return null;
		}
		Object[] values = (Object[]) value;
		if (index > values.length) {
			return null;
		}
		return values[index];
	}

	/**
	 * {@link #getParamsValue(Map, String, int)}
	 * @param params {@link #getParamsValue(Map, String, int)}
	 * @return
	 */
	protected String getParamStringValue(Map<String, String[]> params, String key, int index) {
		return (String) getParamsValue(params, key, index);
	}

	/**
	 * 记录系统异常时，日志的抽取
	 * @param e
	 */
	protected void systemLog(Exception e) {
		// 创建Log对象，以供向LogService传入参数，方便操作
		Log log = new Log(LogLevel.ERROR, LogType.SYSTEM, ServiceMapper.getServiceConfig(view).getId(), e.getMessage());
		logService.addLog(BeanUtils.beanToMap(log));
	}

	/**
	 * 打印params的参数,调试使用
	 * @param params
	 */
	protected void showParams(final Map<String, Object[]> params) {
		if (null != params) {
			StringBuilder builder = new StringBuilder();
			for (Map.Entry<String, Object[]> entry : params.entrySet()) {
				builder.append(entry.getKey() + "=" + Arrays.toString(entry.getValue()) + "\n");
			}
			logger.debug(builder.toString());
		}
	}

	@Override
	public String getResult() {
		return result;
	}

}
