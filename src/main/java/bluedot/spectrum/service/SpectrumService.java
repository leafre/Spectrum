package bluedot.spectrum.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import bluedot.spectrum.api.DataProcessingPluginApi;
import bluedot.spectrum.api.ParsePluginApi;
import bluedot.spectrum.api.PluginApi;
import bluedot.spectrum.api.PluginApiException;
import bluedot.spectrum.api.PluginFactory;
import bluedot.spectrum.api.PluginType;
import bluedot.spectrum.api.Point;
import bluedot.spectrum.api.PretreatmentPluginApi;
import bluedot.spectrum.api.SpectrumFile;
import bluedot.spectrum.api.impl.AnalysisPluginApiImpl;
import bluedot.spectrum.api.impl.CalibrationSpectrumPlugin;
import bluedot.spectrum.api.impl.DerivativeSpectrumPlugin;
import bluedot.spectrum.api.impl.ParsePluginApiImpl;
import bluedot.spectrum.api.impl.PeakSeekingPlugin;
import bluedot.spectrum.api.impl.SmoothingFilterPlugin;
import bluedot.spectrum.api.impl.TXTParsePlugin;
import bluedot.spectrum.bean.ApplicationState;
import bluedot.spectrum.bean.Favorite;
import bluedot.spectrum.bean.Log;
import bluedot.spectrum.bean.Log.LogLevel;
import bluedot.spectrum.bean.Log.LogType;
import bluedot.spectrum.bean.Role;
import bluedot.spectrum.bean.Spectrum;
import bluedot.spectrum.bean.User;
import bluedot.spectrum.cache.Cache;
import bluedot.spectrum.cache.FIFOCache;
import bluedot.spectrum.cache.LFUCache;
import bluedot.spectrum.exception.DaoException;
import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.utils.BeanUtils;
import bluedot.spectrum.utils.FileUtils;
import bluedot.spectrum.utils.GetSpectrumFile;
import bluedot.spectrum.utils.JsonUtils;
import bluedot.spectrum.utils.StringUtils;
import bluedot.spectrum.utils.mail.Mail;
import bluedot.spectrum.utils.mail.SendEmailUtils;
import bluedot.spectrum.web.core.FSConfig;
import bluedot.spectrum.web.core.ServiceMapper;
import bluedot.spectrum.web.core.ServiceResultConfig;

/**
 * 和光谱相关的业务(光谱,报告)
 * 
 * @author longshu 2016年8月13日
 */
public class SpectrumService extends ServiceSupport {

	private final String COLLECT_SPEC_MSG = "collectSpectrum_msg";
	private final String DEL_SPEC_MSG = "deleteSpectrum_msg";
	// 插件缓存 K:pluginID/pluginType V:PluginApi的实例
	private static transient Cache<String, PluginApi> pluginCache = new LFUCache<String, PluginApi>(20);

	public SpectrumService() {
	}

	public SpectrumService(String view, Map<String, Object[]> params, Map<String, Object> data)
			throws ServiceException {
		super(view, params, data);
		execute(view, params, data);
	}

	/**
	 * 根据预处理算法处理谱图
	 * 
	 * @param params
	 * @param data
	 * @author 习文文 刘志超 许海龙
	 * @return 预处理后的光谱
	 */
	String pretreatment(Map<String, String[]> params, Map<String, Object> data) {
		String pluginID = getParamStringValue(params, "pluginID", 0);// 从前台参数中获取算法插件的id
		List<SpectrumFile> spectrumList = getSpectrumFile(params, data, "pretreatment");// 获取光谱文件列表
		SpectrumFile spectrumFile = spectrumList.get(0);// 得到光谱列表的第一个对象
		if (null == spectrumFile) {
			data.put("pretreatment_msg", "不存在该光谱");
			return ServiceResultConfig.FAILED;
		}
		PretreatmentPluginApi plugin = null;
		try {
			if (StringUtils.isBlank(pluginID)) { // 判断传进来的插件ID是否为空
				// 调用默认的预处理方法
				plugin = loadDefaultPlugin(PluginType.PRETREAMENT);
			} else {
				// 加载指定的插件
				plugin = loadPlugin(pluginID);
			}
			if (plugin == null) {
				data.put("pretreatment_msg", "该算法插件不存在!");
				return ServiceResultConfig.FAILED;
			}
			spectrumFile = plugin.pretreatment(spectrumFile, null);
			data.put("SpectrumFile", spectrumFile);
		} catch (PluginApiException e) {
			logger.error(e.getMessage(), e);// 记录错误日志(代码日志)
			systemLog(e);// 记录系统日志(数据库)
			data.put("pretreatment_msg", "发生预处理异常!");
			return ServiceResultConfig.FAILED;
		}
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 收藏光谱到收藏夹
	 * 
	 * @author 刘驭洲
	 * @param params
	 * @param data
	 * @return 收藏是否成功
	 */
	String collectSpectrum(Map<String, String[]> params, Map<String, Object> data) {
		// 收藏的为标准库的光谱
		if (params.get("spectrumID") != null && params.get("spectrumID").length != 0) {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("spectrumID", getParamStringValue(params, "spectrumID", 0));
			param.put("favoriteID", getParamStringValue(params, "favoriteID", 0));
			// 调用collectStandardSpectrum方法 获得业务处理结果
			return collectStandardSpectrum(param, data);
		} else {
			// 收藏的为个人上传的光谱
			// 获取实体类
			Favorite favorite = BeanUtils.mapToBean(params, Favorite.class);
			if (params.get("session_userID") != null) {
				favorite.setUserID(Integer.valueOf(getParamStringValue(params, "session_userID", 0)));
				favorite.setUser(new User());
				favorite.getUser().setUserID(Integer.valueOf(getParamStringValue(params, "session_userID", 0)));
				favorite.getUser().setEmail(getParamStringValue(params, "session_email", 0));
				favorite.setSpectrumList(new ArrayList<Spectrum>());
				favorite.getSpectrumList().add(BeanUtils.mapToBean(params, Spectrum.class));
				favorite.getSpectrumList().get(0)
						.setUserID(Integer.valueOf(getParamStringValue(params, "session_userID", 0)));
				if (params.get("contentIDs") != null) {
					favorite.getSpectrumList().get(0).setContentsIDs(new Integer[params.get("contentIDs").length]);
					for (int i = 0; i < params.get("contentIDs").length; i++) {
						favorite.getSpectrumList().get(0).getContentsIDs()[i] = Integer
								.valueOf(params.get("contentIDs")[i]);
					}
				}
			}
			// 调用collectIndividualSpectrum方法 获取业务处理结果
			return collectIndividualSpectrum(favorite, data);
		}
	}

	/**
	 * 收藏标准库光谱（供collectSpectrum方法调用）
	 * 
	 * @author 刘驭洲
	 * @param params 前台传入的数据
	 * @param data 返回前台的数据
	 * @return 是否收藏成功
	 */
	private String collectStandardSpectrum(Map<String, Object> params, Map<String, Object> data) {
		// 校验数据
		if (((String) params.get("spectrumID")).length() == 18) {
			// 查询是否为标准库、以防破坏收藏规则
			params.put("isStandard", 1);
			List<Map<String, Object>> spectrumStandard = queryService.queryList("querySpectrum", params);
			if (spectrumStandard == null || spectrumStandard.size() == 0) {
				// 若无查询结果，则结束业务
				data.put(COLLECT_SPEC_MSG, "标准库中无此光谱存在，收藏失败。");
				return ServiceResultConfig.FAILED;
			}
			// 不可以重复收藏
			List<Map<String, Object>> isExist = queryService.queryList("queryFavoriteSpectrum", params);
			if (isExist != null && isExist.size() > 0) {
				// 若已被收藏记录信息，结束业务
				data.put(COLLECT_SPEC_MSG, "该光谱已被收藏，本系统不允许重复收藏。");
				return ServiceResultConfig.FAILED;
			}
			// 添加数据库 结束业务
			try {
				baseDao.add("addFavouriteSpectrum", params);
			} catch (DaoException e) {
				// 存入提示信息、记录日志，结束业务
				data.put(COLLECT_SPEC_MSG, "服务器错误，请稍后再试，或联系管理员进行解决。");
				logger.error(e.getMessage(), e);
				systemLog(e);
				return ServiceResultConfig.FAILED;
			}
			data.put(COLLECT_SPEC_MSG, "收藏成功!");
			return ServiceResultConfig.SUCCESS;
		}
		// 存入前台显示信息，结束业务
		data.put(COLLECT_SPEC_MSG, "数据错误，无法收藏");
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 收藏自己上传的光谱到个人收藏夹（供collectSpectrum方法调用）
	 * 
	 * @author 刘驭洲
	 * @param favorite 传入的数据
	 * @param data  存放前台显示的信息
	 * @return 是否收藏成功
	 */
	private String collectIndividualSpectrum(Favorite favorite, Map<String, Object> data) {
		// 校验数据
		if (checkCollectInfo(favorite)) {
			// 查询收藏夹信息
			List<Map<String, Object>> isCollect = queryService.queryList("queryFavorite",
					BeanUtils.beanToMap(favorite));
			// 对查询的信息进行处理
			if (isCollect == null || isCollect.size() == 0) {
				data.put(COLLECT_SPEC_MSG, "该收藏夹不存在，可能已被删除，收藏失败");
				return ServiceResultConfig.FAILED;
			} else {
				favorite.setFavoriteName((String) isCollect.get(0).get("favoriteName"));
				favorite.setPath((String) isCollect.get(0).get("path"));
			}
			// 判断分给该用户的存储空间是否够用
			if (new File(FSConfig.userFavorite(), favorite.getPath()).length() > 1024 * 1024 * 16) {
				data.put(COLLECT_SPEC_MSG, "系统分配给您的存储空间已满，收藏失败！");
				return ServiceResultConfig.FAILED;
			}
			// 设置文件源路径和目标路径 oldPath 临时文件路径 newPath 要存储的路径 beanPath 相对路径
			String oldPath = FSConfig.temp().getAbsolutePath() + File.separator
					+ favorite.getSpectrumList().get(0).getSpectrumFile();
			String newPath = FSConfig.userFavorite().getAbsolutePath() + File.separator + favorite.getPath()
					+ File.separator + favorite.getSpectrumList().get(0).getSpectrumFile();
			String beanPath = new File(favorite.getPath(), favorite.getSpectrumList().get(0).getSpectrumFile())
					.getPath();
			// 复制临时文件到指定文件夹
			try {
				FileUtils.copyFile(oldPath, newPath);
			} catch (FileNotFoundException e) {
				// 记录日志，存入提示信息，结束业务
				data.put(COLLECT_SPEC_MSG, "服务器错误(文件丢失)，请稍后再试，或者联系管理员");
				logger.error(e.getMessage(), e);
				systemLog(e);
				return ServiceResultConfig.FAILED;
			} catch (IOException e) {
				// 记录日志，存入提示信息，结束业务
				data.put(COLLECT_SPEC_MSG, "服务器错误，请稍后再试，或者联系管理员");
				logger.error(e.getMessage(), e);
				systemLog(e);
				return ServiceResultConfig.FAILED;
			}
			// 设置编号 和文件新路径 收藏时间
			String spectrumID = String.format("%02d", favorite.getSpectrumList().get(0).getSpectrumTypeID())
					+ String.format("%03d", favorite.getSpectrumList().get(0).getHardwareID())
					+ String.format("%06d", Integer.valueOf(favorite.getSpectrumList().get(0).getDetectedID()));
			Map<String, Object> idBeQuery = new HashMap<String, Object>();
			idBeQuery.put("spectrumID", spectrumID + "%");
			List<Map<String, Object>> idMap = queryService.queryList("querySpectrumID", idBeQuery);
			logger.debug(idMap.size() + "   " + idMap.toString());
			if (idMap == null || idMap.size() == 0) {
				favorite.getSpectrumList().get(0).setSpectrumID(spectrumID + "0000001");
			} else {
				String idEndSix = ((String) idMap.get(idMap.size() - 1).get("spectrumID")).substring(11,
						((String) idMap.get(0).get("spectrumID")).length());
				logger.debug(idEndSix);
				int idNum = Integer.valueOf(idEndSix) + 1;
				logger.debug(idNum);
				FIFOCache<String, String> spectrumIDCache = new FIFOCache<String, String>(0, 0);
				spectrumIDCache.put("spectrumID", spectrumID + String.format("%07d", idNum));
				favorite.getSpectrumList().get(0).setSpectrumID(spectrumIDCache.get("spectrumID"));
			}
			favorite.getSpectrumList().get(0).setSpectrumFile(beanPath);
			favorite.getSpectrumList().get(0).setSaveTime(System.currentTimeMillis());
			// 插入数据到数据库
			try {
				// 开启事务
				baseDao.transaction();
				// 先插入t_spectrum表
				baseDao.add("addSpectrum", BeanUtils.beanToMap(favorite.getSpectrumList().get(0)));
				// 在插入t_favoriteSpectrum表
				Map<String, Object> favoriteSpectrumMap = new HashMap<String, Object>();
				favoriteSpectrumMap.put("favoriteID", favorite.getFavoriteID());
				favoriteSpectrumMap.put("spectrumID", favorite.getSpectrumList().get(0).getSpectrumID());
				baseDao.add("addFavouriteSpectrum", favoriteSpectrumMap);
				// 插入t_spectrumContent表
				for (int i = 0; i < favorite.getSpectrumList().get(0).getContentsIDs().length; i++) {
					Map<String, Object> spectrumContentMap = new HashMap<String, Object>();
					spectrumContentMap.put("spectrumID", favorite.getSpectrumList().get(0).getSpectrumID());
					spectrumContentMap.put("contentID", favorite.getSpectrumList().get(0).getContentsIDs()[i]);
					baseDao.add("addSpectrumContent", BeanUtils.beanToMap(favorite.getSpectrumList().get(0)));
				}
				// 提交事务
				baseDao.commit();
			} catch (DaoException e) {
				// 事务回滚
				baseDao.rollback();
				// 记录错误信息
				data.put(COLLECT_SPEC_MSG, "服务器发生错误。请稍后再试，或联系管理员");
				// 记录日志
				logger.error(e.getMessage(), e);
				systemLog(e);
				// 删除存放咋个人文件夹下的文件 临时文件不做处理
				if (FileUtils.deleteFile(newPath) == false) {
					logService.addLog(BeanUtils.beanToMap(new Log(LogLevel.ERROR, LogType.SYSTEM,
							ServiceMapper.getServiceConfig(view).getId(), "文件删除失败，地址为：" + newPath)));
				}
				// 结束业务
				return ServiceResultConfig.FAILED;
			}
			// 删除临时文件
			if (FileUtils.deleteFile(oldPath) == false) {
				logService.addLog(BeanUtils.beanToMap(new Log(LogLevel.ERROR, LogType.SYSTEM,
						ServiceMapper.getServiceConfig(view).getId(), "文件删除失败，地址为：" + newPath)));
			}
			// 存入提示信息，结束业务
			data.put(COLLECT_SPEC_MSG, "收藏成功！");
			return ServiceResultConfig.SUCCESS;
		}
		// 存入错误信息，结束业务
		data.put(COLLECT_SPEC_MSG, "数据错误，收藏失败");
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 对收藏光谱的数据进行数据校验
	 * 
	 * @author 刘驭洲
	 * @param favorite 需要校验的数据
	 * @return 数据格式和要求是否正确 正确TRUE 错误FALSE
	 */
	private boolean checkCollectInfo(Favorite favorite) {
		// 校验收藏夹数据
		if (favorite.getFavoriteID() == null) {
			logger.debug(favorite.getFavoriteID());
			return false;
		}
		// 校验光谱数据
		return checkSpectrumInfo(favorite.getSpectrumList().get(0));
	}

	/**
	 * 删除个人空间中的某些光谱（含批量删除）非标准库会连带删除光谱文件和与其相关的报告
	 * 
	 * @author 刘驭洲
	 * @param params  前台传入的参数
	 * @param data  返回的处理结果
	 * @return 操作是否成功
	 */
	String deleteSpectrum(Map<String, String[]> params, Map<String, Object> data) {
		// 数据校验
		if (params.get("spectrumID") == null || params.get("spectrumID").length == 0) {
			data.put(DEL_SPEC_MSG, "数据错误，操作失败");
			return ServiceResultConfig.FAILED;
		}
		// 临时存放spectrumIDs
		String[] spectrumIDs = new String[params.get("spectrumID").length];
		for (int i = 0; i < params.get("spectrumID").length; i++) {
			// (String) (params.get("spectrumID")[i]);
			spectrumIDs[i] = getParamStringValue(params, "spectrumID", i);
		}
		Map<String, Object> spectrumIDMap = new HashMap<String, Object>();
		spectrumIDMap.put("favoriteID", getParamsValue(params, "favoriteID", 0));
		// 临时存储返回信息 （因为可能性太多 所以等最后在合并在一起）
		Map<String, Object> dataPretend = new HashMap<String, Object>();
		dataPretend.put("queryInfo", "");
		dataPretend.put("exception", "");
		dataPretend.put("delete", "");
		// 循环删除
		for (int i = 0; i < spectrumIDs.length; i++) {
			spectrumIDMap.remove("spectrumID");
			spectrumIDMap.put("spectrumID", spectrumIDs[i]);
			logger.debug(spectrumIDMap.toString());
			// 查询数据库，获取需要删除的文件的路径和当前状态
			List<Map<String, Object>> spectrumInfo = queryService.queryList("queryFavoriteSpectrumDetail",
					spectrumIDMap);
			// 判断是否有查询结果
			if (spectrumInfo == null || spectrumInfo.size() == 0) {
				dataPretend.put("queryInfo", dataPretend.get("queryInfo") + spectrumIDs[i] + " ");
				continue;
			}
			// 判断删除的光谱的状态 不是标准库才可以删除 如果是标准库只能删除记录 不能删除文件
			if ((Integer) spectrumInfo.get(0).get("isStandard") != 0) {
				// 删除数据库中用户收藏上的记录
				try {
					baseDao.transaction();
					if (baseDao.delete("deleteFavoriteSpectrum", spectrumIDMap) <= 0) {
						dataPretend.put("delete", dataPretend.get("delete") + spectrumIDs[i] + " ");
					}
					baseDao.commit();
					continue;
				} catch (DaoException e) {
					baseDao.rollback();
					logger.error(e.getMessage(), e);
					systemLog(e);
					dataPretend.put("exception", dataPretend.get("exception") + spectrumIDs[i] + " ");
					continue;
				}
			}
			// 取出List中的数据
			Map<String, Object> daoMap = spectrumInfo.get(0);
			String email = (String) (params.get("session_email")[0]);
			String filePath = new File(FSConfig.userFavorite().getAbsolutePath(), (String) daoMap.get("spectrumFile"))
					.getAbsolutePath();
			String tempPath = new File(FSConfig.temp().getAbsolutePath(), (String) daoMap.get("spectrumFile"))
					.getAbsolutePath();
			logger.debug(filePath + "  " + tempPath);
			// 对需要删除的文件先做备份
			// 报告需要重新查询 因为可能该光谱无报告
			List<Map<String, Object>> reportList = queryService.queryList("queryReport", spectrumIDMap);
			// 报告根路径
			String reportFilePath = FSConfig.userFavorite().getAbsolutePath() + File.separator + email
					+ daoMap.get("favoriteName") + File.separator;
			String reportTempPath = FSConfig.temp().getAbsolutePath() + File.separator;
			try {
				// 备份光谱文件 和 报告
				copyFile(-1, filePath, tempPath, reportFilePath, reportTempPath, reportList, daoMap);
			} catch (FileNotFoundException e) {
				logger.error(e.getMessage(), e);
				systemLog(e);
				dataPretend.put("exception", dataPretend.get("exception") + spectrumIDs[i] + " ");
				continue;
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
				systemLog(e);
				dataPretend.put("exception", dataPretend.get("exception") + spectrumIDs[i] + " ");
				continue;
			}
			// 删除文件
			if (FileUtils.deleteFile(filePath) == false) {
				// 记录信息
				dataPretend.put("exception", dataPretend.get("exception") + spectrumIDs[i] + " ");
				// 删除临时文件(光谱文件和报告文件)
				deleteFileAndReport(tempPath, reportFilePath, reportTempPath, spectrumIDs, i, daoMap, reportList);
				continue;
			}
			// 删除报告
			if (reportList != null && reportList.size() != 0) {
				// 记录删除的是否成功
				boolean flag = true;
				// 删除报告
				for (int j = 0; j < reportList.size(); j++) {
					// 若删除失败，事件进行回滚
					if (FileUtils.deleteFile(reportFilePath + daoMap.get("spectrumName") + "_"
							+ reportList.get(j).get("saveTime") + ".doc") == false) {
						flag = false;
						// 恢复之前删除的文件
						try {
							copyFile(j, tempPath, filePath, reportTempPath, reportFilePath, reportList, daoMap);
							// 删除临时文件(光谱文件和报告文件)
							deleteFileAndReport(tempPath, reportFilePath, reportTempPath, spectrumIDs, i, daoMap,
									reportList);
						} catch (FileNotFoundException e) {
							logger.error(e.getMessage(), e);
							systemLog(e);
							dataPretend.put("exception", dataPretend.get("exception") + spectrumIDs[i] + " ");
						} catch (IOException e) {
							logger.error(e.getMessage(), e);
							systemLog(e);
							dataPretend.put("exception", dataPretend.get("exception") + spectrumIDs[i] + " ");
						}
						// 跳出循环
						break;
					}
				}
				// 如果此过程发生错误则结束本次循环
				logger.debug(flag);
				if (flag == false) {
					continue;
				}
			}
			// 删除数据库数据
			try {
				baseDao.transaction();
				spectrumIDMap.put("isStandard", "0");
				baseDao.delete("deleteFavoriteSpectrum", spectrumIDMap);
				baseDao.delete("deleteSpectrum", spectrumIDMap);
				baseDao.commit();
			} catch (DaoException e) {
				baseDao.rollback();
				// 恢复被删除的文件
				try {
					// 恢复(报告和文件)
					copyFile(-1, tempPath, filePath, reportTempPath, reportFilePath, reportList, daoMap);
				} catch (FileNotFoundException e2) {
					logger.error(e2.getMessage(), e2);
					systemLog(e2);
				} catch (IOException e1) {
					logger.error(e.getMessage(), e);
					systemLog(e1);
				}
				// 删除临时文件(光谱文件和报告文件)
				deleteFileAndReport(tempPath, reportFilePath, reportTempPath, spectrumIDs, i, daoMap, reportList);
				// 记录日志
				logger.error(e.getMessage(), e);
				systemLog(e);
				dataPretend.put("exception", dataPretend.get("exception") + spectrumIDs[i] + " ");
				continue;
			}
			// 删除临时文件(光谱文件和报告文件)
			deleteFileAndReport(tempPath, reportFilePath, reportTempPath, spectrumIDs, i, daoMap, reportList);

		}
		// logger.debug(dataPretend.get("queryInfo").toString().length()+"
		// "+dataPretend.get("exception").toString().length()+"
		// "+dataPretend.get("delete").toString().length());
		// 存入返回前台的信息 结束业务
		if (((String) dataPretend.get("queryInfo")).length() != 0) {
			dataPretend.put("queryInfo", "ID为：" + dataPretend.get("queryInfo") + "查询不到无法删除。");
		} else {
			dataPretend.put("queryInfo", "");
		}
		if (((String) dataPretend.get("exception")).length() != 0) {
			dataPretend.put("exception", "ID为：" + dataPretend.get("exception") + "删除失败（可能被占用），请稍后再试。");
		} else {
			dataPretend.put("exception", "");
		}
		if (((String) dataPretend.get("delete")).length() != 0) {
			dataPretend.put("delete", "ID为：" + dataPretend.get("delete") + "可能已被删除.");
		} else {
			dataPretend.put("delete", "");
		}
		if (dataPretend.get("queryInfo").toString().length() != 0
				|| dataPretend.get("exception").toString().length() != 0
				|| dataPretend.get("delete").toString().length() != 0) {
			data.put(DEL_SPEC_MSG, "" + dataPretend.get("exception") + " " + dataPretend.get("queryInfo") + " "
					+ dataPretend.get("delete") + "其余都已经删除成功！");
		} else {
			data.put(DEL_SPEC_MSG, "删除成功");
		}
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 删除临时文件和报告
	 * 
	 * @author 刘驭洲
	 * @param tempPath
	 * @param view
	 * @param reportTempPath
	 * @param spectrumIDs
	 * @param i
	 * @param daoMap
	 * @param reportList
	 */
	private void deleteFileAndReport(String tempPath, String view, String reportTempPath, String[] spectrumIDs, int i,
			Map<String, Object> daoMap, List<Map<String, Object>> reportList) {
		// 删除临时光谱文件
		if (FileUtils.deleteFile(tempPath) == false) {
			// 记录日志
			logService.addLog(BeanUtils.beanToMap(new Log(LogLevel.ERROR, LogType.SYSTEM,
					ServiceMapper.getServiceConfig(view).getId(), "个人光谱临时文件删除失败，光谱编号为：" + spectrumIDs[i])));
		}
		// 删除临时报告文件
		for (int j = 0; j < reportList.size(); j++) {
			if (FileUtils.deleteFile(reportTempPath + daoMap.get("spectrumName") + "_"
					+ reportList.get(0).get("saveTime") + ".doc") == false) {
				// 记录日志
				logService.addLog(BeanUtils.beanToMap(new Log(LogLevel.ERROR, LogType.SYSTEM,
						ServiceMapper.getServiceConfig(view).getId(),
						"个人临时光谱文件中的临时报告删除失败，光谱编号为：" + spectrumIDs[i] + ",报告编号为：" + reportList.get(j).get("reportID")
								+ "，地址：" + reportTempPath + daoMap.get("spectrumName") + "_"
								+ reportList.get(0).get("saveTime") + ".doc")));
			}
		}
	}

	/**
	 * 复制文件
	 * 
	 * @author 刘驭洲
	 * @param k
	 * @param tempPath
	 * @param filePath
	 * @param reportTempPath
	 * @param reportFilePath
	 * @param reportDao
	 * @param daoMap
	 * @throws IOException
	 */
	private void copyFile(int k, String tempPath, String filePath, String reportTempPath, String reportFilePath,
			List<Map<String, Object>> reportDao, Map<String, Object> daoMap) throws IOException {
		// 复制光谱文件
		FileUtils.copyFile(tempPath, filePath);
		// 复制报告
		if (reportDao != null && reportDao.size() != 0) {
			if (k == -1) {
				for (int j = 0; j < reportDao.size(); j++) {
					FileUtils.copyFile(
							reportTempPath + daoMap.get("spectrumName") + "_" + reportDao.get(j).get("saveTime")
									+ ".doc",
							reportFilePath + daoMap.get("spectrumName") + "_" + reportDao.get(j).get("saveTime")
									+ ".doc");
				}
			} else {
				for (int j = 0; j < k; j++) {
					FileUtils.copyFile(
							reportTempPath + daoMap.get("spectrumName") + "_" + reportDao.get(j).get("saveTime")
									+ ".doc",
							reportFilePath + daoMap.get("spectrumName") + "_" + reportDao.get(j).get("saveTime")
									+ ".doc");
				}
			}
		}
	}

	/**
	 * 更改坐标单位
	 * 
	 * @author YiJie
	 * @param params 存储参数数据，主要是现在的坐标和想要改变的坐标
	 * @param data
	 * @return 转换成对应坐标的光谱
	 */
	String swapCoordinate(Map<String, String[]> params, Map<String, Object> data) {
		// String formula = getParamStringValue(params, "formula", 0);获取更改坐标运算公式
		Map<String, Object> map = new HashMap<String, Object>();
		// 获取光谱文件
		List<SpectrumFile> spectrumFiles = getSpectrumFile(params, data, "swapCoordinate_msg");
		// 默认只处理第一个传入的光谱
		SpectrumFile spectrumFile = spectrumFiles.get(0);
		if (null == spectrumFile) {
			data.put("swapCoordinate_msg", "光谱文件不存在!");
			return ServiceResultConfig.FAILED;
		}
		// 获取原光谱坐标单位
		String xUnit = spectrumFile.getxUnit();
		String yUnit = spectrumFile.getyUnit();
		if (StringUtils.isAnyEmpty(xUnit, yUnit)) {
			data.put("swapCoordinate_msg", "未获取到原光谱的坐标单位！");
			return ServiceResultConfig.FAILED;
		}
		// 获取目标坐标单位
		String newXUnit = getParamStringValue(params, "newXUnit", 0);
		String newYUnit = getParamStringValue(params, "newYUnit", 0);
		if (StringUtils.isAnyEmpty(newXUnit, newYUnit)) {
			data.put("swapCoordinate_msg", "未获取到目标坐标单位！");
			return ServiceResultConfig.FAILED;
		}
		// 判断更改目标坐标单位是否与原坐标单位相同，相同则置空
		if (xUnit.equals(newXUnit)) {
			newXUnit = null;
		}
		if (yUnit.equals(newYUnit)) {
			newYUnit = null;
		}
		// 获取算法插件的id
		String pluginID = getParamStringValue(params, "pluginID", 0);
		if (StringUtils.isEmpty(pluginID)) {
			data.put("swapCoordinate_msg", "未选择执行更改坐标单位的算法！");
			return ServiceResultConfig.FAILED;
		}

		try {
			// 获取数据处理的接口
			DataProcessingPluginApi plugin = loadPlugin(pluginID);
			if (plugin == null) {
				data.put("swapCoordinate_msg", "此插件在服务器上未能找到！");
				return ServiceResultConfig.FAILED;
			}
			// 将目标单位封装成map数据
			map.put("newXUnit", newXUnit);
			map.put("newYUnit", newYUnit);
			// 执行算法
			spectrumFile = plugin.handleData(spectrumFile, map);
			if (spectrumFile == null) {
				data.put("swapCoordinate_msg", "插件执行时发生错误！");
				return ServiceResultConfig.FAILED;
			}
		} catch (PluginApiException e) {
			logger.error(e.getMessage(), e);
			systemLog(e);// 记录日志
			data.put("swapCoordinate_msg", "服务器出现未知错误,请稍后再试,或联系管理员");
			return ServiceResultConfig.FAILED;
		}
		data.put("SpectrumFile", spectrumFile);
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 频率标准化
	 * 
	 * @author 张彦
	 * @param params
	 * @param data
	 */
	String frequencyStandardization(Map<String, String[]> params, Map<String, Object> data) throws DaoException {
		List<SpectrumFile> spectrumFileList = getSpectrumFile(params, data, "frequencyStandardization_msg");
		SpectrumFile spectrumFile = spectrumFileList.get(0);
		if (spectrumFile == null) {
			data.put("frequencyStandardization_msg", "光谱文件未能找到");
			return ServiceResultConfig.FAILED;
		}
		String pluginID = getParamStringValue(params, "pluginID", 0);// 从前台参数中获取算法插件的id
		try {
			DataProcessingPluginApi plugin = loadPlugin(pluginID);
			if (plugin == null) {
				data.put("frequencyStandardization_msg", "此插件在服务器上未能找到");
				return ServiceResultConfig.FAILED;
			}
			spectrumFile = plugin.handleData(spectrumFile, null);
			if (spectrumFile == null) {
				data.put("frequencyStandardization_msg", "插件执行时发生错误");
				return ServiceResultConfig.FAILED;
			}
		} catch (PluginApiException e) {
			logger.error(e.getMessage(), e);
			systemLog(e);
			data.put("frequencyStandardization_msg", "服务器出现未知错误,请稍后再试,或联系管理员");
			return ServiceResultConfig.FAILED;
		}
		data.put("SpectrumFile", spectrumFile);

		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 差谱 待输入数据:差谱系数
	 * @author 海松 2016-09-20
	 * @param params
	 * @param data
	 * @return 处理后的光谱数据
	 */
	String substractSpectrum(Map<String, String[]> params, Map<String, Object> data) {
		SpectrumFile spectrumFileA = null;// 差谱A
		SpectrumFile spectrumFileB = null;// 被差谱B
		String pluginID = getParamStringValue(params, "pluginID", 0);// 从前台参数中获取查询算法插件编号
		String operate = getParamStringValue(params, "operate", 0);// 从前台参数中获取operate
		// 获取两张谱图
		List<SpectrumFile> spectrumFileList = getSpectrumFile(params, data, "substractSpectrum_msg");
		// 判断是否有两张以上光谱
		if (spectrumFileList.size() >= 2) {
			spectrumFileA = spectrumFileList.get(0);
			spectrumFileB = spectrumFileList.get(1);
		} else {
			data.put("substractSpectrum_msg", "获取的光谱少于两张");
			return ServiceResultConfig.FAILED;
		}
		try {
			DataProcessingPluginApi plugin = loadPlugin(pluginID);
			// 判断差谱插件对象是否为空
			if (plugin == null) {
				data.put("substractSpectrum_msg", "该算法插件无法使用，请重新选择");
				return ServiceResultConfig.FAILED;
			}
			// 执行差谱算法插件
			Map<String, Object> ext = new HashMap<String, Object>();
			ext.put("spectrumFileB", spectrumFileB);
			SpectrumFile spectrumFileResult = plugin.handleData(spectrumFileA, ext);
			// 执行插件成功
			if (spectrumFileResult != null) {
				data.put("substractSpectrum_msg", "差谱操作成功");
				data.put("SpectrumFile", spectrumFileResult);
			} else {// 执行插件失败
				data.put("substractSpectrum_msg", "差谱未得出正确结果，请重新操作。");
				return ServiceResultConfig.FAILED;
			}
		} catch (PluginApiException e) {
			logger.error(e.getMessage(), e);
			systemLog(e);
			data.put("substractSpectrum_msg", "该算法插件后台转化失败，无法使用");
			return ServiceResultConfig.FAILED;
		}
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 乘谱 待输入数据:乘谱系数
	 * @author gaozhao
	 * @param params
	 * @param data
	 * @return 处理后的光谱数据
	 */
	String multiplySpectrum(Map<String, String[]> params, Map<String, Object> data) {
		// 获取参数
		SpectrumFile spectrumFile;
		String str_ratio = getParamStringValue(params, "ratio", 0);
		String pluginID = getParamStringValue(params, "pluginID", 0);
		List<SpectrumFile> spectrumFileList = getSpectrumFile(params, data, "multiplySpectrum_msg");

		spectrumFile = spectrumFileList.get(0);
		if (null == spectrumFile) {
			data.put("multiplySpectrum_msg", "光谱数据不存在");
			return ServiceResultConfig.FAILED;
		}
		// 初始化集合
		Map<String, Object> ext = new HashMap<String, Object>();
		if (str_ratio.matches("^[-+]?[0-9]+([.]{1}[0-9]+){0,1}$")) {
			try {
				DataProcessingPluginApi plugin = loadPlugin(pluginID);
				// 未找到资源
				if (plugin == null) {
					data.put("multiplySpectrum_msg", "服务器未存在此插件资源");
					return ServiceResultConfig.FAILED;
				}
				ext.put("ratio", Double.valueOf(str_ratio));
				/// 获取处理结果
				SpectrumFile spectrumFileResult = plugin.handleData(spectrumFile, ext);
				data.put("SpectrumFile", spectrumFileResult);
			} catch (PluginApiException e) {
				logger.error(e.getMessage(), e);
				systemLog(e);
				// 服务器未存在此资源
				data.put("multiplySpectrum_msg", "服务器未存在此插件资源");
				return ServiceResultConfig.FAILED;
			}
		}
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 谱图运算
	 * 
	 * @author 付大石
	 * @param params
	 * @param data
	 * @return
	 */
	String arithmeticSpectrum(Map<String, String[]> params, Map<String, Object> data) {
		// 获取参数并验证
		String formula = getParamStringValue(params, "formula", 0); // 获取谱图运算公式
		String pluginID = getParamStringValue(params, "pluginID", 0);// 从前台参数中获取算法插件的id
		// super.view = super.getParamStringValue(params, "view", 0);
		List<SpectrumFile> spectrumFileList = getSpectrumFile(params, data, "arithmeticSpectrum_msg");
		if (spectrumFileList.size() > 2 || StringUtils.isBlank(pluginID) || StringUtils.isBlank(formula)) {
			data.put("arithmeticSpectrum_msg", "参数异常,数据不完整");
			return ServiceResultConfig.FAILED;
		}

		// 验证单位是否一致
		String xUnit = spectrumFileList.get(0).getxUnit();
		String yUnit = spectrumFileList.get(0).getyUnit();
		for (int i = 1, size = spectrumFileList.size(); i < size; i++) {
			if (!xUnit.equals(spectrumFileList.get(i).getxUnit())) {
				data.put("arithmeticSpectrum_msg", "参数异常2");
				return ServiceResultConfig.FAILED;
			}
			if (!yUnit.equals(spectrumFileList.get(i).getyUnit())) {
				data.put("arithmeticSpectrum_msg", "参数异常3");
				return ServiceResultConfig.FAILED;
			}
		}

		// 验证formula合法性
		if (!checkFormula(formula)) {
			data.put("arithmeticSpectrum_msg", "算术运算式不合法");
			return ServiceResultConfig.FAILED;
		}

		// 获取目标插件
		DataProcessingPluginApi plugin = loadPlugin(pluginID);
		if (plugin == null) {
			data.put("arithmeticSpectrum_msg", "插件加载失败，请稍后重试");
			return ServiceResultConfig.FAILED;
		}

		// 使用算法插件
		Map<String, Object> paramMap = new HashMap<String, Object>();// 构造调用插件的参数
		paramMap.put("spectrum2", spectrumFileList.get(0));
		SpectrumFile resultSpectrum = null;
		try {
			resultSpectrum = plugin.handleData(spectrumFileList.get(0), paramMap);
		} catch (PluginApiException e) {
			logger.error(e.getMessage(), e);
			systemLog(e);
			data.put("arithmeticSpectrum_msg", "插件执行失败,请稍后重试");
			return ServiceResultConfig.FAILED;
		}

		// 执行算法插件
		if (resultSpectrum == null) { // 验证resultSpectrum是否为空
			data.put("arithmeticSpectrum_msg", "执行失败，请稍后重试");
			return ServiceResultConfig.FAILED;
		}

		data.put("SpectrumFile", resultSpectrum);
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 判断用户导入光谱数据时何种性质的光谱文件， 并得到对应的SpectrumFile对象
	 * 
	 * 使用了该方法的方法： measureNoise、measureAverage、 derivativeSpectrum、
	 * smoothSpectrum、getPeaks、calibrationSpectrum 方法
	 * 
	 * <pre>
	 * sequence 获取光谱的顺序,多个光谱时可以标识,
	 * 用","分隔,可取值为 [spectrumID,tempSpectrumFile,jsonSpectrumFile] .
	 * 
	 * Map params中的Key： 
	 * spectrumID 光谱ID 
	 * tempSpectrumFile 临时光谱 
	 * tempSpectrumFileName  临时光谱文件名 
	 * jsonSpectrumFile json型的光谱
	 * </pre>
	 * 
	 * @author 徐泽昆,龙叔
	 * @param params
	 * @param data
	 * @param messageKey
	 * @return SpectrumFile集合 不会为空
	 */
	private List<SpectrumFile> getSpectrumFile(Map<String, String[]> params, Map<String, Object> data,
			String messageKey) {
		String sequence = getParamStringValue(params, "sequence", 0);
		logger.debug("sequence:" + sequence);
		List<SpectrumFile> spectrumFiles = new LinkedList<SpectrumFile>();

		try {
			if (StringUtils.isBlank(sequence)) {// 没有顺序获取/获取一个
				// spectrumID
				String spectrumID = getParamStringValue(params, "spectrumID", 0);
				if (!StringUtils.isBlank(spectrumID)) {
					spectrumFiles.add(GetSpectrumFile.getByID(spectrumID));
				}
				// tempSpectrumFile
				String tempSpectrumFile = getParamStringValue(params, "tempSpectrumFile", 0);
				if (!StringUtils.isBlank(tempSpectrumFile)) {
					File tempFile = new File(FSConfig.temp(), tempSpectrumFile);
					spectrumFiles.add(GetSpectrumFile.fileToObject(tempFile));
				}
				// jsonSpectrumFile
				String jsonSpectrum = getParamStringValue(params, "jsonSpectrumFile", 0);
				if (!StringUtils.isBlank(jsonSpectrum)) {
					spectrumFiles.add(GetSpectrumFile.jsonToObject(jsonSpectrum));
				}
			} else {// 按sequence里的顺序获取
				String[] strings = sequence.split(",");

				for (int i = 0; i < strings.length; i++) {
					String type = strings[i];
					if ("spectrumID".equals(type)) {// ID获取
						String spectrumID = getParamStringValue(params, "spectrumID", i);
						logger.debug(spectrumID);
						spectrumFiles.add(GetSpectrumFile.getByID(spectrumID));
					} else if ("tempSpectrumFile".equals(type)) {// 临时文件
						String tempSpectrumFile = getParamStringValue(params, "tempSpectrumFile", i);
						if (StringUtils.isBlank(tempSpectrumFile)) {
							throw new ServiceException("上传的临时文件不存在");
						}
						File tempFile = new File(FSConfig.temp(), tempSpectrumFile);
						spectrumFiles.add(GetSpectrumFile.fileToObject(tempFile));
					} else if ("jsonSpectrumFile".equals(type)) {// json字符串
						String jsonSpectrum = getParamStringValue(params, "jsonSpectrumFile", i);
						if (StringUtils.isBlank(jsonSpectrum)) {
							throw new ServiceException("光谱文件Json为空");
						}
						spectrumFiles.add(GetSpectrumFile.jsonToObject(jsonSpectrum));
					}
				}
			}
		} catch (ServiceException e) {
			// 存入向前台发送的信息
			data.put(messageKey, e.getMessage());
		}

		return spectrumFiles;
	}

	/**
	 * 加载光谱文件到前台
	 * 
	 * @param params
	 * @param data
	 * @return
	 */
	String loadSpectrumFile(Map<String, String[]> params, Map<String, Object> data) {
		List<SpectrumFile> list = getSpectrumFile(params, data, "loadSpectrumFile_msg");

		if (list.isEmpty()) {
			data.put("loadSpectrumFile_msg", "不存该光谱文件");
			return ServiceResultConfig.FAILED;
		}
		data.put("SpectrumFile", list);

		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 噪声测量 Map params中的Key：
	 * sectionLeft 当前左区间 （X坐标） 
	 * sectionRight 当前右区间 （X坐标）
	 * 传出的Map data中的Key： 
	 * sectionLeft 左区间 （parma中已有）
	 * sectionRight 右区间 （parma中已有）
	 * peaktopeak 得到的峰峰值 RMS 得到的RMS值
	 * 
	 * @author 徐泽昆
	 * @param params
	 * @param data
	 * @return
	 */
	String measureNoise(Map<String, String[]> params, Map<String, Object> data) {
		/* 进行噪声测量计算时需要用到的数据 */
		int count = 0;
		double dataMaxY = -888.888d;
		double dataMinY = 888.888d;
		double RMS = 0;

		List<SpectrumFile> spectrumFileList = getSpectrumFile(params, data, "measureNoise_msg");
		SpectrumFile spectrumFile = spectrumFileList.get(0);
		if (null == spectrumFile) {
			data.put("measureNoise_msg", "没有对应光谱文件，请检查后重试！");
			return ServiceResultConfig.FAILED;
		}

		// 通过spectrumFile对象，得到所有的通过List<Point>保存的光谱数据
		List<Point> listPoints = spectrumFile.getPoints();

		// 得到当前区间 数值上，左区间大于右区间
		String sectionLeft = getParamStringValue(params, "sectionLeft", 0);
		String sectionRight = getParamStringValue(params, "sectionRight", 0);
		// 判断传入的数据是否符合标准
		if (!StringUtils.isBlank(sectionLeft) && !StringUtils.isBlank(sectionRight) && listPoints != null
				&& listPoints.size() > 0) {
			double sectionLeftD = Double.parseDouble(sectionLeft);
			double sectionRightD = Double.parseDouble(sectionRight);

			// 遍历所有光谱数据，
			for (Point point : listPoints) {
				// 得到对应的X坐标
				double dataX = point.getX();
				// 筛选出在所选范围内的光谱数据
				if (dataX >= sectionRightD && dataX <= sectionLeftD) {
					count++;
					if (count == 1) {
						dataMaxY = point.getY();
						dataMinY = point.getY();
					}
					double dataY = point.getY();
					// 得到在此范围内，Y的极大值
					if (dataY > dataMaxY) {
						dataMaxY = dataY;
					}
					// 得到在此范围内，Y的极小值
					if (dataY < dataMinY) {
						dataMinY = dataY;
					}
					// 均方根值的部分计算过程
					RMS = dataY * dataY + RMS;
				}
			}
			// 得到最终的均方根值
			RMS = (double) Math.sqrt((RMS / count));

			data.put("sectionLeft", sectionLeft);
			data.put("sectionRight", sectionRight);
			data.put("peaktopeak", (dataMaxY - dataMinY));
			data.put("RMS", RMS);
			return ServiceResultConfig.SUCCESS;
		}
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 平均值测量 Map params中的Key： 
	 * sectionLeft 当前左区间 （X坐标） 
	 * sectionRight 当前右区间 （X坐标）
	 * 传出的Map data中的Key： 
	 * averageY 平均值测量后的结果Y值
	 * 
	 * @author 徐泽昆
	 * @param params
	 * @param data
	 * @return
	 */
	String measureAverageValue(Map<String, String[]> params, Map<String, Object> data) {
		// 进行平均值测量需要用到的数据
		int count = 0;
		double averageValue = 0;
		double sumValue = 0;

		List<SpectrumFile> spectrumFileList = getSpectrumFile(params, data, "measureAverageValue_msg");
		SpectrumFile spectrumFile = spectrumFileList.get(0);
		if (spectrumFile == null) {
			data.put("measureAverageValue_msg", "没有对应光谱文件，请检查后重试！");
			return ServiceResultConfig.FAILED;
		}

		// 通过spectrumFile对象，得到所有的通过List<Point>保存的光谱数据
		List<Point> listPoints = spectrumFile.getPoints();

		// 得到当前区间 数值上，左区间大于右区间
		String sectionLeft = getParamStringValue(params, "sectionLeft", 0);
		String sectionRight = getParamStringValue(params, "sectionRight", 0);
		// 判断传入的数据是否符合标准
		if (!StringUtils.isBlank(sectionLeft) && !StringUtils.isBlank(sectionRight) && listPoints != null
				&& listPoints.size() > 0) {
			double sectionLeftD = Double.parseDouble(sectionLeft);
			double sectionRightD = Double.parseDouble(sectionRight);

			// 遍历所有光谱数据
			for (Point point : listPoints) {
				// 得到对应的X坐标
				double dataX = point.getX();
				// 筛选出在当前的区间的光谱数据
				if (dataX >= sectionRightD && dataX <= sectionLeftD) {
					count++;
					double dataY = point.getY();
					// 得到该区间Y值的和
					sumValue = dataY + sumValue;
				}
			}
			// 得到该区间Y值的平均值
			averageValue = sumValue / count;
			data.put("averageY", averageValue);
			return ServiceResultConfig.SUCCESS;
		}
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 导数谱图（算法插件没有完成）
	 * Map params中的Key： 
	 * orderNumber 求导阶数 
	 * 传出的Map data中的Key：
	 * derivativeSpectrumFile 导数谱图后的光谱文件
	 * 
	 * @author 徐泽昆
	 * @param params
	 * @param data
	 * @return
	 */
	String derivativeSpectrum(Map<String, String[]> params, Map<String, Object> data) {
		List<SpectrumFile> spectrumFileList = getSpectrumFile(params, data, "derivativeSpectrum_msg");
		SpectrumFile spectrumFile = spectrumFileList.get(0);
		if (null == spectrumFile) {
			data.put("derivativeSpectrum_msg", "没有对应光谱文件，请检查后重试！");
			return ServiceResultConfig.FAILED;
		}
		// 得到求导阶数
		String orderNumber = getParamStringValue(params, "orderNumber", 0);
		Map<String, Object> orderNumberMap = new HashMap<String, Object>();
		orderNumberMap.put("orderNumber", orderNumber);

		if (!StringUtils.isBlank(orderNumber)) {
			List<Point> listPoints = spectrumFile.getPoints();
			if (listPoints != null && listPoints.size() > 0) {
				// 然后传递参数，调用插件
				DataProcessingPluginApi DerivativeSpectrumPlugin = null;
				try {
					DerivativeSpectrumPlugin = PluginFactory.getPlugin(DerivativeSpectrumPlugin.class);
					spectrumFile = DerivativeSpectrumPlugin.handleData(spectrumFile, orderNumberMap);
				} catch (PluginApiException e) {
					logger.error(e.getMessage(), e);
					systemLog(e);
					data.put("derivativeSpectrum_msg", e.getMessage());
					return ServiceResultConfig.FAILED;
				}
				// 返回导数谱图处理后的光谱
				if (spectrumFile != null && spectrumFile.getPoints() != null && spectrumFile.getPoints().size() > 0) {
					data.put("SpectrumFile", spectrumFile);
					return ServiceResultConfig.SUCCESS;
				}
			} else {
				data.put("derivativeSpectrum_msg", "光谱数据出错，请检查后重试!");
				return ServiceResultConfig.FAILED;
			}
		}
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 标峰 Map
	 * params中的Key： 
	 * valueY  基线 
	 * sensitiveValue 灵敏度 
	 * 传出的Map data中的Key：
	 * PeakSpectrumFile 标峰后的光谱文件
	 * 
	 * @author 徐泽昆
	 * @param params
	 * @param data
	 * @return
	 */
	String getPeaks(Map<String, String[]> params, Map<String, Object> data) {
		// 获取光谱数据，基线，灵敏度
		List<SpectrumFile> spectrumFileList = getSpectrumFile(params, data, "getPeaks_msg");
		SpectrumFile spectrumFile = spectrumFileList.get(0);
		if (null == spectrumFile) {
			data.put("getPeaks_msg", "没有对应光谱文件，请检查后重试！");
			return ServiceResultConfig.FAILED;
		}
		String valueY = getParamStringValue(params, "valueY", 0);
		String sensitiveValue = getParamStringValue(params, "sensitiveValue", 0);

		// 为了编译通过，将基线，灵敏度放在Map中
		Map<String, Object> ext = new HashMap<String, Object>();
		ext.put("valueY", valueY);
		ext.put("sensitiveValue", sensitiveValue);
		if (spectrumFile != null) {
			List<Point> listPoints = spectrumFile.getPoints();
			if (listPoints != null && listPoints.size() > 0) {
				// 调用寻峰算法
				DataProcessingPluginApi peakSeekingPlugin = null;
				try {
					peakSeekingPlugin = PluginFactory.getPlugin(PeakSeekingPlugin.class);
					spectrumFile = peakSeekingPlugin.handleData(spectrumFile, ext);
				} catch (PluginApiException e) {
					logger.error(e.getMessage(), e);
					systemLog(e);
					data.put("getPeaks_msg", e.getMessage());
					return ServiceResultConfig.FAILED;
				}
				// 返回寻峰处理后的光谱文件
				if (spectrumFile.getPeaks() != null && spectrumFile.getPeaks().size() > 0) {
					data.put("peaks", spectrumFile.getPeaks());
					return ServiceResultConfig.SUCCESS;
				}
			} else {
				data.put("getPeaks_msg", "光谱数据出错，请检查后重试!");
				return ServiceResultConfig.FAILED;
			}
		}
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 平滑谱图 
	 * 传出的Map data中的Key： 
	 * smoothSpectrumFile 平滑谱图后的光谱文件
	 * 
	 * @author 徐泽昆
	 * @param params
	 * @param data
	 * @return 处理结果,回送至web层
	 */
	String smoothSpectrum(Map<String, String[]> params, Map<String, Object> data) {
		// 得到所选的光谱文件
		List<SpectrumFile> spectrumFileList = getSpectrumFile(params, data, "smoothSpectrum_msg");
		SpectrumFile spectrumFile = spectrumFileList.get(0);
		if (null == spectrumFile) {
			data.put("smoothSpectrum_msg", "没有对应光谱文件，请检查后重试！");
			return ServiceResultConfig.FAILED;
		}
		List<Point> listPoints = spectrumFile.getPoints();
		if (listPoints != null && listPoints.size() > 0) {
			// 调用平滑处理算法
			PretreatmentPluginApi smoothingFilterPlugin = null;
			try {
				smoothingFilterPlugin = PluginFactory.getPlugin(SmoothingFilterPlugin.class);
				spectrumFile = smoothingFilterPlugin.pretreatment(spectrumFile, null);
			} catch (PluginApiException e) {
				logger.error(e.getMessage(), e);
				systemLog(e);
				data.put("smoothSpectrum_msg", e.getMessage());
				return ServiceResultConfig.FAILED;
			}
			if (spectrumFile.getPoints() != null && spectrumFile.getPoints().size() > 0) {
				// 返回平滑处理后的光谱文件
				data.put("SpectrumFile", spectrumFile);
				return ServiceResultConfig.SUCCESS;
			}
		} else {
			data.put("smoothSpectrum_msg", "光谱数据出错，请检查后重试！");
			return ServiceResultConfig.FAILED;
		}
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 自动基线校正谱图
	 * 传出的Map data中的Key： calibrationSpectrumFile 自动基线校正后的光谱文件
	 * 
	 * @author 徐泽昆
	 * @param params
	 * @param data
	 * @return 处理结果,回送至web层
	 */
	String calibrationSpectrum(Map<String, String[]> params, Map<String, Object> data) {
		// 得到所选的光谱文件
		List<SpectrumFile> spectrumFileList = getSpectrumFile(params, data, "calibrationSpectrum_msg");
		SpectrumFile spectrumFile = spectrumFileList.get(0);
		if (null == spectrumFile) {
			data.put("calibrationSpectrum_msg", "没有对应光谱文件，请检查后重试！");
			return ServiceResultConfig.FAILED;
		}
		List<Point> listPoints = spectrumFile.getPoints();
		if (listPoints != null && listPoints.size() > 0) {
			// 调用基线校正算法
			DataProcessingPluginApi calibrationPlugin = null;
			try {
				calibrationPlugin = PluginFactory.getPlugin(CalibrationSpectrumPlugin.class);
				calibrationPlugin.handleData(spectrumFile, null);
			} catch (PluginApiException e) {
				logger.error(e.getMessage(), e);
				systemLog(e);
				data.put("calibrationSpectrum_msg", e.getMessage());
				return ServiceResultConfig.FAILED;
			}
			if (spectrumFile.getPoints() != null && spectrumFile.getPoints().size() > 0) {
				// 得到基线校正后的光谱文件
				data.put("SpectrumFile", spectrumFile);
				return ServiceResultConfig.SUCCESS;
			}
		} else {
			data.put("calibrationSpectrum_msg", "光谱数据出错，请检查后重试！");
			return ServiceResultConfig.FAILED;
		}
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 获取光谱合适的预处理算法 根据传来的光谱的信息，查询数据库，得到相应算法名
	 * 
	 * @author 徐泽昆
	 * @param spectrum
	 * @return 预处理算法路径
	 */
	private List<String> getPretreatmentMethod(Spectrum spectrum) {
		// 光谱类型 被检测物
		if (spectrum != null) {
			int SpectrumTypeID = spectrum.getSpectrumTypeID();
			String detectedID = spectrum.getDetectedID();

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("spectrumTypeID", SpectrumTypeID);
			params.put("detectedID", detectedID);

			if (SpectrumTypeID > 0 && !StringUtils.isBlank(detectedID)) {
				// 得到光谱类型、被检测物对应的算法插件ID集合
				List<Map<String, Object>> queryPluginIDList = queryService.queryList("queryPluginID", params);

				if (queryPluginIDList != null && queryPluginIDList.size() > 0) {
					List<String> plugins = new ArrayList<String>();
					// 遍历该算法插件ID集合，得到每个存放在Map中的pluginID
					for (Map<String, Object> pluginIDMap : queryPluginIDList) {
						if (StringUtils.isBlank((String) pluginIDMap.get("pluginID"))) {
							continue;
						}
						// 将存在Map中的pluginID作为参数 ，查询该pluginID对应的Plugin集合
						List<Map<String, Object>> queryPluginList = queryService.queryList("queryPlugin", pluginIDMap);
						if (queryPluginList != null && queryPluginList.size() > 0) {
							// 遍历这个Plugin集合，得到每个Plugin对应的jarPath，通过其得到完整的路径
							for (Map<String, Object> pluginMap : queryPluginList) {
								String jarPath = (String) pluginMap.get("jarPath");
								if (StringUtils.isBlank(jarPath)) {
									continue;
								}
								jarPath = new File(FSConfig.plugin(), jarPath).getAbsolutePath();
								// 将完整路径放入一个List中返回
								plugins.add(jarPath);
							}
						} else {
							return null;
						}
					}
					return plugins;
				}
			}
		}
		return null;
	}

	/**
	 * 修改个人空间中的光谱
	 * Map params中的Key： spectrumName 光谱名称 spectruDescription 光谱描述
	 * @param params
	 * @param data
	 * @return
	 */
	String modifySpectrum(Map<String, String[]> params, Map<String, Object> data) {
		// 1.根据光谱ID找到对应的spectrum对象
		String spectrumID = getParamStringValue(params, "spectrumID", 0);
		// 为了使编译通过，将光谱ID放到Map中
		Map<String, Object> spectrumIDMap = new HashMap<String, Object>();
		spectrumIDMap.put("spectrumID", spectrumID);
		List<Map<String, Object>> spectrumList = null;
		Spectrum spectrum = null;

		// 根据光谱ID query出 该光谱ID对应的光谱对象（List）
		spectrumList = queryService.queryList("querySpectrum", spectrumIDMap);
		if (spectrumList != null && spectrumList.size() > 0) {
			spectrum = BeanUtils.mapToBean(spectrumList.get(0), Spectrum.class);
		}
		// 2.判断该光谱是否为标准库光谱，如果为标准库光谱，提示不能修改 结束程序
		if (spectrum != null) {
			if (spectrum.isStandard()) {
				data.put("modifySpectrum_msg", "这是标准库光谱，无法修改");
				return ServiceResultConfig.FAILED;
			}
			// 3.如果不是标准库的，则取出params中的的光谱名称 和光谱描述，如果这两个都不为空，则update数据库
			else {
				String spectrumName = getParamStringValue(params, "spectrumName", 0);
				String spectruDescription = getParamStringValue(params, "spectruDescription", 0);

				if (!StringUtils.isBlank(spectrumName) && !StringUtils.isBlank(spectruDescription)) {
					Map<String, Object> spectrumInfoMap = new HashMap<String, Object>();
					spectrumInfoMap.put("spectrumName", spectrumName);
					spectrumInfoMap.put("description", spectruDescription);
					spectrumInfoMap.put("spectrumID", spectrum.getSpectrumID());
					try {
						baseDao.update("updateSpectrum", spectrumInfoMap);
						data.put("modifySpectrum_msg", "光谱信息修改成功");
					} catch (DaoException e) {
						logger.error(e.getMessage(), e);
						systemLog(e);
						// 存入向前台发送的信息
						data.put("modifySpectrum_msg", "服务器错误，操作失败，请稍后再试");
						return ServiceResultConfig.FAILED;
					}

					// 4.update对应的光谱文件
					SpectrumFile spectrumFile = null;
					try {
						File existFile = new File(FSConfig.userFavorite(), spectrum.getSpectrumFile());
						spectrumFile = JsonUtils.Gson(false).fromJson(new FileReader(existFile), SpectrumFile.class);
						spectrumFile.setSpectrumName(spectrumName);
						org.apache.commons.io.FileUtils.writeStringToFile(existFile,
								JsonUtils.Gson(false).toJson(spectrumFile), "UTF-8");
					} catch (FileNotFoundException e) {
						logger.error(e.getMessage(), e);
						systemLog(e);
						data.put("modifySpectrum_msg", "找不到该文件 ");
						return ServiceResultConfig.FAILED;
					} catch (IOException e) {
						logger.error(e.getMessage(), e);
						systemLog(e);
						// 存入向前台发送的信息
						data.put("modifySpectrum_msg", "系统错误，操作失败，请稍后再试");
						return ServiceResultConfig.FAILED;
					}
					return ServiceResultConfig.SUCCESS;
				}
				data.put("modifySpectrum_msg", "参数错误");
				return ServiceResultConfig.FAILED;
			}
		}
		data.put("modifySpectrum_msg", "系统错误，操作失败,光谱不存在");
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 审核上传到标准库的光谱,如果拒绝上传到标准库的关铺，需用EmailUtil工具类发送邮件
	 * @author 雷鑫
	 * @param params
	 * @param data
	 * @return
	 */
	String auditSpectrum(Map<String, String[]> params, Map<String, Object> data) {
		Map<String, Object> condition = new HashMap<String, Object>();
		// 先校验信息,查询该光谱的审核状态，如果不再是待审核状态，说明已经审核。返回失败
		List<Map<String, Object>> spectrumList = null;
		if (checkAuditSpectrum(params)) {
			condition.put("spectrumID", getParamStringValue(params, "spectrumID", 0));
			spectrumList = queryService.queryList("querySubmitSpectrum", condition);
			// 获得审核状态
			if (spectrumList == null || spectrumList.size() == 0) {
				data.put("auditSpectrum_msg", "该光谱可能已被删除");
				return ServiceResultConfig.FAILED;
			}
			if (!spectrumList.get(0).get("state").equals(ApplicationState.WAIT)) {
				data.put("auditSpectrum_msg", "该光谱已经被其他管理员审核了");
				return ServiceResultConfig.FAILED;
			}
		}
		// 获得用户的邮箱
		String userID = getParamStringValue(params, "userID", 0);
		condition.put("userID", Integer.parseInt(userID));
		// 获得用户的邮箱
		List<Map<String, Object>> userList = queryService.queryList("queryUser", condition);
		if (null == userList || userList.isEmpty()) {
			data.put("auditSpectrum_msg", "用户信息错误");
			return ServiceResultConfig.FAILED;
		}
		String email = (String) userList.get(0).get("email");

		if (getParamStringValue(params, "state", 0).equals(ApplicationState.PASS)) {// 通过审核

			File spectrumRealFile = new File(FSConfig.userFavorite(), (String) spectrumList.get(0).get("spectrumFile"));

			if (!spectrumRealFile.exists()) {
				data.put("auditSpectrum_msg", "光谱文件路径错误");
				sendAuditSpectrumEmail("光谱不在原文件夹中，此次审核失败", email, false, data);
				return ServiceResultConfig.FAILED;
			}
			try {
				// 把用户光谱复制到标准库光谱的文件路径下
				org.apache.commons.io.FileUtils.copyFile(spectrumRealFile,
						new File(FSConfig.standSpectrum(), spectrumRealFile.getName()));
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
				systemLog(e);
				data.put("auditSpectrum_msg", "复制用户光谱时出错");
				return ServiceResultConfig.FAILED;
			}
			condition.put("state", ApplicationState.PASS);
			condition.put("isStandard", 1);
			try {
				// 调用DAO的update方法 1.把用户光谱路径修改成标准库光谱路径
				// 2.把用户光谱表的isstandard更新为true3.把光谱提交表的state更新为pass
				baseDao.transaction();
				baseDao.update("updateSubmitSpectrum", condition);
				baseDao.update("updateSpectrum", condition);
				baseDao.commit();
				data.put("auditSpectrum_msg", "标准库更新成功");
				sendAuditSpectrumEmail(null, email, true, data);
				return ServiceResultConfig.SUCCESS;

			} catch (DaoException e) {
				baseDao.rollback();
				logger.error(e.getMessage(), e);
				systemLog(e);
				// 存入向前台发送的信息
				data.put("auditSpectrum_msg", "服务器错误，操作失败，请联系超管或稍后再试");
				return ServiceResultConfig.FAILED;
			}
		}
		// 获得拒绝的原因
		String message = getParamStringValue(params, "reason", 0);
		condition.put("state", ApplicationState.REFUSE);
		// 更新光谱审核状态
		try {
			baseDao.transaction();
			if (baseDao.update("updateSubmitSpectrum", condition) > 0) {
				baseDao.commit();
				sendAuditSpectrumEmail(message, email, false, data);
				return ServiceResultConfig.SUCCESS;
			}
		} catch (DaoException e) {
			baseDao.rollback();
			logger.error(e.getMessage(), e);
			systemLog(e);
			// 存入向前台发送的信息
			data.put("auditSpectrum_msg", "更新光谱状态为拒绝时出错");
			return ServiceResultConfig.FAILED;
		}
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 提交光谱到标准库
	 * @author 雷鑫
	 * @param params
	 * @param data
	 * @return
	 */
	String submitSpectrum(Map<String, String[]> params, Map<String, Object> data) {
		String spectrumID = getParamStringValue(params, "spectrumID", 0);
		if (StringUtils.isBlank(spectrumID)) {
			data.put("submitSpectrum_msg", "验证信息失败，请重试");
			return ServiceResultConfig.FAILED;
		}
		try {
			// 如果验证成功,先判断是否数据库中已经提交了
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put("spectrumID", spectrumID);
			List<Map<String, Object>> list = baseDao.query("querySpectrum", dataMap);
			if (list != null && list.size() != 0) {
				data.put("submitSpectrum_msg", "你的光谱已经提交过了，请耐心等待审核结果");
				return ServiceResultConfig.FAILED;
			}
			// 调用DAO的add方法，在标准光谱提交表中插入一条记录
			dataMap.put("submitTime", System.currentTimeMillis());
			dataMap.put("state", ApplicationState.WAIT);
			baseDao.transaction();
			if (baseDao.add("submitSpectrum", dataMap) > 0) {
				baseDao.commit();
				data.put("submitSpectrum_msg", "申请提交到标准库成功，请等待管理员审核");
				return ServiceResultConfig.SUCCESS;
			}

		} catch (DaoException e) {
			baseDao.rollback();
			logger.error(e.getMessage(), e);
			systemLog(e);
			data.put("submitSpectrum_msg", "服务器错误，操作失败，请联系超管或稍后再试");
			return ServiceResultConfig.FAILED;
		}
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 上传光谱文件
	 * @param params
	 * @param data
	 * @return
	 */
	String uploadSpectrum(Map<String, String[]> params, Map<String, Object> data) {
		// 把前台传来的参数封装成Spectrum对象
		Spectrum spectrum = BeanUtils.mapToBean(params, Spectrum.class);
		// 根据用户角色来跳转应页面显示光谱
		String session_roleID = getParamStringValue(params, "session_roleID", 0);
		String session_userID = getParamStringValue(params, "session_userID", 0);
		if (!StringUtils.isBlank(session_userID)) {
			spectrum.setUserID(Integer.valueOf(session_userID)); // 登录状态session_userID不会为空
		}
		// 对光谱数据进行判空和长度校正
		if (checkSpectrumInfo(spectrum)) {
			// 上传工具处理后的绝对路径
			String spectrumFilePath = getParamStringValue(params, "spectrumFile", 0);
			logger.debug(spectrumFilePath);
			if (StringUtils.endsWith(spectrumFilePath, ".json")) {
				int index = spectrumFilePath.lastIndexOf(File.separator);
				data.put("tempSpectrumFile", spectrumFilePath.substring(index + 1));
				data.put("uploadSpectrum_msg", "上传光谱成功");
				if (("" + Role.LAB).equals(session_roleID)) {
					return "lab";// 实验员分析页面
				}
				return ServiceResultConfig.SUCCESS;
			}
			ParsePluginApi parsePlugin = null;
			try {
				if (StringUtils.endsWith(spectrumFilePath, ".txt")) {
					parsePlugin = PluginFactory.getPlugin(TXTParsePlugin.class);
				}
				if (null == parsePlugin) {
					throw new PluginApiException("没有该插件能解析" + spectrumFilePath);
				}
				SpectrumFile spectrumFile = parsePlugin.parse(spectrumFilePath, null);
				spectrumFile.setSpectrumName(spectrum.getSpectrumName());// 修改名称
				// 如果解析光谱文件成功，就把光谱数据格式转换成统一的JSON格式
				String json = JsonUtils.Gson(false).toJson(spectrumFile, SpectrumFile.class);
				// 把JSON格式的数据写到临时文件中
				int last = spectrumFilePath.lastIndexOf(".");
				String newspectrumFilePath = spectrumFilePath.substring(0, last) + ".json";
				logger.debug(newspectrumFilePath);
				org.apache.commons.io.FileUtils.writeStringToFile(new File(newspectrumFilePath), json, "UTF-8");
				int index = spectrumFilePath.lastIndexOf(File.separator);
				data.put("tempSpectrumFile", newspectrumFilePath.substring(index + 1));
				data.put("uploadSpectrum_msg", "上传光谱成功");
				if (("" + Role.LAB).equals(session_roleID)) {
					return "lab";// 实验员分析页面
				}
				return ServiceResultConfig.SUCCESS;
			} catch (PluginApiException e) {
				logger.error(e.getMessage(), e);
				systemLog(e);
				data.put("uploadSpectrum_msg", "上传的光谱解析失败");
				return ServiceResultConfig.FAILED;
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
				systemLog(e);
				data.put("uploadSpectrum_msg", "服务器错误,请联系管理员稍后重试!");
				return ServiceResultConfig.FAILED;
			}
		}
		data.put("Spectrum", BeanUtils.beanToMap(spectrum));// 返回到前台
		data.put("uploadSpectrum_msg", "上传光谱信息有误");
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 判断光谱信息是否符合规
	 * @author 雷鑫
	 * @param parms
	 * @param data
	 * @return 光谱是否符合规范 这里验证光谱对象，主要是验证信息是否完整，以及非空，长度的判断
	 */
	private boolean checkSpectrumInfo(Spectrum spectrum) {
		if (StringUtils.isBlank(spectrum.getSpectrumName()) || spectrum.getSpectrumName().length() > 16) {
			return false;
		}
		if (spectrum.getHardwareID() == null || spectrum.getSpectrumTypeID() == null
				|| spectrum.getSpectrumFileTypeID() == null || spectrum.getDetectedID() == null
				|| spectrum.getContentsIDs() == null) {

			if (logger.isDebugEnabled())
				logger.debug((spectrum.getHardwareID() == null) + "," + (spectrum.getSpectrumTypeID() == null) + ","
						+ (spectrum.getSpectrumFileTypeID() == null) + "," + (spectrum.getDetectedID() == null) + ","
						+ (spectrum.getContentsIDs() == null));
			return false;
		}
		if (StringUtils.isBlank(spectrum.getSpectrumFile())) {
			return false;
		}
		return true;
	}
	
	/**
	 * 在审核提交到标准库光谱时私有的校验方法
	 * @author 雷鑫
	 * @param params
	 * @return
	 */
	private boolean checkAuditSpectrum(Map<String, String[]> params) {
		if (StringUtils.isBlank(getParamStringValue(params, "spectrumID", 0))) {
			return false;
		}
		if (params.containsKey("reason")) {
			if (getParamStringValue(params, "reason", 0).length() > 256) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 在进行审核标准库光谱时，调用的发送邮件功能
	 * @author 雷鑫
	 * @param message  管理员的审核意见,如果为""，默认为审核通过
	 * @param Email
	 * @param isPass
	 * @param data
	 * @return
	 */
	private boolean sendAuditSpectrumEmail(String message, String Email, boolean isPass, Map<String, Object> data) {
		Mail mail = null;
		if (isPass) {
			String subject = SendEmailUtils.getSubject("auditSpectrum_Subject");
			String content = SendEmailUtils.getContent("auditSpectrumSuccess_Content");
			mail = SendEmailUtils.getMail(Email, subject, content);
			SendEmailUtils.sendMail(mail);
			return true;
		}
		if (!isPass) {
			String subject = SendEmailUtils.getSubject("auditSpectrum_Subject");
			mail = SendEmailUtils.getMail(Email, subject, message);
			SendEmailUtils.sendMail(mail);
			return true;
		}
		return false;
	}

	private boolean checkFormula(String formula) {

		Deque<Character> charStack = new LinkedList<Character>();
		for (int i = 0; i < formula.length(); i++) {

			char ch = formula.charAt(i);
			if (ch == '(' || ch == '[') {

				charStack.push(ch);
			} else if (ch == ')') {

				char testChar = charStack.pollLast();
				if (testChar == 0 || testChar != '(') {

					break;
				}
			} else if (ch == ']') {

				char testChar = charStack.pollLast();
				if (testChar == 0 || testChar != '[') {

					break;
				}
			}
		}
		if (charStack.size() == 0) {

			return true;
		}
		return false;
	}

	/**
	 * 加载插件
	 * @param pluginID
	 * @return
	 * @author longshu
	 */
	@SuppressWarnings("unchecked")
	<T extends PluginApi> T loadPlugin(String pluginID) {
		// 从缓存中获取
		PluginApi cache = pluginCache.get(pluginID);
		if (null != cache) {
			return (T) cache;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pluginID", Integer.parseInt(pluginID));
		// 查询算法插件
		List<Map<String, Object>> pluginList = queryService.queryList("queryPlugin", params);
		try {
			if (null == pluginList || pluginList.isEmpty()) {
				return null;
			} else {
				String jarPath = (String) pluginList.get(0).get("jarPath");
				File jarFile = new File(FSConfig.plugin(), jarPath);
				T plugin = (T) PluginFactory.loadPlugin(jarFile);
				pluginCache.put(pluginID, plugin);// 添加到缓存
				return plugin;
			}
		} catch (PluginApiException e) {
			logger.error(e.getMessage(), e.getCause());
			systemLog(e);
		}
		return null;
	}

	/**
	 * 加载默认的插件
	 * @param pluginType
	 * @return
	 * @author longshu
	 */
	@SuppressWarnings("unchecked")
	private <T extends PluginApi> T loadDefaultPlugin(String pluginType) {
		// 从缓存中获取
		PluginApi cache = pluginCache.get(pluginType);
		if (null != cache) {
			return (T) cache;
		}

		Class<?> clazz = null;

		if (PluginType.PRETREAMENT.equals(pluginType)) {
			clazz = SmoothingFilterPlugin.class;// 平滑处理
		} else if (PluginType.DATAPROCESSING.equals(pluginType)) {
			clazz = PeakSeekingPlugin.class;// 寻峰
		} else if (PluginType.SPECTRUM_PARSE.equals(pluginType)) {
			clazz = ParsePluginApiImpl.class;// 解析json
		} else if (PluginType.SPECTRUM_ANALYSIS.equals(pluginType)) {
			clazz = AnalysisPluginApiImpl.class;// 分析
		}

		try {
			T plugin = (T) PluginFactory.getPlugin(clazz);
			pluginCache.put(pluginType, plugin);// 添加到缓存
			return plugin;
		} catch (PluginApiException e) {
			logger.error(e.getMessage(), e);
			systemLog(e);
		}
		return null;
	}

}
