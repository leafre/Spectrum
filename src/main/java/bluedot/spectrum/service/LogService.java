package bluedot.spectrum.service;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.Map;

import bluedot.spectrum.bean.Log;
import bluedot.spectrum.exception.DaoException;
import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.utils.BeanUtils;

/**
 * 记录数据库日志业务类 （调用addLog记录异常，要先对应service.xml方法编号，配置t_function表）
 * 
 * @author YJ 2016年8月24日
 */
public class LogService extends ServiceSupport {

	public LogService() {
	}

	/**
	 * 添加日志方法
	 * 
	 * @param view操作视图
	 * @param params日志信息集合
	 * @throws ServiceException
	 */
	public void addLog(Map<String, Object> params) {
		Log log = BeanUtils.mapToBean(params, Log.class);// 将传进的Map集合转为Log类

		// 获取数据
		String type = log.getType();// 日志类型
		int userID = log.getUserID();// 用户编号
		String ip = log.getIp();// 用户IP地址
		String level = log.getLevel();// 日志等级
		// int functionID = log.getFunctionID();// 功能编号
		String content = log.getContent();// 操作的具体内容

		// 校验数据
		try {
			isEmpty(type, "日志类型");
			isLegal(type, Log.LogType.class, "日志类型");
			// 判断是否为系统日志，非系统日志则用户编号和用户IP地址不能为空
			if (!log.getType().equals(Log.LogType.SYSTEM)) {
				isEmpty(userID, "用户编号");
				isEmpty(ip, "用户IP地址");
				isOutofRange(ip, 16, "用户IP地址");
			}
			isEmpty(level, "日志等级");
			isLegal(level, Log.LogLevel.class, "日志等级");
			// isEmpty(functionID, "功能编号");
			isOutofRange(content, 1024, "操作的具体内容");

			long time = System.currentTimeMillis();// 创建一个日志时间
			params.put("time", time);
			// log.setTime(time);// 设置日志时间
			//将log类转为Map集合
			// Map<String, Object> map = BeanUtils.beanToMap(log);
			int r = baseDao.add("addLog", params);// 调用DAO层添加一条日志记录
			if (r != 1) {
				String msg = "添加日志失败！";
				logger.error(msg);
			}
		} catch (ServiceException se) {
			logger.error(se.getMessage(), se);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
		}
	}

	/**
	 * 信息校验判断传入参数是否为空
	 * 
	 * @param obj要判断的信息
	 * @param msg_name提示信息名称
	 * @throws ServiceException
	 */
	private void isEmpty(Object obj, String msg_name) throws ServiceException {
		if (obj == null || obj.equals("") || obj.equals(0)) {// 传入数据为String型或int型，判断是否为空
			String msg = msg_name + "不能为空！";
			throw new ServiceException(msg);
		}
	}

	/**
	 * 信息校验判断传入参数是否合法
	 * 
	 * @param s要判断的信息
	 * @param match_class要匹配的静态成员变量所在接口名
	 * @param msg_name提示信息名称
	 * @throws ServiceException
	 */
	private void isLegal(String s, Class<?> match_class, String msg_name) throws ServiceException {
		boolean b = false;// 设定标记传入参数不合法
		Field[] field = match_class.getFields();// 根据接口名反射获取其成员变量
		for (Field f : field) {
			try {
				if (s.equals(f.get(null))) {// 获取成员变量中的静态常量值，与传入字段匹配
					b = true;// 若匹配成功则设置标记为合法
				}
			} catch (Exception e) {
				logger.debug(e.getMessage());
				throw new ServiceException(e);
			}
		}
		if (!b) {// 若循环匹配后，标记传入参数仍为不合法，则写入提示信息，抛出异常
			String msg = msg_name + "不合法！";
			throw new ServiceException(msg);
		}
	}

	/**
	 * 信息校验判断传入参数长度是否超出范围（包括非空校验）
	 * 
	 * @param s要判断的信息
	 * @param length参数长度约束
	 * @param msg_name提示信息名称
	 * @throws ServiceException
	 */
	private void isOutofRange(String s, int length, String msg_name) throws ServiceException {
		if (s != null && !s.equals("")) {// 当信息非空时进行长度是否超出范围校验
			try {
				if (s.getBytes("utf-8").length > length) {
					String msg = msg_name + "长度超出范围！";
					throw new ServiceException(msg);
				}
			} catch (UnsupportedEncodingException e) {
				logger.debug(e.getMessage());
				throw new ServiceException(e);
			}
		}
	}
}
