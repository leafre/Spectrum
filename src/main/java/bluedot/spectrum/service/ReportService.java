package bluedot.spectrum.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bluedot.spectrum.api.AnalysisPluginApi;
import bluedot.spectrum.api.PluginApi;
import bluedot.spectrum.api.PluginApiException;
import bluedot.spectrum.api.PluginFactory;
import bluedot.spectrum.api.SpectrumFile;
import bluedot.spectrum.bean.DetectedContent;
import bluedot.spectrum.bean.DetectedObject;
import bluedot.spectrum.bean.DetectedStandard;
import bluedot.spectrum.bean.Hardware;
import bluedot.spectrum.bean.Log;
import bluedot.spectrum.bean.Log.LogLevel;
import bluedot.spectrum.bean.Log.LogType;
import bluedot.spectrum.cache.Cache;
import bluedot.spectrum.cache.LFUCache;
import bluedot.spectrum.bean.Report;
import bluedot.spectrum.bean.Spectrum;
import bluedot.spectrum.exception.DaoException;
import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.utils.BeanUtils;
import bluedot.spectrum.utils.CreateWordUtils;
import bluedot.spectrum.utils.FileUtils;
import bluedot.spectrum.utils.GetSpectrumFile;
import bluedot.spectrum.utils.StringUtils;
import bluedot.spectrum.web.core.FSConfig;
import bluedot.spectrum.web.core.ServiceMapper;
import bluedot.spectrum.web.core.ServiceResultConfig;

public class ReportService extends ServiceSupport {
	private final String COLLECT_REPORT_MSG = "collectReport_msg";
	// 插件缓存 K:pluginID/pluginType V:PluginApi的实例
	private static transient Cache<String, PluginApi> pluginCache = new LFUCache<String, PluginApi>(20);

	public ReportService() {
	}

	public ReportService(String view, Map<String, Object[]> params, Map<String, Object> data) throws ServiceException {
		super(view, params, data);
		execute(view, params, data);
	}

	/*
	 * 报告相关
	 */
	/**
	 * 通过分析算法分析光谱，分析结构构成分析报告，前台显示
	 * @author 雷鑫
	 * @param params
	 * @param data
	 * @return
	 */
	String getReport(Map<String, String[]> params, Map<String, Object> data) {
		// 因为是为了给前台的报告页面显示报告的内容 ，在这里只要把报告对象存到data中，让对象在报告显示页填空
		Report report = analysis(params, data);
		if (report == null) {
			data.put("getReport_msg", "生成报告时出错,请重试");
			return ServiceResultConfig.FAILED;
		}
		data.put("Report", report);
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 使用分析算法分析光谱，获得报告
	 * @author 雷鑫
	 * @param params
	 * @param data
	 * @return
	 */
	private Report analysis(Map<String, String[]> params, Map<String, Object> data) {
		String pluginID = getParamStringValue(params, "pluginID", 0);
		if (StringUtils.isBlank(pluginID)) {
			data.put("getReport_msg", "算法验证失败");
			return null;
		}
		// 获得选中算法
		AnalysisPluginApi analysisPlugin = loadPlugin(pluginID);
		if (null == analysisPlugin) {
			data.put("getReport_msg", "算法不存在");
			return null;
		}
		// 得到光谱编号，查询光谱的类型是标准库光谱还是收藏光谱
		String spectrumID = getParamStringValue(params, "spectrumID", 0);
		if (StringUtils.isBlank(spectrumID)) {
			data.put("getReport_msg", "光谱验证失败");
			return null;
		}
		/*
		 * 开始分析
		 */
		try {
			SpectrumFile spectrumfile = GetSpectrumFile.getByID(spectrumID);
			// 使用分析算法对spectrum进行分析
			Map<String, Object> spectrumResult = analysisPlugin.analysis(spectrumfile, null);
			if (spectrumResult == null) {
				throw new ServiceException("分析结果为空,分析失败!");
			}
			Map<String,Object>querySpectrum = new HashMap<String,Object>();
			querySpectrum.put("spectrumID",spectrumID);
			List<Map<String,Object>>spectrumIDMap = queryService.queryList("querySpectrumDetail", querySpectrum);
			Report report = new Report();
			Spectrum spectrum = BeanUtils.mapToBean(spectrumIDMap.get(0), Spectrum.class);
			report.setSpectrum(spectrum);
			DetectedContent content = BeanUtils.mapToBean(spectrumResult, DetectedContent.class);
			report.setContent(content);
			float concentration = Float.valueOf("0" + spectrumResult.get("concentration"));
			report.setConcentration(concentration);
			return report;
		} catch (PluginApiException e) {
			logger.error(e.getMessage(), e);
			systemLog(e);
			data.put("analysis_msg", "分析失败,分析算法插件异常");
			return null;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			systemLog(e);
			data.put("analysis_msg", e.getMessage());// 来至GetSpectrumFile.getByID
			return null;
		}
	}

	/**
	 * 加载插件
	 * @param pluginID
	 * @return
	 * @author longshu
	 */
	@SuppressWarnings("unchecked")
	private <T extends PluginApi> T loadPlugin(String pluginID) {
		// 从缓存中获取
		PluginApi cache = pluginCache.get(pluginID);
		if (null != cache) {
			return (T) cache;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pluginID", Integer.parseInt(pluginID));
		// 查询算法插件
		List<Map<String, Object>> pluginList = queryService.queryList("queryPlugin", params);
		try {
			if (null == pluginList || pluginList.isEmpty()) {
				return null;
			} else {
				String jarPath = (String) pluginList.get(0).get("jarPath");
				File jarFile = new File(FSConfig.plugin(), jarPath);
				T plugin = (T) PluginFactory.loadPlugin(jarFile);
				pluginCache.put(pluginID, plugin);// 添加到缓存
				return plugin;
			}
		} catch (PluginApiException e) {
			logger.error(e.getMessage(), e.getCause());
			systemLog(e);
		}
		return null;
	}

	/**
	 * 收藏报告
	 * @author 刘驭洲
	 * @param params
	 * @param data
	 * @return
	 */
	String collectReport(Map<String, String[]> params, Map<String, Object> data) {
		// 转换实体类
		Report report = BeanUtils.mapToBean(params, Report.class);
		initReport(report, params);
		// 数据校验
		if (checkInfoReport(report)) {
			Map<String, Object> reportMap = BeanUtils.beanToMap(report);
			reportMap.put("detectedID", report.getSpectrum().getDetectedObject().getDetectedID());
			// 查询是否存在该光谱
			List<Map<String, Object>> isExistMap = queryService.queryList("querySpectrumDetail", reportMap);
			if (isExistMap == null || isExistMap.size() == 0) {
				data.put(COLLECT_REPORT_MSG, "该光谱不存在，无法对此报告进行收藏");
				return ServiceResultConfig.FAILED;
			}
			long time = System.currentTimeMillis();
			logger.debug(isExistMap.get(0).get("spectrumFile"));
			report.setSaveTime(time);
			String reportName = new File(isExistMap.get(0).get("spectrumFile").toString()).getName().split("\\.")[0]
					+ "_" + time + ".doc";
			if (isExistMap.get(0).get("spectrumType") != null
					&& ((String) isExistMap.get(0).get("spectrumType")).length() != 0) {
				isExistMap.get(0).put("spectrumTypeName", isExistMap.get(0).get("spectrumType"));
				isExistMap.get(0).remove("spectrumType");
			}
			// 模板中的数据
			Map<String, Object> dataMap = new HashMap<String, Object>();
			initReportDataMap(dataMap, report, isExistMap.get(0));
			dataMap.put("saveTime", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(time)));
			dataMap.put("contentChineseName", params.get("contentChineseName")[0]);
			dataMap.put("detectedChineseName", params.get("detectedChineseName")[0]);
			// 创建模板的附加内容
			Map<String, Object> ext = new HashMap<String, Object>();
			initReportTemplateExt(ext, reportName, (String) isExistMap.get(0).get("spectrumFile"));
			// 生成报告文件
			try {
				new CreateWordUtils().createWord(dataMap, ext);
			} catch (FileNotFoundException e) {
				data.put(COLLECT_REPORT_MSG, "模板文件丢失，请联系管理员，收藏失败！");
				logger.error(e.getMessage(), e);
				systemLog(e);
				// 删除生辰错误的报告文件
				deleteErrorReport(ext.get("destPath").toString());
				return ServiceResultConfig.ERROR;
			} catch (IOException e) {
				data.put(COLLECT_REPORT_MSG, "服务器错误，请联系管理员或稍后再试，收藏失败！");
				logger.error(e.getMessage(), e);
				systemLog(e);
				// 删除生辰错误的报告文件
				deleteErrorReport(ext.get("destPath").toString());
				return ServiceResultConfig.ERROR;
			} /*
				 * catch (TemplateException e) { data.put(COLLECT_REPORT_MSG,
				 * "服务器错误，请联系管理员或稍后再试，收藏失败！"); logger.error(e.getMessage(), e);
				 * systemLog(e); // 删除生辰错误的报告文件
				 * deleteErrorReport(ext.get("destPath").toString()); return
				 * ServiceResultConfig.ERROR; }
				 */ catch (Exception e) {
				data.put(COLLECT_REPORT_MSG, "服务器错误，请联系管理员或稍后再试，收藏失败！");
				logger.error(e.getMessage(), e);
				systemLog(e);
				// 删除生辰错误的报告文件
				deleteErrorReport(ext.get("destPath").toString());
				return ServiceResultConfig.ERROR;
			}
			// 插入数据库数据
			try {
				baseDao.add("addReport", reportMap);
			} catch (DaoException de) {
				data.put(COLLECT_REPORT_MSG, "服务器错误，请稍候再试，或联系管理员，收藏失败！");
				logger.error(de.getMessage(), de);
				systemLog(de);
				// 删除报告文件
				if (!FileUtils.deleteFile(ext.get("destPath").toString())) {
					// 记录日志
					logService.addLog(BeanUtils.beanToMap(new Log(LogLevel.ERROR, LogType.SYSTEM,
							ServiceMapper.getServiceConfig(view).getId(), "文件删除失败，路径为：" + ext.get("destPath"))));
				}
				return ServiceResultConfig.ERROR;
			}
			data.put(COLLECT_REPORT_MSG, "收藏成功！");
			return ServiceResultConfig.SUCCESS;
		}
		data.put(COLLECT_REPORT_MSG, "数据错误，收藏失败！");
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 删除个人空间里的报告
	 * 
	 * @author 雷鑫
	 * @param params
	 * @param data
	 * @return
	 */
	String deleteReport(Map<String, String[]> params, Map<String, Object> data) {
		String[] spectrumIDs = params.get("spectrumID");
		// unDeleteReportNameList用来存放不能正常删除的报告名或错误信息
		StringBuffer unDeleteReportNameList = new StringBuffer();
		if (spectrumIDs == null) {
			data.put("deleteReport_msg", "请选择要删除的光谱报告");
			return ServiceResultConfig.FAILED;
		}
		// 判断是否存在为空的光谱编号
		for (int i = 0; i < spectrumIDs.length; i++) {
			if (StringUtils.isBlank(spectrumIDs[i])) {
				unDeleteReportNameList.append(spectrumIDs[i] + "是错误的光谱编号");
				if (i == spectrumIDs.length - 1) {
					if (!unDeleteReportNameList.toString().equals("")) {
						data.put("deleteReport_msg", unDeleteReportNameList);
						return ServiceResultConfig.FAILED;
					}
				}
			}
		}
		// 查询判断该光谱是不是标准库光谱
		for (int i = 0; i < spectrumIDs.length; i++) {
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("spectrumID", spectrumIDs[i]);
			List<Map<String, Object>> list = queryService.queryList("querySpectrum", condition);
			if (list != null && !list.isEmpty()) {
				String isStandard = String.valueOf(list.get(0).get("isStandard"));
				String spectrumName = (String) list.get(0).get("spectrumName");
				if (Integer.parseInt(isStandard) == 1) {
					unDeleteReportNameList.append(spectrumName + "该报告是标准库报告，不能删除");
					continue;
				}
				// 查询report表中的saveTime,用来拼接报告文件名
				List<Map<String, Object>> reportTime = queryService.queryList("queryReport", condition);
				if (reportTime == null || reportTime.isEmpty()) {
					unDeleteReportNameList.append(spectrumName + "光谱报告已被删除，无搜索记录");
					continue;
				}
				// 在光谱表中查询光谱的文件路径
				String spectrumFile = (String) list.get(0).get("spectrumFile");
				long reportSaveTime = (Long) reportTime.get(0).get("saveTime");
				int index = spectrumFile.lastIndexOf("//");
				int last = spectrumFile.lastIndexOf(".");
				// 拼接光谱报告路径
				String reportPath = spectrumFile.substring(0, last) + "_" + reportSaveTime + ".doc";
				String reportFileName = spectrumFile.substring(index + 1, last + 1) + "doc";
				// 判断当前文件是否存在，如果不存在直接continue
				if (!new File(FSConfig.userFavorite(), reportPath).exists()) {
					unDeleteReportNameList.append(spectrumName + "光谱报告不存在,删除失败");
					continue;
				}
				try {
					baseDao.transaction();
					if (baseDao.delete("deleteReport", condition) > 0) {
						baseDao.commit();
						// 删除报告
						if (FileUtils.deleteFile(new File(FSConfig.userFavorite(), reportPath).getAbsolutePath())) {
							if (i == spectrumIDs.length - 1) {
								if (unDeleteReportNameList.toString().equals("")) {
									data.put("deleteReport_msg", "光谱报告删除操作成功");
									return ServiceResultConfig.SUCCESS;
								} else {
									data.put("deleteReport_msg", unDeleteReportNameList);
									return ServiceResultConfig.FAILED;
								}
							}
						} else {
							unDeleteReportNameList.append(reportFileName + "光谱报告删除失败");
							continue;
						}
					}
				} catch (DaoException e) {
					baseDao.rollback();
					logger.error(e.getMessage(), e);
					systemLog(e);
					unDeleteReportNameList.append(reportFileName + "光谱报告删除失败");
					continue;
				}
			} else {
				unDeleteReportNameList.append(spectrumIDs[i] + "我知道你想搞事情，这个编号的光谱不存在\r\n");
				continue;
			}
		}
		if (unDeleteReportNameList.toString().equals("")) {
			data.put("deleteReport_msg", "光谱报告删除操作成功");
			return ServiceResultConfig.SUCCESS;
		} else {
			data.put("deleteReport_msg", unDeleteReportNameList);
			return ServiceResultConfig.FAILED;
		}
	}

	/**
	 * 删除生成的错误的报告文件（可能是未完成的）
	 * @author 刘驭洲
	 * @param path 文件地址
	 */
	private void deleteErrorReport(String path) {
		File reportFile = new File(path);
		if (reportFile.exists()) {
			reportFile.delete();
		}
	}

	/**
	 * 初始化DateMap
	 * @param dataMap
	 * @param report
	 * @param ext
	 */
	private void initReportDataMap(Map<String, Object> dataMap, Report report, Map<String, Object> ext) {
		dataMap.putAll(BeanUtils.beanToMap(report));
		logger.debug(ext.toString());
		dataMap.putAll(BeanUtils.beanToMap(BeanUtils.mapToBean(ext, Spectrum.class)));
		dataMap.put("spectrumType", ext.get("spectrumTypeName"));
		dataMap.putAll(BeanUtils.beanToMap(BeanUtils.mapToBean(ext, Hardware.class)));
		dataMap.putAll(BeanUtils.beanToMap(report.getSpectrum().getDetectedObject()));
		dataMap.putAll(BeanUtils.beanToMap(report.getSpectrum().getDetectedObject().getDetectedStandard().get(0)));
		dataMap.putAll(BeanUtils
				.beanToMap(report.getSpectrum().getDetectedObject().getDetectedStandard().get(0).getContent()));
		logger.debug(dataMap.get("hardwareName"));
	}

	/**
	 * 初始化ExtMap
	 * @param ext
	 * @param reportName
	 * @param path
	 */
	private void initReportTemplateExt(Map<String, Object> ext, String reportName, String path) {
		ext.put("templateName", "report.ftl");
		ext.put("templatePath", FSConfig.template().getAbsolutePath());
		String destPath = new File(FSConfig.userFavorite() + File.separator + new File(path).getParent(), reportName)
				.getAbsolutePath();
		ext.put("destPath", destPath);
		logger.debug(path + "  " + ext.toString());
	}

	/**
	 * 把所有数据放入对象中
	 * @author 刘驭洲
	 * @param report
	 * @param params
	 */
	private void initReport(Report report, Map<String, String[]> params) {
		// 把光谱信息存入report对象
		report.setSpectrum(BeanUtils.mapToBean(params, Spectrum.class));
		// 把硬件信息存入光谱对象
		report.getSpectrum().setHardware(BeanUtils.mapToBean(params, Hardware.class));
		// 把被检测物信息存入光谱对象
		report.getSpectrum().setDetectedObject(BeanUtils.mapToBean(params, DetectedObject.class));
		// 创建list
		report.getSpectrum().getDetectedObject().setDetectedStandard(new HashMap<Integer, DetectedStandard>());
		report.getSpectrum().getDetectedObject().getDetectedStandard().put(0,
				BeanUtils.mapToBean(params, DetectedStandard.class));
		report.getSpectrum().getDetectedObject().getDetectedStandard().get(0)
				.setContent(BeanUtils.mapToBean(params, DetectedContent.class));
	}

	/**
	 * 校验将要存入数据库的数据
	 * @author 刘驭洲
	 * @param report  校验数据
	 * @return 校验结果是否正确
	 */
	private boolean checkInfoReport(Report report) {
		if (report.getContentID() == null || report.getSpectrumID() == null) {
			return false;
		}
		return true;
	}

}
