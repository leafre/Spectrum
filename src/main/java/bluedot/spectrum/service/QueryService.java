package bluedot.spectrum.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bluedot.spectrum.exception.DaoException;
import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.utils.StringUtils;
import bluedot.spectrum.web.core.ServiceMapper;
import bluedot.spectrum.web.core.ServiceResultConfig;

/**
 * 查询业务类
 * @author longshu 2016年8月13日
 */
public class QueryService extends ServiceSupport {
	// query方法得到数据在Map的key
	public static final String QUERYDATA = "querydata";

	public QueryService() {
	}

	/**
	 * 
	 * @param view
	 * @param params
	 * @param data
	 * @throws ServiceException
	 */
	public QueryService(String view, Map<String, Object[]> params, Map<String, Object> data) throws ServiceException {
		super(view, params, data);
		execute(view, params, data);
	}

	/**
	 * 包内使用
	 * @param operate sql的操作
	 * @param param
	 * @return
	 * @author 刘驭洲
	 */
	public List<Map<String, Object>> queryList(String operate, Map<String, Object> param) {
		List<Map<String, Object>> list = null;
		try {
			list = baseDao.query(operate, param);
			logger.debug("query size:" + list.size());
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			systemLog(e);
		}
		return list;
	}

	/**
	 * 外部Servlet使用
	 * @param data
	 * @return 显示界面
	 * @author longshu
	 */
	String query(Map<String, String[]> params, Map<String, Object> data) {
		Map<String, Object> param = new HashMap<String, Object>();
		for (String key : params.keySet()) {
			param.put(key, getParamsValue(params, key, 0));
		}
		// 可选参数 operate,sql的操作
		String operate = ServiceMapper.getServiceConfig(view).getOperate();
		if (StringUtils.isBlank(operate)) {
			operate = view;
		}
		// ajax使用
		String dataType = getParamStringValue(params, "dataType", 0);
		boolean ajax = false;
		if (dataType != null && dataType.equals(ServiceResultConfig.AJAX)) {
			ajax = true;
		}

		try {
			List<Map<String, Object>> result = baseDao.query(operate, param);
			data.put(QUERYDATA, result);
			logger.debug("querydata size:" + result.size());
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			systemLog(e);
			data.put("query_msg", "查询失败,服务器出错!");
			if (ajax) {
				return ServiceResultConfig.AJAX;
			}
			return ServiceResultConfig.FAILED;
		}
		if (ajax) {
			return ServiceResultConfig.AJAX;
		}
		return ServiceResultConfig.SUCCESS;
	}

}
