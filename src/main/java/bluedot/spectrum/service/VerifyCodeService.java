package bluedot.spectrum.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.utils.StringUtils;
import bluedot.spectrum.utils.VerifyCode;
import bluedot.spectrum.web.core.ServiceResultConfig;

/**
 * 动态验证码服务类
 * @author longshu 2016年8月21日
 */
public class VerifyCodeService extends ServiceSupport {
	private static VerifyCode verifyCode;
	private static boolean withLine = true;

	static {
		init();
	}

	public VerifyCodeService(String view, Map<String, Object[]> params, Map<String, Object> data)
			throws ServiceException {
		super(view, params, data);
		execute(view, params, data);
	}

	/**
	 * 返回图像验证码
	 * @param params
	 * @param data
	 * @return
	 */
	String verifycode(Map<String, Object[]> params, Map<String, Object> data) {
		BufferedImage image = null;
		synchronized (verifyCode) {
			if (withLine) {
				image = verifyCode.getImageWithLine();
			} else {
				image = verifyCode.getImage();
			}
			data.put(VerifyCode.KEY_CODE, verifyCode.getText());
			data.put(VerifyCode.KEY_IMG, image);
			logger.debug(VerifyCode.KEY_CODE + ":" + verifyCode.getText());
		}
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 初始化
	 */
	private static void init() {
		int width = 65;
		int height = 30;
		int length = 4;
		String codes = null;
		File propFile = new File(VerifyCodeService.class.getResource("/verifycode.properties").getFile());
		if (propFile.exists()) {
			try {
				Properties prop = new Properties();
				prop.load(new FileInputStream(propFile));
				String heightStr = prop.getProperty("height");
				String widthStr = prop.getProperty("width");
				String lengthStr = prop.getProperty("length");
				String withLineStr = prop.getProperty("withLine");
				codes = prop.getProperty("codes", null);
				/*
				 * 未配置或者配置错误使用默认配置
				 */
				if (StringUtils.matches(heightStr, "^[0-9]*$")) {
					height = Integer.valueOf(heightStr);
				}
				if (StringUtils.matches(widthStr, "^[0-9]*$")) {
					width = Integer.valueOf(widthStr);
				}
				if (StringUtils.matches(lengthStr, "^[1-9]$")) {
					length = Integer.valueOf(lengthStr);
				}
				if (StringUtils.matches(withLineStr, "false")) {
					withLine = false;
				}
				logger.debug("[length:" + length + ",withLine:" + withLine + ",width:" + width + ",height:" + height
						+" ,codes:"+ codes + "]");
			} catch (IOException e) {
				// 已经判断,不会发生
				throw new RuntimeException(e);
			}
		}
		verifyCode = new VerifyCode(width, height, length);
		if (null != codes) {
			verifyCode.setCodes(codes);
		}
	}

}
