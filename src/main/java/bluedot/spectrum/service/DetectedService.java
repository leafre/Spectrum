package bluedot.spectrum.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bluedot.spectrum.bean.DetectedContent;
import bluedot.spectrum.bean.DetectedObject;
import bluedot.spectrum.bean.DetectedStandard;
import bluedot.spectrum.bean.Log;
import bluedot.spectrum.exception.DaoException;
import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.utils.BeanUtils;
import bluedot.spectrum.web.core.ServiceMapper;
import bluedot.spectrum.web.core.ServiceResultConfig;

/**
 * 被检测物,检测内容相关的业务
 * 
 * @author YiJie
 * @date 2016年8月26日
 */

// TODO 判断中文名英文名化学式不能重名
public class DetectedService extends ServiceSupport {
	public static String DATA_MSG_NAME;
	public static String DATA_MSG;
	public static String SUCCESS = "成功！";
	public static String FAILED = "失败！";

	public DetectedService() {
	}

	public DetectedService(String view, Map<String, Object[]> params, Map<String, Object> data)
			throws ServiceException {
		super(view, params, data);
		execute(view, params, data);
	}

	/**
	 * 添加被检测物
	 * 
	 * @param params
	 * @param data
	 * @return
	 */
	String addDetectedObject(Map<String, Object[]> params, Map<String, Object> data) {
		DATA_MSG_NAME = "addDetectedObject_msg";
		DATA_MSG = "添加被检测物";

		// 获取数据
		DetectedObject det = BeanUtils.mapToBean(params, DetectedObject.class);
		String detectedID = det.getDetectedID();// 被检测物编号
		Integer categoryID = det.getCategoryID(); // 被检测物分类编号
		String chineseName = det.getChineseName(); // 中文名
		String englishName = det.getEnglishName(); // 英文名
		// 添加被检测物的同时也要添加对应检测内容的检测标准
		List<DetectedStandard> detStands = getDetectedStandard(detectedID, params);// 封装检测标准集合

		// 校验数据
		try {
			isEmpty(detectedID, "被检测物编号");
			isExist("queryDetectedObject", "detectedID", detectedID, true, "被检测物");
			isDetectedID(detectedID);// 验证被检测物编号是否合法
			isEmpty(categoryID, "被检测物分类编号");
			isEmpty(chineseName, "中文名");
			isOutofRange(chineseName, 32, "中文名");
			// isEmpty(englishName, String.class, "英文名");//英文名可以为空
			isOutofRange(englishName, 16, "英文名");
			// 验证检测标准集合
			isEmpty(detStands, "检测标准");
			// 逐个校验检测标准
			for (DetectedStandard d : detStands) {
				isEmpty(d.getContentID(), "检测内容编号");
				isExist("queryDectectedContent", "contentID", d.getContentID().toString(), false, "检测内容");
				// isEmpty(d.getStandards(), "执行标准");
				isOutofRange(d.getStandards(), 32, "执行标准");
				isEmpty(d.getStandardLine(), "标准线");
				// isEmpty(d.getValue(), "波动值");
				// isEmpty(d.getUnit(), "标准线单位");
				isOutofRange(d.getUnit(), 16, "标准线单位");
			}
		} catch (ServiceException e) {
			return dataError(e, data);
		}

		// 操作数据库
		try {
			baseDao.transaction();// 开启事务
			// 调用baseDao中的add方法添加一条被检测物记录
			if ((baseDao.add(view, BeanUtils.beanToMap(det))) != 1) {
				return recordError(data);
			}
			// 调用baseDao中的add方法逐个添加每条检测标准记录
			for (DetectedStandard d : detStands) {
				if ((baseDao.add(view, BeanUtils.beanToMap(d))) != 1) {
					return recordError(data);
				}
			}
			baseDao.commit();// 提交事务
		} catch (DaoException e) {
			return daoError(e, data);// 记录日志
		}
		data.put(DATA_MSG_NAME, DATA_MSG + SUCCESS);
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 修改被检测物
	 * 
	 * @param params
	 * @param data
	 * @return
	 */
	String modifyDetectedObject(Map<String, Object[]> params, Map<String, Object> data) {
		DATA_MSG_NAME = "modifyDetectedObject_msg";
		DATA_MSG = "修改被检测物";

		// 获取数据
		DetectedObject det = BeanUtils.mapToBean(params, DetectedObject.class);
		String detectedID = det.getDetectedID();// 被检测物编号
		Integer categoryID = det.getCategoryID(); // 被检测物分类编号
		String chineseName = det.getChineseName(); // 中文名
		String englishName = det.getEnglishName(); // 英文名

		// 校验数据
		try {
			isEmpty(detectedID, "被检测物编号");
			isExist("queryDectectedObject", "detectedID", detectedID, false, "被检测物");
			isDetectedID(detectedID);// 验证被检测物编号是否合法
			isEmpty(categoryID, "被检测物分类编号");
			isEmpty(chineseName, "中文名");
			isOutofRange(chineseName, 32, "中文名");
			// isEmpty(englishName, String.class, "英文名");//英文名可以为空
			isOutofRange(englishName, 16, "英文名");
		} catch (ServiceException e) {
			return dataError(e, data);
		}

		// 操作数据库
		try {
			baseDao.transaction();// 开启事务
			// 调用baseDao中的update方法修改该被检测物记录
			if ((baseDao.update(view, BeanUtils.beanToMap(det))) != 1) {
				return recordError(data);
			}
			baseDao.commit();// 提交事务
		} catch (DaoException e) {
			return daoError(e, data);
		}
		data.put(DATA_MSG_NAME, DATA_MSG + SUCCESS);
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 删除被检测物
	 * 
	 * @param params
	 * @param data
	 * @return
	 */
	String deleteDetectedObject(Map<String, Object[]> params, Map<String, Object> data) {
		DATA_MSG_NAME = "deleteDetectedObject_msg";
		DATA_MSG = "删除被检测物";

		// 获取数据
		String detectedID = (String) getParamsValue(params, "detectedID", 0);

		// 校验数据
		try {
			isEmpty(detectedID, "被检测物编号");
			isDetectedID(detectedID);// 验证被检测物编号是否合法
			isExist("queryDetectedObject", "detectedID", detectedID, false, "被检测物");
		} catch (ServiceException e) {
			return dataError(e, data);
		}

		// 操作数据库
		try {
			baseDao.transaction();// 开启事务
			// 封装数据
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("detectedID", detectedID);
			// 调用baseDao中的delete方法删除该被检测物
			if ((baseDao.delete(view, map)) != 1) {
				return recordError(data);
			}
			baseDao.commit();// 提交事务
		} catch (DaoException e) {
			return daoError(e, data);
		}
		data.put(DATA_MSG_NAME, DATA_MSG + SUCCESS);
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 添加检测内容
	 * 
	 * @param params
	 * @param data
	 * @return
	 */
	String addDetectedContent(Map<String, Object[]> params, Map<String, Object> data) {
		DATA_MSG_NAME = "addDetectedContent_msg";
		DATA_MSG = "添加检测内容";

		// 获取数据
		DetectedContent detCon = BeanUtils.mapToBean(params, DetectedContent.class);
		String chineseName = detCon.getChineseName(); // 中文名
		String englishName = detCon.getEnglishName(); // 英文名
		String chemicalName = detCon.getChemicalName(); // 化学式

		// 校验数据
		try {
			isEmpty(chineseName, "中文名");
			isOutofRange(chineseName, 32, "中文名");
			isOutofRange(englishName, 16, "英文名");
			isOutofRange(chemicalName, 64, "化学式");
		} catch (ServiceException e) {
			return dataError(e, data);
		}

		// 操作数据库
		try {
			baseDao.transaction();// 开启事务
			// 调用baseDao中的add方法添加一条被检测物记录
			if ((baseDao.add(view, BeanUtils.beanToMap(detCon))) != 1) {
				return recordError(data);
			}
			baseDao.commit();// 提交事务
		} catch (DaoException e) {
			return daoError(e, data);
		}
		data.put(DATA_MSG_NAME, DATA_MSG + SUCCESS);
		return ServiceResultConfig.SUCCESS;

	}

	/**
	 * 修改检测内容
	 * 
	 * @param params
	 * @param data
	 * @return
	 */
	String modifyDetectedContent(Map<String, Object[]> params, Map<String, Object> data) {
		DATA_MSG_NAME = "modifyDetectedContent_msg";
		DATA_MSG = "修改检测内容";

		// 获取数据
		DetectedContent detCon = BeanUtils.mapToBean(params, DetectedContent.class);
		Integer contentID = detCon.getContentID();// 检测内容编号
		String chineseName = detCon.getChineseName(); // 中文名
		String englishName = detCon.getEnglishName(); // 英文名
		String chemicalName = detCon.getChemicalName(); // 化学式

		// 校验数据
		try {
			isEmpty(contentID, "检测内容编号");
			isEmpty(chineseName, "中文名");
			isOutofRange(chineseName, 32, "中文名");
			isOutofRange(englishName, 16, "英文名");
			isOutofRange(chemicalName, 64, "化学式");
		} catch (ServiceException e) {
			return dataError(e, data);
		}

		// 操作数据库
		try {
			baseDao.transaction();// 开启事务
			// 调用baseDao中的update方法添加一条被检测物记录
			if ((baseDao.update(view, BeanUtils.beanToMap(detCon))) != 1) {
				return recordError(data);
			}
			baseDao.commit();// 提交事务
		} catch (DaoException e) {
			return daoError(e, data);
		}
		data.put(DATA_MSG_NAME, DATA_MSG + SUCCESS);
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 删除检测内容
	 * 
	 * @param params
	 * @param data
	 * @return
	 */
	String deleteDetectedContent(Map<String, Object[]> params, Map<String, Object> data) {
		DATA_MSG_NAME = "deleteDetectedContent_msg";
		DATA_MSG = "删除检测内容";

		// 获取数据
		Integer contentID = (Integer) getParamsValue(params, "contentID", 0);

		// 校验数据
		try {
			isEmpty(contentID, "检测内容编号");
			isExist("queryDetectedContent", "contentID", contentID.toString(), false, "检测内容");
		} catch (ServiceException e) {
			return dataError(e, data);
		}

		// 操作数据库
		try {
			baseDao.transaction();// 开启事务
			// 封装数据
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("contentID", contentID);
			// 调用baseDao中的delete方法删除该被检测物
			if ((baseDao.delete(view, map)) != 1) {
				return recordError(data);
			}
			baseDao.commit();// 提交事务
		} catch (DaoException e) {
			return daoError(e, data);
		}
		data.put(DATA_MSG_NAME, DATA_MSG + SUCCESS);
		return ServiceResultConfig.SUCCESS;
	}
	

	/**
	 * 校验数据异常处理 （包括打印logger、将错误信息写入data）
	 * 
	 * @param e
	 * @param data
	 * @return ServiceResultConfig.FAILED
	 */
	private String dataError(ServiceException e, Map<String, Object> data) {
		data.put(DATA_MSG_NAME, e.getMessage() + DATA_MSG + FAILED);
		logger.error(data.get(DATA_MSG_NAME));
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 数据库操作结果异常处理 （包括回滚事务、打印logger、将错误信息写入data）
	 * 
	 * @param data
	 * @return ServiceResultConfig.FAILED
	 */
	private String recordError(Map<String, Object> data) {
		baseDao.rollback();// 回滚事务
		logger.error(DATA_MSG + FAILED);
		data.put(DATA_MSG_NAME, DATA_MSG + FAILED + "服务器发生错误,请稍后再试!");
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 数据库操作DAO异常处理 （包括回滚事务、记录DAO异常日志、打印logger、将错误信息写入data）
	 * 
	 * @param e
	 * @param data
	 * @return ServiceResultConfig.FAILED
	 */
	private String daoError(DaoException e, Map<String, Object> data) {
		baseDao.rollback();// 回滚事务
		// 数据库记录系统日志
		Integer functionID = ServiceMapper.getServiceConfig(view).getId();
		Log log = new Log(Log.LogLevel.ERROR, Log.LogType.SYSTEM, functionID, e.getMessage());
		logService.addLog(BeanUtils.beanToMap(log));
		
		logger.error(e.getMessage(), e);
		data.put(DATA_MSG_NAME, DATA_MSG + FAILED + "服务器发生错误,我们正在努力修复中。。。");
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 获取传入参数中的检测标准集合
	 * 
	 * @param detectedID被检测物编号
	 * @param params传入的数据集合
	 * @return
	 */
	private List<DetectedStandard> getDetectedStandard(String detectedID, Map<String, Object[]> params) {
		List<DetectedStandard> dets = new ArrayList<DetectedStandard>();// 初始化一个检测标注集合

		// 循环取出数据封装检测标准类
		// 循环条件：按index取出的检测内容编号不为空
		for (int i = 0; i < params.get("contentID").length; i++) {

			DetectedStandard d = new DetectedStandard();
			d.setDetectedID(detectedID);// 设置被检测物编号
			d.setContentID((Integer) getParamsValue(params, "contentID", i));// 设置检测内容编号
			d.setStandards((String) getParamsValue(params, "standards", i));// 设置执行标准
			d.setStandardLine((Double) getParamsValue(params, "standardLine", i));// 设置标准线
			d.setValue((Double) getParamsValue(params, "value", i));// 设置波动值
			d.setUnit((String) getParamsValue(params, "unit", i));// 设置标准线单位

			dets.add(d);// 添加该检测标准到检测标准集合
		}
		return dets;
	}

	/**
	 * 验证被检测物编号（如：RC-001-001）
	 * 
	 * @param detectedID被检测物编号
	 * @throws ServiceException
	 */
	private void isDetectedID(String detectedID) throws ServiceException {
		String s = "^[A-Z]{2}(-\\d{3}){2}$";
		boolean r = detectedID.matches(s);
		if (!r) {
			String msg = "被检测物编号不合法！";
			throw new ServiceException(msg);
		}
	}

	/**
	 * 验证该条记录是否存在是否存在
	 * 
	 * @param view操作视图
	 * @param idName编号名称
	 * @param objectID记录编号
	 * @param result打印异常的查询结果(0为false,1为true)
	 * @param msg_name提示信息名称
	 * @throws ServiceException
	 */
	private void isExist(String view, String idName, String objectID, boolean result, String msg_name)
			throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(idName, objectID);
		int r = queryService.queryList(view, params).size();
		if (result) {
			if (r >= 1) {
				String msg = "已存在编号为“" + objectID + "”的" + msg_name + "记录！";
				throw new ServiceException(msg);
			}
		} else {
			if (r == 0) {
				String msg = "不存在编号为“" + objectID + "”的" + msg_name + "记录！";
				throw new ServiceException(msg);
			}
		}
	}

	/**
	 * 信息校验判断传入参数是否为空
	 * 
	 * @param obj要判断的信息
	 * @param msg_name提示信息名称
	 * @throws ServiceException
	 */
	private void isEmpty(Object obj, String msg_name) throws ServiceException {
		if (obj == null || obj.equals("")) {// 传入数据为String型或Integer型，判断是否为空
			String msg = msg_name + "不能为空！";
			throw new ServiceException(msg);
		}
	}

	/**
	 * 信息校验判断传入参数长度是否超出范围（包括非空校验）
	 * 
	 * @param s要判断的信息
	 * @param length参数长度约束
	 * @param msg_name提示信息名称
	 * @throws ServiceException
	 */
	private void isOutofRange(String s, int length, String msg_name) throws ServiceException {
		if (s != null && !s.equals("")) {// 当信息非空时进行长度是否超出范围校验
			try {
				if (s.getBytes("utf-8").length > length) {
					String msg = msg_name + "长度超出范围！";
					throw new ServiceException(msg);
				}
			} catch (UnsupportedEncodingException e) {
				logger.debug(e.getMessage());
				throw new ServiceException(e);
			}
		}
	}
}
