package bluedot.spectrum.service;

import java.util.HashMap;
import java.util.Map;

import bluedot.spectrum.bean.Hardware;
import bluedot.spectrum.bean.Log;
import bluedot.spectrum.bean.Log.LogLevel;
import bluedot.spectrum.bean.Log.LogType;
import bluedot.spectrum.exception.DaoException;
import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.utils.BeanUtils;
import bluedot.spectrum.web.core.ServiceMapper;
import bluedot.spectrum.web.core.ServiceResultConfig;

/**
 * 硬件相关业务
 * @author 刘驭洲 2016年8月21日
 */
public class HardwareService extends ServiceSupport {

	private final String ADD_MSG = "addHardware_msg";
	private final String MODIFY_MSG = "modifyHardware_msg";
	private final String DEL_MSG = "deleteHardware_msg";

	private Hardware hardware = null;

	public HardwareService() {
	}

	public HardwareService(String view, Map<String, Object[]> params, Map<String, Object> data)
			throws ServiceException {
		super(view, params, data);
		execute(view, params, data);
	}

	/**
	 * 添加硬件信息(测试完毕)
	 * @param params
	 * @param data
	 * @return 成功或失败或错误
	 */
	String addHardware(Map<String, Object[]> params, Map<String, Object> data) {
		// 实例化硬件实体类，用于数据的校验
		hardware = BeanUtils.mapToBean(params, Hardware.class);
		// 设置添加时间
		hardware.setAddTime(System.currentTimeMillis());
		// 校验数据
		if (checkInfo(view, hardware)) {
			try {
				// 调用DAO进行添加
				baseDao.transaction();
				if (baseDao.add("addHardware", BeanUtils.beanToMap(hardware)) > 0) {
					// 关联表插入
					for (int i = 0; i < hardware.getSpectrumTypeIDs().length; i++) {
						baseDao.add("addHardwareSpectrumType", fakeBeanToMap("spectrumTypeID", hardware, i, false));
					}
					for (int i = 0; i < hardware.getSpectrumFileTypeIDs().length; i++) {
						baseDao.add("addHardwareSpectrumFileType",
								fakeBeanToMap("spectrumFileTypeID", hardware, i, false));
					}
					// 事务提交
					return commitAndReturn(data, ADD_MSG, "添加成功", ServiceResultConfig.SUCCESS);
				}
				// 事务提交
				return commitAndReturn(data, ADD_MSG, "添加失败", ServiceResultConfig.FAILED);
			} catch (DaoException e) {
				// 事务回滚
				baseDao.rollback();
				// 代码日志
				logger.error(e.getMessage(), e);
				// 存入向前台发送的信息
				systemLog(data, ADD_MSG, view, e);
				return ServiceResultConfig.FAILED;
			}
		} else {
			data.put(ADD_MSG, "必填数据错误，添加失败");
		}
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 修改硬件信息(测试完成)
	 * @param params
	 * @param data
	 * @return
	 */
	String modifyHardware(Map<String, Object[]> params, Map<String, Object> data) {
		// 实例化硬件实体类，用于数据的校验
		hardware = BeanUtils.mapToBean(params, Hardware.class);
		// 校验数据
		if (checkInfo(view, hardware)) {
			try {
				// 调用DAO层进行修改
				// 开启事务
				baseDao.transaction();
				if (baseDao.update("updateHardware", BeanUtils.beanToMap(hardware)) > 0) {
					// 先删除关联表中的数据在更新成刚刚修改的
					baseDao.delete("deleteHardwareSpectrumType", fakeBeanToMap("", hardware, -1, true));
					baseDao.delete("deleteHardwareSpectrumFileType", fakeBeanToMap("", hardware, -1, true));
					for (int i = 0; i < hardware.getSpectrumTypeIDs().length; i++) {
						baseDao.add("addHardwareSpectrumType", fakeBeanToMap("spectrumTypeID", hardware, i, true));
					}
					for (int i = 0; i < hardware.getSpectrumFileTypeIDs().length; i++) {
						baseDao.add("addHardwareSpectrumFileType",
								fakeBeanToMap("spectrumFileTypeID", hardware, i, true));
					}
					// 提交事务、data中填入提示信息、结束业务
					return commitAndReturn(data, MODIFY_MSG, "修改成功", ServiceResultConfig.SUCCESS);
				} else {
					// 提交事务、向data中存入前台显示信息、结束业务
					return commitAndReturn(data, MODIFY_MSG, "修改失败，该硬件可能已被其他管理员删除", ServiceResultConfig.FAILED);
				}
			} catch (DaoException e) {
				// 事务回滚
				baseDao.rollback();
				// 代码日志
				logger.error(e.getMessage(), e);
				// 在data中存入返回前台的提示信息
				systemLog(data, MODIFY_MSG, view, e);
			}
		} else {
			data.put(MODIFY_MSG, "填写数据错误，操作失败");

		}
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 删除硬件信息（测试完成）
	 * @param params
	 * @param data
	 * @return
	 */
	String deleteHardware(Map<String, Object[]> params, Map<String, Object> data) {
		// 实例化硬件实体类，用于数据的校验
		hardware = BeanUtils.mapToBean(params, Hardware.class);
		// 校验数据
		if (checkInfo(view, hardware)) {
			try {
				// 开启事务
				baseDao.transaction();
				// 调用Dao层进行删除(级联删除)
				if (baseDao.delete("deleteHardware", BeanUtils.beanToMap(hardware)) > 0) {
					// 提交事务，向data中存入信息，结束业务
					return commitAndReturn(data, DEL_MSG, "删除成功", ServiceResultConfig.SUCCESS);
				} else {
					// 提交事务，data中存入信息、结束业务
					return commitAndReturn(data, DEL_MSG, "删除失败，该硬件可能已被删除", ServiceResultConfig.FAILED);
				}
			} catch (DaoException e) {
				// 代码日志
				logger.error(e.getMessage(), e);
				// 添加系统日志
				systemLog(data, DEL_MSG, view, e);
			}
		} else {
			data.put(DEL_MSG, "必填数据为空，删除失败");
		}
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 校验信息
	 * @param view 方法名
	 * @param hardware 需要校验的数据
	 * @return 数据符合要求为true
	 */
	private boolean checkInfo(String view, Hardware hardware) {
		if (isEmpty(view, hardware)) {
			// 封装成bean的时候，若是数据不是纯数字不会放入bean中（数组也是。若其中有一个为字符串，其余全部都不会放入bean中），所以此处不需要再去检验。只要检验为字符串的是否超过长度就可以
			if ((view.equals("addHardware") || view.equals("modifyHardware"))
					&& hardware.getHardwareName().length() < 64)
				return true;
			if (view.equals("deleteHardware")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断是否为空（被数据校验数据方法（checkInfo(String,Hardware)）调用）
	 * @param view 方法名
	 * @param hardware 硬件实体类
	 * @return 返回符合该业务方法的数据是否为空
	 */
	private boolean isEmpty(String view, Hardware hardware) {
		if (view.equals("addHardware") || view.equals("modifyHared")) {
			if (hardware.getHardwareName() == null || hardware.getHardwareName() == ""
					|| hardware.getSpectrumFileTypeIDs() == null || hardware.getSpectrumFileTypeIDs().length == 0
					|| hardware.getSpectrumTypeIDs() == null || hardware.getSpectrumTypeIDs().length == 0) {
				return false;
			}
		}
		if ((view.equals("modifyHardware") || view.equals("deleteHardware")) && hardware.getHardwareID() == null) {
			return false;
		}
		return true;
	}

	/**
	 * 插入系统日志
	 * @param data 记录传向前台的提示信息
	 * @param message map中的key
	 * @param view 方法名
	 * @param e 抛出的异常传入此方法中处理
	 */
	private void systemLog(Map<String, Object> data, String message, String view, Exception e) {
		// 在data中存入返回前台的提示信息
		data.put(message, "服务器错误，操作失败，请联系超管或稍后再试");
		// 添加系统日志
		Log log = new Log(LogLevel.ERROR, LogType.SYSTEM, ServiceMapper.getServiceConfig(view).getId(), e.getMessage());
		// 获取功能点ID，存入log对象
		logService.addLog(BeanUtils.beanToMap(log));
	}

	/**
	 * 伪装的beanToMap（只适用于此类）
	 * @param key 关联表中的另一个ID（hardwareID之外）没有填写“”
	 * @param hardware 硬件实体类
	 * @param i 下标值  没有填写-1
	 * @param modify 是modify方法调用为true，其余为false
	 * @return 转成传入DAO中的Map
	 */
	private Map<String, Object> fakeBeanToMap(String key, Hardware hardware, int i, boolean modify) {
		Map<String, Object> data = new HashMap<String, Object>();
		if (i == -1 || modify) {
			data.put("hardwareID", hardware.getHardwareID());
		} else {
			data.put("hardwareID", baseDao.getPremarykeyValue());
		}
		if (key.equals("spectrumFileTypeID"))
			data.put(key, hardware.getSpectrumFileTypeIDs()[i]);
		if (key.equals("spectrumTypeID"))
			data.put(key, hardware.getSpectrumTypeIDs()[i]);
		logger.debug(data.toString());
		return data;
	}

	/**
	 * 提交事务、并且填入提示信息，返回结束业务时需要返回的字符床
	 * @param data 存提示信息
	 * @param message map中的key
	 * @param value map中value
	 * @param result 结束业务时返回的结果
	 * @return 传入的result
	 */
	private String commitAndReturn(Map<String, Object> data, String message, String value, String result) {
		baseDao.commit();
		data.put(message, value);
		return result;
	}
}
