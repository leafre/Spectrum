package bluedot.spectrum.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bluedot.spectrum.bean.AccountState;
import bluedot.spectrum.bean.ApplicationState;
import bluedot.spectrum.bean.Favorite;
import bluedot.spectrum.bean.Gender;
import bluedot.spectrum.bean.Role;
import bluedot.spectrum.bean.Spectrum;
import bluedot.spectrum.bean.User;
import bluedot.spectrum.exception.DaoException;
import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.utils.BeanUtils;
import bluedot.spectrum.utils.DBUtils;
import bluedot.spectrum.utils.FileUtils;
import bluedot.spectrum.utils.StringUtils;
import bluedot.spectrum.utils.VerifyCode;
import bluedot.spectrum.utils.mail.Mail;
import bluedot.spectrum.utils.mail.SendEmailUtils;
import bluedot.spectrum.web.core.FSConfig;
import bluedot.spectrum.web.core.ServiceResultConfig;

/**
 * 用户业务逻辑处理类(用户,功能点,角色)
 * 
 * @author longshu
 * @author 高朝 
 * @author 付大石
 */
public class UserService extends ServiceSupport {

	private final String ERROR_MSG = "数据异常";
	private final String APPLY_CANCEL_ADMIN = "applyCancelAdmin_msg";
	private final String FORZEN_MSG = "forzen_msg";
	private final String CREATE_FAVORITE_MSG = "createFavorite_msg";
	private final String MODIFY_FAVORITE_MSG = "modifyFavoriteName_msg";
	private final String DELETE_FAVORITE_MSG = "deleteFavorite_msg";
	private final String FIND_PASSWORD_MSG = "findPassword_msg";
	private final String MODIFY_PASSWORD_MSG = "modifyPassword_msg";
	private final String MODIFY_INFO_MSG = "modifyInfo_msg";
	private final String EXAMINE_THAW_APPLICATION_MSG = "examineThawApplication_msg";
	private final String EXAMINE_ROLE_APPLICATION_MSG = "examineRoleApplication_msg";
	private final String APPLY_ROLE_MSG = "applyRole_msg";
	private final String APPLY_THAW_MSG = "applyThaw_msg";
	private final String REGISTER_MSG = "register_msg";
	private final String LOGIN_MSG = "login_msg";
	private final String EMAIL_REGEX = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

	public UserService() {
	}

	public UserService(String view, Map<String, Object[]> params, Map<String, Object> data) throws ServiceException {
		super(view, params, data);
		execute(view, params, data);// 执行业务
	}

	/**
	 * 验证邮箱/用户名、密码是否对应
	 * @author gaozhao
	 * @param parms 存储参数的集合
	 * @param data  登录后产生的数据
	 * @return 和service.xml的result节点的name属性对应
	 */
	String login(Map<String, String[]> params, Map<String, Object> data) {

		Map<String, Object> param = new HashMap<String, Object>();
		int errorCount = 0;
		// 1.获取数据
		String account = getParamStringValue(params, "account", 0);
		String password = getParamStringValue(params, "password", 0);
		String strLoginErrorCount = getParamStringValue(params, "errorCount", 0);
		// 处理登入错误次数
		if (StringUtils.isBlank(strLoginErrorCount) || "NaN".equals(strLoginErrorCount)) {
			errorCount = 0;
		} else {
			errorCount = Integer.parseInt(strLoginErrorCount);
		}
		data.put("errorCount", errorCount);
		// 如果登入错误大于1次，继续请求登入时将出现验证码校验
		if (errorCount > 0) {
			String userVerifyCode = getParamStringValue(params, "verifyCode", 0);
			String sessionVerifyCode = getParamStringValue(params, VerifyCode.KEY_CODE, 0);
			if (StringUtils.isBlank(userVerifyCode)) {
				data.put(LOGIN_MSG, "验证码不能为空");

				return ServiceResultConfig.FAILED;
			} else if (StringUtils.isBlank(sessionVerifyCode)) {
				data.put(LOGIN_MSG, "服务端验证码异常");
				return ServiceResultConfig.FAILED;
			} else if (!sessionVerifyCode.equalsIgnoreCase(userVerifyCode)) {
				data.put(LOGIN_MSG, "验证码错误");
				return ServiceResultConfig.FAILED;
			}
		}

		errorCount++;
		data.put("errorCount", errorCount);

		// 2.校验数据
		if (StringUtils.isBlank(account)) {
			data.put(LOGIN_MSG, ERROR_MSG);
			return ServiceResultConfig.FAILED;
		} else if (account.matches(EMAIL_REGEX)) {// 邮箱
			param.put("email", account);
		} else if (account.length() < 12) {// 用户名
			param.put("username", account);
		} else {
			data.put(LOGIN_MSG, ERROR_MSG);
			return ServiceResultConfig.FAILED;
		}
		if (StringUtils.isBlank(password) || password.length() > 32) {
			data.put(LOGIN_MSG, ERROR_MSG);
			return ServiceResultConfig.FAILED;
		} else {
			param.put("password", password);
		}
		// 数据库查询
		List<Map<String, Object>> userList = queryService.queryList("queryUser", param);
		if (null == userList || userList.isEmpty()) {
			data.put(LOGIN_MSG, "你密码输入错误的次数为[" + errorCount + "]如果次数超过5次你的账号将被冻结");
			// 错误次数超过五次则冻结用户
			if (errorCount > 5) {
				param.put("state", "冻结");
				param.put("thawTime", 1000 * 60 * 20);
				data.put(LOGIN_MSG, "密码输入错误次数超过5次，你的账号将被冻结20分钟");
				try {
					baseDao.update("updateUser", param);
				} catch (DaoException e) {
					logger.error(e.getMessage(), e);
				}
			}
			return ServiceResultConfig.FAILED;
		} else {
			Map<String, Object> userMap = userList.get(0);
			User user = BeanUtils.mapToBean(userMap, User.class);
			user.setRole(BeanUtils.mapToBean(userMap, Role.class));

			// 用户冻结时间到期，需对用户进行解冻操作
			if (userMap.get("thawTime") != null && (Long) userMap.get("thawTime") <= System.currentTimeMillis()) {
				try {
					Map<String, Object> paramThaw = new HashMap<String, Object>();
					paramThaw.put("state", AccountState.NORMAL);
					paramThaw.put("userID", userMap.get("userID"));
					baseDao.update("updateUser", paramThaw);
					// 如果用户有申请解冻操作则拒绝
					List<Map<String, Object>> r_list = queryService.queryList("queryThawApplication", paramThaw);
					if (r_list.size() > 0) {
						paramThaw.clear();
						paramThaw.put("userID", userMap.get("userID"));
						paramThaw.put("state", ApplicationState.REFUSE);
						baseDao.update("updateRoleApplication", paramThaw);
					}
				} catch (DaoException e) {
					logger.error(e.getMessage(), e);
					data.put(LOGIN_MSG, ERROR_MSG);
					return ServiceResultConfig.FAILED;
				}
			}
			if (userMap.get("state").equals(AccountState.FROZEN)) {
				data.put(LOGIN_MSG, "你的账号已被冻结");
				return "login";// 转到冻结页面
			}
			// 保存到session的数据
			data.put("session_user", user);
			return ServiceResultConfig.LOGIN;
		}
	}

	/**
	 * 注册成为普通用户/实验人员，把user中的数据插入到数据库中
	 * 
	 * @author gaozhao
	 * @param parms
	 * @param data
	 * @return
	 */
	String register(Map<String, String[]> params, Map<String, Object> data) {
		int userID = 0;
		Map<String, Object> param = new HashMap<String, Object>();
		Map<String, Object> applyMap = new HashMap<String, Object>();
		String roleID = getParamStringValue(params, "roleID", 0);
		// 新注册的用户初始权限都为user 申请实验人员需等待申核通过后权限才会被修改
		param.put("roleID", Role.USER);
		String email = getParamStringValue(params, "email", 0);
		String tel = getParamStringValue(params, "tel", 0);
		// 验证码校验
		String userVerifyCode = getParamStringValue(params, "verifyCode", 0);
		String sessionVerifyCode = getParamStringValue(params, VerifyCode.KEY_CODE, 0);

		if (StringUtils.isBlank(userVerifyCode)) {
			data.put(this.REGISTER_MSG, "验证码不能为空");
			return ServiceResultConfig.FAILED;
		} else if (!sessionVerifyCode.equalsIgnoreCase(userVerifyCode)) {
			data.put(this.REGISTER_MSG, "验证码不正确");
			return ServiceResultConfig.FAILED;
		}

		String password = getParamStringValue(params, "password", 0);
		String username = getParamStringValue(params, "username", 0);
		if (tel == null || tel.trim().isEmpty()) {
			data.put("register_msg","手机号不能为空");
			return ServiceResultConfig.FAILED;
		} else if (!tel.matches("^0?\\d{11}$")) {
			data.put("register_msg","手机号不正确");
			return ServiceResultConfig.FAILED;
		} else {
			param.put("tel", tel);
		}

		if (email == null || email.trim().isEmpty()) {
			data.put("register_msg","邮箱不为空");
			return ServiceResultConfig.FAILED;
		} else if (!email
				.matches("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$")) {
			data.put("register_msg","邮箱格式不正确");
			return ServiceResultConfig.FAILED;
		} else {
			param.put("email", email);
		}
		if (username == null || username.trim().isEmpty()) {
			data.put("register_msg","用户名不为空");
			return ServiceResultConfig.FAILED;
		} else if (username.length() > 12) {
			data.put("register_msg", "用户名长度太长");
			return ServiceResultConfig.FAILED;
		} else {
			param.put("username", username);
		}
		if (password == null || password.trim().isEmpty()) {
			data.put("register_msg", "密码不为空");
			return ServiceResultConfig.FAILED;
		} else if (password.length() > 32) {
			data.put("register_msg", "密码长度太长");
			return ServiceResultConfig.FAILED;
		} else {
			param.put("password", password);
		}
		// 如果是实验人员填写下面表单
		if (roleID.equals(Role.LAB + "")) {
			String address = getParamStringValue(params, "address", 0);
			String realName = getParamStringValue(params, "realName", 0);
			String gender = getParamStringValue(params, "gender", 0);
			param.put("gender", gender);
			String IDNumber = getParamStringValue(params, "IDNumber", 0).toUpperCase();// 身份证最后一位为x时，一律转为大写X
			String institute = getParamStringValue(params, "institute", 0);
			String position = getParamStringValue(params, "position", 0);
			String detail = getParamStringValue(params, "detail", 0);
			String birthday=getParamStringValue(params, "birthday", 0);
			DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
			//可以为空
			if(StringUtils.isBlank(birthday)){
			
			}else{
				try {
					Date date=df.parse(birthday);
					param.put("birthday", date.getTime());
					
				} catch (ParseException e) {
					data.put(this.REGISTER_MSG,"出生日期格式错误");
					return ServiceResultConfig.FAILED;
				}
				
			}
			//可以为空
			if (address == null || address.trim().isEmpty()) {
			} else {
				param.put("address", address);
			}
			if (realName == null || realName.trim().isEmpty()) {
				data.put("register_msg", "真实姓名不能为空");
				return ServiceResultConfig.FAILED;
			} else if (realName.length() > 16) {
				data.put("register_msg", "真实姓名长度太长");
				return ServiceResultConfig.FAILED;
			} else {
				param.put("realName", realName);
			}
			if (IDNumber == null || tel.trim().isEmpty()) {
				data.put("register_msg","身份证号不能为空");
				return ServiceResultConfig.FAILED;
			} else if (!IDNumber
					.matches("^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([0-9]|X)$")) {
				data.put("register_msg", "身份证号格式不正确");
				return ServiceResultConfig.FAILED;
			} else {
				param.put("IDNumber", IDNumber);
			}

			if (institute == null || institute.trim().isEmpty()) {
				data.put("register_msg", "工作单位不能为空");
				return ServiceResultConfig.FAILED;
			} else if (institute.length() > 64) {
				data.put("register_msg", "工作单位的输入长度过长");
				return ServiceResultConfig.FAILED;
			} else {
				param.put("institute", institute);
			}
			if (position == null || position.trim().isEmpty()) {
				data.put("register_msg", "职位不能为空");
				return ServiceResultConfig.FAILED;
			} else {
				param.put("position", position);
			}
			//可以为空
			if (detail == null || detail.trim().isEmpty()) {
				
			} else if (detail.length() > 256) {
				data.put("register_msg", "描述信息的内容过大");
				return ServiceResultConfig.FAILED;
			} else {
				param.put("detail", detail);
			}

			applyMap.put("state", ApplicationState.WAIT);
			applyMap.put("roleID", Role.LAB);
			String file = getParamStringValue(params, "applyFile", 0);
			applyMap.put("applyFile", file);
		}

		// 检查用户名是否被注册
		Map<String, Object> temp = new HashMap<String, Object>();
		temp.put("username", username);
		List<Map<String, Object>> list = queryService.queryList("queryUser", temp);
		if (list.size() > 0) {
			data.put(this.REGISTER_MSG, "该用户名已被注册");
			return ServiceResultConfig.FAILED;
		}
		// 检查邮箱是否被注册
		Map<String, Object> temp1 = new HashMap<String, Object>();
		temp1.put("email", email);
		List<Map<String, Object>> list1 = queryService.queryList("queryUser", temp1);
		if (list1.size() > 0) {
			data.put(this.REGISTER_MSG, "该邮箱已被注册");
			return ServiceResultConfig.FAILED;
		}

		try {
			logger.debug("baseDao:" + baseDao);

			if (roleID.equals("" + Role.LAB)) {
				DBUtils.beginTransaction();
				if (baseDao.add("addUser", param) < 0) {
					DBUtils.rollbackTransaction();
					data.put(this.REGISTER_MSG, "注册失败1");
					return ServiceResultConfig.FAILED;
				} else {
					userID = baseDao.getPremarykeyValue();

					applyMap.put("userID", userID);
				}
				// 实验人员相关数据写入实验人员申请
				applyMap.put("applyTime", System.currentTimeMillis());
				if (baseDao.add("addRoleApplication", applyMap) < 0) {
					DBUtils.rollbackTransaction();
					data.put(this.REGISTER_MSG, "注册失败2");
					return ServiceResultConfig.FAILED;
				}

				DBUtils.commitTransaction();

			} else {
				baseDao.add("addUser", param);
				userID = baseDao.getPremarykeyValue();

			}
			data.put("user", BeanUtils.mapToBean(param, User.class));
		} catch (SQLException e) {
			data.put(this.REGISTER_MSG, "注册失败3");
			return ServiceResultConfig.FAILED;
		} catch (DaoException e) {
			data.put(this.REGISTER_MSG, "注册失败4");
			return ServiceResultConfig.FAILED;
		}
		// 为用户创建个人空间
		User user = new User();
		user.setEmail(email);
		user.setUserID(userID);
		boolean bool = _createFavorite(user, "我的收藏");
		if (!bool) {
			data.put(this.REGISTER_MSG, "个人空间创建失败");
			return ServiceResultConfig.FAILED;
		}		
	
		data.put(this.REGISTER_MSG, "success");
		 //  发送邮件
		String subject = SendEmailUtils.getSubject("register_subject", email);
		String content = SendEmailUtils.getContent("register_content", email, "你的管理员申请已提交，请耐心等待审核");
		Mail mail = SendEmailUtils.getMail(email, subject, content);
		SendEmailUtils.sendMail(mail);

		return ServiceResultConfig.SUCCESS;
	   
		
	}

	/**
	 * 下载用户角色申请文件
	 * @author gaozhao
	 */
	String downloadFile(Map<String, String[]> params, Map<String, Object> data) {
		String type = getParamStringValue(params, "type", 0);
		String fileName = getParamStringValue(params, "fileName", 0);
		System.out.println("sssssss+=" + fileName);
		if (StringUtils.isAnyBlank(type, fileName)) {
			data.put("downloadFile_msg", "文件不存在或服务器错误");
			return ServiceResultConfig.FAILED;
		} else {
			// test
			// data.put("downFilePath",new
			// File(FSConfig.application(),"application.txt").toString());
			// data.put("downFileName", "application.txt");
			if (type.equals("template")) {
				data.put("downFilePath", new File(FSConfig.template(), fileName).getAbsolutePath());
			} else if (type.equals("application")) {
				data.put("downFilePath", new File(FSConfig.application(), fileName).getAbsolutePath());
			}

			data.put("downFileName", fileName);
			return ServiceResultConfig.SUCCESS;
		}
	}

	/**
	 * 角色申请
	 * 
	 * @author 付大石
	 * @notice 用户可以在申请角色页面完善个人信息（普通管理员申请取消普通管理员权限除外），所以要调用modifyInfo（User）方法修改个人信息
	 * @param data
	 * @return
	 */
	String applyRole(Map<String, String[]> params, Map<String, Object> data) {
		// 获取参数并验证
		String applyRoleIDString = super.getParamStringValue(params, "applyRoleID", 0);
		String applyFile = super.getParamStringValue(params, "applyFile", 0);
		String userID = super.getParamStringValue(params, "session_userID", 0);
		String roleIDString = super.getParamStringValue(params, "session_roleID", 0);
		String email = super.getParamStringValue(params, "session_email", 0);
		if (StringUtils.isBlank(applyRoleIDString)) {
			data.put(APPLY_ROLE_MSG, "未选择申请的角色");
			return ServiceResultConfig.FAILED;
		}
		if (StringUtils.isBlank(applyFile)) {
			data.put(APPLY_ROLE_MSG, "未上传申请文件或文件格式不合规范");
			return ServiceResultConfig.FAILED;
		}
		if (StringUtils.isBlank(userID)) {
			data.put(APPLY_ROLE_MSG, "userID不能为空");
			return ServiceResultConfig.FAILED;
		}
		if (StringUtils.isBlank(roleIDString)) {
			data.put(APPLY_ROLE_MSG, "roleID不能为空");
			return ServiceResultConfig.FAILED;
		}
		applyFile = new File(applyFile).getName();
		User user = new User();
		user = BeanUtils.mapToBean(params, User.class);
		user.setBirthday(super.getParamStringValue(params, "birthday", 0)); // 不知为何，birthday，mapToBean放不进去....
		user.setUserID(Integer.parseInt(userID));
		user.setRoleID(Integer.parseInt(applyRoleIDString));
		user.setEmail(email);
		int roleID = Integer.parseInt(roleIDString);
		int applyRoleID = Integer.parseInt(applyRoleIDString);
		// 判断用户申请的角色是否存在出入
		if (roleID == Role.USER) { // 用户角色为普通用户
			if (applyRoleID != Role.LAB && applyRoleID != Role.ADMIN) {
				data.put(APPLY_ROLE_MSG, "您申请的角色不合规定");
				return ServiceResultConfig.FAILED;
			}
		}
		if (roleID == Role.LAB) { // 用户角色为实验人员
			if (applyRoleID != Role.ADMIN) {
				data.put(APPLY_ROLE_MSG, "您申请的角色不合规定");
				return ServiceResultConfig.FAILED;
			}
		}
		if (!fileIsAccept(applyFile, data)) { // 验证申请文件是否符合规范
			return ServiceResultConfig.FAILED;
		}
		// 查询是否已经提交过申请
		Map<String, Object> selectRoleApplicationMap = new HashMap<String, Object>();
		selectRoleApplicationMap.put("app_state", ApplicationState.WAIT);
		selectRoleApplicationMap.put("userID", userID);
		selectRoleApplicationMap.put("app_roleID", applyRoleID);
		List<Map<String, Object>> list = queryService.queryList("queryUserRoleApplication", selectRoleApplicationMap);
		if (list.size() > 0) { // 用户已提交过申请
			data.put(APPLY_ROLE_MSG, "系统拒绝申请，请耐心等待您上次的申请结果 :)");
			return ServiceResultConfig.FAILED;
		}
		super.baseDao.transaction();// 开启事务
		// 更新用户信息
		if (!modifyUserInfo2(user)) {
			data.put(APPLY_ROLE_MSG, "申请失败，请稍后重试 :)");
			super.baseDao.rollback();
			return ServiceResultConfig.FAILED;
		}

		// 插入数据
		Map<String, Object> insertRoleApplication = new HashMap<String, Object>();
		insertRoleApplication.put("userID", userID);
		insertRoleApplication.put("applyTime", new Date().getTime());
		insertRoleApplication.put("roleID", applyRoleID);
		insertRoleApplication.put("applyFile", applyFile);
		try {
			if (baseDao.add("addRoleApplication", insertRoleApplication) <= 0) {
				super.baseDao.rollback();
				data.put(APPLY_ROLE_MSG, "申请失败，请稍后重试 :)");
				return ServiceResultConfig.FAILED;
			}
		} catch (DaoException e) {
			super.baseDao.rollback();
			data.put(APPLY_ROLE_MSG, "申请失败，请稍后重试 :)");
			logger.error(e.getMessage(), e);
			return ServiceResultConfig.FAILED;
		}

		// 移动文件到FSConfig配置的相应位置中去
		try {
			File file = new File(FSConfig.temp(), applyFile);
			logger.debug("上传文件地址:" + file.getAbsolutePath());
			// logger.debug("复制目的地址:" + new File(FSConfig.application(), applyFile).getAbsolutePath());
			FileUtils.copyFile(file.getAbsolutePath(), new File(FSConfig.application(), applyFile).getAbsolutePath());
		} catch (IOException e) {
			super.baseDao.rollback();
			data.put(APPLY_ROLE_MSG, "申请失败，请稍后重试 :)");
			logger.error(e.getMessage(), e);
			return ServiceResultConfig.FAILED;
		}
		super.baseDao.commit();// 提交事务
		// 为了更新session中的信息，将user中的数据放进data中,再在servlet层中更新session
		user.setRoleID(Integer.parseInt(roleIDString));
		data.put("session_user", user);
		data.put(APPLY_ROLE_MSG, "申请成功，审核结果会通过邮箱发送，请耐心等待结果 :)");
		return ServiceResultConfig.UPDATE_USER_SESSION;
	}

	private boolean fileIsAccept(String path, Map<String, Object> data) {

		// $验证是否是word文档
		String postfix = path.substring(path.lastIndexOf(".") + 1);
		if (postfix == null) {

			throw new RuntimeException(path + "	文件路径不合规范，无法取出后缀名");
		}
		if (!postfix.equals("doc") && !postfix.equals("docx") && !postfix.equals("wps")) {// 后缀名是doc、docx、wps中的一个

			data.put(APPLY_ROLE_MSG, "文件必须是word或WPS文档");
			return false;
		}

		// $验证文件大小是否超过2M
		File file = new File(FSConfig.temp(), path);
		if (file.length() > 2 * 1024 * 1024) { // 如果文件大小超过2M
			data.put(APPLY_ROLE_MSG, "文件大小不得超过2M");
			return false;
		}
		return true;
	}

	/**
	 * 申请取消管理员权限 
	 * 
	 * @author 付大石
	 * @param params
	 * @param data
	 * @return
	 */
	String applyCancelAdmin(Map<String, String[]> params, Map<String, Object> data) {
		// 获取参数并提交
		String userID = super.getParamStringValue(params, "session_userID", 0);

		if (StringUtils.isBlank(userID)) {
			data.put(APPLY_CANCEL_ADMIN, "数据异常");
			return ServiceResultConfig.FAILED;
		}

		// 查询验证是否提交过
		Map<String, Object> queryRoleApplicationMap = new HashMap<String, Object>();
		queryRoleApplicationMap.put("userID", userID);
		queryRoleApplicationMap.put("state", ApplicationState.WAIT);
		queryRoleApplicationMap.put("roleID", 4);
		List<Map<String, Object>> list = queryService.queryList("queryUserRoleApplication", queryRoleApplicationMap);
		if (list.size() >= 1) { // 说明应提交过申请，且未处理
			data.put(APPLY_CANCEL_ADMIN, "系统拒绝申请，请耐心等待您上次的申请结果 :)");
			return ServiceResultConfig.FAILED;
		}

		// 插入申请
		Map<String, Object> insertRoleApplicationMap = new HashMap<String, Object>();
		insertRoleApplicationMap.put("userID", userID);
		insertRoleApplicationMap.put("applyTime", new Date().getTime());
		insertRoleApplicationMap.put("roleID", 1);
		try {
			if (baseDao.add("addRoleApplication", insertRoleApplicationMap) <= 0) {
				data.put(APPLY_CANCEL_ADMIN, "插入失败，请稍后重试 :)");
				return ServiceResultConfig.FAILED;
			}
		} catch (DaoException ex) {
			logger.error(ex.getMessage(), ex);
			data.put(APPLY_CANCEL_ADMIN, "插入失败，请稍后重试 :)");
			return ServiceResultConfig.FAILED;
		}

		data.put(APPLY_CANCEL_ADMIN, "申请成功，审核结果将以邮件形式通知您 :)");
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 解冻申请
	 * @author 付大石
	 * @param data
	 * @return
	 */
	String applyThaw(Map<String, String[]> params, Map<String, Object> data) {

		// 取出数据并验证
		String reason = super.getParamStringValue(params, "reason", 0);
		String userID = super.getParamStringValue(params, "session_userID", 0);
		if (StringUtils.isAnyBlank(reason, userID)) {
			data.put(APPLY_THAW_MSG, "数据异常");
			return ServiceResultConfig.FAILED;
		}

		// 查询该用户对于这次申请是否已经提交了申请，并且该申请正在等待处理(只有申请被拒接后才可再次提交)
		Map<String, Object> queryThawAppplicationMap = new HashMap<String, Object>();
		queryThawAppplicationMap.put("userID", userID);
		queryThawAppplicationMap.put("state", ApplicationState.WAIT);
		List<Map<String, Object>> thawApplications = queryService.queryList("queryThawApplication",
				queryThawAppplicationMap);
		if (thawApplications.size() >= 1) {
			data.put(APPLY_THAW_MSG, "系统拒绝申请，请耐心等待您上次的申请结果 :)");
			return ServiceResultConfig.FAILED;
		}

		// 插入解冻申请
		Map<String, Object> insertThawApplicationMap = new HashMap<String, Object>();
		insertThawApplicationMap.put("userID", userID);
		insertThawApplicationMap.put("applyTime", new Date().getTime());
		insertThawApplicationMap.put("reason", reason);

		try {
			if (baseDao.add("addThawApplication", insertThawApplicationMap) <= 0) {
				data.put(APPLY_THAW_MSG, "申请失败，请稍后重试 :)");
				return ServiceResultConfig.FAILED;
			}
		} catch (DaoException ex) {
			logger.error(ex.getMessage(), ex);
			data.put(APPLY_THAW_MSG, "申请失败，请稍后重试 :)");
			return ServiceResultConfig.FAILED;
		}
		// 往data中存入成功信息
		data.put(APPLY_THAW_MSG, "申请成功，审核结果会已邮件方式通知您 :)");
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 外部调用的冻结方法，普通管理员输入用户名冻结账号
	 * 
	 * @author 付大石
	 * @notice 解冻时间经过前台JS处理，传过来是个毫秒值
	 * @notice 理由需要
	 * @param params
	 * @param data
	 * @return
	 */
	String frozen(Map<String, String[]> params, Map<String, Object> data) {

		// 获取数据并验证
		String reason = super.getParamStringValue(params, "reason", 0);
		String userID = super.getParamStringValue(params, "userID", 0);
		String thawTime = super.getParamStringValue(params, "thawTime", 0);
		String email = super.getParamStringValue(params, "email", 0);

		if (StringUtils.isAnyBlank(reason, userID, thawTime, email)) {
			data.put(FORZEN_MSG, "数据异常");
			return ServiceResultConfig.FAILED;
		}
		if (reason.length() == 0 || reason.length() > 256) {
			data.put(FORZEN_MSG, "冻结理由需小于256字节 :)");
			return ServiceResultConfig.FAILED;
		}

		User user = new User();
		user.setUserID(Integer.parseInt(userID));
		user.setEmail(email);
		user.setThawTime(Long.parseLong(thawTime));

		// 调用frozen,更新数据库
		if (!_frozen(user, reason)) { // 如果冻结失败
			data.put(FORZEN_MSG, "冻结失败，请重试");
			return ServiceResultConfig.FAILED;
		}
		data.put(FORZEN_MSG, "冻结成功");
		data.put("userID", userID);
		// 退出函数
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 审核角色申请
	 * 
	 * @author 付大石
	 * @notice 申请形式:普通用户--->实验人员,普通用户--->普通管理员,实验人员--->普通管理员,普通管理员--->普通用户
	 * @param params
	 * @param data
	 * @return
	 */
	String examineRoleApplication(Map<String, String[]> params, Map<String, Object> data) {
		// 取出数据并验证
		String state = super.getParamStringValue(params, "state", 0);// 接受或者拒绝
		String email = super.getParamStringValue(params, "email", 0);
		String applicationID = super.getParamStringValue(params, "applicationID", 0);
		String reason = super.getParamStringValue(params, "reason", 0);// 操作理由
		String applyRoleID = super.getParamStringValue(params, "applyRoleID", 0);// 申请角色编号
		String userID = super.getParamStringValue(params, "userID", 0);
		String adminName = super.getParamStringValue(params, "session_username", 0);
		String username = super.getParamStringValue(params, "username", 0);

		if (StringUtils.isBlank(state)) {

			logger.debug("state不能为空");
			data.put(EXAMINE_ROLE_APPLICATION_MSG, "请选择拒绝还是接受");
			return ServiceResultConfig.FAILED;
		}

		if (StringUtils.isBlank(applicationID)) {

			logger.debug("applicationID不能为空");
			data.put(EXAMINE_ROLE_APPLICATION_MSG, "数据异常，请稍后再试  :)");
			return ServiceResultConfig.FAILED;
		}

		if (StringUtils.isBlank(userID)) {

			logger.debug("userID不能为空");
			data.put(EXAMINE_ROLE_APPLICATION_MSG, "数据异常，请稍后再试  :)");
			return ServiceResultConfig.FAILED;
		}

		if (StringUtils.isBlank(applyRoleID)) {

			logger.debug("applyRoleID不能为空");
			data.put(EXAMINE_ROLE_APPLICATION_MSG, "数据异常，请稍后再试  :)");
			return ServiceResultConfig.FAILED;
		}

		if (StringUtils.isBlank(adminName)) {

			logger.debug("adminName不能为空");
			data.put(EXAMINE_ROLE_APPLICATION_MSG, "数据异常，请稍后再试  :)");
			return ServiceResultConfig.FAILED;
		}

		if (StringUtils.isBlank(username)) {

			logger.debug("username不能为空");
			data.put(EXAMINE_ROLE_APPLICATION_MSG, "数据异常，请稍后再试  :)");
			return ServiceResultConfig.FAILED;
		}

		if (StringUtils.isBlank(email)) {

			logger.debug("email不能为空");
			data.put(EXAMINE_ROLE_APPLICATION_MSG, "数据异常，请稍后再试  :)");
			return ServiceResultConfig.FAILED;
		}

		if ((ApplicationState.REFUSE + ApplicationState.PASS).indexOf(state) <= -1) {

			logger.debug("state值能是" + ApplicationState.REFUSE + "或" + ApplicationState.PASS + "中的一种");
			data.put(EXAMINE_ROLE_APPLICATION_MSG, "数据异常，请稍后再试  :)");
			return ServiceResultConfig.FAILED;
		}

		if (ApplicationState.REFUSE.equals(state) && StringUtils.isBlank(reason)) {
			data.put(EXAMINE_ROLE_APPLICATION_MSG, "如果您拒绝用户的这份解冻申请，请输入理由（256字节以内） :)");
			return ServiceResultConfig.FAILED;
		}

		if (!StringUtils.isBlank(reason)) {

			if (reason.length() > 256) {

				data.put(EXAMINE_THAW_APPLICATION_MSG, "理由需在256字节以内  :)");
				return ServiceResultConfig.FAILED;
			}
		}

		// 查询数据库，查看是否已被处理
		Map<String, Object> selectRoleApplicationMap = new HashMap<String, Object>();
		selectRoleApplicationMap.put("applicationID", applicationID);
		selectRoleApplicationMap.put("state", ApplicationState.WAIT);
		List<Map<String, Object>> thawApplications = queryService.queryList("queryRoleApplication",
				selectRoleApplicationMap);
		if (thawApplications.size() <= 0) {
			data.put(EXAMINE_THAW_APPLICATION_MSG, "该请求已处理");
			return ServiceResultConfig.FAILED;
		}

		// 更新角色申请表
		Map<String, Object> updateRoleApplicationMap = new HashMap<String, Object>();
		updateRoleApplicationMap.put("applicationID", applicationID);
		updateRoleApplicationMap.put("state", state);
		baseDao.transaction();// 开启事务
		try {
			if (baseDao.update("updateRoleApplication", updateRoleApplicationMap) <= 0) {
				baseDao.rollback();// 回滚事务
				data.put(EXAMINE_ROLE_APPLICATION_MSG, "更新失败，请稍后重试");
				return ServiceResultConfig.FAILED;
			}
		} catch (DaoException e) {
			baseDao.rollback();// 回滚事务
			logger.error(e.getMessage(), e);
			data.put(EXAMINE_ROLE_APPLICATION_MSG, "更新失败，请稍后重试");
			return ServiceResultConfig.FAILED;
		}
		if (ApplicationState.PASS.equals(state)) { // 如果接受，则更新用户角色
			Map<String, Object> updateUserRoleMap = new HashMap<String, Object>();
			updateUserRoleMap.put("roleID", applyRoleID);
			updateUserRoleMap.put("userID", userID);
			try {
				if (baseDao.update("updateUser", updateUserRoleMap) <= 0) {
					baseDao.rollback();// 回滚事务
					data.put(EXAMINE_ROLE_APPLICATION_MSG, "更新失败，请稍后重试");
					return ServiceResultConfig.FAILED;
				}
			} catch (DaoException e) {
				baseDao.rollback();// 回滚事务
				logger.error(e.getMessage(), e);
				data.put(EXAMINE_ROLE_APPLICATION_MSG, "更新失败，请稍后重试");
				return ServiceResultConfig.FAILED;
			}
			// 发送接收邮件
			String subject = SendEmailUtils.getSubject("roleApply_subject");
			String content = SendEmailUtils.getContent("roleApply_accpet_content", username, adminName, getNowDate());
			Mail mail = SendEmailUtils.getMail(email, subject, content);
			SendEmailUtils.sendMail(mail);
		} else { // 如果是拒绝操作
			String subject = SendEmailUtils.getSubject("roleApply_subject");
			String content = SendEmailUtils.getContent("roleApply_refuse_content", username, reason, adminName,
					getNowDate());
			Mail mail = SendEmailUtils.getMail(email, subject, content);
			SendEmailUtils.sendMail(mail);
		}

		baseDao.commit();// 提交事务
		data.put(EXAMINE_ROLE_APPLICATION_MSG, "操作成功");
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 审核解冻申请
	 * 
	 * @author 付大石
	 * @notice 接受也可能会有理由，操作结果需用邮箱通知对方
	 * @notice 更新数据库前先查询下该application是否未被处理，防止对一个请求多次操作
	 * @param params
	 * @param data
	 * @return
	 */
	String examineThawApplication(Map<String, String[]> params, Map<String, Object> data) {

		// 取出数据并验证
		String state = super.getParamStringValue(params, "state", 0);// 接受或者拒绝
		String applicationID = super.getParamStringValue(params, "applicationID", 0);
		String reason = super.getParamStringValue(params, "reason", 0);// 操作理由
		String userID = super.getParamStringValue(params, "userID", 0);
		String adminName = super.getParamStringValue(params, "session_username", 0);
		String username = super.getParamStringValue(params, "usernmae", 0);
		String email = super.getParamStringValue(params, "email", 0);
		if (StringUtils.isBlank(state) || StringUtils.isBlank(applicationID) || StringUtils.isBlank(userID)
				|| StringUtils.isBlank(adminName) || StringUtils.isBlank(username)) {

			data.put(EXAMINE_THAW_APPLICATION_MSG, "数据异常");
			return ServiceResultConfig.FAILED;
		}
		if ((ApplicationState.REFUSE + ApplicationState.PASS).indexOf(state) <= -1) {

			data.put(EXAMINE_THAW_APPLICATION_MSG, "数据异常");
			return ServiceResultConfig.FAILED;
		}
		if (ApplicationState.REFUSE.equals(state) && StringUtils.isBlank(reason)) {

			data.put(EXAMINE_THAW_APPLICATION_MSG, "如果您拒绝用户的这份解冻申请，请输入理由（256字节以内） :)");
			return ServiceResultConfig.FAILED;
		}
		User user = BeanUtils.mapToBean(params, User.class);
		if (!StringUtils.isBlank(reason)) {

			if (reason.length() > 256) {

				data.put(EXAMINE_THAW_APPLICATION_MSG, "理由需在256字节以内  :)");
				return ServiceResultConfig.FAILED;
			}
		}

		// 查询数据库，查看是否已被处理
		Map<String, Object> selectThawApplicationMap = new HashMap<String, Object>();
		selectThawApplicationMap.put("applicationID", applicationID);
		selectThawApplicationMap.put("t_thaw_application.state", ApplicationState.WAIT);
		selectThawApplicationMap.put("t_user.state", AccountState.FROZEN);
		List<Map<String, Object>> thawApplications = queryService.queryList("queryThawApplication",
				selectThawApplicationMap);
		if (thawApplications.size() <= 0) {

			data.put(EXAMINE_THAW_APPLICATION_MSG, "该请求已处理");
			return ServiceResultConfig.FAILED;
		}

		// 更新数据库,开启事务控制
		Map<String, Object> updateThawApplicationMap = new HashMap<String, Object>();
		updateThawApplicationMap.put("applicationID", applicationID);
		updateThawApplicationMap.put("state", state);
		baseDao.transaction();
		try {

			if (baseDao.update("updateThawApplication", updateThawApplicationMap) <= 0) {

				baseDao.rollback();// 回滚事务
				data.put(EXAMINE_THAW_APPLICATION_MSG, "操作失败，请稍后重试");
				return ServiceResultConfig.FAILED;
			}
		} catch (DaoException ex) {

			baseDao.rollback();// 回滚事务
			logger.error(ex.getMessage(), ex);
			data.put(EXAMINE_THAW_APPLICATION_MSG, "操作失败，请稍后重试");
			return ServiceResultConfig.FAILED;
		}
		if (ApplicationState.PASS.equals(state)) { // 如果是接受操作,调用thawUser（）方法解冻账号

			if (!thawUser(user)) {

				baseDao.rollback();// 解冻失败，回滚事务
				data.put(EXAMINE_THAW_APPLICATION_MSG, "操作失败，请稍后重试");
				return ServiceResultConfig.FAILED;
			}
			String subject = SendEmailUtils.getSubject("thawApply_subject");
			String content = SendEmailUtils.getContent("thawApply_accept_content", username, adminName, getNowDate());
			Mail mail = SendEmailUtils.getMail("email", subject, content);
			SendEmailUtils.sendMail(mail);

		} else { // 如果是拒绝操作

			String subject = SendEmailUtils.getSubject("thawApply_subject");
			String content = SendEmailUtils.getContent("thawApply_refuse_content", username, reason, adminName,
					getNowDate());
			Mail mail = SendEmailUtils.getMail(email, subject, content);
			SendEmailUtils.sendMail(mail);

		}

		baseDao.commit(); // 提交事务
		data.put(EXAMINE_THAW_APPLICATION_MSG, "操作成功");
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 修改用户基本信息：照片、性别、手机号、职位、单位、单位地址、个人描述、性别
	 * 
	 * @author 付大石
	 * @param data
	 * @return
	 */
	String modifyUserInfo(Map<String, String[]> params, Map<String, Object> data) {

		// 取出数据并验证
		User user = BeanUtils.mapToBean(params, User.class);
		user.setUserID(Integer.parseInt(super.getParamStringValue(params, "session_userID", 0)));
		user.setRoleID(Integer.parseInt(super.getParamStringValue(params, "session_roleID", 0)));
		user.setEmail(super.getParamStringValue(params, "session_email", 0));
		user.setBirthday(super.getParamStringValue(params, "birthday", 0)); // 不知为何，birthday，mapToBean放不进去....
		if (!this.modifyUserInfo2(user)) {

			data.put(MODIFY_INFO_MSG, "更新失败，请稍后重试");
			return ServiceResultConfig.FAILED;
		}
		// 为了更新session中的信息，将user中的数据放进data中,再在servlet层中更新session
		data.put("session_user", user);
		return ServiceResultConfig.UPDATE_USER_SESSION;
	}

	/**
	 * 内部调用方法，修改用户基本信息：照片、性别、手机号、职位、单位、单位地址、个人描述、性别
	 * 
	 * @author 付大石
	 * @param data
	 * @return
	 */
	private boolean modifyUserInfo2(User user) {

		boolean infoOk = user.getRoleID() == Role.USER ? checkPlainUserInfo(user) : checkUserInfo(user);
		if (!infoOk) { // 如果信息不和规范
			return false;
		}

		// 更新数据库
		Map<String, Object> updateUserInfoMap = new HashMap<String, Object>();
		updateUserInfoMap.put("userID", user.getUserID());
		updateUserInfoMap.put("photo", user.getPhoto());
		updateUserInfoMap.put("birthday", user.getBirthday());
		updateUserInfoMap.put("gender", user.getGender());
		updateUserInfoMap.put("tel", user.getTel());
		updateUserInfoMap.put("position", user.getPosition());
		updateUserInfoMap.put("insititute", user.getInstitute());
		updateUserInfoMap.put("detail", user.getDetail());
		updateUserInfoMap.put("address", user.getAddress());
		updateUserInfoMap.put("realName", user.getRealName());
		if (user.getIDNumber() != null) {
			updateUserInfoMap.put("IDNumber", user.getIDNumber().toUpperCase());// 有的身份证后面是字母x,这个处理使其统一大写
		}
		updateUserInfoMap.put("realName", user.getRealName());
		try {
			if (baseDao.update("updateUser", updateUserInfoMap) <= 0) { // 如果更新失败
				return false;
			}
		} catch (DaoException ex) {
			logger.error(ex.getMessage(), ex);
			return false;
		}
		return true;
	}

	/**
	 * 测试普通用户的个人信息，普通用户可以不能填写工作单位、职位身份证号等信息
	 * @return
	 */
	private boolean checkPlainUserInfo(User user) {

		if (user.getRoleID() != Role.USER)
			throw new IllegalArgumentException("User对象必须是普通用户");
		// 判断各个数据是否合法
		if (user.getIDNumber() != null && !"".equals(user.getIDNumber().trim())) {
			if (user.getIDNumber().length() != 18) {// 如果身份证长度不是18位

				logger.debug("身份证号长度应为18位");
				return false;
			}
		}
		if (user.getRealName() != null && !"".equals(user.getRealName().trim())) {
			if (user.getRealName().length() > 10 && user.getRealName().length() < 4) {// 如果真实姓名长度不在4-10之间

				logger.debug("真实姓名长度应在4-10之间");
				return false;
			}
		}
		if (user.getAddress() != null && !"".equals(user.getAddress().trim())) {
			if (user.getAddress().length() > 64) {// 如果地址的长度小于64个字节

				logger.debug("地址长度应小于等于64");
				return false;
			}
		}
		if (user.getDetail() != null && !"".equals(user.getDetail().trim())) {
			if (user.getDetail().length() > 256) {// 如果个人描述大于256

				logger.debug("个人描述应小于等于256");
				return false;
			}
		}
		if (user.getGender() != null && !"".equals(user.getGender().trim())) {
			if ((Gender.MAN + Gender.WOMAN + Gender.SECRECY).indexOf(user.getGender()) < 0) {// 如果个人性别不是男/女/保密中的一种

				logger.debug("性别数据不是男、女、保密中的一种");
				return false;
			}
		}
		if (user.getInstitute() != null && !"".equals(user.getInstitute().trim())) {
			if (user.getInstitute().length() > 64) {// 如果单位小于等于64

				logger.debug("单位名称应小于等于64");
				return false;
			}
		}
		if (user.getPhoto() != null && !"".equals(user.getPhoto().trim())) {
			if (user.getPhoto().length() > 256) {// 如果照片存放地址小于256

				logger.debug("照片路径长度应小于256");
				return false;
			}
		}
		if (user.getPosition() != null && !"".equals(user.getPosition().trim())) {
			if (user.getPosition().length() > 16) {// 如果职位小于16

				logger.debug("职位名称应小于等于16");
				return false;
			}
		}
		if (user.getTel() != null && !"".equals(user.getTel().trim())) {
			if (user.getTel().length() != 11) {// 电话号码是11位
				logger.debug("电话号码长度应是11位");
				return false;
			}
		}
		return true;
	}

	/**
	 * 测试非普通用户的个人信息，非普通用户的个人信息必须完整
	 * @return
	 */
	private boolean checkUserInfo(User user) {

		if (user.getRoleID() == Role.USER)
			throw new IllegalArgumentException("User对象不能是普通用户");
		// 判断各个数据是否合法
		if (user.getIDNumber() != null && !"".equals(user.getIDNumber().trim())) {
			if (user.getIDNumber().length() != 18) {// 如果身份证长度不是18位

				logger.debug("身份证号长度应为18位");
				return false;
			}
		} else {

			logger.debug("身份证号不能为空");
			return false;
		}
		if (user.getRealName() != null && !"".equals(user.getRealName().trim())) {
			if (user.getRealName().length() > 10 && user.getRealName().length() < 4) {// 如果真实姓名长度不在4-10之间

				logger.debug("真实姓名长度应在4-10之间");
				return false;
			}
		} else {

			logger.debug("真实姓名不能为空");
			return false;
		}
		if (user.getAddress() != null && !"".equals(user.getAddress().trim())) {
			if (user.getAddress().length() > 64) {// 如果地址的长度小于64个字节

				logger.debug("地址长度应小于等于64");
				return false;
			}
		} else {

			logger.debug("地址不能为空");
			return false;
		}
		if (user.getDetail() != null && !"".equals(user.getDetail().trim())) {
			if (user.getDetail().length() > 256) {// 如果个人描述大于256

				logger.debug("个人描述应小于等于256");
				return false;
			}
		} else {

			logger.debug("个人描述不能为空");
			return false;
		}
		if (user.getGender() != null && !"".equals(user.getGender().trim())) {
			if ((Gender.MAN + Gender.WOMAN + Gender.SECRECY).indexOf(user.getGender()) < 0) {// 如果个人性别不是男/女/保密中的一种

				logger.debug("性别数据不是男、女、保密中的一种");
				return false;
			}
		} else {

			logger.debug("性别不能为空");
			return false;
		}
		if (user.getInstitute() != null && !"".equals(user.getInstitute().trim())) {
			if (user.getInstitute().length() > 64) {// 如果单位小于等于64

				logger.debug("单位名称应小于等于64");
				return false;
			}
		} else {

			logger.debug("工作单位不能为空");
			return false;
		}
		// if(user.getPhoto()!=null && !"".equals(user.getPhoto().trim())) {
		// if (user.getPhoto().length() > 256) {//如果照片存放地址小于256
		//
		// logger.debug("照片路径长度应小于256");
		// return false;
		// }
		// } else {
		//
		// logger.debug("照片不能为空");
		// return false;
		// }
		if (user.getPosition() != null && !"".equals(user.getPosition().trim())) {
			if (user.getPosition().length() > 16) {// 如果职位小于16

				logger.debug("职位名称应小于等于16");
				return false;
			}
		} else {

			logger.debug("工作职位不能为空");
			return false;
		}
		if (user.getTel() != null && !"".equals(user.getTel().trim())) {
			if (user.getTel().length() != 11) {// 电话号码是11位
				logger.debug("电话号码长度应是11位");
				return false;
			}
		} else {

			logger.debug("手机号不能为空");
			return false;
		}
		return true;
	}

	/**
	 * 修改密码，需先验证码是否正确，若正确才能修改密码
	 * 
	 * @author 付大石
	 * @param parms
	 * @param data
	 * @return
	 */
	String modifyPassword(Map<String, String[]> params, Map<String, Object> data) {
		// 取出数据并验证
		String userID = super.getParamStringValue(params, "session_userID", 0);
		String email = super.getParamStringValue(params, "session_email", 0);
		String username = super.getParamStringValue(params, "session_username", 0);
		String oldPassword = super.getParamStringValue(params, "oldPassword", 0);
		String newPassword = super.getParamStringValue(params, "newPassword", 0);
		String confirmPassword = super.getParamStringValue(params, "confirmPassword", 0);
		String countString = super.getParamStringValue(params, "errorCount", 0);
		String verifyCode = super.getParamStringValue(params, "varifyCode", 0);
		String sessionVerifyCode = super.getParamStringValue(params, VerifyCode.KEY_CODE, 0);
		if (StringUtils.isBlank(userID)) {
			data.put(this.MODIFY_PASSWORD_MSG, "缺少 userID");
			return ServiceResultConfig.FAILED;
		}
		if (StringUtils.isBlank(oldPassword)) {
			data.put(this.MODIFY_PASSWORD_MSG, "缺少 oldPassword");
			return ServiceResultConfig.FAILED;
		}
		if (StringUtils.isBlank(newPassword)) {
			data.put(this.MODIFY_PASSWORD_MSG, "缺少 newPassword");
			return ServiceResultConfig.FAILED;
		}
		if (StringUtils.isBlank(confirmPassword)) {
			data.put(this.MODIFY_PASSWORD_MSG, "缺少 confirmPassword");
			return ServiceResultConfig.FAILED;
		}
		if (StringUtils.isBlank(countString)) {
			data.put(this.MODIFY_PASSWORD_MSG, "缺少 countString");
			return ServiceResultConfig.FAILED;
		}
		if (StringUtils.isBlank(verifyCode)) {
			data.put(this.MODIFY_PASSWORD_MSG, "缺少 verifyCode");
			return ServiceResultConfig.FAILED;
		}
		if (StringUtils.isBlank(sessionVerifyCode)) {
			data.put(this.MODIFY_PASSWORD_MSG, "缺少 sessionVerifyCode");
			return ServiceResultConfig.FAILED;
		}
		if (StringUtils.isBlank(email)) {
			data.put(this.MODIFY_PASSWORD_MSG, "缺少 email");
			return ServiceResultConfig.FAILED;
		}

		User user = new User();
		user.setUserID(Integer.parseInt(userID));
		user.setEmail(email);
		user.setUsername(username);
		int count = Integer.parseInt(countString);
		if (newPassword.length() < 8 || newPassword.length() > 16) { // 密码不合规范
			data.put(this.MODIFY_PASSWORD_MSG, "密码长度应大于8小于16");
			return ServiceResultConfig.FAILED;
		}
		if (newPassword.equals(oldPassword)) { // 新密码与旧密码不能相同
			data.put(this.MODIFY_PASSWORD_MSG, "新密码与旧密码不能相同");
			return ServiceResultConfig.FAILED;
		}
		if (!newPassword.equals(confirmPassword)) { // 新密码、确认密码输入不同
			data.put(this.MODIFY_PASSWORD_MSG, "两次新密码输入不同");
			return ServiceResultConfig.FAILED;
		}
		if (!verifyCode.equalsIgnoreCase(sessionVerifyCode)) { // 验证验证码是否正确
			data.put(MODIFY_PASSWORD_MSG, "验证码错误");
			return ServiceResultConfig.FAILED;
		}

		// 查询旧密码是否正确
		Map<String, Object> queryUserPasswordMap = new HashMap<String, Object>();
		queryUserPasswordMap.put("userID", userID);
		queryUserPasswordMap.put("password", oldPassword);
		List<Map<String, Object>> users = queryService.queryList("queryUser", queryUserPasswordMap);
		if (users.size() <= 0) { // 说明密码错误
			count++;
			if (count >= 5) { // 密码连续输入错误5次，冻结账号20min
				Calendar thawTime = new GregorianCalendar();
				thawTime.add(Calendar.MINUTE, 20);
				user.setThawTime(thawTime.getTimeInMillis());
				this._frozen(user, "密码已连续输入错误超过5次，账号冻结20min");
				data.put(this.MODIFY_PASSWORD_MSG, "密码已连续输入错误超过5次，账号冻结20min");
				data.put("errorCount", data);
				return ServiceResultConfig.EXIT;
			} else {
				data.put("errorCount", count);
				data.put(MODIFY_PASSWORD_MSG, "密码输入错误" + count + "次，如果超过5次，账号便会冻结");
				return ServiceResultConfig.FAILED;
			}
		}

		// 修改密码
		Map<String, Object> updateUserPasswordMap = new HashMap<String, Object>();
		updateUserPasswordMap.put("userID", userID);
		updateUserPasswordMap.put("password", newPassword);
		try {
			if (baseDao.update("updateUser", updateUserPasswordMap) <= 0) {
				data.put(MODIFY_PASSWORD_MSG, "修改失败，请重试");
				return ServiceResultConfig.FAILED;
			}
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			data.put(MODIFY_PASSWORD_MSG, "修改失败，请重试");
			return ServiceResultConfig.FAILED;
		}
		data.put(MODIFY_PASSWORD_MSG, "密码修改成功，请重新登陆");

		// 发送邮件告知用户，您的账号已被冻结
		String subject = SendEmailUtils.getSubject("modifyPassword_subject");
		String content = SendEmailUtils.getContent("modifyPassword_content", user.getUsername(), getNowDatetime());
		Mail mail = SendEmailUtils.getMail(user.getEmail(), subject, content);
		SendEmailUtils.sendMail(mail);
		return ServiceResultConfig.EXIT;// 修改密码后，要求用户重写登录
	}

	/**
	 * 发送找密码的邮件
	 * @param params
	 * @param data
	 * @return
	 */
	String findPasswordEmail(Map<String, String[]> params, Map<String, Object> data) {
		String email = getParamStringValue(params, "email", 0);
		if (StringUtils.isBlank(email)) {
			data.put("findPasswordEmail_msg", "邮箱不能为空");
			return ServiceResultConfig.TYPE_EMAIL;
		}
		// 随机邮箱验证码
		String session_verifycode = new VerifyCode().getRandomCode(0);
		data.put(VerifyCode.KEY_CODE, session_verifycode);// 保存到session
		data.put(ServiceResultConfig.TYPE_EMAIL, email);
		// 邮件模板
		String subject = SendEmailUtils.getSubject("findPasswordEmail_subject");
		String content = SendEmailUtils.getContent("findPasswordEmail_content", session_verifycode);// 发送验证码到邮箱
		Mail mail = SendEmailUtils.getMail(email, subject, content);
		SendEmailUtils.sendMail(mail);
		return ServiceResultConfig.TYPE_EMAIL;
	}

	/**
	 * 亦称忘记密码，通过邮箱找回密码，输入新密码重置原密码
	 * 
	 * @author 付大石
	 * @param parms
	 * @param data
	 * @return 转发页面
	 */
	String findPassword(Map<String, String[]> params, Map<String, Object> data) {
		// 取出数据并验证
		String email = super.getParamStringValue(params, "email", 0);
		String password = super.getParamStringValue(params, "password", 0);
		String verifyCode = super.getParamStringValue(params, "verifyCode", 0);
		String sessionVerifyCode = super.getParamStringValue(params, VerifyCode.KEY_CODE, 0);
		if (StringUtils.isAnyBlank(email, password, verifyCode, sessionVerifyCode)) {
			data.put(FIND_PASSWORD_MSG, "数据异常");
			return ServiceResultConfig.FAILED;
		}
		if (password.length() < 8 || password.length() > 16) {
			data.put(FIND_PASSWORD_MSG, "密码长度应大于8小于16");
			return ServiceResultConfig.FAILED;
		}
		if (!verifyCode.equals(sessionVerifyCode)) {
			data.put(FIND_PASSWORD_MSG, "验证码错误");
			return ServiceResultConfig.FAILED;
		}
		if (!email.matches("^(\\w)+(\\.\\w+)*@(\\w)+\\.(\\w)+$")) {
			data.put(FIND_PASSWORD_MSG, "邮箱格式不合法");
			return ServiceResultConfig.FAILED;
		}

		Map<String, Object> updatePasswordMap = new HashMap<String, Object>();
		updatePasswordMap.put("password", password);
		updatePasswordMap.put("email", email);
		try {
			if (super.baseDao.update("updateUser", updatePasswordMap) <= 0) {
				data.put(FIND_PASSWORD_MSG, "更新失败");
				return ServiceResultConfig.FAILED;
			}
		} catch (DaoException e) {
			data.put(FIND_PASSWORD_MSG, "更新失败");
			logger.error(e.getMessage(), e);
			return ServiceResultConfig.FAILED;
		}
		data.put(FIND_PASSWORD_MSG, "密码重置成功");
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 新建收藏夹，前台参数，userID，favoriteName,email(用于生成收藏夹路径)
	 * favorite的path自动生成，形式：FSConfig.userFavorite()+email+favoriteName
	 * 
	 * @author 付大石
	 * @notice 修改前需查询，保存收藏夹名称在用户的个人空间范围内必须唯一
	 * @param params
	 * @param data
	 * @return 转发页面
	 */
	String createFavorite(Map<String, String[]> params, Map<String, Object> data) {
		// 获取前台参数并验证
		String userID = getParamStringValue(params, "session_userID", 0);
		String email = getParamStringValue(params, "session_email", 0);
		String favoriteName = getParamStringValue(params, "favoriteName", 0).trim();
		if (StringUtils.isBlank(userID)) {
			data.put(CREATE_FAVORITE_MSG, "数据异常");
			logger.debug("缺少userID");
			return ServiceResultConfig.FAILED;
		}
		if (StringUtils.isBlank(email)) {
			data.put(CREATE_FAVORITE_MSG, "数据异常");
			logger.debug("缺少email");
			return ServiceResultConfig.FAILED;

		}
		if (StringUtils.isBlank(favoriteName)) {
			data.put(CREATE_FAVORITE_MSG, "数据异常");
			logger.debug("缺少favoriteName");
			return ServiceResultConfig.FAILED;
		}

		// 查询个人空间，验证收藏夹名是否重复
		if (isRepeat(userID, favoriteName)) {
			data.put(CREATE_FAVORITE_MSG, "收藏夹名称重复");
			return ServiceResultConfig.FAILED;
		}

		// 物理新建文件夹
		String dirPath = FileUtils.makeDirName(FSConfig.userFavorite().getPath(), email, favoriteName);

		// 插入一个收藏夹记录
		Map<String, Object> insertFavoriteMap = new HashMap<String, Object>();
		insertFavoriteMap.put("favoriteName", favoriteName);
		insertFavoriteMap.put("userID", userID);
		insertFavoriteMap.put("path", email + File.separatorChar + favoriteName);
		int favoriteID = -1;
		try {
			super.baseDao.transaction();
			if (baseDao.add("addFavorite", insertFavoriteMap) <= 0) {
				FileUtils.deleteDirectory(dirPath);
				data.put(CREATE_FAVORITE_MSG, "新建失败");
				return ServiceResultConfig.FAILED;
			}
			favoriteID = super.baseDao.getPremarykeyValue();
			super.baseDao.commit();
		} catch (DaoException e) {
			super.baseDao.rollback();
			try {
				FileUtils.deleteDirectory(dirPath);
			} catch (IOException ex) {
				logger.error(e.getMessage(), ex);
				data.put(CREATE_FAVORITE_MSG, "服务器异常");
				return ServiceResultConfig.FAILED;
			}

			logger.error(e.getMessage(), e);
			data.put(CREATE_FAVORITE_MSG, "新建失败");
			return ServiceResultConfig.FAILED;
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			data.put(CREATE_FAVORITE_MSG, "服务器异常");
			return ServiceResultConfig.FAILED;
		}
		Favorite favorite = new Favorite();
		favorite.setFavoriteID(favoriteID);
		favorite.setFavoriteName(favoriteName);
		data.put("Favorite", favorite);// 用于前端JS处理
		data.put(CREATE_FAVORITE_MSG, "新建成功");// 提示信息
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 修改收藏夹名称
	 * 
	 * @author 付大石
	 * @notice 修改前需查询，因为名称在用户的个人空间范围内必须唯一
	 * @param params
	 * @param data
	 * @return 转发页面
	 */
	String modifyFavoriteName(Map<String, String[]> params, Map<String, Object> data) {

		// 获取参数并验证
		String userID = getParamStringValue(params, "session_userID", 0);
		String email = getParamStringValue(params, "session_email", 0);
		String favoriteID = getParamStringValue(params, "favoriteID", 0);
		String newFavoriteName = getParamStringValue(params, "newFavoriteName", 0);
		if (StringUtils.isBlank(userID) || StringUtils.isBlank(favoriteID) || StringUtils.isBlank(newFavoriteName)
				|| StringUtils.isBlank(email)) {

			data.put(MODIFY_FAVORITE_MSG, "数据异常");
			return ServiceResultConfig.FAILED;
		}
		if (newFavoriteName.length() > 16) {

			data.put(MODIFY_FAVORITE_MSG, "收藏夹名称长度需小于16个字节");
			return ServiceResultConfig.FAILED;
		}

		// 查询个人空间，验证收藏夹名是否重复
		if (isRepeat(userID, newFavoriteName)) {
			data.put(MODIFY_FAVORITE_MSG, "收藏夹名称重复");
			return ServiceResultConfig.FAILED;
		}

		// 查询出旧名称，用于修改存储在服务器上的收藏夹名称
		Map<String, Object> queryFavorite = new HashMap<String, Object>();
		queryFavorite.put("favoriteID", favoriteID);
		List<Map<String, Object>> list = queryService.queryList("queryFavorite", queryFavorite);
		if (list.size() <= 0) { // 如果查询失败，说明收藏夹不存在，数据库异常

			data.put(MODIFY_FAVORITE_MSG, "数据库异常，请重试");
			return ServiceResultConfig.FAILED;
		}
		Favorite favorite = BeanUtils.mapToBean(list.get(0), Favorite.class);
		if (favorite.getFavoriteName().equals(newFavoriteName)) {

			data.put(MODIFY_FAVORITE_MSG, "新、旧名称重复,请换个名称  :)");
			return ServiceResultConfig.FAILED;
		}

		// 更新收藏夹名称
		Map<String, Object> updateFavoriteNameMap = new HashMap<String, Object>();
		updateFavoriteNameMap.put("favoriteID", favoriteID);
		updateFavoriteNameMap.put("favoriteName", newFavoriteName);
		updateFavoriteNameMap.put("path", email + File.separatorChar + newFavoriteName);
		baseDao.transaction();// 开启事务
		try {

			if (baseDao.update("updateFavorite", updateFavoriteNameMap) <= 0) {

				super.baseDao.rollback();// 发生异常，回滚事务
				data.put("modifyFavoriteName_msg", "收藏夹重命名失败,请稍后再试");
				return ServiceResultConfig.FAILED;
			}
		} catch (DaoException e) {

			super.baseDao.rollback();
			super.baseDao.rollback();// 发生异常，回滚事务
			logger.error(e.getMessage(), e);
			data.put("modifyFavoriteName_msg", "收藏夹重命名失败,请稍后再试");
			return ServiceResultConfig.FAILED;
		}

		// 更改物理地址的收藏夹名称
		if (!renameDirectory(FSConfig.userFavorite() + File.separator + favorite.getPath(),
				FSConfig.userFavorite() + File.separator + email + File.separatorChar + newFavoriteName)) {

			super.baseDao.rollback();// 发生异常，回滚事务
			data.put(MODIFY_FAVORITE_MSG, "收藏夹重命名失败,请稍后重试");
			return ServiceResultConfig.FAILED;
		}
		super.baseDao.commit();// 提交事务
		data.put("modifyFavoriteName_msg", "收藏夹重命名成功");
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 重命名文件夹名称
	 * 
	 * @TODO 通知海松抽到FileUtils里去
	 * @param fromDir
	 * @param toDir
	 * @return
	 */
	private static boolean renameDirectory(String fromDir, String toDir) {

		File from = new File(fromDir);
		if (!from.exists() || !from.isDirectory()) {
			return false;
		}
		File to = new File(toDir);
		return from.renameTo(to);
	}

	/**
	 * 删除收藏夹，使用FileUtils删除用户收藏夹文件
	 * 
	 * @author 付大石
	 * @notice 删除收藏夹要先删除数据库中的记录（非标准库光谱、报告）
	 * @param params
	 * @param data
	 * @return 转发页面
	 */
	String deleteFavorite(Map<String, String[]> params, Map<String, Object> data) {
		// 获取参数并验证
		String favoriteID = super.getParamStringValue(params, "favoriteID", 0);
		if (StringUtils.isBlank(favoriteID)) {
			data.put(DELETE_FAVORITE_MSG, "数据异常");
			return ServiceResultConfig.FAILED;
		}

		// 查询出该收藏夹中的光谱
		Map<String, Object> querySpectrumMap = new HashMap<String, Object>();
		querySpectrumMap.put("favoriteID", favoriteID);
		List<Map<String, Object>> spectrumAndReport = queryService.queryList("queryFavoriteSpectrumDetail",
				querySpectrumMap);
		// 查询该收藏夹的路径，用于备份
		Map<String, Object> queryFavorite = new HashMap<String, Object>();
		queryFavorite.put("favoriteID", favoriteID);
		List<Map<String, Object>> favoriteList = queryService.queryList("queryFavorite", querySpectrumMap);
		if (favoriteList.size() <= 0) {
			data.put(DELETE_FAVORITE_MSG, "数据库异常，请稍后重试");
			return ServiceResultConfig.FAILED;
		}
		Favorite favorite = BeanUtils.mapToBean(favoriteList.get(0), Favorite.class);
		String path = favorite.getPath();
		// 删除前事先备份
		if (!backupFavorite(path)) { // 如果备份失败
			data.put(DELETE_FAVORITE_MSG, "服务器异常，请稍后重试  :)");
			return ServiceResultConfig.FAILED;
		}

		// 如果该收藏夹中含有光谱，则从数据库中删除这些光谱及其报告
		baseDao.transaction();// 开启事务
		// 删除非标准库光谱和报告
		try {
			if (!deleteSpectrumAndReport(spectrumAndReport)) { // 如果删除失败的话
				baseDao.rollback();
				data.put(DELETE_FAVORITE_MSG, "删除失败，请稍后重试");
				recoverFavorite(path);
				return ServiceResultConfig.FAILED;
			}
		} catch (DaoException ex) {
			recoverFavorite(path);
			baseDao.rollback();
			data.put(DELETE_FAVORITE_MSG, "删除失败，请稍后重试");
			recoverFavorite(path);
			logger.error(ex.getMessage(), ex);
			return ServiceResultConfig.FAILED;
		}

		// 从数据库中删除收藏夹记录
		Map<String, Object> deleteFavoriteMap = new HashMap<String, Object>();
		deleteFavoriteMap.put("favoriteID", favoriteID);
		try {
			if (baseDao.delete("deleteFavorite", deleteFavoriteMap) <= 0) {
				recoverFavorite(path);
				baseDao.rollback();
				data.put(DELETE_FAVORITE_MSG, "删除失败，请稍后重试");
				return ServiceResultConfig.FAILED;
			}
		} catch (DaoException e) {
			recoverFavorite(path);
			baseDao.rollback();
			data.put(DELETE_FAVORITE_MSG, "删除失败，请稍后重试");
			logger.error(e.getMessage(), e);
			return ServiceResultConfig.FAILED;
		}

		// 从服务器中删除收藏夹
		try {
			FileUtils.deleteDirectory(FSConfig.userFavorite() + File.separator + path);
		} catch (IOException e) {
			if (e instanceof FileNotFoundException) {
				logger.warn(e.getMessage());
				baseDao.commit();// 提交事务
				data.put(DELETE_FAVORITE_MSG, "删除成功");
				return ServiceResultConfig.SUCCESS;
			}
			recoverFavorite(path);
			baseDao.rollback();
			data.put(DELETE_FAVORITE_MSG, "删除失败，请稍后重试");
			logger.error(e.getMessage(), e);
			return ServiceResultConfig.FAILED;
		}
		baseDao.commit();// 提交事务
		data.put(DELETE_FAVORITE_MSG, "删除成功");
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 退出/注销账号
	 * 
	 * @author 付大石
	 * @notice CoreServlet会注销session
	 * @param params
	 * @param data
	 * @return 转发页面
	 */
	String logout(Map<String, String[]> params, Map<String, Object> data) {
		return ServiceResultConfig.EXIT;
	}

	/**
	 * 用户收藏夹
	 * @param params
	 * @param data
	 * @return
	 */
	String myFavorite(Map<String, String[]> params, Map<String, Object> data) {
		String userID = getParamStringValue(params, "session_userID", 0);
		if (StringUtils.isBlank(userID)) {
			return ServiceResultConfig.FAILED;
		}
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("userID", userID);
		List<Map<String, Object>> list = queryService.queryList("queryFavorite", param);
		if (null == list) {
			return ServiceResultConfig.FAILED;
		}
		data.put("Favorite", list);
		return ServiceResultConfig.SUCCESS;
	}

	/**
	 * 验证收藏夹名称是否重复
	 * 
	 * @param user
	 * @param favoriteName
	 * @return
	 */
	public boolean isRepeat(String userID, String favoriteName) {

		Map<String, Object> queryFavoriteNameMap = new HashMap<String, Object>();
		queryFavoriteNameMap.put("userID", userID);
		queryFavoriteNameMap.put("favoriteName", favoriteName);
		List<Map<String, Object>> nameList = queryService.queryList("queryFavorite", queryFavoriteNameMap);
		if (nameList.size() > 0) {

			return true;
		}
		return false;
	}

	/**
	 * 删除非标准库光谱和报告
	 * @author 付大石
	 * @param spectrumAndReport
	 * @return 删除是否成功
	 * @throws DaoException
	 */
	private boolean deleteSpectrumAndReport(List<Map<String, Object>> spectrumAndReport) throws DaoException {
		if (spectrumAndReport.size() <= 0)
			return true;
		Map<String, Object> deleteSpectrumAndReport = new HashMap<String, Object>();
		for (int i = (spectrumAndReport.size() - 1); i >= 0; i--) {

			Spectrum spectrum = BeanUtils.mapToBean(spectrumAndReport.get(i), Spectrum.class);
			spectrum.setStandard((Integer) spectrumAndReport.get(i).get("isStandard"));
			if (!spectrum.isStandard()) { // 删除非标准库光谱记录
				deleteSpectrumAndReport.put("spectrumID", spectrum.getSpectrumID());
				deleteSpectrumAndReport.put("isStandard", "0");
				if (baseDao.delete("deleteSpectrum", deleteSpectrumAndReport) <= 0) // 判断是否删除失败
					return false;
				if (spectrumAndReport.get(i).get("reportID") != null) { // 如果该光谱含有报告,则删除该报告记录
					if (baseDao.delete("deleteReport", deleteSpectrumAndReport) <= 0)
						return false;
				}
			}
		}
		return true;
	}

	/**
	 * 新建收藏夹，内部调用函数，每当用户注册账号成功后，调用该方法新建收藏夹
	 * 
	 * @author gaozhao
	 * @param user
	 * @param favoriteName
	 * @return
	 */
	private boolean _createFavorite(User user, String favoriteName) {
		// 判断收藏夹名是否重复
		Map<String, Object> queryFavoriteMap = new HashMap<String, Object>();
		queryFavoriteMap.put("favoriteName", favoriteName);
		queryFavoriteMap.put("userID", user.getUserID());
		List<Map<String, Object>> favorites = queryService.queryList("queryFavorite", queryFavoriteMap);// "queryFavoriteName"对应sqlConfig.xml的operation
		if (favorites.size() > 0) {
			return false;// 收藏夹名称已存在
		}
		// 物理地址创建收藏夹
		File file = new File(FSConfig.userFavorite().getPath() + File.separatorChar + user.getEmail(), favoriteName);
		boolean bool = file.mkdirs();
		if (!bool) {
			return false;
		}
		// 数据库记录插入
		Map<String, Object> insertFavoriteMap = new HashMap<String, Object>();
		insertFavoriteMap.put("favoriteName", favoriteName);
		insertFavoriteMap.put("userID", user.getUserID());
		insertFavoriteMap.put("path", FSConfig.userFavorite().getPath() + File.separatorChar + user.getEmail()
				+ File.separatorChar + favoriteName);
		try {
			if (baseDao.add("addFavorite", insertFavoriteMap) <= 0) {
				return false;// 插入失败
			}
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			return false;// 插入失败
		}

		return true;
	}

	private boolean recoverFavorite(String favoritePath) {
		try {
			FileUtils.copyDirectory(new File(FSConfig.temp(), favoritePath).getAbsolutePath(),
					new File(FileUtils.getTempDirectoryPath(), favoritePath).getAbsolutePath());
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	private boolean backupFavorite(String favoritePath) {
		try {
			FileUtils.copyDirectory(new File(FSConfig.userFavorite(), favoritePath).getAbsolutePath(),
					new File(FileUtils.getTempDirectoryPath(), favoritePath).getAbsolutePath());
		} catch (IOException e) {
			if (e instanceof FileNotFoundException) {
				logger.warn(e.getMessage());
				return true;
			}
			logger.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	/**
	 * 解冻用户账号，内部调用方法(普通管理员审核用户解冻申请时，可能会调用该函数)
	 * 
	 * @author 付大石
	 * @param  user
	 * @return 解冻是否成功
	 */
	private boolean thawUser(User user) {

		// 构建更新数据库所需的数据
		Map<String, Object> updateMap = new HashMap<String, Object>();
		updateMap.put("userID", user.getUserID());
		updateMap.put("state", AccountState.NORMAL);
		// 开始更新数据库
		try {
			if (baseDao.update("updateUser", updateMap) <= 0) {
				return false; // 更新影响行数小于0，更新失败，返回false
			}
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			return false; // 数据库异常，更新失败，返回false
		}
		return true;// 更新成功
	}

	/**
	 * 冻结账号,内部调用(用户登录、修改密码、普通管理员冻结账号等都可能会调用该方法)
	 * 参数传入之前已验证合法性,无需再次验证
	 * 
	 * @author 付大石
	 * @param user 要冻结的用户
	 * @param reason 冻结理由
	 * @param date 解冻日期
	 * @return 冻结是否成功
	 */
	private boolean _frozen(User user, String reason) {
		// 构建更新数据库所需的数据
		Map<String, Object> updateUserStateMap = new HashMap<String, Object>();
		updateUserStateMap.put("userID", user.getUserID());
		updateUserStateMap.put("thawTime", user.getThawTime());
		updateUserStateMap.put("state", AccountState.FROZEN);
		// 开始更新数据库
		try {
			if (baseDao.update("updateUser", updateUserStateMap) <= 0) {
				return false; // 更新影响行数小于0，更新失败，返回false
			}
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			return false; // 数据库异常，更新失败，返回false
		}

		// 发送邮件告知用户，您的账号已被冻结
		String subject = SendEmailUtils.getSubject("frozen_subject");
		String content = SendEmailUtils.getContent("frozen_content", user.getUsername(), reason,
				getNowDatetime(user.getThawTime()));
		Mail mail = SendEmailUtils.getMail(user.getEmail(), subject, content);
		SendEmailUtils.sendMail(mail);

		return true;// 更新成功
	}

	private String getNowDate() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(date);
	}

	private String getNowDatetime() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(date);
	}

	private String getNowDatetime(long time) {
		Date date = new Date();
		date.setTime(time);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(date);
	}

}