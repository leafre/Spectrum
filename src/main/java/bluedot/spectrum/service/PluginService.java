package bluedot.spectrum.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bluedot.spectrum.api.PluginType;
import bluedot.spectrum.bean.ApplicationState;
import bluedot.spectrum.bean.DetectedObject;
import bluedot.spectrum.bean.Hardware;
import bluedot.spectrum.bean.Plugin;
import bluedot.spectrum.exception.DaoException;
import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.utils.BeanUtils;
import bluedot.spectrum.utils.FileUtils;
import bluedot.spectrum.utils.StringUtils;
import bluedot.spectrum.web.core.FSConfig;
import bluedot.spectrum.web.core.ServiceResultConfig;

/**
 * 算法插件Service
 * @author 刘驭洲 2016年8月13日
 */
public class PluginService extends ServiceSupport {

	private static final String UPLOAD_MSG = "uploadPlugin_msg";
	private static final String DOWNLOAD_MSG = "downloadPlugin_msg";
	private static final String MODIFY_MSG = "modify_msg";
	private static final String DEL_MSG = "deletePlugin_msg";
	private static final String AUDIT_MSG = "auditPlugin_msg";

	private Plugin plugin = null;

	public PluginService() {
	}

	/**
	 * 
	 * @param view
	 * @param params
	 * @param data
	 * @throws ServiceException
	 */
	public PluginService(String view, Map<String, Object[]> params, Map<String, Object> data) throws ServiceException {
		super(view, params, data);
		execute(view, params, data);
	}

	/**
	 * 上传算法插件，插件需经过普通管理员审核,
	 * jar包和源码上传到服务器，记录插入到数据库(测试完成)
	 * @param params
	 * @param data
	 * @return
	 */
	String uploadPlugin(Map<String, String[]> params, Map<String, Object> data) {
		plugin = BeanUtils.mapToBean(params, Plugin.class);
		initPlugin(plugin, params);
		long time = System.currentTimeMillis();
		plugin.setAddTime(time);
		// 校验数据
		if (checkInfo(view, plugin)) {
			// 之前的临时文件路径
			String oldCodePath = plugin.getCodePath();
			String oldJarPath = plugin.getJarPath();
			logger.debug(oldCodePath + "\n" + oldJarPath);
			// 移动后的新路径
			File newCodePath = fileEndSrc(FSConfig.userPlugin(), plugin.getCodePath());
			File newJarPath = fileEndSrc(FSConfig.userPlugin(), plugin.getJarPath());
			logger.debug(newJarPath + "\n" + newCodePath);
			try {
				// 把临时文件移动到指定文件夹
				org.apache.commons.io.FileUtils.moveFile(new File(oldCodePath), newCodePath);
				org.apache.commons.io.FileUtils.moveFile(new File(oldJarPath), newJarPath);
				// FileUtils.copyFile(oldCodePath, newCodePath);
				// FileUtils.copyFile(oldJarPath, newJarPath);
				// 在plugin中更新路径
				plugin.setJarPath(newJarPath.getName());
				plugin.setCodePath(newCodePath.getName());
				logger.debug("已经成功：临时文件移动,plugin更新路径");
			} catch (FileNotFoundException e) {
				// 删除临时文件和已经复制的文件
				FileUtils.deleteFile(oldJarPath, oldCodePath, newJarPath.getPath(), newCodePath.getPath());
				logger.info("deleteFile");
				// 向data中输入提示信息（在前台显示）
				data.put(UPLOAD_MSG, "服务器发生错误(文件丢失)，操作失败，请稍候再试或联系管理员");
				// 调试信息
				logger.error(e.getMessage(), e);
				// 添加系统日志
				systemLog(e);
				// 结束业务
				return ServiceResultConfig.FAILED;
			} catch (IOException ioe) {
				// 删除临时文件和已经复制的文件
				FileUtils.deleteFile(oldJarPath, oldCodePath, newJarPath.getPath(), newCodePath.getPath());
				logger.info("deleteFile");
				data.put(UPLOAD_MSG, "服务器发生错误，操作失败，请稍候再试或联系管理员");
				logger.error(ioe.getMessage(), ioe);
				systemLog(ioe);
				// 结束业务
				return ServiceResultConfig.FAILED;
			}
			// 把数据插入进入数据库
			try {
				// 开启事务
				baseDao.transaction();
				// 插入基本表
				if (baseDao.add("addPlugin", BeanUtils.beanToMap(plugin)) > 0) {
					// 插入关联表
					insertAssociative(false, plugin);
				}
				// 提交事务
				baseDao.commit();
			} catch (DaoException de) {
				// 事务回滚
				baseDao.rollback();
				// 删除临时备份和已经移动到目的地的文件
				FileUtils.deleteFile(newJarPath.getPath(), newCodePath.getPath());
				logger.info("deleteFile");
				data.put(UPLOAD_MSG, "服务器出现错误，操作失败，请稍候再试或联系管理员");
				logger.error(de.getMessage(), de);
				systemLog(de);
				return ServiceResultConfig.FAILED;
			}
			// 删除临时文件
			// FileUtils.deleteFile(oldJarPath, oldCodePath);
			data.put(UPLOAD_MSG, "上传成功，请等待管理员的审核，具体审核结果会发送至您的邮箱中，如有其他问题请联系管理员");
			// TODO 发邮件

			return ServiceResultConfig.SUCCESS;
		}
		data.put(UPLOAD_MSG, "数据错误，操作失败");
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 下载算法插件（包含源码和jar包）(测试成功)
	 * @param params
	 * @param data
	 * @return
	 */
	String downloadPlugin(Map<String, Object[]> params, Map<String, Object> data) {
		plugin = BeanUtils.mapToBean(params, Plugin.class);
		// 校验信息
		if (checkInfo(view, plugin)) {
			// 查询需要下载的文件的信息
			QueryService queryService = new QueryService();
			List<Map<String, Object>> queryResult = queryService.queryList("queryPlugin", BeanUtils.beanToMap(plugin));
			// 查询不到结果：输入提示信息，结束业务
			if (queryResult.size() == 0) {

				data.put(DOWNLOAD_MSG, "找不到该算法插件、请稍后再试，或联系管理员");
				return ServiceResultConfig.FAILED;
			}
			// 把查询结果赋给实体类,方便以后操作
			plugin = BeanUtils.mapToBean(queryResult.get(0), Plugin.class);
			// 获取文件地址
			if (plugin.getState().equals(ApplicationState.PASS)) {
				data.put("codePath", new File(FSConfig.plugin().getAbsolutePath(), plugin.getCodePath()));
				data.put("jarPath", new File(FSConfig.plugin().getAbsolutePath(), plugin.getJarPath()));
			} else {
				data.put("codePath", new File(FSConfig.userPlugin().getAbsolutePath(), plugin.getCodePath()));
				data.put("jarPath", new File(FSConfig.userPlugin().getAbsolutePath(), plugin.getJarPath()));
			}
			return ServiceResultConfig.SUCCESS;
		}
		data.put(DOWNLOAD_MSG, "数据错误，操作失败");
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 删除算法插件，只能删除审核未通过或者等待审核的插件(测试成功)
	 * @param params
	 * @param data
	 * @return
	 */
	String deletePlugin(Map<String, Object[]> params, Map<String, Object> data) {
		// 得到plugin实体类对象，用于储存从数据库查出的算法信息
		plugin = BeanUtils.mapToBean(params, Plugin.class);
		// 校验数据
		if (checkInfo(view, plugin)) {
			// 查询该算法的信息，用于判断是否存在该算法，以及目前该算法的状态是怎么样的
			QueryService queryService = new QueryService();
			List<Map<String, Object>> queryResult = queryService.queryList("queryPlugin", BeanUtils.beanToMap(plugin));
			// 判断系统中是否存在该算法、若不存在，直接在data中存入提示信息，结束业务
			if (queryResult == null || queryResult.size() == 0) {
				data.put(DEL_MSG, "您要删除的算法不存在,可能已被删除，若想了解详情，请联系管理员");
				return ServiceResultConfig.FAILED;
			}
			// 把查询的数据赋给plugin对象
			plugin = BeanUtils.mapToBean(queryResult.get(0), Plugin.class);
			// 判断算法的状态是否已通过，如果已经通过则结束业务，把提示信息装入data返回给前台
			if (plugin.getState().equals(ApplicationState.PASS)) {
				data.put(DEL_MSG, "您要删除的算法已经通过了审核，系统不允许删除");
				return ServiceResultConfig.FAILED;
			}
			// 对将要删除的算法文件进行临时备份
			String oldJarPath = new File(FSConfig.userPlugin(), plugin.getJarPath()).getAbsolutePath();
			String oldCodePath = new File(FSConfig.userPlugin(), plugin.getCodePath()).getAbsolutePath();
			String destJarPath = new File(FSConfig.temp(), plugin.getJarPath()).getAbsolutePath();
			String destCodePath = new File(FSConfig.temp(), plugin.getCodePath()).getAbsolutePath();
			logger.debug("临时路径：" + destJarPath + "   " + destCodePath);
			logger.debug("原文件地址：" + oldJarPath + "  " + oldCodePath);
			try {
				// 临时备份
				FileUtils.copyFile(oldCodePath, destCodePath);
				FileUtils.copyFile(oldJarPath, destJarPath);
			} catch (FileNotFoundException e) {
				// 发生系统异常，把错误信息发在data中（显示在前台用）
				data.put(DEL_MSG, "服务器错误(文件丢失)，操作失败，请稍候再试或联系管理员");
				// 调试信息
				logger.error(e.getMessage(), e);
				// 添加系统日志
				systemLog(e);
				// 删除临时备份 结束业务
				FileUtils.deleteFile(destJarPath, destCodePath);
				return ServiceResultConfig.FAILED;
			} catch (IOException ioe) {
				data.put(DEL_MSG, "服务器错误，操作失败，请稍候再试或联系管理员");
				logger.error(ioe.getMessage(), ioe);
				systemLog(ioe);
				// 删除临时备份 结束业务
				FileUtils.deleteFile(destJarPath, destCodePath);
				return ServiceResultConfig.FAILED;
			}
			// 删除原文件
			if (!FileUtils.deleteFile(oldJarPath, oldCodePath)) {
				try {
					FileUtils.copyFile(destCodePath, oldCodePath);
					FileUtils.copyFile(destJarPath, oldJarPath);
				} catch (IOException e) {
					data.put(DEL_MSG, "服务器错误，操作失败，请稍候再试或联系管理员");
					logger.error(e.getMessage(), e);
					systemLog(e);
					return ServiceResultConfig.FAILED;
				}
				data.put(DEL_MSG, "服务器错误，操作失败，请稍候再试或联系管理员");
				FileUtils.deleteFile(destJarPath, destCodePath);
				return ServiceResultConfig.FAILED;
			}
			// 调用数据库删除该条插件的记录
			try {
				plugin.setState(ApplicationState.PASS);
				if (baseDao.delete("deletePlugin", BeanUtils.beanToMap(plugin)) <= 0) {
					data.put(DEL_MSG, "该算法可能已被删除");
				}
			} catch (DaoException de) {
				data.put(DEL_MSG, "服务器发生错误，操作失败，请稍候再试或联系管理员");
				logger.error(de.getMessage(), de);
				systemLog(de);
				// 用临时备份进行恢复
				try {
					FileUtils.copyFile(destJarPath, oldJarPath);
					FileUtils.copyFile(destCodePath, oldCodePath);
				} catch (FileNotFoundException e) {
					logger.error(e.getMessage(), e);
					systemLog(e);
					// 删除没有完全恢复成功的文件
					FileUtils.deleteFile(oldJarPath, oldCodePath);
				} catch (IOException ioe) {
					logger.error(ioe.getMessage(), ioe);
					systemLog(ioe);
					// 删除没有完全恢复成功的文件
					FileUtils.deleteFile(oldJarPath, oldCodePath);
				}
				return ServiceResultConfig.FAILED;
			}
			// 删除临时备份
			FileUtils.deleteFile(destJarPath, destCodePath);
			// 向data中存入显示到前台的提示信息
			data.put(DEL_MSG, "删除成功");
			// TODO 发送邮件

			return ServiceResultConfig.SUCCESS;
		}
		data.put(DEL_MSG, "数据错误，操作失败");
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 审核算法插件（测试成功）
	 * 算法插件状态变为通过,拒绝算法插件，插法插件状态变为拒绝。
	 * 无论通过与否，都使用EmailUtil发送邮件
	 * @param params
	 * @param data
	 * @return
	 */
	String auditPlugin(Map<String, Object[]> params, Map<String, Object> data) {
		plugin = BeanUtils.mapToBean(params, Plugin.class);
		// 存储审核状态
		String state = plugin.getState();
		// 校验数据
		if (checkInfo(view, plugin)) {
			// 查询此算法的信息 判断是否存在该算法、审核状态是否为待审核
			QueryService queryService = new QueryService();
			List<Map<String, Object>> queryResult = queryService.queryList("queryPlugin", BeanUtils.beanToMap(plugin));
			// 判断算法是否存在
			if (queryResult == null || queryResult.size() == 0) {
				// 在data中存入向前台显示的提示信息，结束业务
				data.put(AUDIT_MSG, "该算法不存在，可能已被用户删除，具体原因可查看日志了解");
				return ServiceResultConfig.FAILED;
			}
			plugin = BeanUtils.mapToBean(queryResult.get(0), Plugin.class);
			// 判断算法的审核状态是否符合要求，只有在待审核状态是才能进行修改审核状态
			if (plugin.getState().equals(ApplicationState.WAIT)) {
				// 拒绝状态处理
				if (state.equals(ApplicationState.REFUSE)) {
					// 修改数据库
					plugin.setState(state);
					try {
						baseDao.update("updatePlugin", auditBeanToMap(plugin));
					} catch (DaoException e) {
						data.put(AUDIT_MSG, "服务器发生错误，操作失败，请稍候再试或查看日志");
						logger.error(e.getMessage(), e);
						systemLog(e);
						return ServiceResultConfig.FAILED;
					}
					// TODO 发送邮件

					data.put(AUDIT_MSG, "审核成功，审核结果已发送至被审核人的邮箱");
					return ServiceResultConfig.SUCCESS;
				}
				// 通过状态处理
				// 配置新路径 存储旧路径
				String oldJarPath = new File(FSConfig.userPlugin(), plugin.getJarPath()).getAbsolutePath();
				String oldCodePath = new File(FSConfig.userPlugin(), plugin.getCodePath()).getAbsolutePath();
				String newJarPath = new File(FSConfig.plugin(), plugin.getJarPath()).getAbsolutePath();
				String newCodePath = new File(FSConfig.plugin(), plugin.getCodePath()).getAbsolutePath();
				logger.debug(oldJarPath + "\n" + oldCodePath + "\n newPath:" + newJarPath + "\n" + newCodePath);
				try {
					// 把算法文件复制到制定的文件夹下
					FileUtils.copyFile(plugin.getJarPath(), newJarPath);
					FileUtils.copyFile(plugin.getCodePath(), newCodePath);
				} catch (FileNotFoundException e) {
					// 把提示信息放入data中（前台显示）业务结束
					data.put(AUDIT_MSG, "服务器发生错误，操作失败，请稍候再试或查看日志");
					logger.error(e.getMessage(), e);
					systemLog(e);
					// 删除没有复制完成的文件
					FileUtils.deleteFile(newJarPath, newCodePath);
					return ServiceResultConfig.FAILED;
				} catch (IOException ioe) {
					data.put(AUDIT_MSG, "服务器发生错误，操作失败，请稍候再试或查看日志");
					logger.error(ioe.getMessage(), ioe);
					systemLog(ioe);
					// 删除没有复制完成的文件
					FileUtils.deleteFile(newJarPath, newCodePath);
					return ServiceResultConfig.FAILED;
				}
				// 更新路径 和状态
				logger.debug(new File(newJarPath).getName() + "   " + new File(newCodePath).getName());
				plugin.setJarPath(new File(newJarPath).getName());
				plugin.setCodePath(new File(newCodePath).getName());
				plugin.setState(state);
				// 修改数据库中的数据
				try {
					baseDao.update("updatePlugin", auditBeanToMap(plugin));
				} catch (DaoException de) {
					data.put(AUDIT_MSG, "服务器发生错误，操作失败，请稍候再试，具体信息请查看日志");
					logger.error(de.getMessage(), de);
					systemLog(de);
					// 删除已经复制到系统目录下的文件
					FileUtils.deleteFile(newJarPath, newCodePath);
					return ServiceResultConfig.FAILED;
				}
				// 删除用户目录文件文件
				FileUtils.deleteFile(oldJarPath, oldCodePath);
				// TODO 发邮件

				data.put(AUDIT_MSG, "审核成功，审核结果已发送至被审核人的邮箱");
				return ServiceResultConfig.SUCCESS;
			}
			// 若不是WAIT状态 则向data中存入提示信息，并结束该业务
			data.put(AUDIT_MSG, "该算法已被审核过，不允许重复审核，具体详情，请查看日志了解");
			return ServiceResultConfig.FAILED;
		}
		data.put(AUDIT_MSG, "数据错误，请求失败");
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 修改算法插件的信息（只有待审核和拒绝状态下才可以修改）
	 * @param params
	 * @param data
	 * @return
	 */
	String modifyPlugin(Map<String, String[]> params, Map<String, Object> data) {
		plugin = BeanUtils.mapToBean(params, Plugin.class);
		initPlugin(plugin, params);
		// 校验数据
		if (checkInfo(view, plugin)) {
			// 查询此算法插件的信息
			QueryService queryService = new QueryService();
			List<Map<String, Object>> queryResult = queryService.queryList("queryPlugin", BeanUtils.beanToMap(plugin));
			if (queryResult == null || queryResult.size() == 0) {
				data.put(MODIFY_MSG, "查询不到该算法，可能不存在，如有疑问请联系管理员");
				return ServiceResultConfig.FAILED;
			}
			// 获取查询出的数据
			Plugin pluginDB = BeanUtils.mapToBean(queryResult.get(0), Plugin.class);
			// 验证此算法的审核状态 为待审核或者拒绝才允许继续操作
			if (pluginDB.getState().equals(ApplicationState.PASS)
					|| pluginDB.getState().equals(ApplicationState.REFUSE)) {
				data.put(MODIFY_MSG, "该算法已被审核，不允许修改");
				return ServiceResultConfig.FAILED;
			}
			// 修改算法
			// 状态为待审核
			// 插入数据库
			try {
				baseDao.transaction();
				if (baseDao.update("updatePlugin", BeanUtils.beanToMap(plugin)) > 0) {
					insertAssociative(true, plugin);
				}
				baseDao.commit();
			} catch (DaoException e) {
				// 事务回滚
				baseDao.rollback();
				// 发生系统异常，把错误信息发在data中（显示在前台用）
				data.put(MODIFY_MSG, "服务器错误，操作失败，请稍候再试或联系管理员");
				logger.error(e.getMessage(), e);
				systemLog(e);
				return ServiceResultConfig.FAILED;
			}
			data.put(MODIFY_MSG, "修改成功！");
			return ServiceResultConfig.SUCCESS;

		}
		// 把提示信息存在data中 结束业务
		data.put(MODIFY_MSG, "数据格式错误，操作失败");
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 校验数据
	 * @param view
	 * @param plugin
	 * @return
	 */
	private boolean checkInfo(String view, Plugin plugin) {
		if (view.equals("auditPlugin") || view.equals("downloadPlugin") || view.equals("modifyPlugin")
				|| view.equals("deletePlugin")) {
			if (plugin.getPluginID() == null) {
				return false;
			}
			if (view.equals("auditPlugin") && plugin.getState().equals(ApplicationState.PASS) == false
					&& plugin.getState().equals(ApplicationState.REFUSE) == false) {
				return false;
			}
		}

		if (view.equals("uploadPlugin")) {
			if (plugin.getUserID() == null || StringUtils.isBlank(plugin.getJarPath())
					|| StringUtils.isBlank(plugin.getCodePath())) {
				logger.debug((plugin.getUserID() == null) + "  " + StringUtils.isBlank(plugin.getJarPath()) + "  "
						+ StringUtils.isBlank(plugin.getCodePath()));
				return false;
			}
			if (plugin.getJarPath().length() > 256 || plugin.getCodePath().length() > 256) {
				return false;
			}
		}

		if (view.equals("modifyPlugin") || view.equals("uploadPlugin")) {
			if (StringUtils.isBlank(plugin.getDescription()) || StringUtils.isBlank(plugin.getPluginName())
					|| StringUtils.isBlank(plugin.getPluginType()) || StringUtils.isBlank(plugin.getVersion())) {
				return false;
			}
			if (plugin.getPluginName().length() > 32 || plugin.getVersion().length() > 16) {
				return false;
			}
			if (plugin.getPluginType().equals(PluginType.DATAPROCESSING) == false
					&& plugin.getPluginType().equals(PluginType.PRETREAMENT) == false
					&& plugin.getPluginType().equals(PluginType.SPECTRUM_ANALYSIS) == false
					&& plugin.getPluginType().equals(PluginType.SPECTRUM_PARSE) == false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 把原文件路径截取出文件名，和目的文件及的路径拼接在一起形成最终文件
	 * @param folder
	 * @param filePath
	 * @return
	 */
	private File fileEndSrc(File folder, String filePath) {
		return new File(folder, new File(filePath).getName());
	}

	/**
	 * 上传算法方法调用的beanToMap
	 * @param key 
	 * @param plugin
	 * @param i 循环变量
	 * @param j 循环变量
	 * @param modify
	 * @return
	 */
	private Map<String, Object> fakeBeanToMap(String key, Plugin plugin, int i, int j, boolean modify) {
		Map<String, Object> data = new HashMap<String, Object>();
		int premarykey = baseDao.getPremarykeyValue();
		logger.debug("主键：" + premarykey);
		if (modify == false) {
			data.put("pluginID", premarykey);
		} else {
			if (key.equals("rel")) {
				data.put("mainID", plugin.getPluginID());
				data.put("subID", plugin.getPluginID());
			} else {
				data.put("pluginID", plugin.getPluginID());
			}
		}
		if (key.equals("hardwareIDs")) {
			data.put("detectedID", plugin.getHardwareIDs()[i]);
			data.put("spectrumTypeID", plugin.getHardware().get(i).getSpectrumTypeIDs()[j]);
		}
		if (key.equals("detectedIDs")) {
			data.put("detectedID", plugin.getDetectedObjectIDs()[i]);
			data.put("contentID", plugin.getDetectedObject().get(i).getContentIDs()[j]);
		}
		if (key.equals("hardwareID")) {
			data.put(key, plugin.getHardwareIDs()[i]);
		}
		if (key.equals("detectedID")) {
			data.put(key, plugin.getDetectedObjectIDs()[i]);
		}
		if (key.equals("contentID")) {
			data.put(key, plugin.getDetectedObject().get(0).getContentIDs()[i]);
		}
		if (key.equals("spectrumTypeID")) {
			data.put(key, plugin.getHardware().get(0).getSpectrumTypeIDs()[i]);
		}
		if (key.equals("mainID")) {
			data.put(key, plugin.getPluginMainIDs()[i]);
			if (modify) {
				data.put("subID", plugin.getPluginID());
			} else {
				data.put("subID", premarykey);
			}
		}
		if (key.equals("subID")) {
			if (modify) {
				data.put("mainID", plugin.getPluginID());
			} else {
				data.put("mainID", premarykey);
			}
			data.put(key, plugin.getPluginSubIDs()[i]);
		}
		logger.debug(data.toString());
		return data;
	}

	/**
	 * 向plugin对象中插入DetectedObject对象，把contentIDs插入
	 * @param plugin
	 * @param params
	 */
	private void initPlugin(Plugin plugin, Map<String, String[]> params) {
		plugin.setDetectedObject(new ArrayList<DetectedObject>());
		plugin.setHardware(new ArrayList<Hardware>());
		if (params.get("detectedObjectIDs") != null) {
			for (int i = 0; i < params.get("detectedObjectIDs").length; i++) {
				if (params.get("contentIDs") != null) {
					Integer[] temp = new Integer[params.get("contentIDs").length];
					for (int j = 0; j < params.get("contentIDs").length; j++) {
						temp[j] = Integer.parseInt(params.get("contentIDs")[j]);
					}
					plugin.getDetectedObject().add(new DetectedObject(plugin.getDetectedObjectIDs()[i], temp));
				} else {
					plugin.getDetectedObject().add(new DetectedObject(plugin.getDetectedObjectIDs()[i]));
				}
			}
		} else if (params.get("contentIDs") != null && params.get("contentIDs").length > 0) {
			Integer[] temp = new Integer[params.get("contentIDs").length];
			for (int i = 0; i < params.get("contentIDs").length; i++) {
				temp[i] = Integer.parseInt(params.get("contentIDs")[i]);
			}
			DetectedObject detected = new DetectedObject();
			detected.setContentIDs(temp);
			plugin.getDetectedObject().add(detected);
		}
		if (params.get("hardwareIDs") != null) {
			for (int i = 0; i < params.get("hardwareIDs").length; i++) {
				if (params.get("spectrumTypeIDs") != null) {
					Integer[] temp = new Integer[params.get("spectrumTypeIDs").length];
					for (int j = 0; j < params.get("spectrumTypeIDs").length; j++) {
						temp[j] = Integer.parseInt(params.get("spectrumTypeIDs")[j]);
					}
					plugin.getHardware().add(new Hardware(plugin.getHardwareIDs()[i], temp));
				} else {
					plugin.getHardware().add(new Hardware(plugin.getHardwareIDs()[i]));
				}
			}
		} else if (params.get("spectrumTypeIDs") != null && params.get("spectrumTypeIDs").length > 0) {
			Integer[] temp = new Integer[params.get("spectrumTypeIDs").length];
			for (int i = 0; i < params.get("spectrumTypeIDs").length; i++) {
				temp[i] = Integer.parseInt(params.get("spectrumTypeIDs")[i]);
			}
			plugin.getHardware().add(new Hardware(null, temp));
		}
		if (params.get("session_userID") != null) {
			plugin.setUserID(Integer.valueOf(getParamStringValue(params, "session_userID", 0)));
		}
	}

	/**
	 * 审核的BeanToMap
	 * @param plugin
	 * @return
	 */
	private Map<String, Object> auditBeanToMap(Plugin plugin) {
		Map<String, Object> data = BeanUtils.beanToMap(plugin);
		data.put("t_plugin.state", ApplicationState.WAIT);
		logger.debug(data.toString());
		return data;
	}

	/**
	 * 插入关联表
	 * @param modify
	 * @param plugin
	 * @throws DaoException
	 */
	private void insertAssociative(boolean modify, Plugin plugin) throws DaoException {
		// 删除硬件关联
		if (modify) {
			baseDao.delete("deletePluginHardwareSpectrumType", fakeBeanToMap("", plugin, -1, -1, modify));// BeanToMap
		}
		// 添加硬件关联
		if (plugin.getHardwareIDs() != null && plugin.getHardwareIDs().length > 0) {
			for (int i = 0; i < plugin.getHardwareIDs().length; i++) {
				if (plugin.getHardware().get(i).getSpectrumTypeIDs() != null
						&& plugin.getHardware().get(i).getSpectrumTypeIDs().length > 0) {
					for (int j = 0; j < plugin.getHardware().get(i).getSpectrumTypeIDs().length; j++) {
						logger.debug("第i+j=" + i + j);
						baseDao.add("addPluginHardwareSpectrumType",
								fakeBeanToMap("hardwareIDs", plugin, i, j, modify));
					}
				} else {
					baseDao.add("addPluginHardwareSpectrumType", fakeBeanToMap("hardwareID", plugin, i, -1, modify));
				}
			}
		} else if (plugin.getHardware() != null && plugin.getHardware().size() > 0) {
			for (int i = 0; i < plugin.getHardware().get(0).getSpectrumTypeIDs().length; i++) {
				baseDao.add("addPluginHardwareSpectrumType", fakeBeanToMap("spectrumTypeID", plugin, i, -1, modify));
			}
		}
		// 删除物质关联
		if (modify) {
			baseDao.delete("deletePluginDetectedContent", fakeBeanToMap("", plugin, -1, -1, modify));
		}
		// 添加物质关联
		if (plugin.getDetectedObjectIDs() != null && plugin.getDetectedObjectIDs().length > 0) {
			for (int i = 0; i < plugin.getDetectedObjectIDs().length; i++) {
				if (plugin.getDetectedObject().get(i).getContentIDs() != null
						&& plugin.getDetectedObject().get(i).getContentIDs().length > 0) {
					for (int j = 0; j < plugin.getDetectedObject().get(i).getContentIDs().length; j++) {
						logger.debug("i+j=" + i + j);
						baseDao.add("addPluginDetectedContent", fakeBeanToMap("detectedIDs", plugin, i, j, modify));
					}
				} else {
					baseDao.add("addPluginDetectedContent", fakeBeanToMap("detectedID", plugin, i, -1, modify));
				}
			}
		} else if (plugin.getDetectedObject() != null && plugin.getDetectedObject().size() > 0) {
			for (int i = 0; i < plugin.getDetectedObject().get(0).getContentIDs().length; i++) {
				baseDao.add("addPluginDetectedContent", fakeBeanToMap("contentID", plugin, i, -1, modify));
			}
		}
		// 删除算法关联
		if (modify) {
			baseDao.add("deletePluginRel", fakeBeanToMap("rel", plugin, -1, -1, modify));
		}
		// 添加算法关联
		if (plugin.getPluginMainIDs() != null && plugin.getPluginMainIDs().length > 0) {
			for (int i = 0; i < plugin.getPluginMainIDs().length; i++) {
				baseDao.add("addPluginRel", fakeBeanToMap("mainID", plugin, i, -1, modify));
			}
		}
		if (plugin.getPluginSubIDs() != null && plugin.getPluginSubIDs().length > 0) {
			for (int i = 0; i < plugin.getPluginSubIDs().length; i++) {
				baseDao.add("addPluginRel", fakeBeanToMap("subID", plugin, i, -1, modify));
			}
		}
	}

}