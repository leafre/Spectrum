package bluedot.spectrum.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import bluedot.spectrum.bean.Backup;
import bluedot.spectrum.bean.Log;
import bluedot.spectrum.bean.Log.LogLevel;
import bluedot.spectrum.bean.Log.LogType;
import bluedot.spectrum.bean.Recover;
import bluedot.spectrum.exception.DaoException;
import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.utils.BeanUtils;
import bluedot.spectrum.utils.FileUtils;
import bluedot.spectrum.utils.MySQLBackupRecoverUtil;
import bluedot.spectrum.web.core.FSConfig;
import bluedot.spectrum.web.core.ServiceMapper;
import bluedot.spectrum.web.core.ServiceResultConfig;

/**
 * 系统维护业务
 * @author 刘驭洲 2016年8月13日
 */
public class MaintainService extends ServiceSupport {

	/**
	 * 构造方法
	 * @param view
	 * @param params
	 * @param data
	 * @throws ServiceException
	 */
	public MaintainService(String view, Map<String, Object[]> params, Map<String, Object> data) throws ServiceException {
		super(view, params, data);
		execute(view, params, data);
	}

	/**
	 * 备份系统(测试完成)
	 * @param params 存储参数的集合
	 * @param data
	 * @return
	 */
	String backup(Map<String, String[]> params, Map<String, Object> data) {
		//logger.debug(Arrays.toString(params.get("content"))+"  "+params.toString());
		Backup backup = BeanUtils.mapToBean(params, Backup.class);
		backup.setContent(arraysToString(params.get("content")));
		//logger.debug(backup.getContent());
		//校验数据
		if(checkInfo(view,backup)){
			//从配置文件获取需要备份的大类的地址，和备份文件要存的目的地址
			//进行备份
			backup.setContent(conversionName("backup",backup.getContent()));
			String[]paths = backup.getContent().split(",");
			if(paths.length == 0){
				paths = new String[]{backup.getContent()};
			}
			long time = System.currentTimeMillis();
			backup.setTime(time);
			boolean existDatabase = false;
			File sqlFile = new File(FSConfig.backup().getAbsolutePath()
					+File.separator+backup.getBackupName()+"_"+backup.getTime()
					+File.separator+MySQLBackupRecoverUtil.getDatabaseName()+".sql");
			//logger.debug(backup.getContent()+"  "+Arrays.toString(paths));
			try {
				for(int i=0;i<paths.length;i++){
					if(paths[i].equals("database")){
						existDatabase = true;
						continue;
					}
					/*logger.debug(getDirPath(paths[i])+"   "+FSConfig.backup().getAbsolutePath()
							+File.separator+backup.getBackupName()+"_"+backup.getTime()
							+File.separator+paths[i].substring(0, paths[i].length()-3));*/
					FileUtils.copyDirectory(getDirPath(paths[i]), FSConfig.backup().getAbsolutePath()
							+File.separator+backup.getBackupName()+"_"+backup.getTime()
							+File.separator+paths[i].substring(0, paths[i].length()-3));
				}
			} catch(FileNotFoundException e){
				//向data中存入前台显示的提示信息
				data.put("backup_msg", "服务器出现错误(文件丢失)，操作失败，请稍候再试，或查看日志");
				//调试信息
				logger.error(e.getMessage(),e);
				//添加系统日志,结束业务
				systemLog(view,e);
				//删除备份文件
				deleteBackupFail(backup.getBackupName(),paths,backup.getTime());
				return ServiceResultConfig.FAILED;
			} catch (IOException ioe) {
				//向data中存入前台显示的提示信息
				data.put("backup_msg", "服务器出现错误，操作失败，请稍候再试，或查看日志");
				//调试信息
				logger.error(ioe.getMessage(),ioe);
				//添加系统日志,结束业务
				systemLog(view,ioe);
				//删除备份文件
				deleteBackupFail(backup.getBackupName(),paths,backup.getTime());
				return ServiceResultConfig.FAILED;
			}
			//向数据库中插入记录
			backup.setContent(conversionName("recover", backup.getContent()));
			try{
				baseDao.transaction();
				baseDao.add("addBackup", BeanUtils.beanToMap(backup));
				if(existDatabase){
					if(!MySQLBackupRecoverUtil.backup(sqlFile)){
						baseDao.rollback();
						//向data中存入前台显示的提示信息
						data.put("backup_msg", "服务器出现错误(数据库备份失败)，操作失败，请稍候再试，或查看日志");
						//调试信息
						logger.error("数据库备份失败");
						//删除备份文件
						if(sqlFile.exists()){
							sqlFile.delete();
						}
						deleteBackupFail(backup.getBackupName(),paths,backup.getTime());
						return ServiceResultConfig.FAILED;
					}
				}
				baseDao.commit();
			}catch(DaoException de){
				baseDao.rollback();
				//向data中存入前台显示的信息
				data.put("backup_msg", "服务器出现错误，操作失败，请稍候再试，或查看日志");
				//调试信息
				logger.error(de.getMessage(),de);
				//添加系统日志
				systemLog(view,de);
				//删除已备份的文件  
				deleteBackupFail(backup.getBackupName(),paths,backup.getTime());
				//删除数据库备份文件
				if(sqlFile.exists()){
					sqlFile.delete();
				}
				return ServiceResultConfig.FAILED;
			}
			//返回成功信息
			data.put("backup_msg", "备份成功");
			return ServiceResultConfig.SUCCESS;
		}
		data.put("backup_msg", "数据有错误，操作失败");
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 选中备份，选定内容，进行恢复
	 * @param params 存储参数的集合
	 * @param data 回送给web层的参数,存储了对应的用户信息
	 * @return 成功OR失败
	 */
	String recover(Map<String, String[]> params, Map<String, Object> data) {
		Recover recover = BeanUtils.mapToBean(params,Recover.class);
		//校验数据
		if(checkInfo(view,recover)){
			//判断有没有数据库备份时使用
			boolean isExistDatabase = false;
			//把需要恢复的内容做临时备份
			long time = System.currentTimeMillis();
			recover.setTime(time);
			recover.setContent(conversionName("backup", recover.getContent()));
			String[]paths = recover.getContent().split(",");
			logger.debug(Arrays.toString(paths));
			File sqlFile = new File(FSConfig.temp().getAbsolutePath()
					+File.separator+time
					+File.separator+MySQLBackupRecoverUtil.getDatabaseName()+".sql");
			try {
				for(int i=0;i<paths.length;i++){
					if("database".equals(paths[i])){
						isExistDatabase = true;
						if(!MySQLBackupRecoverUtil.backup(sqlFile)){
							deleteRecoverFail(time);
							data.put("recover_msg", "服务器发生错误(数据库临时备份失败)，操作失败，请稍后再试，或查看日志");
							logger.error("临时备份数据库错误");
							return ServiceResultConfig.FAILED;
						}
						continue;
					}
					logger.debug(getDirPath(paths[i])+"    "+FSConfig.temp().getAbsolutePath()+File.separator+time);
					FileUtils.copyDirectory(getDirPath(paths[i]), FSConfig.temp().getAbsolutePath()+File.separator+time);
				}
			} catch(FileNotFoundException e){
				//向data中存入提示信息
				data.put("recover_msg", "服务器发生错误(文件丢失)，操作失败，请稍后再试，或查看日志");
				//调试信息
				logger.error(e.getMessage(),e);
				//添加系统日志
				systemLog(view,e);
				//删除刚刚临时备份的文件
				deleteRecoverFail(time);
				return ServiceResultConfig.FAILED;
			} catch (IOException ioe) {
				//向data中存入提示信息
				data.put("recover_msg", "服务器发生错误，操作失败，请稍后再试，或查看日志");
				//调试信息
				logger.error(ioe.getMessage(),ioe);
				//添加系统日志
				systemLog(view,ioe);
				//删除刚刚临时备份的文件
				deleteRecoverFail(time);
				return ServiceResultConfig.FAILED;
			}
			//查询备份信息
			recover.setBackup(new Backup());
			QueryService queryService = new QueryService();
			recover.getBackup().setBackupID(recover.getBackupID());
			recover.setBackup(BeanUtils.mapToBean(queryService.queryList("queryBackup", BeanUtils.beanToMap(recover.getBackup())).get(0),Backup.class));
			recover.getBackup().setContent(conversionName("backup",recover.getBackup().getContent()));
			//删除需要更新的文件夹 
			try {
				logger.debug(Arrays.toString(getDirPaths(paths,isExistDatabase)));
				String[]dirpaths = getDirPaths(paths,isExistDatabase);
				//防止只有数据库恢复出现异常
				if(dirpaths!=null&&dirpaths.length!=0){
					FileUtils.deleteDirectory(getDirPaths(paths,isExistDatabase));
				}
			} catch (IOException e) {
				data.put("recover_msg", "服务器错误，操作失败，请稍后再试、或查看日志");
				//调试信息
				logger.error(e.getMessage(),e);
				systemLog(view, e);
				//恢复删除 
				try{
					for(int i=0;i<paths.length;i++){
						if("database".equals(paths[i])){
							continue;
						}
						FileUtils.copyDirectory(FSConfig.temp().getAbsolutePath()
								+File.separator+time
								+File.separator+paths[i], FSConfig.temp().getParent());
					}
				} catch(FileNotFoundException fne){
					logger.error(fne.getMessage(),fne);
					systemLog(view, fne);
					return ServiceResultConfig.FAILED;
				} catch(IOException ioe){
					logger.error(ioe.getMessage(),ioe);
					systemLog(view, ioe);
					return ServiceResultConfig.FAILED;
				}
				//删除临时
				deleteRecoverFail(time);
				return ServiceResultConfig.FAILED;
			}
			//恢复备份
			logger.debug(FSConfig.temp().getAbsolutePath()+"    "+FSConfig.temp().getParent());
			//String[] path = FSConfig.temp().getAbsolutePath().split("\\");
			//logger.debug("dest:"+path[0]+"   "+path.length);
			try{
				for(int i=0;i<paths.length;i++){
					if("database".equals(paths[i])){
						if(!MySQLBackupRecoverUtil.recover(FSConfig.backup().getAbsolutePath()
								+File.separator+recover.getBackup().getBackupName()+"_"+recover.getBackup().getTime()
								+File.separator+MySQLBackupRecoverUtil.getDatabaseName()+".sql")){
							data.put("recover_msg", "服务器错误(恢复数据库失败)，操作失败，请稍后再试、或查看日志");
							//删除数据库临时备份
							if(!MySQLBackupRecoverUtil.recover(sqlFile)){
								logger.error("恢复临时数据库备份失败");
								return ServiceResultConfig.FAILED;
							}
							//恢复删除文件  从1开始
							for(int j=1;j<paths.length;j++){
								FileUtils.copyDirectory(FSConfig.temp().getAbsolutePath()
										+File.separator+time+paths[i], FSConfig.temp().getParent());
							}
							//删除临时备份
							deleteRecoverFail(time);
							return ServiceResultConfig.FAILED;
						}
						continue;
					}
					logger.debug("恢复备份：src:"+FSConfig.backup().getAbsolutePath()
							+File.separator+recover.getBackup().getBackupName()+"_"+recover.getBackup().getTime()+File.separator+paths[i].substring(0, paths[i].length()-3));
					FileUtils.copyDirectory(FSConfig.backup().getAbsolutePath()
							+File.separator+recover.getBackup().getBackupName()+"_"+recover.getBackup().getTime()+File.separator+paths[i].substring(0, paths[i].length()-3), FSConfig.temp().getParent()+File.separator+paths[i].substring(0, paths[i].length()-3));
				}
			} catch(FileNotFoundException e){
				data.put("recover_msg", "服务器错误，操作失败，请稍后再试、或查看日志");
				//调试信息
				logger.error(e.getMessage(),e);//dkfjkljfliajksdhf_1474614631233
				systemLog(view, e);
				//恢复删除文件
				try{
					for(int i=0;i<paths.length;i++){
						if("database".equals(paths[i])){
							if(!MySQLBackupRecoverUtil.recover(sqlFile)){
								logger.error("恢复临时数据库文件失败");
								return ServiceResultConfig.FAILED;
							}
							continue;
						}
						FileUtils.copyDirectory(FSConfig.temp().getAbsolutePath()
								+File.separator+time
								+File.separator+paths[i], FSConfig.temp().getParent());
					}
				} catch(FileNotFoundException fne){
					logger.error("恢复临时文件失败");
					systemLog(view,fne);
					return ServiceResultConfig.FAILED;
				} catch(IOException ioe){
					logger.error("恢复临时文件失败");
					systemLog(view,ioe);
					return ServiceResultConfig.FAILED;
				}
				//删除临时备份
				deleteRecoverFail(time);
				return ServiceResultConfig.FAILED;
			}catch(IOException e){
				data.put("recover_msg", "服务器错误，操作失败，请稍后再试、或查看日志");
				//调试信息
				logger.error(e.getMessage(),e);
				systemLog(view, e);
				//恢复删除文件
				try{
					for(int i=0;i<paths.length;i++){
						if("database".equals(paths[i])){
							if(!MySQLBackupRecoverUtil.recover(sqlFile)){
								logger.error("恢复临时数据库文件失败");
								return ServiceResultConfig.FAILED;
							}
							continue;
						}
						FileUtils.copyDirectory(FSConfig.temp().getAbsolutePath()
								+File.separator+time
								+File.separator+paths[i], FSConfig.temp().getParent());
					}
				} catch(FileNotFoundException fne){
					logger.error("恢复临时文件失败");
					systemLog(view,fne);
					return ServiceResultConfig.FAILED;
				} catch(IOException ioe){
					logger.error("恢复临时文件失败");
					systemLog(view,ioe);
					return ServiceResultConfig.FAILED;
				}
				//删除临时备份
				deleteRecoverFail(time);
				return ServiceResultConfig.FAILED;
			}
			recover.setContent(conversionName("recover",recover.getContent()));
			//恢复信息存入数据库
			try{
				baseDao.add("addRecover", BeanUtils.beanToMap(recover));
			}catch(DaoException de){
				//向data中存入前台显示的提示信息
				data.put("recover_msg", "服务器错误，操作失败，请稍后再试、或查看日志");
				//调试信息
				logger.error(de.getMessage(),de);
				//添加系统日志，结束业务
				systemLog(view,de);
				//恢复成 恢复之前的文件状态（把被恢复的地方恢复成未恢复之前的状态、之后删除临时备份）
				try {
					//把被恢复的地方恢复成未恢复之前的状态
					logger.debug("恢复备份：src:"+FSConfig.backup().getAbsolutePath()
							+File.separator+recover.getBackup().getBackupName()+"_"+recover.getBackup().getTime());
					for(int i=0;i<paths.length;i++){
						//恢复数据库
						if("database".equals(paths[i])){
							if(!MySQLBackupRecoverUtil.recover(FSConfig.temp()
									+File.separator+time
									+File.separator+MySQLBackupRecoverUtil.getDatabaseName()+".sql")){
								data.put("recover_msg", "恢复临时备份出现错误！");
								return ServiceResultConfig.FAILED;
							}
							continue;
						}
						//恢复文件系统
						FileUtils.copyDirectory(FSConfig.temp().getAbsolutePath()
								+File.separator+recover.getBackup().getBackupName()+"_"+time
								+File.separator+paths[i].substring(0, paths[i].length()-3), FSConfig.temp().getParent());
					}
				} catch(FileNotFoundException e){
					data.put("recover_msg", "服务器错误，操作失败，请稍后再试、或查看日志");
					//调试信息
					logger.error(e.getMessage(),e);
					systemLog(view, e);
					return ServiceResultConfig.FAILED;
				}catch (IOException e) {
					data.put("recover_msg", "服务器错误，操作失败，请稍后再试、或查看日志");
					//调试信息
					logger.error(e.getMessage(),e);
					systemLog(view, e);
					return ServiceResultConfig.FAILED;
				}
				//删除临时备份
				deleteRecoverFail(time);
				return ServiceResultConfig.FAILED;
			}
			//删除临时备份
			deleteRecoverFail(time);
			data.put("recover_msg", "恢复成功");
			return ServiceResultConfig.SUCCESS;
		}
		//把前台显示的提示信息存入data
		data.put("recover_msg", "数据有误，操作失败");
		return ServiceResultConfig.FAILED;
	}

	/**
	 * 校验数据
	 * @param view
	 * @param bean
	 * @return
	 */
	private <T> boolean checkInfo(String view, T bean){
		logger.debug("进入校验");
		if(view.equals("backup")){
			logger.debug("进入校验backup");
			Backup backup = (Backup) bean;
			if(backup.getBackupName()==null
					||backup.getBackupName().equals("")
					||backup.getBackupName()==null
					||backup.getContent()==null
					||backup.getContent().equals("")){
				return false;
			}
			if(backup.getBackupName().length() > 32 || backup.getContent().length()>64){
				return false;
			}
			if(backup.getDescription() != null 
					&& backup.getDescription().equals("") == false 
					&& backup.getDescription().length() > 256){
				return false;
			}
			return true;
		}else if(view.equals("recover")){
			logger.debug("进入校验recover");
			Recover recover = (Recover) bean;
			if(recover.getBackupID() == null){
				logger.debug("校验ID");
				return false;
			}
			if(recover.getContent().equals("")
					||recover.getContent() == null
					||recover.getContent().length()>64){
				logger.debug("校验内容");
				return false;
			}
			if(recover.getDescription() != null 
					&& recover.getDescription().equals("") == false 
					&& recover.getDescription().length()>256){
				logger.debug("校验描述");
				return false;
			}
			return true;
		}
		return false;
	}
	
	/**
	 * 添加系统日志
	 * @param view
	 * @param e
	 */
	private void systemLog(String view, Exception e){
		//添加系统日志
		Log log = new Log(LogLevel.ERROR,LogType.SYSTEM,ServiceMapper.getServiceConfig(view).getId(),e.getMessage());
		logService.addLog(BeanUtils.beanToMap(log));
	}
	
	/**
	 * 获取原文件夹所在地址
	 * @params path
	 * @return
	 */
	private String getDirPath(String path){
		if(path.equals("userFavoriteDir")){
			return FSConfig.userFavorite().getAbsolutePath();
		}else if(path.equals("applicationDir")){
			return FSConfig.application().getAbsolutePath();
		}else if(path.equals("pluginDir")){
			return FSConfig.plugin().getAbsolutePath();
		}else if(path.equals("userPluginDir")){
			return FSConfig.userPlugin().getAbsolutePath();
		}else if(path.equals("standSpectrumDir")){
			return FSConfig.standSpectrum().getAbsolutePath();
		}else if(path.equals("temp")){
			return FSConfig.temp().getAbsolutePath();
		}
		return "";
	}
	
	/**
	 * 获取多个原文件夹所在地址
	 * @param paths
	 * @return
	 */
	private String[] getDirPaths(String[]paths,boolean database){
		String[] resultPaths = null;
		if(database){
			resultPaths= new String[paths.length-1];
		}else{
			resultPaths= new String[paths.length];
		}
		boolean existDatabase = false;
		for(int i=0;i<paths.length;i++){
			if("database".equals(paths[i])){
				existDatabase = true;
				continue;
			}
			if(existDatabase){
				resultPaths[i-1] = getDirPath(paths[i]);
			}else{
				resultPaths[i] = getDirPath(paths[i]);
			}
		}
		return resultPaths;
	}
	
	/**
	 * 删除备份失败文件
	 * @param paths
	 * @param time
	 */
	private void deleteBackupFail(String backupName,String[]paths,long time){
		for(int i=0;i<paths.length;i++){
			if(!new File(FSConfig.backup().getAbsolutePath()
					+File.separator+backupName+"_"+time
					+File.separator+paths[i].substring(0, paths[i].length()-3)).exists()){
				break;
			}
			try {
				FileUtils.deleteDirectory(FSConfig.backup().getAbsolutePath()
						+File.separator+backupName+"_"+time
						+File.separator+paths[i].substring(0, paths[i].length()-3));
			} catch (IOException e) {
				//调试信息
				logger.error(e.getMessage(),e);
				//记录日志
				systemLog(view,e);
			}
		}
	}
	
	/**
	 * 删除恢复失败文件
	 * @param time
	 */
	private void deleteRecoverFail(long time){
		try {
			FileUtils.deleteDirectory(FSConfig.temp().getAbsolutePath()+File.separator+time);
		} catch (IOException ioe2) {
			//调试信息
			logger.error(ioe2.getMessage(),ioe2);
			//添加系统日志
			systemLog(view,ioe2);
		}
	}
	
	/**
	 * 文件名中英文转换
	 * @param view
	 * @param names
	 * @return
	 */
	private String conversionName(String view,String names){
		String[] name = names.split(",");
		String[] nameAlter=new String[name.length];
		//logger.debug(Arrays.toString(name));
		if(view.equals("backup")){
			for(int i=0;i<name.length;i++){
				logger.debug(name[i]);
				if("用户收藏夹".equals(name[i])){
					nameAlter[i] = "userFavoriteDir";
				}else if("申请文件文件夹".equals(name[i])){
					nameAlter[i] = "applicationDir";
				}else if("系统算法文件夹".equals(name[i])){
					nameAlter[i] = "pluginDir";
				}else if("用户算法文件夹".equals(name[i])){
					nameAlter[i] = "userPluginDir";
				}else if("标准库光谱文件夹".equals(name[i])){
					nameAlter[i] = "standSpectrumDir";
				}else if("数据库".equals(name[i])){
					nameAlter[i] = "database";
					if(i>0){
						String temp = nameAlter[0];
						nameAlter[0] = nameAlter[i];
						nameAlter[i] = temp;
					}
				}
			}
		}else{
			for(int i=0;i<name.length;i++){
				if("userFavoriteDir".equals(name[i])){
					nameAlter[i] = "用户收藏夹";
				}else if("applicationDir".equals(name[i])){
					nameAlter[i] = "申请文件文件夹";
				}else if("pluginDir".equals(name[i])){
					nameAlter[i] = "系统算法文件夹";
				}else if("userPluginDir".equals(name[i])){
					nameAlter[i] = "用户算法文件夹";
				}else if("standSpectrumDir".equals(name[i])){
					nameAlter[i] = "标准库光谱文件夹";
				}else if("database".equals(name[i])){
					nameAlter[i] = "数据库";
					if(i>0){
						String temp = nameAlter[0];
						nameAlter[0] = nameAlter[i];
						nameAlter[i] = temp;
					}
				}
			}
		}
		StringBuffer finalName = new StringBuffer();
		for(int i=0;i<name.length-1;i++){
			finalName.append(nameAlter[i]+",");
		}
		finalName.append(nameAlter[name.length-1]);
		return finalName.toString();
	}
	
	/**
	 * 数组转换字符串
	 * @param content 需要变成字符串的数组
	 * @return
	 */
	private String arraysToString(String[]content){
		StringBuffer result = new StringBuffer();
		if(content==null||content.length==0){
			return "";
		}
		for(int i=0;i<content.length-1;i++){
			result.append(content[i]+",");
		}
		result.append(content[content.length-1]);
		return result.toString();
	}
}
