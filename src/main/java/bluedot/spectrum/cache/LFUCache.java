package bluedot.spectrum.cache;

import java.util.HashMap;
import java.util.Iterator;

/**
 * LFU(least frequently used,最不经常使用页置换算法)实现类
 * @author longshu 2016年9月18日
 */
public class LFUCache<K, V> extends AbstractCache<K, V> {

	/**
	 * {@link AbstractCache#AbstractCache(int, long)}
	 */
	public LFUCache(int cacheSize) {
		this(cacheSize, 0);
	}

	/**
	 * {@link AbstractCache#AbstractCache(int, long)}
	 */
	public LFUCache(int cacheSize, long expire) {
		super(cacheSize, expire);
		this.cacheMap = new HashMap<K, CacheObject>(cacheSize + 1);
	}

	/**
	 * 实现删除过期对象 和 删除访问次数最少的对象
	 */
	@Override
	protected int eliminateCache() {
		int count = 0;
		CacheObject comin = null;

		// remove expired items and find cached object with minimal access count
		Iterator<CacheObject> values = cacheMap.values().iterator();
		while (values.hasNext()) {
			CacheObject co = values.next();
			if (co.isExpired()) {
				values.remove();
				count++;
				continue;
			}

			if (comin == null) {
				comin = co;
			} else {
				if (co.accessCount < comin.accessCount) {
					comin = co;
				}
			}
		}

		if (!isFull()) {
			return count;
		}

		// decrease access count to all cached objects
		if (comin != null) {
			long minAccessCount = comin.accessCount;
			values = cacheMap.values().iterator();
			while (values.hasNext()) {
				CacheObject co = values.next();
				co.accessCount -= minAccessCount;
				if (co.accessCount <= 0) {
					values.remove();
					count++;
				}
			}
		}
		return count;
	}

}
