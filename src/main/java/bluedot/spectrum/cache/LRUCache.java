package bluedot.spectrum.cache;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

/**
 * LRU(least recently used,最近最少使用置换算法)具体实现
 * @author longshu 2016年9月19日
 */
public class LRUCache<K, V> extends AbstractCache<K, V> {
	
	/**
	 * {@link AbstractCache#AbstractCache(int, long)}
	 */
	public LRUCache(int cacheSize) {
		super(cacheSize, 0);
	}

	/**
	 * {@link AbstractCache#AbstractCache(int, long)}
	 */
	public LRUCache(int cacheSize, long expire) {
		super(cacheSize, expire);
		/*
		 * linkedHash已经实现LRU算法 是通过双向链表来实现，当某个位置被命中，通过调整链表的指向将该位置调整到头位置，
		 * 新加入的内容直接放在链表头，如此一来，最近被命中的内容就向链表头移动，
		 * 需要替换时，链表最后的位置就是最近最少使用的位置
		 */
		this.cacheMap = new LinkedHashMap<K, CacheObject>(cacheSize + 1, 1.0f, true) {
			private static final long serialVersionUID = -5762666106199825347L;

			@Override
			protected boolean removeEldestEntry(Entry<K, CacheObject> eldest) {
				return LRUCache.this.removeEldestEntry(size());
			}
		};
	}

	/**
	 * Removes the eldest entry if current cache size exceed cache size.
	 */
	protected boolean removeEldestEntry(int currentSize) {
		if (cacheSize == 0) {
			return false;
		}
		return currentSize > cacheSize;
	}

	@Override
	protected int eliminateCache() {
		return 0;
	}

}
