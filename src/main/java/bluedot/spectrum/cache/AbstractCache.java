package bluedot.spectrum.cache;

import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 基本实现抽象类
 * @author longshu 2016年9月18日
 */
public abstract class AbstractCache<K, V> implements Cache<K, V> {

	protected Map<K, CacheObject> cacheMap;
	protected transient int cacheSize; // 缓存大小， 0 - 无限制
	protected long expire; // 默认过期时间, 0 -> 永不过期
	protected boolean existCustomExpire; // 是否设置默认过期时间

	private final ReentrantReadWriteLock cacheLock = new ReentrantReadWriteLock();
	private final Lock readLock = cacheLock.readLock();
	private final Lock writeLock = cacheLock.writeLock();

	/**
	 * @param cacheSize 缓存大小
	 * @param expire 过期时间
	 */
	public AbstractCache(int cacheSize, long expire) {
		this.cacheSize = cacheSize;
		this.expire = expire;
	}

	protected boolean isNeedClearExpiredObject() {
		return expire > 0 || existCustomExpire;
	}

	@Override
	public int size() {
		return cacheMap.size();
	}

	@Override
	public long getExpire() {
		return this.expire;
	}

	@Override
	public void put(K key, V value) {
		put(key, value, expire);

	}

	@Override
	public void put(K key, V value, long expire) {
		writeLock.lock();
		try {
			CacheObject co = new CacheObject(key, value, expire);
			if (expire != 0) {
				existCustomExpire = true;
			}
			if (isFull()) {
				eliminate();
			}
			cacheMap.put(key, co);
		} finally {
			writeLock.unlock();
		}
	}

	@Override
	public V get(K key) {
		readLock.lock();
		try {
			CacheObject co = cacheMap.get(key);
			if (co == null) {
				return null;
			}
			if (co.isExpired()) {
				cacheMap.remove(key);
				return null;
			}
			return co.getObject();
		} finally {
			readLock.unlock();
		}
	}

	@Override
	public final int eliminate() {
		writeLock.lock();
		try {
			return eliminateCache();
		} finally {
			writeLock.unlock();
		}
	}

	/**
	 * 淘汰对象具体实现
	 * @return
	 */
	protected abstract int eliminateCache();

	@Override
	public boolean isFull() {
		if (cacheSize == 0) {
			return false;
		}
		return cacheMap.size() >= cacheSize;
	}

	@Override
	public void remove(K key) {
		writeLock.lock();
		try {
			cacheMap.remove(key);
		} finally {
			writeLock.unlock();
		}

	}

	@Override
	public void clear() {
		writeLock.lock();
		try {
			cacheMap.clear();
		} finally {
			writeLock.unlock();
		}
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public boolean contains(K key) {
		return cacheMap.containsKey(key);
	}

	/**
	 * 缓存对象
	 */
	class CacheObject {
		final K key;
		final V cacheObject;
		long lastAccess; // 最后访问时间
		long accessCount; // 访问次数
		long ttl; // 对象存活时间<time-to-live)

		CacheObject(K key, V value, long ttl) {
			this.key = key;
			this.cacheObject = value;
			this.ttl = ttl;
			this.lastAccess = System.currentTimeMillis();
		}

		/**
		 * 是否过期
		 * @return
		 */
		boolean isExpired() {
			if (ttl == 0) {
				return false;
			}
			boolean flag = lastAccess + ttl < System.currentTimeMillis();
			return flag;
		}

		V getObject() {
			accessCount++;
			lastAccess = System.currentTimeMillis();
			return cacheObject;
		}
	}

}
