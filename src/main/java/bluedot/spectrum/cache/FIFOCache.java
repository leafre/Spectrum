package bluedot.spectrum.cache;

import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * FIFO(first in first out,先进先出置换算法)具体实现
 * @author longshu 2016年9月18日
 */
public class FIFOCache<K, V> extends AbstractCache<K, V> {

	/**
	 * {@link AbstractCache#AbstractCache(int, long)}
	 */
	public FIFOCache(int cacheSize) {
		this(cacheSize, 0);
	}

	/**
	 * {@link AbstractCache#AbstractCache(int, long)}
	 */
	public FIFOCache(int cacheSize, long expire) {
		super(cacheSize, expire);
		cacheMap = new LinkedHashMap<K, CacheObject>(cacheSize + 1);
	}

	@Override
	protected int eliminateCache() {
		int count = 0;
		K firstKey = null;

		Iterator<CacheObject> iterator = cacheMap.values().iterator();
		while (iterator.hasNext()) {
			CacheObject cacheObject = iterator.next();

			if (cacheObject.isExpired()) {
				iterator.remove();
				count++;
			} else {
				if (firstKey == null)
					firstKey = cacheObject.key;
			}
		}

		if (firstKey != null && isFull()) {// 删除过期对象还是满,继续删除链表第一个
			cacheMap.remove(firstKey);
		}

		return count;
	}

}
