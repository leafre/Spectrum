package bluedot.spectrum.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.log4j.Logger;

import bluedot.spectrum.dao.cfg.SqlResult;
import bluedot.spectrum.dao.cfg.SqlResult.SqlData;
import bluedot.spectrum.exception.DaoException;
import bluedot.spectrum.utils.DBUtils;
import bluedot.spectrum.utils.TxQueryRunner;

/**
 * BaseDao实现类
 * @author longshu 2016年8月19日
 */
public class BaseDaoMySQLImpl implements BaseDao {
	private static Logger logger = Logger.getLogger(BaseDaoMySQLImpl.class);
	private QueryRunner qr = new TxQueryRunner();
	private int premarykey = 0;
	private int flag = -2;
	private String keySql = "SELECT LAST_INSERT_ID()";

	/**
	 * 得到自增主键值
	 * 获取自增主键的部分要和它上一条插入语句处于同一个connection内，
	 * 所以要开启事务，否则有可能会插入时连接池所给的连接对象突然在得自增主键时变了，
	 * 就得不到自增主键了！！！
	 */
	public int getPremarykeyValue() {
		if (flag == 0) {
			Number key = null;
			try {
				key = qr.query(keySql, new ScalarHandler<Number>());
				flag = 3;
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
			}
			logger.debug("premarykey:" + key);
			if (key != null) {
				premarykey = key.intValue();
			} else {
				logger.error("这条插入语句无法得到自增主键");
				throw new RuntimeException("这条插入语句无法得到自增主键");
			}
			return premarykey;
		} else {
			if (flag == 1) {
				logger.error("上一条语句为删除语句，不允许得到自增主键");
				throw new RuntimeException("上一条语句为删除语句，不允许得到自增主键");
			} else if (flag == 2) {
				logger.error("上一条语句为更新语句，不允许得到自增主键");
				throw new RuntimeException("上一条语句为更新语句，不允许得到自增主键");
			} else if (flag == 3) {
				logger.error("上一条语句为查询语句，不允许得到自增主键");
				throw new RuntimeException("上一条语句为查询语句，不允许得到自增主键");
			} else {
				logger.error("出现未知bug，请稍后再试");
				throw new RuntimeException("出现为止bug，请稍后再试");
			}
		}
	}

	@Override
	public int add(String operate, Map<String, Object> params) throws DaoException {
		flag = 0;
		SqlData sqlData = SqlResult.getSqlData(operate, params);
		String sql = sqlData.getSql();
		try {
			int count = qr.update(sql, sqlData.getParList().toArray());
			return count;
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
			throw new DaoException(e);
		}
	}

	@Override
	public int delete(String operation, Map<String, Object> params) throws DaoException {
		flag = 1;
		return update(operation, params);
	}

	@Override
	public int update(String operate, Map<String, Object> params) throws DaoException {
		flag = 2;
		SqlData sqlData = SqlResult.getSqlData(operate, params);
		String sql = sqlData.getSql();
		if (sql.indexOf("where") == -1) {
			throw new DaoException(new DaoException("删除/更新操作不能没有条件，请检查参数或上层操作"));
		}
		try {
			int count = qr.update(sql, sqlData.getParList().toArray());
			return count;
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
			throw new DaoException(e);
		}
	}

	@Override
	public List<Map<String, Object>> query(String operate, Map<String, Object> params) throws DaoException {
		flag = 3;
		SqlData sqlData = SqlResult.getSqlData(operate, params);
		String sql = sqlData.getSql();
		List<Map<String, Object>> list = null;
		try {
			list = qr.query(sql, new MapListHandler(), sqlData.getParList().toArray());
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		return list;
	}

	@Override
	public List<Map<String, Object>> query(String operate, Map<String, Object> params, String strWhere)
			throws DaoException {
		flag = 3;
		SqlData sqlData = SqlResult.getSqlData(operate, params);
		sqlData.ChangeByConditions(strWhere);
		String sql = sqlData.getSql();
		List<Map<String, Object>> list = null;
		try {
			list = qr.query(sql, new MapListHandler(), sqlData.getParList().toArray());
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		return list;
	}

	/**
	 * 回滚事务
	 */
	public void rollback() {
		try {
			DBUtils.rollbackTransaction();
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Override
	public void transaction() {
		try {
			DBUtils.beginTransaction();// 开启事务
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Override
	public void commit() {
		try {
			DBUtils.commitTransaction();// 提交事务
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}

}
