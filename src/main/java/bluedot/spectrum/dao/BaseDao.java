package bluedot.spectrum.dao;

import java.util.List;
import java.util.Map;

import bluedot.spectrum.exception.DaoException;

/**
 * @author longshu 2016年8月19日
 */
public interface BaseDao {

	/**
	 * 获取自增主键
	 * @return
	 */
	public int getPremarykeyValue();
	/**
	 * 添加到数据库
	 * @param operation
	 * @param params
	 * @return 数据库操作返回值
	 * @throws DaoException
	 */
	public int add(String operation, Map<String, Object> params) throws DaoException;

	/**
	 * 删除记录
	 * @param operation
	 * @param params
	 * @return 数据库操作返回值
	 * @throws DaoException
	 */
	public int delete(String operation, Map<String, Object> params) throws DaoException;

	/**
	 * 更新记录
	 * @param operation
	 * @param params
	 * @return 数据库操作返回值
	 * @throws DaoException
	 */
	public int update(String operation, Map<String, Object> params) throws DaoException;

	/**
	 * 查询,如果要分页分页信息在params中获取
	 * @param operation
	 * @param params 查询条件
	 * @return 查询结果
	 * @throws DaoException
	 */
	public List<Map<String, Object>> query(String operation, Map<String, Object> params) throws DaoException;

	/**
	 * 查询，根据用户提供外传where子句替换sql，自动生成sql和修改参数
	 * 如：提供子句为，where userID = ? ,则替换原配置文件解析得到的sql中的where后的内容
	 * 提供子句为：limit(0,20) ,则在原配置文件得到的sql语句中后添加上去
	 * 
	 * @param operation
	 * @param params
	 * @param strWhere
	 * @return
	 * @throws DaoException
	 */
	public List<Map<String, Object>> query(String operation, Map<String, Object> params, String strWhere)
			throws DaoException;

	/**
	 * 开启事务,Service来控制
	 */
	public void transaction();

	/**
	 * 提交事务,Service来控制
	 */
	public void commit();

	/**
	 * 回滚事务,Service来控制
	 */
	public void rollback();

}
