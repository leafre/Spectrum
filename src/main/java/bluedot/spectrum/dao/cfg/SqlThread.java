package bluedot.spectrum.dao.cfg;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;

import bluedot.spectrum.exception.DaoException;

class SqlThread extends Thread {
	private CountDownLatch threadsSignal;// 识别线程执行个数
	// 执行获取方法需要的参数
	private String tem;
	private Opetem opetem;
	private Map<String, Object> map;
	private String operation;
	private List<String> message;
	private List<Object> parList = new ArrayList<Object>();
	private static Logger logger = Logger.getLogger(SqlThread.class);
	private String result;// 当前线程执行后返回的获取的参数

	public SqlThread(CountDownLatch threadsSignal, String tem, Opetem opetem,
			Map<String, Object> map,String operation,List<String> message) {
		super();
		this.threadsSignal = threadsSignal;
		this.tem = tem;
		this.opetem = opetem;
		this.map = map;
		this.operation = operation;
		this.message = message;
	}

	public List<String> getMessage() {
		return message;
	}

	public CountDownLatch getThreadsSignal() {
		return threadsSignal;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<Object> getParList() {
		if (parList.size() == 0)
			return null;
		else
			return parList;
	}

	// column替换信息的获取
	protected String replaceColumn() throws DaoException {
		StringBuffer strcol = new StringBuffer();
		if (tem.equals("add")) {
			if (opetem.getColList().size() != 0) {
				strcol.append("(");
				for (Opetem.Column col : opetem.getColList()) {
					if (map.get(col.getColumn()) != null) {
						strcol.append(col.getColumn() + ",");
					}
				}
				strcol.deleteCharAt(strcol.length() - 1);
				strcol.append(")");
			}
			return strcol.toString();
		} else if (tem.equals("update")) {
			for (Opetem.Column col : opetem.getColList()) {
				Object par = map.get(col.getColumn());
				if (par != null) {
					strcol.append(col.getColumn() + "=?,");
					parList.add(par);
				}
			}
			try {
				return strcol.substring(0, strcol.length() - 1);
			} catch (java.lang.StringIndexOutOfBoundsException e) {
				logger.error(operation+"service传入的参数丢失");
				throw new DaoException(operation+"service传入的参数丢失");
			}
		} else if (tem.equals("delete")) {
			return "";
		} else if (tem.equals("select")) {
			if (opetem.getColList().isEmpty()) {
				throw new DaoException(operation+"配置文件未配出column列，不允许的操作");
			}
			for (Opetem.Column col : opetem.getColList()) {
				if (col.getColumn().equals("*")) {
					strcol.append(col.getColumn() + ",");
					break;
				} else {
					strcol.append(col.getColumn() + ",");
				}
			}
			return strcol.substring(0, strcol.length() - 1);
		} else {
			String message = operation+"未定义[" + tem + "]节点";
			logger.error(message);
			throw new DaoException(message);
		}
	}

	// tabel替换信息的获取
	protected String replaceTabel() throws DaoException {
		if (opetem.getTable().getMany() == null) {
			logger.debug("table---> 单表");
			return opetem.getTable().getTablename();
		} else if (opetem.getTable().getMany().equals("true")) {
			StringBuffer tabelbuff = new StringBuffer();
			tabelbuff.append(opetem.getTable().getTablename());
			for (Opetem.Table.Join join : opetem.getTable().getJoinList()) {
				if (join.getOperation().equals("1")) {
					tabelbuff.append(" left join " + join.getTablename())
							.append(" on ");
				} else if (join.getOperation().equals("2")) {
					tabelbuff.append(" right join " + join.getTablename())
							.append(" on ");
				} else if (join.getOperation().equals("3")) {
					tabelbuff.append(" join " + join.getTablename()).append(
							" on ");
				} else {
					String message = operation+"配置文件错误，错误的join->operation信息";
					logger.error(message);
					throw new DaoException(message);
				}
				if (join.getFtabel() == null) {
					Joindeal(join, opetem.getTable().getTablename(), tabelbuff);
				} else {
					Joindeal(join, join.getFtabel(), tabelbuff);
				}
			}
			// logger.debug("table--->" + tabelbuff.toString());
			return tabelbuff.toString();
		} else {
			String message = operation+"配置文件错误，错误的table->many信息";
			logger.error(message);
			throw new DaoException(message);
		}
	}

	// conditions替换信息的获取
	protected String replaceConditions() throws DaoException {
		StringBuffer strcon = new StringBuffer();
		if (tem.equals("add")) {
			if (opetem.getColList().size() != 0) {
				for (int i1 = 0; i1 < opetem.getColList().size(); i1++) {
					Object par = map.get(opetem.getColList().get(i1)
							.getColumn());
					if (par != null) {
						strcon.append("?,");
						parList.add(par);
					}
				}
				try {
					return strcon.toString().substring(0, strcon.length() - 1);
				} catch (java.lang.StringIndexOutOfBoundsException e) {
					logger.error(operation+"service传入的参数丢失");
					throw new DaoException(operation+"service传入的参数丢失");
				}
			} else {
				for (int i1 = 0; i1 < opetem.getConList().size(); i1++) {
					Object par = map.get(opetem.getConList().get(i1)
							.getConditions());
					if (par != null) {
						strcon.append("?,");
						parList.add(par);
					}
				}
				try {
					return strcon.toString().substring(0, strcon.length() - 1);
				} catch (java.lang.StringIndexOutOfBoundsException e) {
					logger.error(operation+"service传入的参数丢失");
					throw new DaoException(operation+"service传入的参数丢失");
				}
			}
		} else {
			String con;
			if (opetem.getConList().size() == 0) {
				return null;
			} else {
				boolean bl = false;
				int flag = 0;
				for (int i1 = 0; i1 < opetem.getConList().size(); i1++) {
					con = opetem.getConList().get(i1).getConditions();
					Object par = map.get(con);
					if (con.indexOf(".") != -1) {
						con = con.split("\\.", 2)[1];
						if (map.get(con) != null) {
							par = map.get(con);
						}
					}
					if (par != null) {
						bl = true;
						flag++;
						if (flag == 1) {
							strcon.append(
									opetem.getConList().get(i1).getConditions())
									.append(" "
											+ opetem.getConList().get(i1)
													.getOperator())
									.append(" ? ");
							parList.add(par);
						} else {
							strcon.append(
									opetem.getConList().get(i1).getConnector())
									.append(" ")
									.append(opetem.getConList().get(i1)
											.getConditions())
									.append(" "
											+ opetem.getConList().get(i1)
													.getOperator())
									.append(" ? ");
							parList.add(par);
						}
					}
				}
				if (!bl) {
					return null;
				} else {
					return strcon.toString();
				}
			}
		}
	}

	// table多表链接是替换信息的补充
	public  void Joindeal(Opetem.Table.Join join, String ftabel,
			StringBuffer tabelbuff) throws DaoException {
		String Fkey = join.getFkey(), Pid = join.getPid();
		if (Fkey.indexOf(",") != -1) {
			if (Pid.indexOf(",") == -1) {
				String message = operation+"配置文件错误，错误的join->Fkey或Pid信息";
				logger.error(message);
				throw new DaoException(message);
			}
			String[] Fkeys = Fkey.split(",");
			String[] Pids = Pid.split(",");
			for (int i = 0; i < Fkeys.length; i++) {
				tabelbuff.append(ftabel).append(".").append(Fkeys[i])
						.append(" = ").append(join.getTablename()).append(".")
						.append(Pids[i] + " and ");
			}
			tabelbuff.delete(tabelbuff.length() - 5, tabelbuff.length());
		} else {
			tabelbuff.append(ftabel).append(".").append(Fkey).append(" = ")
					.append(join.getTablename()).append(".").append(Pid);
		}
	}
}
