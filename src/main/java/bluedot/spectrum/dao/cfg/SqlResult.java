package bluedot.spectrum.dao.cfg;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;

import bluedot.spectrum.exception.DaoException;

public class SqlResult {
	static int threadNum = 3;
	private static Logger logger = Logger.getLogger(SqlResult.class);
	
	static {
		SqlPars.parser();
	}

	public static void preParser() { // for static { }
	}

	public static synchronized SqlData getSqlData(String operation, Map<String, Object> map)
			throws DaoException {
		// 初始化
		CountDownLatch threadSignal = new CountDownLatch(threadNum);
		Opetem opetem = null;
		List<Object> parList = new ArrayList<Object>();
		opetem = SqlOpe.sqlMap.get(operation);
		 List<String> message = new ArrayList<String>();
		String tem = null;
		String sqlTem;
		// 操作选择
		try {
			tem = opetem.getTem();
		} catch (NullPointerException e) {
			String msg = "配置文件错误，未找到符合传入的[" + operation + "]的配置文件";
			logger.error(msg);
			throw new DaoException(msg);
		}
		if (tem.equals("add")) {
			sqlTem = SqlTemplate.getAdd();
		} else if (tem.equals("delete")) {
			sqlTem = SqlTemplate.getDelete();
		} else if (tem.equals("update")) {
			sqlTem = SqlTemplate.getUpdate();
		} else if (tem.equals("select")) {
			sqlTem = SqlTemplate.getSelect();
		} else {
			String msg = operation+"配置文件错误,错误的: opetem-->tem信息";
			logger.error(msg);
			throw new DaoException(msg);
		}
		/**
		 * 线程1，得到column替换信息
		 */
		SqlThread t1 = new SqlThread(threadSignal, tem, opetem, map,operation,message) {
			public void run() {
				try {
					this.setResult(replaceColumn());
				} catch (DaoException e) {
					this.getMessage().add(e.getMessage());
				}
				this.getThreadsSignal().countDown();
			}
		};
		/**
		 * 线程2，得到table替换信息
		 */
		SqlThread t2 = new SqlThread(threadSignal, tem, opetem, map,operation,message) {
			public void run() {
				try {
					this.setResult(replaceTabel());
				} catch (DaoException e) {
					this.getMessage().add(e.getMessage());
				}
				this.getThreadsSignal().countDown();
			}
		};
		/**
		 * 线程3，得到conditions替换信息
		 */
		SqlThread t3 = null;
		if (map != null) {
			t3 = new SqlThread(threadSignal, tem, opetem, map,operation,message) {
				public void run() {
					try {
						this.setResult(replaceConditions());
					} catch (DaoException e) {
						this.getMessage().add(e.getMessage());
					}
					this.getThreadsSignal().countDown();
				}
			};
			t3.start();// 线程3开始
		} else {
			threadSignal.countDown();
		}
		t1.start();// 线程1开始
		t2.start();// 线程2开始
		try {
			threadSignal.await();// 等待线程同步
			checkException(message);//捕获子线程异常
			// column替换，parList参数获取
			sqlTem = sqlTem.replaceFirst("#column#", t1.getResult());
			if (t1.getParList() != null)
				parList.addAll(t1.getParList());
			// table替换，parList参数获取
			sqlTem = sqlTem.replaceFirst("#table#", t2.getResult());// table替换
			if (t2.getParList() != null)
				parList.addAll(t2.getParList());
			// conditions替换，parList参数获取
			if (t3 != null) {
				if (t3.getResult() != null) {
					sqlTem = sqlTem.replaceFirst("#conditions#", t3.getResult());
					if (t3.getParList() != null)
						parList.addAll(t3.getParList());
				} else
					sqlTem = sqlTem.replaceFirst(" where #conditions#", "");
			} else {
				sqlTem = sqlTem.substring(0, sqlTem.length() - 18);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		logger.debug("传入map-->:" + map);
		logger.debug("生成SQL-->:[" + sqlTem + "]");
		logger.debug("对应参数-->:" + parList);
		return new SqlData(sqlTem, parList, map);
	}
	
	public static synchronized void checkException(List<String> message) throws DaoException{
		if(message.size()!=0){
			logger.error(message);
			throw new DaoException(message.toString());
		}
	}

	public static class SqlData implements Serializable {
		private static final long serialVersionUID = -2152077889888313888L;
		private String sql;
		private List<Object> parList;
		private Map<String, Object> map;

		public SqlData() {
		}

		public SqlData(String sql, List<Object> parList, Map<String, Object> map) {
			super();
			this.sql = sql;
			this.parList = parList;
			this.map = map;
		}

		/**
		 * 提供根据外传where子句替换sql的方法，并自动从原map中选择需要的数据替换现有参数。
		 * 
		 * @param strWhere
		 * @return
		 */
		public SqlData ChangeByConditions(String strWhere) {
			if (strWhere.indexOf("where") != -1) {
				Object[] keys1 = getKey(this.getSql());
				Object[] keys2 = getKey(strWhere);
				for (Object key1 : keys1) {
					this.parList.remove(this.map.get(key1));
				}
				for (Object key2 : keys2) {
					this.parList.add(this.map.get(key2));
				}
				this.sql = (replaceStr(this.sql, strWhere));
			} else {
				this.sql = (this.getSql() + strWhere);
			}
			return this;

		}

		private String replaceStr(String str, String strWhere) {
			String reStr[] = str.split("where");
			String reStrWhere[] = strWhere.split("where");
			return reStr[0] + "where" + reStrWhere[1];
		}

		private Object[] getKey(String strWhere) {
			String reStr[] = strWhere.split("where");
			String reCon[] = reStr[1].split(" ", 0);
			List<String> keyList = new ArrayList<String>();
			for (String str11 : reCon) {
				String[] con = str11.split("=");
				for (String str22 : con) {
					if (!(str22.equals("") || str22.equals("?")
							|| str22.equals("and") || str22.equals("or") || str22
								.equals(","))) {
						logger.debug("condition->" + str22);
						keyList.add(str22);
					}
				}
			}
			return keyList.toArray();
		}

		public String getSql() {
			return sql;
		}

		public List<Object> getParList() {
			return parList;
		}

	}
}
