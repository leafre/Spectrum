package bluedot.spectrum.dao.cfg;

import java.util.List;


public class Opetem {
	private String tem;
	private int order;
	private List<Column> colList;
	private List<Condition> conList;
	private Table table;

	
	@Override
	public String toString() {
		return "Opetem [tem=" + tem + ", order=" + order + ", colList="
				+ colList + ", conList=" + conList + ", table=" + table + "]";
	}
	public Table getTable() {
		return table;
	}
	public void setTable(Table tabel) {
		this.table = tabel;
	}
	public List<Condition> getConList() {
		return conList;
	}
	public void setConList(List<Condition> conList) {
		this.conList = conList;
	}
	public List<Column> getColList() {
		return colList;
	}
	public void setColList(List<Column> colList) {
		this.colList = colList;
	}

	public String getTem() {
		return tem;
	}
	public void setTem(String tem) {
		this.tem = tem;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}	
	
	class Table{

		private String many = null;
		private String tablename;
		private List<Join> joinList = null;
			
		public String getMany() {
			return many;
		}
		public void setMany(String many) {
			this.many = many;
		}
		public String getTablename() {
			return tablename;
		}
		public void setTablename(String tabelname) {
			this.tablename = tabelname;
		}
		public List<Join> getJoinList() {
			return joinList;
		}
		public void setJoinList(List<Join> joinList) {
			this.joinList = joinList;
		}


		class Join{
			private String tablename;
			private String fkey;
			private String pid;
			private String operation;
			private String ftable;

			public String getFtabel() {
				return ftable;
			}
			public void setFtable(String ftable) {
				this.ftable = ftable;
			}
			public String getTablename() {
				return tablename;
			}
			public void setTablename(String tabelname) {
				this.tablename = tabelname;
			}
			public String getFkey() {
				return fkey;
			}
			public void setFkey(String fkey) {
				this.fkey = fkey;
			}
			public String getPid() {
				return pid;
			}
			public void setPid(String pid) {
				this.pid = pid;
			}
			public String getOperation() {
				return operation;
			}
			public void setOperation(String operation) {
				this.operation = operation;
			}
		}
	}
	
	class Column{
		private String column;
		private String type = "all";
		
		
		@Override
		public String toString() {
			return "Column [column=" + column + ", type=" + type + "]";
		}
		public String getColumn() {
			return column;
		}
		public void setColumn(String column) {
			this.column = column;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		
	}
	
	class Condition{
		private String conditions;
		private String type;
		private String connector;
		private String operator;
		
		
		@Override
		public String toString() {
			return "Condition [conditions=" + conditions + ", type=" + type
					+ ", connector=" + connector + ", operator=" + operator
					+ "]";
		}
		public String getOperator() {
			return operator;
		}
		public void setOperator(String operator) {
			this.operator = operator;
		}
		public String getConnector() {
			return connector;
		}
		public void setConnector(String connector) {
			this.connector = connector;
		}
		public String getConditions() {
			return conditions;
		}
		public void setConditions(String conditions) {
			this.conditions = conditions;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
	}
}
