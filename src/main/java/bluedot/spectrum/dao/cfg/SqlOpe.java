package bluedot.spectrum.dao.cfg;
import java.util.HashMap;
import java.util.Map;


public class SqlOpe {
	static Map<String, Opetem> sqlMap = new HashMap<String, Opetem>();

	public static Map<String, Opetem> getSqlMap() {
		return sqlMap;
	}

	public static Opetem getSqlOpetem(String key) {
		return sqlMap.get(key);
	}
	
	public static void setSqlMap(Map<String, Opetem> sqlMap) {
		SqlOpe.sqlMap = sqlMap;
	}

}
