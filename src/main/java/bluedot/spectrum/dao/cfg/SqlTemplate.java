package bluedot.spectrum.dao.cfg;

import java.io.Serializable;

public class SqlTemplate implements Serializable{
	private static final long serialVersionUID = 1417773254324096651L;
	private static String add;
	private static String delete;
	private static String update;
	private static String select;

	public static String getAdd() {
		return add;
	}

	public static void setAdd(String add) {
		SqlTemplate.add = add;
	}

	public static String getDelete() {
		return delete;
	}

	public static void setDelete(String delete) {
		SqlTemplate.delete = delete;
	}

	public static String getUpdate() {
		return update;
	}

	public static void setUpdate(String update) {
		SqlTemplate.update = update;
	}

	public static String getSelect() {
		return select;
	}

	public static void setSelect(String select) {
		SqlTemplate.select = select;
	}
}