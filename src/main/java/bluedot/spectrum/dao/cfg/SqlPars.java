package bluedot.spectrum.dao.cfg;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class SqlPars {
	private static Logger logger = Logger.getLogger(SqlPars.class);
	private static String configFile = "/sqlConfig.xml";

	public static void reload() {
		synchronized (SqlOpe.sqlMap) {
			parser();
		}
	}

	public static void parser() {
		SAXReader saxReader = new SAXReader();
		URL url = SqlPars.class.getResource(configFile);
		logger.debug("configFile:" + url);
		Document document;
		try {
			document = saxReader.read(url);
		} catch (DocumentException e) {
			throw new RuntimeException("解析[" + configFile + "]失败!\n" + e);
		}
		Element root = document.getRootElement();
		// 获取sqltem节点
		@SuppressWarnings("unchecked")
		List<Element> eleSqls = root.elements("sqltem");
		for (Element eleSql : eleSqls) {
			SqlTemplate.setAdd(eleSql.element("add").getStringValue());
			SqlTemplate.setDelete(eleSql.element("delete").getStringValue());
			SqlTemplate.setUpdate(eleSql.element("update").getStringValue());
			SqlTemplate.setSelect(eleSql.element("select").getStringValue());
		}

		// 获取operation节点
		@SuppressWarnings("unchecked")
		List<Element> eleoperations = root.elements("operation");
		Map<String, Opetem> sqlMap = new HashMap<String, Opetem>();
		for (Element eleoperation : eleoperations) {
			Element eleopetem = eleoperation.element("opetem");
			Opetem opetem = new Opetem();
			List<Opetem.Condition> conList = new ArrayList<Opetem.Condition>();
			List<Opetem.Column> colList = new ArrayList<Opetem.Column>();
			opetem.setTem(eleopetem.attributeValue("tem"));
			opetem.setOrder(Integer.valueOf(eleopetem.attributeValue("order")));
			@SuppressWarnings("unchecked")
			List<Element> eleparms = eleopetem.elements();
			Element eletable = eleopetem.element("table");
			Opetem.Table table = opetem.new Table();
			List<Opetem.Table.Join> joinList = new ArrayList<Opetem.Table.Join>();
			table.setTablename(eletable.attributeValue("tablename"));
			table.setMany(eletable.attributeValue("many"));
			if (table.getMany() != null) {
				@SuppressWarnings("unchecked")
				List<Element> elejoins = eletable.elements("join");
				for (Element elejoin : elejoins) {
					Opetem.Table.Join join = table.new Join();
					join.setTablename(elejoin.attributeValue("tablename"));
					join.setFkey(elejoin.attributeValue("fkey"));
					join.setPid(elejoin.attributeValue("pid"));
					join.setOperation(elejoin.attributeValue("operation"));
					join.setFtable(elejoin.attributeValue("ftable"));
					joinList.add(join);
				}
				table.setJoinList(joinList);
			}
			opetem.setTable(table);
			for (Element eleparm : eleparms) {
				if (eleparm.getName().equals("column")) {
					Opetem.Column col = opetem.new Column();
					col.setColumn(eleparm.getStringValue().trim());
					colList.add(col);
				}
				if (eleparm.getName().equals("conditions")) {
					Opetem.Condition con = opetem.new Condition();
					con.setConditions(eleparm.getStringValue().trim());
					con.setOperator(eleparm.attributeValue("operator"));
					con.setConnector(eleparm.attributeValue("connector"));
					conList.add(con);
				}
			}
			opetem.setColList(colList);
			opetem.setConList(conList);

			sqlMap.put(eleoperation.attributeValue("name"), opetem);
			SqlOpe.setSqlMap(sqlMap);
		}
	}

}
