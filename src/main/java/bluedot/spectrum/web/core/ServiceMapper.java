package bluedot.spectrum.web.core;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import bluedot.spectrum.utils.StringUtils;

/**
 * 解析 service.xml,得到映射关系
 * @author longshu 2016年8月12日
 */
public class ServiceMapper {
	private static Logger logger = Logger.getLogger(ServiceMapper.class);
	private static String configFile = "/service.xml";
	// key为ServiceConfig的name
	private static Map<String, ServiceConfig> services = new HashMap<String, ServiceConfig>();

	static {
		ServiceMapper.parser();
	}

	public static void preParser() { // for static { }
	}

	/**
	 * 获取ServiceConfig
	 * @param name ServiceConfig的name
	 * @return ServiceConfig
	 */
	public static ServiceConfig getServiceConfig(String name) {
		return services.get(name);
	}

	/**
	 * 重新加载配置文件
	 */
	public static void reload() {
		logger.info("reload");
		synchronized (services) {
			services.clear();
			parser();
		}
	}

	/**
	 * 解析service.xml配置文件,初始化ServiceMapper
	 */
	@SuppressWarnings("unchecked")
	public static void parser() {
		SAXReader saxReader = new SAXReader();
		URL url = ServiceMapper.class.getResource(configFile);
		Document document;
		try {
			logger.debug("configFile:" + url.getFile());
			document = saxReader.read(url);
		} catch (DocumentException e) {
			throw new RuntimeException("解析[" + url.getPath() + "]失败!\n" + e);
		}
		Element root = document.getRootElement();
		// 获取service节点
		List<Element> eleServices = root.elements("service");
		for (Element eleService : eleServices) {
			String idStr = eleService.attributeValue("id");
			String name = eleService.attributeValue("name");
			String className = eleService.attributeValue("class");
			if (StringUtils.isBlank(idStr) || StringUtils.isBlank(name) || StringUtils.isBlank(className)) {
				logger.error("id,name和class都不能为空!\n" + eleService.asXML());
				// continue;
				break;
			}
			ServiceConfig service = new ServiceConfig(Integer.valueOf(idStr), name, className);
			String methodName = eleService.attributeValue("method");
			String operate = eleService.attributeValue("operate");
			if (null != methodName && !methodName.isEmpty()) {
				service.setMethodName(methodName);
			}
			if (null != operate && !operate.isEmpty()) {
				service.setOperate(operate);
			}
			// 获取service的result
			List<Element> results = eleService.elements("result");
			addResult(service, results);
			// 将service添加到map
			services.put(service.getName(), service);
		}
		logger.info("Service Size:" + services.size());
	}

	/**
	 * @param service
	 * @param results
	 */
	private static void addResult(ServiceConfig service, List<Element> results) {
		for (Element res : results) {
			ServiceResultConfig result = new ServiceResultConfig();
			String name = res.attributeValue("name");
			String type = res.attributeValue("type");
			if (null != name && !name.isEmpty()) {
				result.setName(name);
			}
			if (null != type && !type.isEmpty()) {
				result.setType(type);
			}
			result.setLocation(res.getStringValue());
			// 添加到service
			service.addResultConfig(result);
		}
	}

}
