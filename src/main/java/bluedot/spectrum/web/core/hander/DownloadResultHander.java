package bluedot.spectrum.web.core.hander;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import bluedot.spectrum.utils.EncryptDecrypt;
import bluedot.spectrum.utils.StringUtils;
import bluedot.spectrum.web.core.CoreServlet;

/**
 * 下载文件处理类
 * @author longshu 2016年10月13日
 */
public class DownloadResultHander extends ResultHander {

	public DownloadResultHander(HttpServletRequest request, HttpServletResponse response) {
		super(request, response);
	}

	/**
	 * 下载文件
	 * @param downFilePath 文件全路径
	 * @param downFileName 文件名
	 */
	public void download(String downFilePath, String downFileName) throws IOException, ServletException {
		if (StringUtils.isBlank(downFilePath)) {// 文件路径为空
			CoreServlet.notfound(request, response);
			return;
		}
		File downFile = new File(downFilePath);
		if (!downFile.exists() || !downFile.isFile()) {
			CoreServlet.notfound(request, response);
			return;
		}

		setHeader(downFileName);

		FileInputStream input = new FileInputStream(downFile);
		try {
			OutputStream output = response.getOutputStream();
			IOUtils.copy(input, output);
		} finally {
			IOUtils.closeQuietly(input);
		}
	}

	/**
	 * 设置下载头信息
	 * @param downFileName
	 * @throws IOException
	 */
	private void setHeader(String downFileName) throws IOException {
		String contentType = request.getSession().getServletContext().getMimeType(downFileName);// 通过文件名称获取MIME类型
		downFileName = downFileNameEncoding(downFileName, request);// 文件名编码
		logger.debug("downFileName=" + downFileName);
		// 设置头
		response.setHeader("Content-Type", contentType);
		response.setHeader("Content-Disposition", "attachment;filename=" + downFileName);
	}

	// 用来对下载的文件名称进行编码
	private String downFileNameEncoding(String filename, HttpServletRequest request) throws IOException {
		String agent = request.getHeader("User-Agent"); // 获取浏览器
		String encoding = request.getCharacterEncoding();
		if (agent.equalsIgnoreCase("firefox")) {
			filename = "=?" + encoding + "?B?" + EncryptDecrypt.encodeBASE64(filename) + "?=";
		} else {
			filename = URLEncoder.encode(filename, encoding);
		}
		return filename;
	}

}
