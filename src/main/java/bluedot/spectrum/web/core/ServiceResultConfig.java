package bluedot.spectrum.web.core;

import java.io.Serializable;

/**
 * 对应service.xml的result节点
 * @author longshu 2016年8月12日
 */
public class ServiceResultConfig implements Serializable {
	private static final long serialVersionUID = -653913662212037403L;
	/**
	 * 处理类型
	 */
	public static final String TYPE_REDIRECT = "redirect";// 重定向
	public static final String TYPE_DISPATCHER = "dispatcher";// 转发
	public static final String TYPE_UPLOAD = "upload";
	public static final String TYPE_DOWNLOAD = "download";
	public static final String TYPE_AJAX = "ajax";
	public static final String TYPE_VERIFYCODE = "verifycode";// 验证码
	public static final String TYPE_EMAIL = "Email";// 邮件发送
	public static final String UPDATE_USER_SESSION = "updateUserSession";
	/**
	 * Action方法执行结果类型
	 */
	public static final String SUCCESS = "success";
	public static final String FAILED = "failed";
	public static final String ERROR = "ERROR";
	public static final String LOGIN = "login";
	public static final String EXIT = "exit";
	public static final String AJAX = TYPE_AJAX;
	public static final String UPLOAD = TYPE_UPLOAD;

	private String name = SUCCESS;// 执行结果类型
	private String type = TYPE_DISPATCHER;// 跳转类型
	private String location;// 跳转的目标

	public ServiceResultConfig() {
	}

	public ServiceResultConfig(String name, String type, String location) {
		this.name = name;
		this.type = type;
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Result [name=" + name + ", type=" + type + ", location=" + location + "]";
	}

}
