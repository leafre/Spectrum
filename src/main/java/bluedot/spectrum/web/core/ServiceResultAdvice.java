package bluedot.spectrum.web.core;

/**
 * 获取Service的方法执行后的结果的接口
 * @author longshu 2016年8月12日
 */
public interface ServiceResultAdvice {
	public String getResult();
}
