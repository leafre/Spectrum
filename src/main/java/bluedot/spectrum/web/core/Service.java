package bluedot.spectrum.web.core;

import java.util.Map;

import bluedot.spectrum.exception.ServiceException;

/**
 * Service的基类,构造方法作为Servlet的接口
 * @author longshu 2016年8月12日
 */
public abstract class Service {

	public Service() {
	}

	/**
	 * Service统一的构造方法
	 * @param name service.xml中的name和视图对应
	 * @param params request入口的参数数组
	 * @param data 返回给Servlet的出口参数
	 * @throws ServiceException
	 */
	public Service(String name, Map<String, Object[]> params, Map<String, Object> data) throws ServiceException {
	}
}
