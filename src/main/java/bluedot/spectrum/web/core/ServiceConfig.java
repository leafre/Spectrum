package bluedot.spectrum.web.core;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 对应service.xml的service节点
 * @author longshu 2016年8月12日
 */
public class ServiceConfig implements Serializable {
	private static final long serialVersionUID = 9155176332319817291L;
	private int id;
	private String name;
	private String className;
	private String methodName = "service";
	private String operate;// 可选参数,配置SQL的操作
	// key为ResultConfig的name
	private Map<String, ServiceResultConfig> results = new HashMap<String, ServiceResultConfig>();

	public ServiceConfig() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ServiceConfig(int id, String name, String className) {
		this.id = id;
		this.name = name;
		this.className = className;
	}

	public ServiceConfig(String name, String className, String methodName) {
		this.name = name;
		this.className = className;
		this.methodName = methodName;
	}

	public ServiceResultConfig getResultConfig(String resultName) {
		return results.get(resultName);
	}

	public void addResultConfig(ServiceResultConfig result) {
		if (null != result)
			results.put(result.getName(), result);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * 可选参数,配置SQL的操作
	 */
	public String getOperate() {
		return operate;
	}

	public void setOperate(String operate) {
		this.operate = operate;
	}

	public Map<String, ServiceResultConfig> getResults() {
		return results;
	}

	public void setResults(Map<String, ServiceResultConfig> results) {
		this.results = results;
	}

	@Override
	public String toString() {
		return "ServiceConfig [id=" + id + ", name=" + name + ", className=" + className + ", methodName=" + methodName
				+ ", operate=" + operate + ", results=" + results + "]";
	}

}
