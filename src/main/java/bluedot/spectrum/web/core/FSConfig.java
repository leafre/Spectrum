package bluedot.spectrum.web.core;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import bluedot.spectrum.utils.FileUtils;
import bluedot.spectrum.utils.StringUtils;

/**
 * 文件路径配置
 * 如果fs.properties没有配，则默认在项目的WEB-INF/data下
 * @author longshu 2016年8月25日
 */
public class FSConfig {
	private static Logger logger = Logger.getLogger(FSConfig.class);
	private static Properties prop = new Properties();
	private static Map<String, File> dirs = new HashMap<String, File>();
	private static String[] values;

	static {
		values = new String[] { "userFavorite", "standSpectrum", "application", "backup", "plugin", "userPlugin",
				"temp", "template" };
		URL resource = FSConfig.class.getClassLoader().getResource("fs.properties");
		logger.debug(resource);
		if (null != resource) {
			try {
				InputStream in = resource.openStream();
				prop.load(new InputStreamReader(in, "UTF-8"));
				in.close();
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * 初始化
	 */
	public static void init(String baseDir) {
		if (StringUtils.isBlank(baseDir)) {
			baseDir = FileUtils.getTempDirectoryPath();
		}
		logger.info("baseDir:" + baseDir);
		StringBuilder sb = new StringBuilder();
		
		for (String value : values) {
			String _dir = prop.getProperty(value + "Dir");
			File dir = null;
			if (StringUtils.isBlank(_dir)) {
				dir = new File(baseDir, value);
			} else {
				dir = new File(_dir);
			}
			if (!dir.exists()) {
				dir.mkdirs();
			}
			dirs.put(value, dir);
			if (logger.isDebugEnabled()) {
				sb.append(value + ":" + dir.getAbsolutePath()).append("\n\n");
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug(sb.toString());
		}
	}

	public static File userFavorite() {
		return dirs.get("userFavorite");
	}

	public static File standSpectrum() {
		return dirs.get("standSpectrum");
	}

	public static File application() {
		return dirs.get("application");
	}

	public static File backup() {
		return dirs.get("backup");
	}

	public static File plugin() {
		return dirs.get("plugin");
	}

	public static File userPlugin() {
		return dirs.get("userPlugin");
	}

	public static File temp() {
		return dirs.get("temp");
	}

	public static File template() {
		return dirs.get("template");
	}

}
