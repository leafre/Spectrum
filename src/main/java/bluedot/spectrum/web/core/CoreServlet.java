package bluedot.spectrum.web.core;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import bluedot.spectrum.cache.Cache;
import bluedot.spectrum.cache.LFUCache;
import bluedot.spectrum.utils.DBUtils;
import bluedot.spectrum.utils.MultipartUtils;
import bluedot.spectrum.utils.StringUtils;
import bluedot.spectrum.utils.VerifyCode;
import bluedot.spectrum.web.core.hander.AjaxResultHander;
import bluedot.spectrum.web.core.hander.DownloadResultHander;

/**
 * 作为核心控制器,根据service.xml的name值匹配URL请求调用对应的Service
 * @author longshu 2016年8月12日
 */
public class CoreServlet extends HttpServlet {
	private static final long serialVersionUID = -3511963730889010211L;
	private static Logger logger;
	// K:path V:path去除.do/.action的service配置
	public static Cache<String, ServiceConfig> serviceConfigCache;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		// 初始化log4j日志文件保存路径
		initLog4j(getServletContext());
		logger = Logger.getLogger(this.getClass());
		logger.debug("log4j_path:" + System.getProperty("log4j_path"));
		logger.info("CoreServlet init");

		// 初始化FSConfig
		FSConfig.init(getServletContext().getRealPath("/WEB-INF/data/"));
		// 初始化缓存
		serviceConfigCache = new LFUCache<String, ServiceConfig>(100);
	}

	/**
	 * 初始化log4j日志文件保存路径
	 * @param clazz
	 * @param servletContext
	 */
	public static void initLog4j(ServletContext servletContext) {
		// 设置log4j日志文件保存路径
		String log4j_path = System.getProperty("log4j_path");
		if (null == log4j_path) {
			log4j_path = servletContext.getRealPath("/WEB-INF/log4j/");
			System.setProperty("log4j_path", log4j_path);
		}
	}

	@Override
	public void destroy() {
		super.destroy();
		DBUtils.destroy();// 关闭连接池
		logger.info("CoreServlet destroy");
	}

	@Override
	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		setCharacterEncoding(request, response);// 编码处理
		// 将请求映射到Service上
		ServiceConfig targetService = request2ServiceConfig(request);
		if (null == targetService) {
			notfound(request, response);
			return;
		}
		try {
			/**
			 * 请求 的所有数据(普通数据,验证码,上传信息)
			 */
			Map<String, String[]> params = getRequestParams(request, targetService);
			Map<String, Object> data = new HashMap<String, Object>();// 存放Service处理后的数据
			if (logger.isDebugEnabled())
				logger.debug("params:" + params);
			// 创建Service对象
			Service service = createService(targetService, params, data);

			if (logger.isDebugEnabled()) {
				@SuppressWarnings("unchecked")
				Collection<Object> querydata = (Collection<Object>) data.get("querydata");
				if (querydata != null) {
					if (querydata.size() > 100) {
						logger.debug("data:" + data.size() + ": querydata...");
					} else {
						logger.debug("data:" + data.size() + data);
					}
				} else {
					logger.debug("data:" + data.size() + data);
				}
			}

			// 得到要跳转的页面
			String result = getExecuteResult(service);
			logger.info("result:" + result);

			// 记录错误次数
			if (targetService.getName().equals("modifyPassword") || targetService.getName().equals("login")) {
				if (ServiceResultConfig.FAILED.equals(result)) {
					Integer errorCount = (Integer) data.get("errorCount");
					if (null != errorCount) {
						request.getSession().setAttribute("errorCount", errorCount);
					}
				}
			}
			// 处理结果
			handleResult(request, response, data, targetService.getResultConfig(result));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * @param request
	 * @param response
	 * @throws UnsupportedEncodingException
	 */
	public static void setCharacterEncoding(HttpServletRequest request, HttpServletResponse response) {
		try {
			if (null != request) {
				request.setCharacterEncoding("UTF-8");
			}
			if (null != response) {
				response.setCharacterEncoding("UTF-8");
				response.setContentType("text/html;charset=UTF-8");// 处理响应编码
			}
		} catch (UnsupportedEncodingException e) {
		}
	}

	/**
	 * 将请求映射到ServiceConfig
	 * @param request
	 * @return 
	 */
	public static ServiceConfig request2ServiceConfig(HttpServletRequest request) {
		String path = request.getRequestURI();
		// logger.debug("path:" + path);

		ServiceConfig cache = serviceConfigCache.get(path);
		if (null != cache)
			return cache;

		// 只处理action或do结尾的请求
		// if (StringUtils.endsWithAny(path, ".do", ".action")) {
		// 获取请求名
		String reqName = path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf("."));
		cache = ServiceMapper.getServiceConfig(reqName);
		serviceConfigCache.put(path, cache);// 加入到缓存
		return cache;
		// }
		// return null;
	}

	/**
	 * 获取请求的数据
	 * @param request
	 * @param targetService
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Map<String, String[]> getRequestParams(HttpServletRequest request, ServiceConfig targetService) {
		Map<String, String[]> results = new HashMap<String, String[]>();
		Map<String, String[]> params = request.getParameterMap();// 前台传入的普通数据
		Map<String, String[]> multiParams = null;// 复合表单数据

		// 在此完成上传
		ServiceResultConfig resultConfig = targetService.getResultConfig(ServiceResultConfig.SUCCESS);
		if (resultConfig == null) {
			resultConfig = targetService.getResultConfig(ServiceResultConfig.UPDATE_USER_SESSION);// 用于applyRole
		}
		if (null != resultConfig) {
			if (ServiceResultConfig.TYPE_UPLOAD.equals(resultConfig.getType())) {
				multiParams = MultipartUtils.handle(request);
				if (null != multiParams) {
					results.putAll(multiParams);
				}
			}
		}
		if (null != params) {
			results.putAll(params);
		}

		// 验证码处理
		String sessionVC = (String) request.getSession().getAttribute(VerifyCode.KEY_CODE);
		if (!StringUtils.isEmpty(sessionVC)) {
			results.put(VerifyCode.KEY_CODE, new String[] { sessionVC });
		}
		return results;
	}

	/**
	 * 反射创建Service对象
	 * @param targetService
	 * @param params
	 * @param data
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Service createService(ServiceConfig targetService, Map<String, String[]> params, Map<String, Object> data)
			throws Exception {
		Class<Service> clazz = (Class<Service>) Class.forName(targetService.getClassName());
		Class<?>[] parameterTypes = new Class[] { String.class, Map.class, Map.class, };
		Constructor<Service> constructor = clazz.getConstructor(parameterTypes);
		return constructor.newInstance(targetService.getName(), params, data);
	}

	/**
	 * 获取Service执行方法后的结果
	 * @param service
	 * @return
	 */
	private String getExecuteResult(Service service) {
		if (service instanceof ServiceResultAdvice) {
			return ((ServiceResultAdvice) service).getResult();
		}
		return ServiceResultConfig.ERROR;
	}

	/**
	 * 处理Service执行后的结果
	 * @param request
	 * @param response
	 * @param data
	 * @param result
	 * @throws ServletException
	 * @throws IOException
	 */
	private void handleResult(HttpServletRequest request, HttpServletResponse response, Map<String, Object> data,
			ServiceResultConfig result) throws ServletException, IOException {
		if (null == result)
			return;
		logger.debug(result);
		String type = result.getType();

		String resultName = result.getName();
		/*
		 *  保存数据
		 */
		// 处理验证码
		if (ServiceResultConfig.TYPE_VERIFYCODE.equals(type)) {
			request.getSession().setAttribute(VerifyCode.KEY_CODE, data.get(VerifyCode.KEY_CODE));
			BufferedImage image = (BufferedImage) data.get(VerifyCode.KEY_IMG);
			response.setContentType("image/jpeg");
			VerifyCode.output(image, response.getOutputStream());
			return;
		}
		if (ServiceResultConfig.TYPE_EMAIL.equals(resultName)) {
			request.getSession().setAttribute(VerifyCode.KEY_CODE, data.get(VerifyCode.KEY_CODE));
			// 用邮箱来保存发用邮件的时间
			String email = (String) data.get(ServiceResultConfig.TYPE_EMAIL);
			request.getSession().setAttribute(email, String.valueOf(new Date().getTime()));
			data.remove(VerifyCode.KEY_CODE);
		}
		if (ServiceResultConfig.LOGIN.equals(resultName)) {// 登陆特殊处理
			for (Entry<String, Object> entry : data.entrySet()) {
				request.getSession().setAttribute(entry.getKey(), entry.getValue());
			}
			request.getSession().removeAttribute(VerifyCode.KEY_CODE);
			Integer errorCount = (Integer) request.getSession().getAttribute("errorCount");
			if (errorCount != null) {
				request.getSession().setAttribute("errorCount", 0);
			}
		} else if (ServiceResultConfig.EXIT.equals(resultName)) {// 退出处理
			request.getSession().invalidate();
		} else if (ServiceResultConfig.UPDATE_USER_SESSION.equals(resultName)) {
			request.getSession().setAttribute("session_user", data.get("session_user"));
		}
		
		// 保存其他业务的所有数据
		if (!data.isEmpty()) {
			for (Entry<String, Object> entry : data.entrySet()) {
				request.setAttribute(entry.getKey(), entry.getValue());
			}
		}
		
		// 转发
		if (ServiceResultConfig.TYPE_DISPATCHER.equals(type)) {
			request.getRequestDispatcher(result.getLocation()).forward(request, response);
			return;
		}
		// 重定向
		if (ServiceResultConfig.TYPE_REDIRECT.equals(type)) {
			response.sendRedirect(request.getContextPath() + result.getLocation());
			return;
		}
		// 异步请求/JSON数据
		if (ServiceResultConfig.TYPE_AJAX.equals(type)) {
			new AjaxResultHander(request, response).doAjax(data);
			return;
		}
		if (ServiceResultConfig.TYPE_UPLOAD.equals(type)) {
			request.getRequestDispatcher(result.getLocation()).forward(request, response);
			return;
		}
		// 下载 ,downFilePath文件全路径,downFileName文件名称
		if (ServiceResultConfig.TYPE_DOWNLOAD.equals(type)) {
			String downFilePath = (String) data.get("downFilePath");
			String downFileName = (String) data.get("downFileName");
			logger.debug("downFilePath=" + downFilePath);
			new DownloadResultHander(request, response).download(downFilePath, downFileName);
			return;
		}
	}

	/**
	 * forward to 404
	 */
	public static void notfound(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		request.getRequestDispatcher("/WEB-INF/jsps/404.jsp").forward(request, response);
	}

}
