package bluedot.spectrum.web.filter;

import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * 
 * @author longshu 2016年9月19日
 */
public class ParameterRequestWrapper extends HttpServletRequestWrapper {

	private Map<String, String[]> parameters = new HashMap<String, String[]>();
	private HttpServletRequest request;

	/**
	 * @param request
	 */
	@SuppressWarnings("unchecked")
	public ParameterRequestWrapper(HttpServletRequest request) {
		super(request);
		this.request = request;
		parameters.putAll(request.getParameterMap());
	}

	/**
	 * 重载一个构造方法
	 * @param request
	 * @param extendParams
	 */
	public ParameterRequestWrapper(HttpServletRequest request, Map<String, Object> extendParams) {
		super(request);
		addAllParameters(extendParams);
	}

	@Override
	public String getParameter(String name) {
		String[] values = parameters.get(name);
		if (values == null || values.length == 0) {
			return null;
		}
		// 乱码处理
		if ("GET".equalsIgnoreCase(this.getMethod())) {
			try {
				return new String(values[0].getBytes("ISO-8859-1"), request.getCharacterEncoding());
			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException(e);
			}
		}
		return values[0];
	}

	@Override
	public String[] getParameterValues(String name) {
		return parameters.get(name);
	}

	@Override
	public Map<String, String[]> getParameterMap() {
		// 乱码处理
		if ("GET".equalsIgnoreCase(this.getMethod())) {
			for (String key : parameters.keySet()) {
				String[] values = parameters.get(key);
				if (null == values) {
					continue;
				}
				String newvalues[] = new String[values.length];
				for (int i = 0; i < values.length; i++) {
					try {
						String value = new String(values[i].getBytes("ISO-8859-1"), request.getCharacterEncoding());
						newvalues[i] = value;
					} catch (UnsupportedEncodingException e) {
						throw new RuntimeException(e);
					}
				}
				parameters.put(key, newvalues);
			}
		}
		return parameters;
	}

	@Override
	public Enumeration<String> getParameterNames() {
		return new Vector<String>(parameters.keySet()).elements();
	}

	/**
	 * 增加参数
	 * @param name
	 * @param value
	 */
	public void addParameter(String name, Object value) {
		if (value != null) {
			if (value instanceof String[]) {
				parameters.put(name, (String[]) value);
			} else if (value instanceof String) {
				parameters.put(name, new String[] { (String) value });
			} else {
				parameters.put(name, new String[] { String.valueOf(value) });
			}
		}
	}

	/**
	 * 增加多个参数
	 * @param otherParams
	 */
	public void addAllParameters(Map<String, Object> otherParams) {
		for (Map.Entry<String, Object> entry : otherParams.entrySet()) {
			addParameter(entry.getKey(), entry.getValue());
		}
	}

}
