package bluedot.spectrum.web.filter;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import bluedot.spectrum.bean.Function;
import bluedot.spectrum.bean.Log.LogLevel;
import bluedot.spectrum.bean.Log.LogType;
import bluedot.spectrum.bean.Role;
import bluedot.spectrum.bean.User;
import bluedot.spectrum.cache.Cache;
import bluedot.spectrum.cache.LFUCache;
import bluedot.spectrum.dao.cfg.SqlResult;
import bluedot.spectrum.service.LogService;
import bluedot.spectrum.service.QueryService;
import bluedot.spectrum.utils.BeanUtils;
import bluedot.spectrum.utils.StringUtils;
import bluedot.spectrum.utils.mail.SendEmailUtils;
import bluedot.spectrum.web.core.CoreServlet;
import bluedot.spectrum.web.core.ServiceConfig;
import bluedot.spectrum.web.core.ServiceMapper;

/**
 * 用户过滤器
 * @author longshu 2016年8月14日
 */
public class UserFilter implements Filter, Serializable {
	private static final long serialVersionUID = -4823482930009933384L;
	private Logger logger;
	private LogService logService;
	private QueryService queryService;
	// K:sessionID V:User
	private volatile Cache<String, User> sessionUserCache;
	// K: roleID , V: functions
	private volatile Map<Integer, List<Function>> roleFunction;

	public void init(FilterConfig fConfig) throws ServletException {
		logService = new LogService();
		queryService = new QueryService();
		sessionUserCache = new LFUCache<String, User>(128, 1000 * 60 * 1);// 1分钟的缓存时间
		roleFunction = new HashMap<Integer, List<Function>>();

		CoreServlet.initLog4j(fConfig.getServletContext());
		logger = Logger.getLogger(this.getClass());
		logger.info("log4j_path:" + System.getProperty("log4j_path"));
		logger.info("UserFilter init");

		// 预初始化 .xml映射
		ServiceMapper.preParser();
		SqlResult.preParser();
		// 邮件模板
		try {
			SendEmailUtils.init(null);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}

		getRoleFunction();
	}

	/**
	 * 获取角色权限
	 * @author longshu
	 */
	private void getRoleFunction() {
		List<Map<String, Object>> queryList = queryService.queryList("queryPower", null);
		if (null == queryList || queryList.isEmpty()) {
			logger.error("queryPower is null");
		} else {
			Integer roleID = 0;
			// roleID的功能点
			List<Function> functions = new LinkedList<Function>();
			for (Map<String, Object> map : queryList) {
				Integer rID = (Integer) map.get("roleID");
				if (rID == null) {
					String warn = "roleID 不能为空";
					logger.warn(warn);
					throw new IllegalArgumentException(warn);
				}
				if (roleID != rID) {
					if (0 != roleID) {
						roleFunction.put(roleID, functions);
						logger.debug("roleID: " + roleID + " , functions: " + functions.size());
						functions = new LinkedList<Function>();// 下一个roleID的功能点
					}
					roleID = rID;
				}
				functions.add(BeanUtils.mapToBean(map, Function.class));
			}
			roleFunction.put(roleID, functions);// 最后一个
			logger.debug("roleID: " + roleID + " , functions: " + functions.size());
			logger.debug("roleFunction: " + roleFunction.size());
		}
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		CoreServlet.setCharacterEncoding((HttpServletRequest) request, (HttpServletResponse) response);// 编码设置
		ParameterRequestWrapper reqWrapper = new ParameterRequestWrapper((HttpServletRequest) request);
		HttpServletResponse resp = (HttpServletResponse) response;
		/*
		 * 业务代码
		 */
		User session_user = preSessionUser(reqWrapper);
		boolean access = doPermission(reqWrapper, resp, session_user);
		// addLog(reqWrapper, session_user);

		logger.debug("access:" + access);
//		if (!access) {// 没权限
//			forbidden(reqWrapper, resp);
//			return;
//		}

		chain.doFilter(reqWrapper, response);
	}

	/**
	 * 处理session中的User数据
	 * @param requestWrapper
	 */
	private User preSessionUser(ParameterRequestWrapper requestWrapper) {
		User session_user = getSessionUser(requestWrapper);
		if (null != session_user) {
			requestWrapper.addParameter("session_userID", session_user.getUserID());
			requestWrapper.addParameter("session_username", session_user.getUsername());
			requestWrapper.addParameter("session_email", session_user.getEmail());
			requestWrapper.addParameter("session_roleID", session_user.getRoleID());
		}
		return session_user;
	}

	/**
	 * 获取Session中的User,先从缓存中获取,在从session获取
	 * @param request
	 * @return
	 */
	private User getSessionUser(HttpServletRequest request) {
		String sessionID = request.getSession().getId();
		User session_user = sessionUserCache.get(sessionID);
		if (null == session_user) {
			session_user = (User) request.getSession().getAttribute("session_user");
			if (null != session_user) {
				sessionUserCache.put(sessionID, session_user);
			}
		}
		return session_user;
	}

	/**
	 * 权限控制
	 * @param request
	 * @throws IOException 
	 * @throws ServletException 
	 */
	private boolean doPermission(ParameterRequestWrapper request, HttpServletResponse response, User session_user)
			throws ServletException, IOException {
		String path = request.getRequestURI();
		Integer roleID;
		logger.debug("path:" + path);
		if (StringUtils.endsWithAny(path, ".do", ".action")) {
			// 获取用户roleID
			if (null == session_user) {// 游客角色ID
				roleID = Role.TOURIST;
			} else {
				roleID = session_user.getRoleID();
			}

			ServiceConfig serviceConfig = CoreServlet.request2ServiceConfig(request);
			if (null == serviceConfig) {
				return true;
			}
			// 记录错误次数
			if (serviceConfig.getName().equals("modifyPassword") || serviceConfig.getName().equals("login")) {
				Integer errorCount = (Integer) request.getSession().getAttribute("errorCount");
				if (null != errorCount) {
					request.getSession().setAttribute("errorCount", errorCount);
					request.addParameter("errorCount", errorCount + "");
				}
			}

			List<Function> functions = roleFunction.get(roleID);// 角色的功能列表
			if (null == functions) {
				return false;
			}
			for (Function function : functions) {
				if (function.getFunctionID() == serviceConfig.getId())// 有权限访问
					return true;
			}
			return false;
		} else {
			return true;
		}
	}

	/**
	 *  forward to 403
	 */
	public void forbidden(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		request.getRequestDispatcher("/WEB-INF/jsps/403.jsp").forward(request, response);
	}

	public void destroy() {
		logger.info("UserFilter destroy");
	}

	/**
	 * 记录用户的日志
	 * @param request
	 */
	private void addLog(HttpServletRequest request, User session_user) {
		if (null == session_user)
			return;

		String path = request.getRequestURI();
		ServiceConfig serviceConfig = CoreServlet.serviceConfigCache.get(path);

		if (null != serviceConfig) {
			Map<String, Object> param = new HashMap<String, Object>();
			String ip = request.getRemoteAddr();
			String content = serviceConfig.getName();
			param.put("level", LogLevel.INFO);
			param.put("type", getLogType(session_user));
			param.put("userID", session_user.getUserID());
			param.put("ip", ip);
			param.put("functionID", serviceConfig.getId());
			param.put("content", content);
			logService.addLog(param);
			logger.debug(content);
		}
	}

	private String getLogType(User user) {
		int roleID = user.getRoleID();
		switch (roleID) {
		case Role.ADMIN:
			return LogType.ADMIN;
		case Role.SUPERADMIN:
			return LogType.ADMIN;
		case Role.LAB:
			return LogType.USER;
		case Role.USER:
			return LogType.USER;
		default:
			break;
		}
		return LogType.SYSTEM;
	}

}
