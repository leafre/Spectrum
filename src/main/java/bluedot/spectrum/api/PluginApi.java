package bluedot.spectrum.api;

import java.util.Map;

/**
 * 算法插件接口
 * @author longshu 2016年8月23日
 */
public interface PluginApi extends PluginType {

	public static final String cfgFile = "plugin";

	/**
	 * 算法插件的方法都通过这个方法传入数据和执行并返回数据
	 * @param data 传入的数据
	 * @param ext 扩展的数据
	 * @return 返回的数据
	 * @throws PluginApiException
	 */
	public Object execute(Object data, Map<String, Object> ext) throws PluginApiException;

	/**
	 * 获取插件类型{@link PluginType}
	 * @return
	 */
	public String getPluginType();

}
