package bluedot.spectrum.api;

/**
 * 算法插件异常
 * @author longshu 2016年8月23日
 */
public class PluginApiException extends Exception{
	private static final long serialVersionUID = -2363327889151997222L;

	public PluginApiException() {
	}

	public PluginApiException(String message, Throwable cause) {
		super(message, cause);
	}

	public PluginApiException(String message) {
		super(message);
	}

	public PluginApiException(Throwable cause) {
		super(cause);
	}

}
