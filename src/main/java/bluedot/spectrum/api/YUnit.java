package bluedot.spectrum.api;

/**
 * 光谱Y轴单位
 * @author longshu 2016年8月25日
 */
public interface YUnit {
	public static final String ABSORBANCE = "吸光度";
	public static final String ABSORBANCE_DIFFERENCE = "吸光度差值";
	public static final String PERCENT_LIGHT_TRANSMISSION_SPECTRUM = "%透光谱";
	public static final String PERCENT_REFLECTIVITY = "%反射率";
	public static final String PHOTOACOUSTIC = "光声";
	public static final String LOG = "Log(1/R)";
	public static final String KUBELKA_MUNK = "Kubelka-Munk";
}
