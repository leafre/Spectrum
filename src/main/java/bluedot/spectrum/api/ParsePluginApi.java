package bluedot.spectrum.api;

import java.util.Map;

/**
 * 光谱文件解析插件
 * @author longshu 2016年8月23日
 */
public interface ParsePluginApi extends PluginApi {
	
	/**
	 * 解析光谱文件
	 * @param path 文件路径
	 * @param ext 扩展数据
	 * @return 解析后生成的光谱文件数据
	 * @throws PluginApiException
	 */
	public SpectrumFile parse(String path, Map<String, Object> ext) throws PluginApiException;

}
