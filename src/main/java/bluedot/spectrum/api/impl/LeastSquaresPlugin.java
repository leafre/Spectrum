package bluedot.spectrum.api.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import bluedot.spectrum.api.DataProcessingPluginApi;
import bluedot.spectrum.api.PluginApiException;
import bluedot.spectrum.api.Point;
import bluedot.spectrum.api.SpectrumFile;

/**
 * 最小二乘算法 
 * @author 夏天的冬天
 */
public class LeastSquaresPlugin implements DataProcessingPluginApi {

	@Override
	public Object execute(Object data, Map<String, Object> ext)
			throws PluginApiException {
		if (data instanceof SpectrumFile) {
			return handleData((SpectrumFile) data, ext);
		}
		throw new PluginApiException("data必须是SpectrumFile类型");
	}

	@Override
	public String getPluginType() {
		return DATAPROCESSING;
	}

	@Override
	public SpectrumFile handleData(SpectrumFile spectrumFile,
			Map<String, Object> ext) throws PluginApiException {
		if (spectrumFile == null) {
			throw new PluginApiException("数据不符合光谱标准");
		}
		List<Point> points = spectrumFile.getPoints();
		List<Point> newPoints = LeastSquares(points);
		System.out.println("********************88");
		spectrumFile.setPoints(newPoints);
		return spectrumFile;
	}
	
	private List<Point> LeastSquares(List<Point> points){
		int size = points.size();
		double XP = 0,YP =0;
		double[] XY = new double[size];
		double[] XX = new double[size];
		double[] X1 = new double[size];
		Point point = null;
		for(int i=0;i<size;i++){
			point = points.get(i);
			double tx = point.getX();
			double ty = point.getY();
			X1[i] = tx;
			XP+=tx;
			YP+=ty;
			XY[i] = tx*ty;
			XX[i] = tx*tx;
		}
		XP /=size;
		YP/=size;
		double b = (getM(XY)-size*XP*YP)/(getM(XX)-size*XP*XP);
		double a = YP-b*XP;
		List<Point> ppp = new ArrayList<Point>();
		for(int i=0;i<size;i++){
			double x1 = X1[i];
			double y1 = a+b*x1;
			ppp.add(new Point(x1,y1));
		}
		return ppp;
	}
	
	private double getM(double[] S){
		double c = 0;
		for(double i:S){
			c+=i;
		}
		return c;
	}
}
