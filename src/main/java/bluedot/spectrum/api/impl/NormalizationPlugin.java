package bluedot.spectrum.api.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import bluedot.spectrum.api.PluginApiException;
import bluedot.spectrum.api.Point;
import bluedot.spectrum.api.PretreatmentPluginApi;
import bluedot.spectrum.api.SpectrumFile;

/**
 * 归一化处理：总强度归一化
 * 
 * @author 刘志超
 */
public class NormalizationPlugin implements PretreatmentPluginApi {

	@Override
	public SpectrumFile pretreatment(SpectrumFile spectrumFile, Map<String, Object> ext) throws PluginApiException {
		if (spectrumFile == null) {
			throw new PluginApiException("数据不符合光谱标准");
		}
		List<Point> data = spectrumFile.getPoints();

		// 定义通道的序号
		int pass = (int) Math.ceil(data.size() / 5);
		int pass2 = pass;
		int start = 0;
		int j = 1;
		for (int i = 0; i < 5; i++) { // 定义了5个通道
			List<Double> list = getSinglePass(data, start, pass2);// 得到通道内归一化后的数据
			for (int k = 0, temp = start; k < list.size(); k++, temp++) {
				data.get(temp).setY(list.get(k)); // 将归一化后的数据保存在data中
			}
			j++;
			start = pass2;
			System.out.println(pass2 + ",i=" + i + ",j=" + j);
			if (5 == j) {
				pass2 = data.size();
			} else {
				pass2 = pass * j;
			}
		}
		spectrumFile.setPoints(data);
		return spectrumFile;
	}

	private List<Double> getSinglePass(List<Point> data, int start, int pass) {
		double strength = 0;// 通道內总强度
		System.out.println(start + "," + pass + ",data," + data.size());
		for (int j = start; j < pass; j++) {
			strength += data.get(j).getY();
		}

		List<Double> pass1 = new ArrayList<Double>();// 保存每个点的相对强度

		double max = data.get(start).getY() / strength; // 通道内相对强度最大值
		double min = data.get(start).getY() / strength; // 通道内相对强度最小值
		for (int i = start; i < pass; i++) {
			double strength1_1 = data.get(i).getY() / strength;
			if (strength1_1 > max) {
				max = strength1_1;
			} else if (strength1_1 < min) {
				min = strength1_1;
			}
			pass1.add(strength1_1);
		}
		System.out.println("pass1," + pass1.size());
		for (int i = 0; i < pass - start; i++) {
			double strength1_1_1 = pass1.get(i); // 单一像素相对强度
			// double normalizationSgth = (strength1_1_1 - min) / (max - min);
			// // 归一化后的相对强度
			BigDecimal normalizationSgth = BigDecimal.valueOf(strength1_1_1 - min).divide(BigDecimal.valueOf(max - min),
					BigDecimal.ROUND_CEILING);
			BigDecimal y = normalizationSgth.multiply(BigDecimal.valueOf(data.get(i).getY()));
			// double y = data.get(i).getY() * normalizationSgth; //
			// 获得每个点归一化后的数值
			pass1.set(i, y.doubleValue()); // 将数值设到list集合
		}
		return pass1;
	}

	@Override
	public Object execute(Object data, Map<String, Object> ext) throws PluginApiException {
		if (data instanceof SpectrumFile) {
			return pretreatment((SpectrumFile) data, ext);
		}
		throw new PluginApiException("data必须是SpectrumFile类型");
	}

	@Override
	public String getPluginType() {
		return PRETREAMENT;
	}

}
