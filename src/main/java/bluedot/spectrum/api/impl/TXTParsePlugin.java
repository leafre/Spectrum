package bluedot.spectrum.api.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import bluedot.spectrum.api.ParsePluginApi;
import bluedot.spectrum.api.PluginApiException;
import bluedot.spectrum.api.Point;
import bluedot.spectrum.api.SpectrumFile;
import bluedot.spectrum.utils.StringUtils;

/**
 * 光谱文件解析插件(txt类型)
 * @author longshu 2016年9月22日
 */
public class TXTParsePlugin implements ParsePluginApi {

	@Override
	public Object execute(Object data, Map<String, Object> ext) throws PluginApiException {
		return null;
	}

	@Override
	public String getPluginType() {
		return SPECTRUM_PARSE;
	}

	@Override
	public SpectrumFile parse(String path, Map<String, Object> ext) throws PluginApiException {
		if (StringUtils.isBlank(path)) {
			throw new PluginApiException("[" + path + "] 错误的path路径");
		}
		if (!StringUtils.endsWith(path, ".txt")) {
			throw new PluginApiException("不能解析[" + path + "] 只能解析.txt文件");
		}
		File file = new File(path);
		SpectrumFile spectrumFile = new SpectrumFile();
		List<Point> points = new LinkedList<Point>();
		String split = null;// 分隔符
		if (file.isFile()) {
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new FileReader(file));
				String line = null;
				while ((line = reader.readLine()) != null) {
					if (null == split) {
						split = getSplit(line.trim());
					}
					points.add(parsePoint(line.trim(), split));
				}
				spectrumFile.setPoints(points);
				spectrumFile.setSpectrumName(file.getName());
				return spectrumFile;
			} catch (IOException e) {
				throw new PluginApiException(e);
			} finally {
				try {
					if (null != reader)
						reader.close();
				} catch (IOException e) {
				}
			}
		} else {
			throw new PluginApiException("[" + path + "] 错误的路径");
		}
	}

	/**
	 * 获取数据分隔符
	 * @param line
	 * @return
	 */
	private String getSplit(String line) {
		// TODO 改用正则表达式更正确
		if (line.contains(",")) {
			return ",";
		}
		if (line.contains("\t")) {
			return "\t";
		}
		return " ";
	}

	/**
	 * 解析每一行数据为点
	 * @param line
	 * @param split
	 * @return
	 * @throws PluginApiException 
	 */
	private Point parsePoint(String line, String split) throws PluginApiException {
		try {
			String[] points = line.split(split);
			return new Point(Double.parseDouble(points[0]), Double.parseDouble(points[1]));
		} catch (Exception e) {
			throw new PluginApiException(e);
		}
	}
}
