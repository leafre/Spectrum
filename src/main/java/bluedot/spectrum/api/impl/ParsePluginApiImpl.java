package bluedot.spectrum.api.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Map;

import bluedot.spectrum.api.ParsePluginApi;
import bluedot.spectrum.api.PluginApiException;
import bluedot.spectrum.api.SpectrumFile;
import bluedot.spectrum.utils.JsonUtils;
import bluedot.spectrum.utils.StringUtils;

/**
 * 默认的光谱文件解析插件(json类型)
 * @author longshu 2016年8月24日
 */
public class ParsePluginApiImpl implements ParsePluginApi {

	@Override
	public Object execute(Object data, Map<String, Object> ext) throws PluginApiException {
		return parse((String) data, ext);
	}

	@Override
	public SpectrumFile parse(String path, Map<String, Object> ext) throws PluginApiException {
		if (StringUtils.isBlank(path)) {
			throw new PluginApiException("[" + path + "] 错误的path路径");
		}
		if (!StringUtils.endsWith(path, ".json")) {
			throw new PluginApiException("不能解析[" + path + "] 只能解析.json文件");
		}
		File jsonFile = new File(path);
		if (jsonFile.isFile()) {
			InputStreamReader streamReader = null;
			try {
				streamReader = new InputStreamReader(new FileInputStream(jsonFile), Charset.forName("UTF-8"));
				return JsonUtils.Gson(false).fromJson(streamReader, SpectrumFile.class);
			} catch (Exception e) {
				throw new PluginApiException(e);
			} finally {
				if (null != streamReader) {
					try {
						streamReader.close();
					} catch (IOException e) {
					}
				}
			}
		} else {
			throw new PluginApiException("[" + path + "] 错误的路径");
		}
	}

	@Override
	public String getPluginType() {
		return SPECTRUM_PARSE;
	}

}
