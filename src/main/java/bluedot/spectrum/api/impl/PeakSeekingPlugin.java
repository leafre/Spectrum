package bluedot.spectrum.api.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import bluedot.spectrum.api.DataProcessingPluginApi;
import bluedot.spectrum.api.PluginApiException;
import bluedot.spectrum.api.Point;
import bluedot.spectrum.api.SpectrumFile;

/**
 * 标峰算法--采用动态规划精确计算
 * 
 * @author 夏天的冬天
 *
 */
public class PeakSeekingPlugin implements DataProcessingPluginApi {

	@Override
	public Object execute(Object data, Map<String, Object> ext)
			throws PluginApiException {
		if (data instanceof SpectrumFile) {
			return handleData((SpectrumFile) data, ext);
		}
		throw new PluginApiException("data必须是SpectrumFile类型");
	}

	@Override
	public String getPluginType() {
		return DATAPROCESSING;
	}

	@Override
	public SpectrumFile handleData(SpectrumFile spectrumFile,
			Map<String, Object> ext) throws PluginApiException {
		if (spectrumFile == null) {
			throw new PluginApiException("数据不符合光谱标准");
		}
		List<Point> points = spectrumFile.getPoints();
		List<Point> peaks = null;
		Double valueY;// 基线
		int sensitiveValue;//灵敏度
		try {
			valueY = Double.valueOf((String) ext.get("valueY"));
			//页面定下来后，有了基线、灵敏度的设置，这里记得取消注释↓
			//sensitiveValue = Integer.valueOf((String)ext.get("sensitiveValue"));
			//页面定下来后，有了基线、灵敏度的设置，这里记得取消注释↑
			} catch (Exception e) {
			// 这里用运行时，是因为基线和灵敏度都是有默认值的，哪怕用户不选。会出现异常，则表示是我们的代码写错了，没给默认值。回去改代码去
			throw new RuntimeException("请检查是否有基线和灵敏度！");
		}
		//页面定下来后，有了基线、灵敏度的设置，这里记得删掉↓
		sensitiveValue = 100;
		valueY = (double) 3000;
		//页面定下来后，有了基线、灵敏度的设置，这里记得删掉↑
		peaks = getPeak(points, valueY, sensitiveValue);
		spectrumFile.setPeaks(peaks);// 设置峰值
		return spectrumFile;
	}

	/**
	 * 得到峰值
	 * 
	 * @param original原始点的集合
	 *            。在这个点的范围内进行计算
	 * @param valueY
	 *            基线
	 * @param sensitive
	 *            灵敏度，与计算深度成反比
	 * @return
	 */
	private List<Point> getPeak(List<Point> original, double valueY,
			int sensitive) {
		int cont = original.size();
		int depth = 100 - sensitive + 1;// 计算深度
		if(depth<=0||depth>100){
			throw new RuntimeException("前台开发人员注意了：灵敏度取值设置错了。灵敏度：【1，100】");
		}
		List<Point> peaks = new ArrayList<Point>();
		for (int i = 0; i < cont; i++) {
			if (original.get(i).getY() < valueY) {
				continue;// 在基线下的点，不进入计算
			}
			// 采用动态规划计算，消耗资源数比函数计算大，但准确度是绝对的！
			if (Deal1(original, original.get(i), i, depth)) {
				peaks.add(original.get(i));
			}
		}
		return peaks;
	}

	/**
	 * 输入参数
	 * 
	 * @param original
	 *            ,原始点的集合。
	 * @param point
	 *            ，当前点，判断该点在该灵敏度下是否为峰
	 * @param index
	 *            ,当前点的坐标
	 * @param depth
	 *            ，计算深度，深度越大表示灵敏度越小，得到的点越少，计算越慢。深度越小表示灵敏度越大，得到的点越多，计算越快
	 * @return 是峰则返回true，否则返回false
	 */
	private boolean Deal1(List<Point> original, Point point, int index,
			int depth) {
		if (depth == 1) {
			Point left, right;
			left = getBorder(original, point, index, depth, 1);
			right = getBorder(original, point, index, depth, 2);
			return Deal2(left, right, point);
		} else {
			if (Deal1(original, point, index, depth - 1)) {
				Point left, right;
				left = getBorder(original, point, index, depth, 1);
				right = getBorder(original, point, index, depth, 2);
				if((left.getX()==point.getX()||left.getY()<original.get(index-depth+1).getY())&&(right.getX()==point.getX()||right.getY()<original.get(index+depth-1).getY()))
					return true;
				else
					return false;
			}
			return false;
		}
	}

	/**
	 * 简单判别三点内，是否为极值
	 */
	private boolean Deal2(Point left, Point right, Point point) {
		if (point.getY() > left.getY() && point.getY() > right.getY())
			return true;
		else
			return false;
	}

	/**
	 * 得到相邻点，falg=1表示左相邻，flag=2表示右相邻
	 */
	private Point getBorder(List<Point> original, Point point, int index,
			int depth, int flag) {
		if (flag == 1) {
			if (index - depth < 0) {
				return new Point(point.getX(), point.getY() - 1);
			} else {
				return original.get(index - depth);
			}
		} else if (flag == 2) {
			if (index + depth > original.size()) {
				return new Point(point.getX(), point.getY() - 1);
			} else {
				return original.get(index + depth);
			}
		} else {
			throw new RuntimeException("代码使用异常");
		}
	}
}
