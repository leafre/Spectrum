package bluedot.spectrum.api.impl;

import java.util.Map;

import bluedot.spectrum.api.AnalysisPluginApi;
import bluedot.spectrum.api.PluginApiException;
import bluedot.spectrum.api.SpectrumFile;

/**
 * 
 * @author longshu 2016年8月24日
 */
public class AnalysisPluginApiImpl implements AnalysisPluginApi{

	@Override
	public Object execute(Object data, Map<String, Object> ext) throws PluginApiException {
		return null;
	}

	@Override
	public Map<String, Object> analysis(SpectrumFile spectrumFile, Map<String, Object> ext) throws PluginApiException {
		return null;
	}

	@Override
	public String getPluginType() {
		return SPECTRUM_ANALYSIS;
	}

}
