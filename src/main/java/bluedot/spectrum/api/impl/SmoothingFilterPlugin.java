package bluedot.spectrum.api.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import bluedot.spectrum.api.PluginApiException;
import bluedot.spectrum.api.Point;
import bluedot.spectrum.api.PretreatmentPluginApi;
import bluedot.spectrum.api.SpectrumFile;

/**
 * 平滑处理
 * @author 刘志超
 */
public class SmoothingFilterPlugin implements PretreatmentPluginApi {

	public SpectrumFile pretreatment(SpectrumFile spectrumFile, Map<String, Object> ext) throws PluginApiException {

		if (spectrumFile == null) {
			throw new PluginApiException("光谱数据不能为空");
		}
		List<Point> data = spectrumFile.getPoints();
		List<Point> rn = new ArrayList<Point>();
		rn.add(data.get(0));
		for (int i = 1; i < data.size() - 1; i++) {
			/*
			 * ROUND_CEILING 接近正无穷大的舍入模式。 如果 BigDecimal 为正，则舍入行为与 ROUND_UP 相同;
			 * 如果为负，则舍入行为与 ROUND_DOWN 相同。 注意，此舍入模式始终不会减少计算值。
			 */
			// 取得三点的X轴上的值
			BigDecimal current = BigDecimal.valueOf(data.get(i).getX());
			BigDecimal preceed = BigDecimal.valueOf(data.get(i - 1).getX());
			BigDecimal last = BigDecimal.valueOf(data.get(i + 1).getX());
			BigDecimal res = current.add(preceed).add(last).divide(BigDecimal.valueOf(3), BigDecimal.ROUND_CEILING);
			data.get(i).setX(res.doubleValue());

			// 取得三点的Y轴上的值
			current = BigDecimal.valueOf(data.get(i).getY());
			preceed = BigDecimal.valueOf(data.get(i - 1).getY());
			last = BigDecimal.valueOf(data.get(i + 1).getY());
			res = current.add(preceed).add(last).divide(BigDecimal.valueOf(3), BigDecimal.ROUND_CEILING);
			data.get(i).setY(res.doubleValue());

			rn.add(data.get(i));
		}
		rn.add(data.get(data.size() - 1));
		spectrumFile.setPoints(rn);
		return spectrumFile;
	}

	@Override
	public Object execute(Object data, Map<String, Object> ext) throws PluginApiException {
		if (data instanceof SpectrumFile) {
			return pretreatment((SpectrumFile) data, ext);
		}
		throw new PluginApiException("data必须是SpectrumFile类型");
	}

	@Override
	public String getPluginType() {
		return PRETREAMENT;
	}

}
