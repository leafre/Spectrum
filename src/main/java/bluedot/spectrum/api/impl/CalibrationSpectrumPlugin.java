package bluedot.spectrum.api.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoints;

import bluedot.spectrum.api.DataProcessingPluginApi;
import bluedot.spectrum.api.PluginApiException;
import bluedot.spectrum.api.Point;
import bluedot.spectrum.api.SpectrumFile;

/**
 * 基线校正算法
 * @author 徐泽昆
 */
public class CalibrationSpectrumPlugin implements DataProcessingPluginApi {
	// 设置多项式最高次数
	final static int times = 5;


	@Override
	public Object execute(Object data, Map<String, Object> ext) throws PluginApiException {
		return null;
	}

	@Override
	public String getPluginType() {
		return DATAPROCESSING;
	}
	
	@Override
	public SpectrumFile handleData(SpectrumFile spectrumFile, Map<String, Object> ext) throws PluginApiException {
		//设置拟合次数
		//拟合次数大于1  会导致内存溢出（算法有问题，递归？）
		int count = 1;
		if (null == spectrumFile) {
			throw new PluginApiException("光谱文件不能为空");
		}
		if (spectrumFile.getPoints() == null || spectrumFile.getPoints().size()<=0) {
			throw new PluginApiException("光谱数据不能为空");
		}
		return getMultipleFittingList(spectrumFile, ext, count);
	}
	
	//进行一次拟合的过程
	private SpectrumFile handleDataTemp(SpectrumFile spectrumFile, Map<String, Object> ext) throws PluginApiException {
		List<Point> points = spectrumFile.getPoints();
		List<Point> data = new ArrayList<Point>();
		if (points != null && points.size() > 0) {
			final WeightedObservedPoints obs = new WeightedObservedPoints();

			for (Point d : points) {
				obs.add(d.getX(), d.getY());
			}
			// 设置拟合项数（5代表了 x的最高次数为5，有六项）
			final PolynomialCurveFitter fitter = PolynomialCurveFitter.create(times);
			// a0 + a1*X + a2*X*x + a3*x*x*x 这里coeff的第一个数为a0，以此类推
			final double[] coeff = fitter.fit(obs.toList());
			// 遍历所有的数据点，将曲线上的Y值赋给初始Y值
			for (Point point : points) {
				point.setY(getcurveValue(coeff, point.getX()));
				data.add(point);
			}
		}
		spectrumFile.setPoints(data);
		return spectrumFile;
	}
	
	// 该方法用于得到，多项式曲线上的Y值
	private double getcurveValue(double[] coeff, double xValue) {
		double curveValue = 0;
		for (int i = 0; i <= times; i++) {
			// a0 + a1*X + a2*X*x + a3*x*x*x+...+
			curveValue = (curveValue + coeff[i] * Math.pow(xValue, i));
		}
		return curveValue;
	}

	//多次拟合
	private SpectrumFile getMultipleFittingList(SpectrumFile spectrumFile, Map<String, Object> ext, int count)
			throws PluginApiException {
		if (count == 1) {
			return handleDataTemp(spectrumFile, ext);
		} else if(count>1) {
			
			return getMultipleFittingList(handleDataTemp(spectrumFile, ext), ext, count--);
		}
		else {
			throw new PluginApiException("拟合次数要为正整数");
		}
	}

}
