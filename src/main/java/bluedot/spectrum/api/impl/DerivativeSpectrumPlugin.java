package bluedot.spectrum.api.impl;

import java.util.Map;

import bluedot.spectrum.api.DataProcessingPluginApi;
import bluedot.spectrum.api.PluginApiException;
import bluedot.spectrum.api.SpectrumFile;

/**
 * 导数谱图算法（没有完成）
 * 下载MATLAB，调用其diff方法，得到对应的求导结果
 * @author 徐泽昆
 * @return 
 */
public class DerivativeSpectrumPlugin implements DataProcessingPluginApi {

	@Override
	public Object execute(Object data, Map<String, Object> ext) throws PluginApiException {
		return null;
	}

	@Override
	public String getPluginType() {
		return null;
	}

	@Override
	public SpectrumFile handleData(SpectrumFile spectrumFile, Map<String, Object> ext) throws PluginApiException {
		return null;
	}


}
