package bluedot.spectrum.api.impl;

import java.util.Map;

import bluedot.spectrum.api.PluginApiException;
import bluedot.spectrum.api.PretreatmentPluginApi;
import bluedot.spectrum.api.SpectrumFile;

/**
 * 预处理实现
 * @author longshu 2016年8月24日
 */
public class PretreatmentPluginApiImpl implements PretreatmentPluginApi {

	@Override
	public Object execute(Object data, Map<String, Object> ext) throws PluginApiException {
		return null;
	}

	@Override
	public SpectrumFile pretreatment(SpectrumFile spectrumFile, Map<String, Object> ext) throws PluginApiException {
		return null;
	}

	@Override
	public String getPluginType() {
		return PRETREAMENT;
	}

}
