package bluedot.spectrum.api;

import java.util.Map;

/**
 * 数据处理算法接口
 * @author longshu 2016年9月1日
 */
public interface DataProcessingPluginApi extends PluginApi {

	/**
	 * 处理光谱数据
	 * @param spectrumFile 光谱文件数据
	 * @param ext 扩展数据
	 * @return 处理完的光谱文件数据
	 */
	public SpectrumFile handleData(SpectrumFile spectrumFile, Map<String, Object> ext) throws PluginApiException;;

}
