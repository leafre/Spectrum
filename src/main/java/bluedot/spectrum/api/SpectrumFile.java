package bluedot.spectrum.api;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 光谱文件对应的类 
 * @author longshu 2016年8月25日
 */
public class SpectrumFile implements Serializable {
	private static final long serialVersionUID = -1053856434629893682L;

	private String spectrumName;// 光谱名称,如 敌敌畏
	private String spectrumType; // 光谱类型名称,比如拉曼
	private String xUnit = XUnit.WAVE_NUMBER;// 光谱x轴单位,不为空,默认 波数
	private String yUnit = YUnit.ABSORBANCE;// 光谱y轴单位,不为空,默认 吸光度
	private Double minY = Double.MAX_VALUE;// 最小Y值
	private Double maxY = Double.MIN_VALUE;// 最大Y值
	// 光谱文件扩展的信息,因为不是所有光谱都有
	private Map<String, String> extInfo = new HashMap<String, String>();
	private List<Point> points;// 光谱数据点
	private List<Point> peaks;// 峰值点

	public SpectrumFile() {
	}

	public SpectrumFile(String spectrumName, String spectrumType, List<Point> points) {
		this.spectrumName = spectrumName;
		this.spectrumType = spectrumType;
		this.points = points;
	}

	public String getSpectrumName() {
		return spectrumName;
	}

	public void setSpectrumName(String spectrumName) {
		this.spectrumName = spectrumName;
	}

	public String getSpectrumType() {
		return spectrumType;
	}

	public void setSpectrumType(String spectrumType) {
		this.spectrumType = spectrumType;
	}

	public String getxUnit() {
		return xUnit;
	}

	public void setxUnit(String xUnit) {
		this.xUnit = xUnit;
	}

	public String getyUnit() {
		return yUnit;
	}

	public void setyUnit(String yUnit) {
		this.yUnit = yUnit;
	}

	public void sort() {
		double min = points.get(0).getY(), max = min;

		for (Point point : points) {
			if (min > point.getY()) {
				min = point.getY();
			}
			if (max < point.getY()) {
				max = point.getY();
			}
		}
		this.minY = min;
		this.maxY = max;
	}

	public Double getMinY() {
		if (this.minY == Double.MAX_VALUE)
			sort();
		return minY;
	}

	public Double getMaxY() {
		if (this.maxY == Double.MIN_VALUE)
			sort();
		return maxY;
	}

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
		sort();
	}

	public List<Point> getPeaks() {
		return peaks;
	}

	public void setPeaks(List<Point> peaks) {
		this.peaks = peaks;
	}

	public Map<String, String> getExtInfo() {
		return extInfo;
	}

	public void setExtInfo(Map<String, String> extInfo) {
		this.extInfo = extInfo;
	}

	public String getExtInfo(String name) {
		return extInfo.get(name);
	}

	public void setExtInfo(String name, String value) {
		this.extInfo.put(name, value);
	}

	@Override
	public String toString() {
		StringBuilder buffer = new StringBuilder("SpectrumFile");
		buffer.append("{\n[spectrumName=" + spectrumName + ", spectrumType=" + spectrumType + ", xUnit=" + xUnit
				+ ", yUnit=" + yUnit + "]");
		buffer.append("\nextInfo=" + extInfo);
		buffer.append("\npoints=" + points);
		buffer.append("\npeaks=" + peaks + "\n}");
		return buffer.toString();
	}

}