package bluedot.spectrum.api;

import java.util.Map;

/**
 * 光谱预处理接口
 * @author longshu 2016年8月23日
 */
public interface PretreatmentPluginApi extends PluginApi {

	/**
	 * 预处理
	 * @param spectrumFile 光谱文件数据
	 * @param ext 扩展数据
	 * @return 预处理后的光谱文件数据
	 * @throws PluginApiException
	 */
	public SpectrumFile pretreatment(SpectrumFile spectrumFile, Map<String, Object> ext) throws PluginApiException;

}
