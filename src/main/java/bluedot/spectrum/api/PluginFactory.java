package bluedot.spectrum.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.log4j.Logger;

import bluedot.spectrum.utils.StringUtils;

/**
 * 获取PluginApi的实现工厂类
 * @author longshu 2016年8月23日
 */
public class PluginFactory {
	private static Logger logger = Logger.getLogger(PluginFactory.class);

	/**
	 * 给出插件jar/zip路径返回PluginApi实例
	 * 
	 * @param jarPath
	 *            包含了plugin配置文件且实现了PluginApi plugin文件内容如:
	 *            bluedot.spectrum.api.ParsePluginApi
	 * @return PluginApi实例
	 */
	public static PluginApi loadPlugin(String jarPath) throws PluginApiException {
		logger.debug(jarPath);
		if (StringUtils.isBlank(jarPath)) {
			throw new PluginApiException("jarPath不能为空");
		}
		return loadPlugin(new File(jarPath));
	}

	public static PluginApi loadPlugin(File jarFile) throws PluginApiException {
		if (jarFile.exists() && jarFile.isFile() && StringUtils.endsWithAny(jarFile.getName(), ".jar", ".zip")) {
			String className = null;
			try {
				URL jarUrl = jarFile.toURI().toURL();
				JarFile jarTargetFile = new JarFile(jarFile); // 得到jarFile文件对应的Jar对象
				JarEntry jarEntry = jarTargetFile.getJarEntry(PluginApi.cfgFile);// 得到cfgFile对应的文件实体
				InputStream is = jarTargetFile.getInputStream(jarEntry); // 得到文件实体的流
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"));
				className = reader.readLine();

				reader.close();
				is.close();
				jarTargetFile.close();

				logger.debug(className);
				URLClassLoader loader = URLClassLoader.newInstance(new URL[] { jarUrl });
				Class<?> clazz = loader.loadClass(className);
				Object instance = clazz.newInstance();
				if (instance instanceof PluginApi) {
					return (PluginApi) instance;
				}
				String msg = jarFile + "的" + clazz + "没有实现PluginApi";
				logger.error(msg);
				throw new PluginApiException(msg);
			} catch (Exception e) {
				logger.debug(e.getMessage(), e);
				throw new PluginApiException(e);
			}
		}
		String msg = "jarFile不能为空且必须是jar/zip文件";
		logger.error(msg);
		throw new PluginApiException(msg);
	}

	/**
	 * 插件已在classpath中,返回PluginApi实例
	 * 
	 * @param className
	 * @return
	 * @throws PluginApiException
	 */
	public static PluginApi getPluginByName(String className) throws PluginApiException {
		logger.debug(className);
		if (StringUtils.isBlank(className)) {
			throw new PluginApiException("className不能为空");
		}
		try {
			Class<?> clazz = Class.forName(className);
			return (PluginApi) getPlugin(clazz);
		} catch (ClassNotFoundException e) {
			logger.debug(e.getMessage(), e);
			throw new PluginApiException(e);
		}
	}

	public static <T> T getPlugin(Class<T> clazz) throws PluginApiException {
		if (null == clazz) {
			throw new PluginApiException("clazz不能为空");
		}
		try {
			T instance = clazz.newInstance();
			if (instance instanceof PluginApi) {
				return instance;
			}
			String msg = clazz + "没有实现PluginApi";
			logger.error(msg);
			throw new PluginApiException(msg);
		} catch (Exception e) {
			logger.debug(e.getMessage(), e);
			throw new PluginApiException(e);
		}
	}

}
