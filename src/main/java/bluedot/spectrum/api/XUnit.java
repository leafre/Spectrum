package bluedot.spectrum.api;

/**
 * 光谱X轴单位
 * @author longshu 2016年8月25日
 */
public interface XUnit {
	public static final String WAVE_NUMBER = "波数";
	public static final String NANOMETER = "纳米";
	public static final String MICRON = "微米";
}
