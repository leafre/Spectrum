package bluedot.spectrum.api;

/**
 * 算法插件类型
 * @author longshu 2016年8月23日
 */
public interface PluginType {
	public static final String PRETREAMENT = "预处理算法";
	public static final String DATAPROCESSING = "数据处理算法";
	public static final String SPECTRUM_PARSE = "光谱解析算法";
	public static final String SPECTRUM_ANALYSIS = "光谱分析算法";
}