package bluedot.spectrum.api;

import java.util.Map;

/**
 * 光谱数据分析数据分析接口
 * @author longshu 2016年8月23日
 */
public interface AnalysisPluginApi extends PluginApi {

	/**
	 * 光谱分析
	 * @param spectrumFile 光谱文件数据
	 * @param ext 扩展数据
	 * @return 分析完的数据
	 * @throws PluginApiException
	 */
	public Map<String, Object> analysis(SpectrumFile spectrumFile, Map<String, Object> ext) throws PluginApiException;

}
