package bluedot.spectrum.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Map;


import freemarker.cache.FileTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;
/**
 * 生成doc文件
 * @author 刘驭洲 2016年9月19日
 */
public class CreateWordUtils {
	private Configuration configuration = null;
	
	public CreateWordUtils(){
		configuration = new Configuration(new Version("2.3.0"));
		configuration.setDefaultEncoding("UTF-8");
	}
	/**
	 * 生成模板样式的doc文件
	 * @param dataMap 模板中需要填写的内容
	 * @param ext 附件内容 ：模板路径、模板名称、报告路径
	 * @throws Exception FileNotFoundException TemplateException IOException
	 */
	public void createWord(Map<String,Object> dataMap,Map<String,Object> ext) throws Exception{
		//configuration.setClassForTemplateLoading(this.getClass(), "模板路径");
		TemplateLoader templateLoader = null;
		//需要装填的模板
		String path = (String)ext.get("templateName");
		//文件路径
		templateLoader = new FileTemplateLoader(new File((String) ext.get("templatePath")));
		configuration.setTemplateLoader(templateLoader);
		Template  template = configuration.getTemplate(path,"UTF-8");
		//生成的文档路径
		File outFile = new File(""+ext.get("destPath"));
		//生成文件
		Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile)));
		template.process(dataMap, out);
	}
}
