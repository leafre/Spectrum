package bluedot.spectrum.utils;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * Base64图片转换
 * @author longshu 2016年10月12日
 */
@SuppressWarnings("restriction")
public class Base64Image implements Serializable {
	private static final long serialVersionUID = -4774089139795889577L;
	private static BASE64Encoder encoder = new BASE64Encoder();
	private static BASE64Decoder decoder = new BASE64Decoder();
	private static String header = "data:image/";

	private String str; // base64编码图片的字符串,包含头信息
	private String data;// 编码内容
	private String ext = ".jpg";// 图片的后缀名,如 .png

	public Base64Image(String str) {
		this.str = str;
		if (str.startsWith(header)) {
			ext = str.substring(str.indexOf("/") + 1, str.indexOf(";"));
			if ("jpeg".equals(ext)) {
				ext = ".jpg";
			} else if ("x-icon".equals(ext)) {
				ext = ".ico";
			} else {
				ext = "." + ext;
			}
			data = str.substring(str.lastIndexOf(",") + 1);
		} else {
			data = str;
		}
	}

	public void toImage(String destPath) throws IOException {
		if (null == data) {
			data = str.substring(str.lastIndexOf(",") + 1);
		}
		byte[] buffer = decoder.decodeBuffer(data);
		FileOutputStream out = new FileOutputStream(destPath);
		out.write(buffer);
		out.close();
	}

	public static String toBase64Data(String imgFile) throws IOException {
		FileInputStream in = new FileInputStream(imgFile);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		encoder.encode(in, out);
		in.close();
		out.close();
		return out.toString();
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}
}
