package bluedot.spectrum.utils;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * jdbc工具,c3p0创建连接池,事务支持
 * @author longshu 2016年8月17日
 */
public class DBUtils {
	// 配置文件的默认配置！要求你必须给出c3p0-config.xml
	private static ComboPooledDataSource dataSource = new ComboPooledDataSource();
	// 事务使用的连接,处理多线程问题
	private static ThreadLocal<Connection> tl = new ThreadLocal<Connection>();

	private DBUtils() {
	}

	/**
	 * 返回连接池对象
	 * @return dataSource
	 */
	public static DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * 获取连接,事务支持要在这之前调用 {@link #beginTransaction()}
	 * 然后成功调用{@link #commitTransaction()}
	 * 失败调用{@link #rollbackTransaction()}
	 * @return
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException {
		Connection connection = tl.get();
		if (connection != null) {
			return connection;
		}
		return dataSource.getConnection();
	}

	/**
	 * 开启事务。 1.获取一个Connection，2.保证dao中使用的Connection一致
	 * @throws SQLException
	 */
	public static void beginTransaction() throws SQLException {
		Connection connection = tl.get();
		if (connection != null)
			throw new SQLException("事务已开启!\n");
		connection = getConnection();
		connection.setAutoCommit(false);
		tl.set(connection);
	}

	/**
	 * 提交事务。使用beginTransaction的Connection。
	 * @throws SQLException
	 */
	public static void commitTransaction() throws SQLException {
		Connection connection = tl.get();// 获取当前线程的专用连接
		if (connection == null)
			throw new SQLException("事务未开启!\n");
		connection.commit();
		connection.close();
		tl.remove();// 移除连接
	}

	/**
	 * 回滚事务
	 * @throws SQLException
	 */
	public static void rollbackTransaction() throws SQLException {
		Connection connection = tl.get();
		if (connection == null)
			throw new SQLException("事务未开启!\n");
		connection.rollback();
		connection.close();
		tl.remove();// 移除连接
	}

	/**
	 * 释放连接回连接池
	 * @param con
	 * @throws SQLException
	 */
	public static void releaseConnection(Connection con) throws SQLException {
		Connection connection = tl.get();
		/*
		 * 判断是否开启了事务
		 */
		if (connection == null) // 说明没有开启事务,con不是事务使用的要关闭
			con.close();
		// connection!=null 有事务.判断参数连接是否与connection相等，!=说明连接不是事务的。
		if (connection != con)
			con.close();
	}

	/**
	 * 关闭连接池
	 */
	public static void destroy() {
		dataSource.close();
	}

}
