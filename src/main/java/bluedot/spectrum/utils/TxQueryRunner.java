package bluedot.spectrum.utils;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

public class TxQueryRunner extends QueryRunner {

	@Override
	public int[] batch(String sql, Object[][] params) throws SQLException {
		Connection con = DBUtils.getConnection();
		int[] result = super.batch(con, sql, params);
		DBUtils.releaseConnection(con);
		return result;
	}

	@Override
	public <T> T query(String sql, ResultSetHandler<T> rsh, Object... params)
			throws SQLException {
		Connection con = DBUtils.getConnection();
		T result = super.query(con, sql, rsh, params);
		DBUtils.releaseConnection(con);
		return result;
	}
	
	@Override
	public <T> T query(String sql, ResultSetHandler<T> rsh) throws SQLException {
		Connection con = DBUtils.getConnection();
		T result = super.query(con, sql, rsh);
		DBUtils.releaseConnection(con);
		return result;
	}

	@Override
	public int update(String sql) throws SQLException {
		Connection con = DBUtils.getConnection();
		int result = super.update(con, sql);
		DBUtils.releaseConnection(con);
		return result;
	}

	@Override
	public int update(String sql, Object param) throws SQLException {
		Connection con = DBUtils.getConnection();
		int result = super.update(con, sql, param);
		DBUtils.releaseConnection(con);
		return result;
	}

	@Override
	public int update(String sql, Object... params) throws SQLException {
		Connection con = DBUtils.getConnection();
		int result = super.update(con, sql, params);
		DBUtils.releaseConnection(con);
		return result;
	}
}
