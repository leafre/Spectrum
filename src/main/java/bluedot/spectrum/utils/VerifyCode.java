package bluedot.spectrum.utils;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

import javax.imageio.ImageIO;

/**
 * 验证码生成类
 */
public class VerifyCode {
	public static final String KEY_CODE = "session_verifycode";
	public static final String KEY_IMG = "verifycode_image";
	private int width = 65;// 验证码宽度
	private int height = 28;// 高度
	private int length = 6;// 验证码长度
	private static Random random = new Random();
	// {"宋体", "华文楷体", "黑体", "华文新魏", "华文隶书", "微软雅黑", "楷体_GB2312"}
	private String[] fontNames = { "宋体", "华文楷体", "黑体", "微软雅黑", "楷体_GB2312" };
	private String codes = "23456789abcdefghjkmnopqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ";
	private Color bgColor = new Color(255, 255, 255);
	private String text;

	public VerifyCode() {
	}

	/**
	 * 指定验证码大小
	 * @param width
	 * @param height
	 */
	public VerifyCode(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public VerifyCode(int width, int height, int length) {
		this.width = width;
		this.height = height;
		this.length = length;
	}

	private Color randomColor() {
		int red = random.nextInt(150);
		int green = random.nextInt(150);
		int blue = random.nextInt(150);
		return new Color(red, green, blue);
	}

	private Font randomFont() {
		int index = random.nextInt(fontNames.length);
		String fontName = fontNames[index];
		int style = random.nextInt(4);
		int size = random.nextInt(5) + 24;
		return new Font(fontName, style, size);
	}

	private void drawLine(BufferedImage image) {
		int num = 3;
		Graphics2D g2 = (Graphics2D) image.getGraphics();
		for (int i = 0; i < num; i++) {
			int x1 = random.nextInt(width);
			int y1 = random.nextInt(height);
			int x2 = random.nextInt(width);
			int y2 = random.nextInt(height);
			g2.setStroke(new BasicStroke(1.5F));
			g2.setColor(Color.BLUE);
			g2.drawLine(x1, y1, x2, y2);
		}
	}

	private char randomChar() {
		int index = random.nextInt(codes.length());
		return codes.charAt(index);
	}

	private BufferedImage createImage() {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = (Graphics2D) image.getGraphics();
		g2.setColor(this.bgColor);
		g2.fillRect(0, 0, width, height);
		return image;
	}

	public BufferedImage getImage() {
		BufferedImage image = createImage();
		Graphics2D g2 = (Graphics2D) image.getGraphics();
		StringBuilder sb = new StringBuilder();
		// 向图片中画length个字符
		for (int i = 0; i < length; i++) {
			String s = randomChar() + "";
			sb.append(s);
			float x = i * 1.0F * width / length;
			g2.setFont(randomFont());
			g2.setColor(randomColor());
			g2.drawString(s, x, height - 5);
		}
		this.text = sb.toString();

		return image;
	}

	/**
	 * 带干扰线
	 * @return
	 */
	public BufferedImage getImageWithLine() {
		BufferedImage image = getImage();
		drawLine(image);
		return image;
	}

	/**
	 * 获取验证码字符串
	 * @return 验证码字符串
	 */
	public String getText() {
		return text;
	}

	/**
	 * 获取指定长度的随机字符串
	 * @param length
	 * @return
	 */
	public String getRandomCode(int length) {
		if (length <= 0)
			length = this.length;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			sb.append(randomChar());
		}
		return sb.toString();
	}

	/**
	 * 保存验证码到指定输出流
	 * @param image
	 * @param out
	 * @throws IOException
	 */
	public static void output(BufferedImage image, OutputStream out) throws IOException {
		ImageIO.write(image, "JPEG", out);
	}

	/**
	 * 自定义验证码字符串
	 * @param codes
	 */
	public void setCodes(String codes) {
		this.codes = codes;
	}
}