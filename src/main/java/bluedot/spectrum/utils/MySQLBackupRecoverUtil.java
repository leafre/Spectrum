package bluedot.spectrum.utils;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import bluedot.spectrum.utils.mail.SendEmailUtils;

/**
 * MySQL数据库备份恢复工具
 * @author longshu 2016年10月7日
 */
public class MySQLBackupRecoverUtil {
	private static Logger logger = Logger.getLogger(MySQLBackupRecoverUtil.class);
	private static String mysql_home;
	private static boolean withhome = false;
	private static String host = "127.0.0.1";
	private static String database;
	private static String ignore_backuptable = "";// 忽略备份的表
	// private static String ignore_recovertable = "";// 忽略恢复的表
	private static String user = "root";
	private static String password = "root";

	private static Properties prop;
	private static Runtime runtime = Runtime.getRuntime();
	private static String backupBin = "mysqldump";
	private static String recoverBin = "mysql";

	static {
		init(null);
	}

	public static void main(String[] args) {
		String sql = "test/backup.sql";

		if (backup(sql)) {
			System.out.println("备份[" + database + "]成功");
		} else {
			System.out.println("备份[" + database + "]失败");
		}

		if (recover(sql)) {
			System.out.println("恢复[" + database + "]成功");
		} else {
			System.out.println("恢复[" + database + "]失败");
		}
	}

	public static String getDatabaseName() {
		return database;
	}

	/**
	 * 备份数据库
	 * @param sqlDest
	 * @return 
	 * @throws IOException
	 */
	public static boolean backup(File destSql) {
		logger.info(destSql.getAbsolutePath());
		Process process = null;
		try {
			if (!destSql.getParentFile().exists()) {
				destSql.getParentFile().mkdirs();
			}
			if (withhome) {
				process = runtime.exec(backupCommand(database), null, new File(mysql_home));
			} else {
				process = runtime.exec(backupCommand(database));
			}
			InputStream inputStream = process.getInputStream();
			FileOutputStream outputStream = new FileOutputStream(destSql);
			IOUtils.copy(inputStream, outputStream);
			closeIO(outputStream);
			closeIO(inputStream);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			return false;
		} finally {
			if (null != process) {
				process.destroy();
			}
		}
		return true;
	}

	/**
	 * 备份数据库
	 * @param sqlDest
	 * @return 
	 * @throws IOException
	 */
	public static boolean backup(String destSql) {
		return backup(new File(destSql));
	}

	/**
	 * 恢复数据库
	 * @param sqlFile
	 * @return 
	 * @throws IOException
	 */
	public static boolean recover(File sqlFile) {
		logger.info(sqlFile.getAbsolutePath());
		Process process = null;
		try {
			if (withhome) {
				process = runtime.exec(recoverCommand(database), null, new File(mysql_home));
			} else {
				process = runtime.exec(recoverCommand(database));
			}
			FileInputStream inputStream = new FileInputStream(sqlFile);
			OutputStream outputStream = process.getOutputStream();
			IOUtils.copy(inputStream, outputStream);
			closeIO(outputStream);
			closeIO(inputStream);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			return false;
		} finally {
			if (null != process) {
				process.destroy();
			}
		}
		return true;
	}

	/**
	 * 恢复数据库
	 * @param sqlPath
	 * @return 
	 * @throws IOException
	 */
	public static boolean recover(String sqlPath) {
		return recover(new File(sqlPath));
	}

	private static String backupCommand(String database) {
		String command = backupBin + " -h " + host + " -u" + user + " -p" + password + " " + database
				+ ignore_backuptable;
		logger.debug(command);
		return command;
	}

	private static String recoverCommand(String database) {
		String command = recoverBin + " -h " + host + " -u" + user + " -p" + password + " " + database;
		logger.debug(command);
		return command;
	}

	public static void closeIO(Closeable closeable) {
		if (null == closeable)
			return;
		try {
			closeable.close();
		} catch (IOException e) {
		}
	}

	public static void init(String configFile) {
		if (StringUtils.isBlank(configFile)) {
			configFile = "mysql.properties";
		}
		prop = new Properties();
		URL url = SendEmailUtils.class.getClassLoader().getResource(configFile);
		logger.debug(url.getFile());
		try {
			InputStream in = url.openStream();
			prop.load(new InputStreamReader(in, "UTF-8"));

			String temp = prop.getProperty("mysql_home").trim();
			if (!StringUtils.isBlank(temp)) {
				mysql_home = temp;
				withhome = true;// 后面处理空格中文
			}
			temp = prop.getProperty("host").trim();
			if (!StringUtils.isBlank(temp)) {
				host = temp;
			}
			temp = prop.getProperty("database");
			if (StringUtils.isBlank(temp)) {
				logger.warn("database不能为空!");
			}
			database = temp;
			temp = prop.getProperty("ignore_backuptable").trim();
			if (!StringUtils.isBlank(temp)) {
				logger.info("ignore_backuptable:" + temp);
				String[] tables = temp.split(",");
				for (String table : tables) {
					ignore_backuptable += " --ignore-table=" + database + "." + table;
				}
			}
			/*temp = prop.getProperty("ignore_recovertable").trim();
			if (!StringUtils.isBlank(temp)) {
				logger.info("ignore_recovertable:" + temp);
				String[] tables = temp.split(",");
				for (String table : tables) {
					ignore_recovertable += " --ignore-table=" + database + "." + table;
				}
			}*/
			temp = prop.getProperty("user");
			if (!StringUtils.isBlank(temp)) {
				user = temp;
			}
			temp = prop.getProperty("password");
			if (!StringUtils.isBlank(temp)) {
				password = temp;
			}
			in.close();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
