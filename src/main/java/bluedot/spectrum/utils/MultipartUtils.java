package bluedot.spectrum.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import bluedot.spectrum.web.core.FSConfig;

/**
 * 复合表单工具类,主要处理上传
 * @author 海松    2016年9月1日
 * @author longshu 2016年9月6日
 */
public class MultipartUtils {
	private static Logger logger = Logger.getLogger(MultipartUtils.class);
	private static int sizeThreshold = DiskFileItemFactory.DEFAULT_SIZE_THRESHOLD * 2;// 20K缓存
	private static int sizeMax = 1024 * 1024 * 10;// 限制整个表单大小为10M
	private static int fileSizeMax = 1024 * 1024 * 4;// 单文件大小限制4M
	private static String[] fileType = new String[] { ".jpg", ".jpeg", ".png", ".txt", ".json", ".xml", ".doc", ".docx",
			".zip", ".jar" };// 允许上传类型
	private static ServletFileUpload fileUpload;

	static {
		init();
	}

	private static void init() {
		DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
		fileItemFactory.setSizeThreshold(sizeThreshold);
		fileUpload = new ServletFileUpload(fileItemFactory);
		fileUpload.setSizeMax(sizeMax);// 表单大小
		fileUpload.setSizeMax(fileSizeMax);// 单文件大小限制，在parseRequest执行之前设置
	}

	/**
	 * 解析request,把multipart的表单信息保存在Map<String, Object[]> data
	 * @param request
	 * @return 表单信息,文件路径将会是服务器端的绝对路径
	 */
	public static Map<String, String[]> handle(HttpServletRequest request) {
		if (!request.getMethod().equalsIgnoreCase("post")) {
			logger.warn("应发送POST请求");
			return null;
		}
		if (!StringUtils.startsWith(request.getContentType(), FileUpload.MULTIPART)) {
			logger.warn("表单enctype格式不正确");
			return null;
		}

		Map<String, String[]> data = new HashMap<String, String[]>();// 要返回的数据
		List<String> formData = new ArrayList<String>();// 普通表单数据
		List<String> fileData = new ArrayList<String>();// 文件表单数据
		String lastFieldName = null; // 多值使用
		// 将request的值进行遍历
		List<FileItem> items = null;
		try {
			items = fileUpload.parseRequest(request);// 解析request 得到FileItem集合
			if (null == items)
				return null;
		} catch (FileUploadException e) {
			// 表单域大小限制异常处理
			if (e instanceof FileUploadBase.SizeLimitExceededException) {
				data.put("upload_msg", new String[] { "文件大小不能超过规定大小" });
				return data;
			}
			// 单文件超过限制异常处理
			if (e instanceof FileUploadBase.FileSizeLimitExceededException) {
				data.put("upload_msg", new String[] { "文件大小不能超过规定大小" });
				return data;
			}
			logger.error(e.getMessage(), e);
		}
		/*
		 * 遍历表单
		 */
		for (int i = 0; i < items.size(); ++i) {
			FileItem item = items.get(i);
			String fieldName = item.getFieldName();// 表单标签name属性的值,作为data的key
			/*
			 * 多值处理
			 */
			if (!fieldName.equals(lastFieldName)) {
				if (null != lastFieldName) {
					if (!formData.isEmpty()) {
						data.put(lastFieldName, toStringArray(formData.toArray()));
						logger.debug("formData:" + formData.size() + " =:" + formData);
						formData.clear();
					}
					if (!fileData.isEmpty()) {
						data.put(lastFieldName, toStringArray(fileData.toArray()));
						logger.debug("fileData:" + fileData.size() + " =" + fileData);
						fileData.clear();
					}
				}
				lastFieldName = fieldName;
			}
			/*
			 * 普通表单处理
			 */
			if (item.isFormField()) {
				try {
					String value = item.getString(request.getCharacterEncoding());
					if (!StringUtils.isBlank(value))
						formData.add(value);

					logger.debug(fieldName + "=[" + value + "]");
					continue;
				} catch (UnsupportedEncodingException e) {// 不会发生的异常
					logger.error(e.getMessage(), e);
				}
			}
			/*
			 * 文件表单处理
			 */
			String fileNmae = item.getName();// 获取文件名称,作为data的value
			logger.debug(fieldName + "=" + fileNmae + ",fileSize=" + item.getSize());
			if (StringUtils.isBlank(fileNmae)) {// 空文件
				continue;
			}
			/*
			 * 文件格式检查
			 */
			String fileExt = fileNmae.substring(fileNmae.lastIndexOf("."));// 文件后缀
			if (!isAllow(fileExt)) {// 不符合
				logger.debug(fileExt + ":文件类型不合规范");
				continue;
			}
			fileNmae = FileUtils.getUUIDFilename(fileNmae, true);// 重新定义文件名
			/*
			 * 保存到临时文件夹
			 */
			File tempFile = new File(FSConfig.temp(), fileNmae);
			tempFile.deleteOnExit();// JVM退出时输出
			try {
				item.write(tempFile);
				fileData.add(tempFile.getAbsolutePath());
				logger.debug("tempFile:" + tempFile);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		// 最后一项
		if (null != lastFieldName) {
			if (!fileData.isEmpty()) {
				data.put(lastFieldName, toStringArray(fileData.toArray()));
				logger.debug("fileData last:" + fileData.size() + " =" + fileData);
			}
			if (!formData.isEmpty()) {
				data.put(lastFieldName, toStringArray(formData.toArray()));
				logger.debug("formData last:" + formData.size() + " =" + formData);
			}
		}
		return data;
	}

	private static String[] toStringArray(Object[] objs) {
		String[] strings = new String[objs.length];
		for (int i = 0; i < objs.length; i++) {
			Object obj = objs[i];
			if (obj instanceof String) {
				strings[i] = (String) obj;
			} else {
				strings[i] = new String(obj.toString());
			}
		}
		return strings;
	}

	/**
	 * 判断文件是否符合
	 * @param fileExt 文件后缀
	 * @return
	 */
	public static boolean isAllow(String fileExt) {
		boolean allowFlag = false;
		for (String type : fileType) {
			if (type.equalsIgnoreCase(fileExt)) {
				allowFlag = true;
				break;
			}
		}
		return allowFlag;
	}

}