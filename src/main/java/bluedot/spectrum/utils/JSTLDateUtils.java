package bluedot.spectrum.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * jstl日期处理
 */
public class JSTLDateUtils extends TagSupport {
	private static final long serialVersionUID = 1L;
	private String value;
	private String parttern;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getParttern() {
		return parttern;
	}

	public void setParttern(String parttern) {
		this.parttern = parttern;
	}

	public int doStartTag() throws JspException {
		if (null == value || value.isEmpty() || null == parttern || parttern.isEmpty()) {
			return super.doStartTag();
		}
		SimpleDateFormat dateformat = new SimpleDateFormat(parttern);
		String s = dateformat.format(new Date(Long.valueOf(value)));
		try {
			pageContext.getOut().write(s);
		} catch (IOException e) {
			throw new JspException(e);
		}
		return super.doStartTag();
	}
}
