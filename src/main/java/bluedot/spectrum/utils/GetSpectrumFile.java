package bluedot.spectrum.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import bluedot.spectrum.api.SpectrumFile;
import bluedot.spectrum.bean.Spectrum;
import bluedot.spectrum.exception.ServiceException;
import bluedot.spectrum.service.QueryService;
import bluedot.spectrum.web.core.FSConfig;

/**
 * 获取SpectrumFile的工具类,SpectrumService业务使用
 * @author longshu 2016年10月8日
 */
public class GetSpectrumFile {
	private static Logger logger = Logger.getLogger(GetSpectrumFile.class);
	private static QueryService queryService = new QueryService();// 查询业务

	public static SpectrumFile getByID(String spectrumID) throws ServiceException {
		if (StringUtils.isBlank(spectrumID)) {
			return null;
		}
		// 查询数据库
		Map<String, Object> param = new Hashtable<String, Object>();
		param.put("spectrumID", spectrumID);
		List<Map<String, Object>> spectrumList = queryService.queryList("querySpectrum", param);
		if (null == spectrumList || spectrumList.isEmpty()) {
			return null;
		}
		// 得到该光谱对象
		Spectrum spectrum = BeanUtils.mapToBean(spectrumList.get(0), Spectrum.class);

		// 根据该光谱是否为标准光谱，及前台传来的光谱文件名，得到相应的File的实例
		File existFile = null;
		if (spectrum.isStandard()) {
			existFile = new File(FSConfig.standSpectrum(), spectrum.getSpectrumFile());
		} else {
			existFile = new File(FSConfig.userFavorite(), spectrum.getSpectrumFile());
		}
		return fileToObject(existFile);
	}

	/**
	 * 通过JsonUtils，给定某个File实例及转化类，转换成该类的对象
	 * @param existFile
	 * @return
	 * @throws ServiceException
	 */
	public static SpectrumFile fileToObject(File existFile) throws ServiceException {
		logger.debug(existFile.getAbsolutePath());
		if (!existFile.exists()) {
			throw new ServiceException(existFile.getName() + "不存在,或已删除");
		}
		InputStreamReader streamReader = null;
		try {// JsonSyntaxException
			streamReader = new InputStreamReader(new FileInputStream(existFile), Charset.forName("UTF-8"));
			return JsonUtils.Gson(false).fromJson(streamReader, SpectrumFile.class);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException("系统错误,操作失败,请联系管理员稍后再试!");
		} finally {
			if (null != streamReader) {
				try {
					streamReader.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public static SpectrumFile jsonToObject(String jsonSpectrum) throws ServiceException {
		return JsonUtils.Gson(false).fromJson(jsonSpectrum, SpectrumFile.class);
	}

}
