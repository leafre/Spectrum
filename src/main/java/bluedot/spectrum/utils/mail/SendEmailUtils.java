package bluedot.spectrum.utils.mail;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Properties;

import javax.mail.Session;

import org.apache.log4j.Logger;

import bluedot.spectrum.utils.StringUtils;

/**
 * 
 * @author longshu 2016年9月2日
 */
public class SendEmailUtils {
	private static Logger logger = Logger.getLogger(SendEmailUtils.class);
	private static Properties prop;
	private static Session session;
	private static boolean debugMail = false;
	private static boolean ssl_enable = true;// 是否开启SSL协议

	/**
	 * 初始化,加载模板文件
	 * @param templateFile 如果为空,默认为email_template.properties在类路径下
	 * @throws IOException
	 */
	public static void init(String templateFile) throws IOException {
		if (StringUtils.isBlank(templateFile)) {
			templateFile = "email_template.properties";
		}
		URL url = SendEmailUtils.class.getClassLoader().getResource(templateFile);
		logger.debug("templateFile:" + url.getFile());
		InputStream in = url.openStream();
		try {
			prop = new Properties();
			prop.load(new InputStreamReader(in, "UTF-8"));

			String ssl_enableStr = prop.getProperty("ssl_enable");
			if (StringUtils.matches(ssl_enableStr, "false")) {
				ssl_enable = false;
			}
			String debugMailStr = prop.getProperty("debug");
			if (StringUtils.matches(debugMailStr, "true")) {
				debugMail = true;
			}

			session = MailUtils.createSession(prop.getProperty("host"), prop.getProperty("username"),
					prop.getProperty("password"), ssl_enable);

			session.setDebug(debugMail);
			logger.info("ssl_enable=" + ssl_enable);
			logger.info("debugMail=" + debugMail);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} finally {
			if (null != in)
				in.close();
		}
	}

	/**
	 * 获取subject模板
	 * @param subjectKey
	 * @param arguments
	 * @return
	 */
	public static String getSubject(String subjectKey, Object... arguments) {
		String subject = prop.getProperty(subjectKey);
		subject = MessageFormat.format(subject, arguments);
		logger.debug("subject=" + subject);
		return subject;
	}

	/**
	 * 获取content模板
	 * @param contentKey
	 * @param arguments
	 * @return
	 */
	public static String getContent(String contentKey, Object... arguments) {
		String content = prop.getProperty(contentKey);
		content = MessageFormat.format(content, arguments);
		logger.debug("content=" + content);
		return content;
	}

	public static Mail getMail(String to, String subject, String content) {
		return new Mail(prop.getProperty("from"), to, subject, content);
	}

	/**
	 * 发送邮件
	 * @param mail
	 */
	public static void sendMail(final Mail mail) {
		new SendEmailThread(mail).start();
	}

	/**
	 * 发送邮件线程
	 */
	private static class SendEmailThread extends Thread {
		private final Mail mail;

		private SendEmailThread(final Mail mail) {
			this.mail = mail;
		}

		@Override
		public void run() {
			try {
				MailUtils.send(session, mail);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
		}
	}

}
