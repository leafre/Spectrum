package bluedot.spectrum.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 文件操作工具
 * commons.io二次包装
 * @author 龙叔  2016年8月15日
 */
public class FileUtils {
	private static org.apache.commons.io.FileUtils utils = new org.apache.commons.io.FileUtils();

	/**
	 * 使用UUID获取唯一的文件名
	 * @param 	filename	文件名
	 * @param 	hasSuffix	是否有后缀名
	 * @return	返回经过UUID处理后的filename
	 */
	public static String getUUIDFilename(String filename, boolean hasSuffix) {
		String name = filename;
		String ext = "";
		// 判断文件名是否为空
		if (filename.equals("") || filename == null) {
			return null;
		}
		if (hasSuffix) {
			ext = filename.substring(filename.lastIndexOf("."), filename.length());
			name = filename.substring(0, filename.lastIndexOf("."));
		}
		return name + "_" + UUID.randomUUID().toString().replaceAll("-", "") + ext;
	}

	/**
	 * 使用UUID和时间获取唯一的带有日期信息的文件
	 * @param 	filename	文件名
	 * @param 	hasSuffix	是否有后缀名
	 * @param 	date		日期
	 * @return	经过处理后的文件名
	 */
	public static String getDateFilename(String filename, boolean hasSuffix) {
		Date date = new Date();
		String name = filename;
		String ext = "";
		String time = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmssSS");
		// 判断文件名是否为空
		if (filename.equals("") || filename == null) {
			return null;
		}
		if (hasSuffix) {
			ext = filename.substring(filename.lastIndexOf("."), filename.length());
			name = filename.substring(0, filename.lastIndexOf("."));
		}
		time = dateFormat.format(date);
		return name + "_" + time + ext;
	}

	/**
	 * 构造一个文件夹
	 * @param 	dirs 基于dirs构造文件夹
	 * @return	文件夹路径(dirs[0]+"\\"+dirs[1]+"\\"+dirs[2]...)
	 */
	public static String makeDirName(String... dirs) {
		// 判断路径是否为空
		if (dirs == null) {
			return null;
		}
		File file = new File(dirs[0]);
		// 文件夹不存在则创建
		for (int i = 1; i < dirs.length; i++) {
			file = new File(file, dirs[i]);
		}
		if (!file.exists()) {
			file.mkdirs();
		}
		return file.getPath();
	}

	/**
	 * 批量删除文件
	 * @param	files	文件路径
	 * @return	成功删除的文件数量
	 */
	public static boolean deleteFile(String... filepaths) {
		int result = 0;
		File file;
		for (String filepath : filepaths) {
			file = new File(filepath);
			if (file.exists()) {
				if (!file.delete()) {
					result = 1;
				}
			}
		}
		if (result == 1) {
			return false;
		}
		return true;
	}

	/**
	 * 删除文件
	 * @param	dir	文件路径
	 * @return	删除是否成功
	 * @throws IOException 
	 */
	public static void deleteDirectory(String... dirs) throws IOException {
		for (String dir : dirs) {
			org.apache.commons.io.FileUtils.deleteDirectory(new File(dir));
		}
	}

	/**
	 * 获取java临时目录
	 * @return
	 */
	public static String getTempDirectoryPath() {
		return System.getProperty("java.io.tmpdir");
	}

	/**
	 * 获取java临时目录
	 * @return
	 */
	public static File getTempDirectory() {
		return new File(getTempDirectoryPath());
	}

	public static org.apache.commons.io.FileUtils Utils() {
		return utils;
	}

	public static void copyFile(final String srcFile, final String destFile) throws IOException {
		org.apache.commons.io.FileUtils.copyFile(new File(srcFile), new File(destFile));
	}

	public static void copyFileToDirectory(final String srcFile, final String destDir) throws IOException {
		org.apache.commons.io.FileUtils.copyFileToDirectory(new File(srcFile), new File(destDir));
	}

	public static void copyDirectory(final String srcDir, final String destDir) throws IOException {
		org.apache.commons.io.FileUtils.copyDirectory(new File(srcDir), new File(destDir));
	}

	public static void copyDirectoryToDirectory(final String srcDir, final String destDir) throws IOException {
		org.apache.commons.io.FileUtils.copyDirectoryToDirectory(new File(srcDir), new File(destDir));
	}

	public static void copyInputStreamToFile(final InputStream source, final String destination) throws IOException {
		org.apache.commons.io.FileUtils.copyInputStreamToFile(source, new File(destination));
	}

	public static void moveDirectory(final String srcDir, final String destDir) throws IOException {
		org.apache.commons.io.FileUtils.moveDirectory(new File(srcDir), new File(destDir));
	}

	public static void moveDirectoryToDirectory(final String src, final String destDir) throws IOException {
		org.apache.commons.io.FileUtils.moveDirectoryToDirectory(new File(src), new File(destDir), true);
	}

	public static void moveFile(final String srcFile, final String destFile) throws IOException {
		org.apache.commons.io.FileUtils.moveFile(new File(srcFile), new File(destFile));
	}

	public static void moveFileToDirectory(final String srcFile, final String destDir) throws IOException {
		org.apache.commons.io.FileUtils.moveFileToDirectory(new File(srcFile), new File(destDir), true);
	}

	public static long sizeOf(final String file) {
		return org.apache.commons.io.FileUtils.sizeOf(new File(file));
	}

	public static long sizeOfDirectory(final String directory) {
		return org.apache.commons.io.FileUtils.sizeOfDirectory(new File(directory));
	}
}
