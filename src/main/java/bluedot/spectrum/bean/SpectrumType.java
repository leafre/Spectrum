package bluedot.spectrum.bean;

import java.io.Serializable;

public class SpectrumType implements Serializable {
	private static final long serialVersionUID = -1391688195619195651L;
	private Integer spectrumTypeID; // 光谱类型编号
	private String spectrumType; // 光谱类型名称

	public SpectrumType() {
	}

	public SpectrumType(Integer spectrumTypeID, String spectrumType) {
		this.spectrumTypeID = spectrumTypeID;
		this.spectrumType = spectrumType;
	}

	public Integer getSpectrumTypeID() {
		return spectrumTypeID;
	}

	public void setSpectrumTypeID(Integer spectrumTypeID) {
		this.spectrumTypeID = spectrumTypeID;
	}

	public String getSpectrumType() {
		return spectrumType;
	}

	public void setSpectrumType(String spectrumType) {
		this.spectrumType = spectrumType;
	}

}
