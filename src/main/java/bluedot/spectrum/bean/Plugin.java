package bluedot.spectrum.bean;

import java.io.Serializable;
import java.util.List;

public class Plugin implements Serializable {
	private static final long serialVersionUID = 1240119216584420197L;
	private Integer pluginID;// 算法编号
	private Integer userID;// 用户编号
	private User user;// 算法的上传者信息
	private String pluginType;// 算法类型
	private String pluginName;// 算法名称
	private String description;// 算法描述
	private String version;// 算法版本号
	private String codePath;// 算法源码位置
	private String jarPath;// 算法jar位置
	private Long addTime;// 算法添加时间
	// 算法审核状态（默认为等待）
	private String state = ApplicationState.WAIT;
	private Integer[] pluginMainIDs;
	private List<Plugin> pluginMain;// pluginID当subID时查出的
	private Integer[] pluginSubIDs;
	private List<Plugin> pluginSub;// pluginID当mainID时查出的
	// 该算法支持的硬件类型及其与硬件类型相关联的光谱类型
	private Integer[] hardwareIDs;
	private List<Hardware> hardware;
	// 该算法支持的被检测物以及包含的检测内容
	private String[] detectedObjectIDs;
	private List<DetectedObject> detectedObject;

	public Integer getPluginID() {
		return pluginID;
	}

	public void setPluginID(Integer pluginID) {
		this.pluginID = pluginID;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPluginType() {
		return pluginType;
	}

	public void setPluginType(String pluginType) {
		this.pluginType = pluginType;
	}

	public String getPluginName() {
		return pluginName;
	}

	public void setPluginName(String pluginName) {
		this.pluginName = pluginName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCodePath() {
		return codePath;
	}

	public void setCodePath(String codePath) {
		this.codePath = codePath;
	}

	public String getJarPath() {
		return jarPath;
	}

	public void setJarPath(String jarPath) {
		this.jarPath = jarPath;
	}

	public Long getAddTime() {
		return addTime;
	}

	public void setAddTime(Long addTime) {
		this.addTime = addTime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer[] getPluginMainIDs() {
		return pluginMainIDs;
	}

	public void setPluginMainIDs(Integer[] pluginMainIDs) {
		this.pluginMainIDs = pluginMainIDs;
	}

	public List<Plugin> getPluginMain() {
		return pluginMain;
	}

	public void setPluginMain(List<Plugin> pluginMain) {
		this.pluginMain = pluginMain;
	}

	public Integer[] getPluginSubIDs() {
		return pluginSubIDs;
	}

	public void setPluginSubIDs(Integer[] pluginSubIDs) {
		this.pluginSubIDs = pluginSubIDs;
	}

	public List<Plugin> getPluginSub() {
		return pluginSub;
	}

	public void setPluginSub(List<Plugin> pluginSub) {
		this.pluginSub = pluginSub;
	}

	public Integer[] getHardwareIDs() {
		return hardwareIDs;
	}

	public void setHardwareIDs(Integer[] hardwareIDs) {
		this.hardwareIDs = hardwareIDs;
	}

	public List<Hardware> getHardware() {
		return hardware;
	}

	public void setHardware(List<Hardware> hardware) {
		this.hardware = hardware;
	}

	public String[] getDetectedObjectIDs() {
		return detectedObjectIDs;
	}

	public void setDetectedObjectIDs(String[] detectedObjectIDs) {
		this.detectedObjectIDs = detectedObjectIDs;
	}

	public List<DetectedObject> getDetectedObject() {
		return detectedObject;
	}

	public void setDetectedObject(List<DetectedObject> detectedObject) {
		this.detectedObject = detectedObject;
	}

}