package bluedot.spectrum.bean;

/**
 * 检测标准
 * 
 * @author YiJie
 * @date 2016年8月26日
 */
public class DetectedStandard {
	private String detectedID; // 被检测物编号
	private Integer contentID; // 检测内容编号
	private DetectedContent content;// 检测内容
	private String standards; // 执行标准
	private Double standardLine; // 标准线
	private Double value; // 波动值
	private String unit; // 标准线单位

	public String getDetectedID() {
		return detectedID;
	}

	public void setDetectedID(String detectedID) {
		this.detectedID = detectedID;
	}

	public Integer getContentID() {
		return contentID;
	}

	public void setContentID(Integer contentID) {
		this.contentID = contentID;
	}

	public DetectedContent getContent() {
		return content;
	}

	public void setContent(DetectedContent content) {
		this.content = content;
	}

	public String getStandards() {
		return standards;
	}

	public void setStandards(String standards) {
		this.standards = standards;
	}

	public Double getStandardLine() {
		return standardLine;
	}

	public void setStandardLine(Double standardLine) {
		this.standardLine = standardLine;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
}
