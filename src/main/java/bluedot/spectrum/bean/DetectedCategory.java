package bluedot.spectrum.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 被检测物分类
 */
public class DetectedCategory implements Serializable {
	private static final long serialVersionUID = -4924883578525207438L;
	private Integer categoryID; // 被检测物分类编号
	private String categoryName; // 被检测物分类名称
	private Integer parent; // 父被检测物编号
	private List<DetectedCategory> children; // 子被检测物分类或检测对象

	public DetectedCategory() {
	}

	public DetectedCategory(Integer categoryID, String categoryName, Integer parent) {
		this.categoryID = categoryID;
		this.categoryName = categoryName;
		this.parent = parent;
	}

	public Integer getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(Integer categoryID) {
		this.categoryID = categoryID;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getParent() {
		return parent;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}

	public List<DetectedCategory> getChildren() {
		return children;
	}

	public void setChildren(List<DetectedCategory> children) {
		this.children = children;
	}

}