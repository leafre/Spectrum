package bluedot.spectrum.bean;

import java.io.Serializable;

public class Report implements Serializable {
	private static final long serialVersionUID = -2378502928435677552L;
	private Integer reportID;// 光谱报告id
	private String spectrumID;
	private Spectrum spectrum;// 与报告关联的光谱
	private Integer contentID; // 检测内容编号
	private DetectedContent content;// 报告检测的内容
	private Float concentration;// 浓度
	private Long saveTime;// 保存的时间

	/**
	 * 默认构造函数
	 */
	public Report() {
	}

	Report(Integer reportID, Float concentration, Long saveTime) {
		this.reportID = reportID;
		this.concentration = concentration;
		this.saveTime = saveTime;
	}

	public Report(Integer reportID, String spectrumID, Integer contentID, Float concentration, Long saveTime) {
		this(reportID, concentration, saveTime);
		this.spectrumID = spectrumID;
		this.contentID = contentID;
	}

	public Report(Integer reportID, Spectrum spectrum, DetectedContent content, Float concentration, Long saveTime) {
		this(reportID, concentration, saveTime);
		this.spectrum = spectrum;
		this.content = content;
	}

	public Integer getReportID() {
		return reportID;
	}

	public void setReportID(Integer reportID) {
		this.reportID = reportID;
	}

	public String getSpectrumID() {
		return spectrumID;
	}

	public void setSpectrumID(String spectrumID) {
		this.spectrumID = spectrumID;
	}

	public Spectrum getSpectrum() {
		return spectrum;
	}

	public void setSpectrum(Spectrum spectrum) {
		this.spectrum = spectrum;
	}

	public Integer getContentID() {
		return contentID;
	}

	public void setContentID(Integer contentID) {
		this.contentID = contentID;
	}

	public DetectedContent getContent() {
		return content;
	}

	public void setContent(DetectedContent content) {
		this.content = content;
	}

	public Float getConcentration() {
		return concentration;
	}

	public void setConcentration(Float concentration) {
		this.concentration = concentration;
	}

	public Long getSaveTime() {
		return saveTime;
	}

	public void setSaveTime(Long saveTime) {
		this.saveTime = saveTime;
	}
}