package bluedot.spectrum.bean;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class User implements Serializable {
	private static final long serialVersionUID = 4446614544212973211L;
	private static final String S_D_F = "MM/dd/yyyy";//simple date format
	private Integer userID;// 用户编号
	private String username;// 用户名
	private Integer roleID;// 角色编号
	private Role role;// 用户角色
	private String email;// 用户邮箱
	private String password;// 用户密码
	private String realName;// 真实姓名
	private String IDNumber;// 身份证号
	private String tel;// 手机号
	private String gender = Gender.SECRECY;// 性别
	private Long birthday;// 生日
	private String address;// 个人地址
	private String institute;// 工作单位
	private String position;// 职位
	private String photo;// 个人照片
	private String detail;// 个人详细描述
	private String state = AccountState.NORMAL;// 账户状态
	private Long thawTime;// 解冻时间,如果账户状态为解冻

	@Override
	public boolean equals(Object obj) {
		if (null == obj) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj instanceof User) {
			User user = (User) obj;
			if (user.userID == this.userID || user.username.equals(this.username)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "User [userID=" + userID + ", username=" + username + ", roleID=" + roleID + ", role=" + role
				+ ", email=" + email + ", password=" + password + ", realName=" + realName + ", IDNumber=" + IDNumber
				+ ", tel=" + tel + ", gender=" + gender + ", birthday=" + birthday + ", address=" + address
				+ ", institute=" + institute + ", position=" + position + ", photo=" + photo + ", detail=" + detail
				+ ", state=" + state + ", thawTime=" + thawTime + "]";
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getRoleID() {
		return roleID;
	}

	public void setRoleID(Integer roleID) {
		this.roleID = roleID;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getIDNumber() {
		return IDNumber;
	}

	public void setIDNumber(String iDNumber) {
		IDNumber = iDNumber;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Long getBirthday() {
		return birthday;
	}
	/**
	 * 日期格式：10/17/2016
	 * 前台传来的日期是上述格式的字符串形式，为了便于业务层mapToBean，特加此方法
	 * @param birthday
	 */
	public void setBirthday(String birthday) {
		SimpleDateFormat sdf = new SimpleDateFormat(User.S_D_F);
		try {
			this.birthday = sdf.parse(birthday).getTime();
		} catch (ParseException e) {
			
			throw new RuntimeException("日期可是不正确，应为:1970/01/01");
		}
	}
	public void setBirthday(Long birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getInstitute() {
		return institute;
	}

	public void setInstitute(String institute) {
		this.institute = institute;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Long getThawTime() {
		return thawTime;
	}

	public void setThawTime(Long thawTime) {
		this.thawTime = thawTime;
	}

}