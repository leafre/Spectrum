package bluedot.spectrum.bean;

import java.io.Serializable;
import java.util.List;

public class Favorite implements Serializable {
	private static final long serialVersionUID = 4924849350616934536L;
	private Integer favoriteID;// 收藏夹ID
	private Integer userID;// 用户编号
	private User user;// 所属用户
	private String favoriteName;// 收藏夹名
	private String path;// 收藏路径
	private List<Spectrum> spectrumList;// 收藏夹关谱

	public Favorite() {
	}

	public Favorite(Integer favoriteID, User user, String favoriteName, String path) {
		super();
		this.favoriteID = favoriteID;
		this.user = user;
		this.favoriteName = favoriteName;
		this.path = path;
	}

	public Integer getFavoriteID() {
		return favoriteID;
	}

	public void setFavoriteID(Integer favoriteID) {
		this.favoriteID = favoriteID;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getFavoriteName() {
		return favoriteName;
	}

	public void setFavoriteName(String favoriteName) {
		this.favoriteName = favoriteName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<Spectrum> getSpectrumList() {
		return spectrumList;
	}

	public void setSpectrumList(List<Spectrum> spectrumList) {
		this.spectrumList = spectrumList;
	}
}