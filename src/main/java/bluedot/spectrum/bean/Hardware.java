package bluedot.spectrum.bean;

import java.io.Serializable;
import java.util.List;

public class Hardware implements Serializable {
	private static final long serialVersionUID = -9222444386558315493L;
	private Integer hardwareID; // 设备编号
	private String hardwareName; // 设备名称
	private Long addTime; // 添加时间
	//该硬件产生的光谱类型集合
	private Integer[] spectrumTypeIDs;
	private List<SpectrumType> spectrumTypeList;
	//该硬件产生光谱的文件格式集合
	private Integer[] spectrumFileTypeIDs;
	private List<SpectrumFileType> spectrumFileTypeList;

	/**
	 * 无参构造函数
	 */
	public Hardware() {
	}
	
	public Hardware(Integer hardwareID){
		this.hardwareID = hardwareID;
	}

	/**
	 * 构造只需要ID的对象用词构造方法
	 * @param hardwareID
	 * @param spectrumTypeIDs
	 */
	public Hardware(Integer hardwareID,Integer[]spectrumTypeIDs){
		this.hardwareID = hardwareID;
		this.spectrumTypeIDs = spectrumTypeIDs;
	}
	
	/**
	 * @param hardwareID 设备编号
	 * @param hardwareName 硬件名称
	 * @param addTime 添加时间
	 */
	public Hardware(Integer hardwareID, String hardwareName, long addTime) {
		super();
		this.hardwareID = hardwareID;
		this.hardwareName = hardwareName;
		this.addTime = addTime;
	}

	public Integer getHardwareID() {
		return hardwareID;
	}

	public void setHardwareID(Integer hardwareID) {
		this.hardwareID = hardwareID;
	}

	public String getHardwareName() {
		return hardwareName;
	}

	public void setHardwareName(String hardwareName) {
		this.hardwareName = hardwareName;
	}

	public Long getAddTime() {
		return addTime;
	}

	public void setAddTime(Long addTime) {
		this.addTime = addTime;
	}

	public Integer[] getSpectrumTypeIDs(){
		return spectrumTypeIDs; 		
	}
	
	public void setSpectrumTypeIDs(Integer[] spectrumTypeIDs){
		this.spectrumTypeIDs = spectrumTypeIDs;
	}
	
	public List<SpectrumType> getSpectrumTypeList() {
		return spectrumTypeList;
	}

	public void setSpectrumTypeList(List<SpectrumType> spectrumTypeList) {
		this.spectrumTypeList = spectrumTypeList;
	}

	public Integer[] getSpectrumFileTypeIDs(){
		return spectrumFileTypeIDs;
	}
	
	public void setSpectrumFileTypeIDs(Integer[] spectrumFileTypeIDs){
		this.spectrumFileTypeIDs = spectrumFileTypeIDs;
	}
	
	public List<SpectrumFileType> getSpectrumFileTypeList() {
		return spectrumFileTypeList;
	}

	public void setSpectrumFileTypeList(List<SpectrumFileType> spectrumFileTypeList) {
		this.spectrumFileTypeList = spectrumFileTypeList;
	}

}