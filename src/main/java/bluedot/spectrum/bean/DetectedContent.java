package bluedot.spectrum.bean;

import java.io.Serializable;

/**
 * 检测内容
 */
public class DetectedContent implements Serializable {
	private static final long serialVersionUID = -6032703703679369657L;
	private Integer contentID; // 检测内容编号
	private Integer categoryID; // 检测内容分类编号
	private DetectedContentCategory contentCategory;// 检测内容分类
	private String chineseName; // 中文名
	private String englishName; // 英文名
	private String chemicalName; // 化学式

	public Integer getContentID() {
		return contentID;
	}

	public void setContentID(Integer contentID) {
		this.contentID = contentID;
	}

	public Integer getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(Integer categoryID) {
		this.categoryID = categoryID;
	}

	public DetectedContentCategory getContentCategory() {
		return contentCategory;
	}

	public void setContentCategory(DetectedContentCategory contentCategory) {
		this.contentCategory = contentCategory;
	}

	public String getChineseName() {
		return chineseName;
	}

	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getChemicalName() {
		return chemicalName;
	}

	public void setChemicalName(String chemicalName) {
		this.chemicalName = chemicalName;
	}
}