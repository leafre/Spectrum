package bluedot.spectrum.bean;

import java.io.Serializable;

public class Backup implements Serializable {
	private static final long serialVersionUID = -5570152470736405631L;
	private Integer backupID;// 系统备份编号
	private String backupName;// 备份名称
	private String content;// 备份内容
	private String description;// 备份描述
	private Long time;// 备份时间

	public Backup() {
	}

	public Backup(Integer backupID, String backupName, String content, String description, Long time) {
		this.backupID = backupID;
		this.backupName = backupName;
		this.content = content;
		this.description = description;
		this.time = time;
	}

	public Integer getBackupID() {
		return backupID;
	}

	public void setBackupID(Integer backupID) {
		this.backupID = backupID;
	}

	public String getBackupName() {
		return backupName;
	}

	public void setBackupName(String backupName) {
		this.backupName = backupName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "Backup [backupID=" + backupID + ", backupName=" + backupName + ", content=" + content + ", description="
				+ description + ", time=" + time + "]";
	}

}