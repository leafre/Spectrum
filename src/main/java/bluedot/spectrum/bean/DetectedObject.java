package bluedot.spectrum.bean;

import java.io.Serializable;
import java.util.Map;

/**
 * 被检测物
 */
public class DetectedObject implements Serializable {
	private static final long serialVersionUID = -7495954122994383036L;
	private String detectedID; // 被检测物编号
	private Integer categoryID; // 被检测物分类编号
	private DetectedCategory detectedCategory; // 被检测物分类
	private String chineseName; // 中文名
	private String englishName; // 英文名
	private Integer[] contentIDs;// 对应的检测内容编号
//	private List<DetectedContent> contents; // 对应的检测内容
	private Map<Integer, DetectedStandard> detectedStandard; // 对应的检测内容的检测标准
	public DetectedObject() {
	}
	
	public DetectedObject(String detectedID){
		this.detectedID = detectedID;
	}
	
	/**
	 * 构造的对象只需要ID时 可用此构造方法
	 * @param detectedID
	 * @param contentIDs
	 */
	public DetectedObject(String detectedID,Integer[]contentIDs){
		this.detectedID = detectedID;
		this.contentIDs = contentIDs;
	}
	
	public DetectedObject(String detectedID, Integer categoryID, DetectedCategory detectedCategory, String chineseName) {
		this.detectedID = detectedID;
		this.categoryID = categoryID;
		this.detectedCategory = detectedCategory;
		this.chineseName = chineseName;
	}

	public String getDetectedID() {
		return detectedID;
	}

	public void setDetectedID(String detectedID) {
		this.detectedID = detectedID;
	}

	public Integer getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(Integer categoryID) {
		this.categoryID = categoryID;
	}

	public DetectedCategory getDetectedCategory() {
		return detectedCategory;
	}

	public void setDetectedCategory(DetectedCategory detectedCategory) {
		this.detectedCategory = detectedCategory;
	}

	public String getChineseName() {
		return chineseName;
	}

	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public Integer[] getContentIDs() {
		return contentIDs;
	}

	public void setContentIDs(Integer[] contentIDs) {
		this.contentIDs = contentIDs;
	}

//	public List<DetectedContent> getContents() {
//		return contents;
//	}
//
//	public void setContents(List<DetectedContent> contents) {
//		this.contents = contents;
//	}

	public Map<Integer, DetectedStandard> getDetectedStandard() {
		return detectedStandard;
	}

	public void setDetectedStandard(Map<Integer, DetectedStandard> detectedStandard) {
		this.detectedStandard = detectedStandard;
	}

}