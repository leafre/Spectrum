package bluedot.spectrum.bean;

import java.io.Serializable;

public class Recover implements Serializable {
	private static final long serialVersionUID = -3133622513661656766L;
	private Integer recoverID;// 系统恢复编号
	private Integer backupID;// 系统备份编号
	private Backup backup;// 恢复的备份信息
	private Long time;// 恢复时间
	private String content;// 恢复内容
	private String description;// 恢复描述

	/**
	 * 默认构造函数
	 */
	public Recover() {
	}

	Recover(Integer recoverID, Long time, String content, String description) {
		this.recoverID = recoverID;
		this.time = time;
		this.content = content;
		this.description = description;
	}

	public Recover(Integer recoverID, Integer backupID, long time, String content, String description) {
		this(recoverID, time, content, description);
		this.backupID = backupID;
	}

	public Recover(Integer recoverID, Backup backup, long time, String content, String description) {
		this(recoverID, time, content, description);
		this.backup = backup;
	}

	public Integer getRecoverID() {
		return recoverID;
	}

	public void setRecoverID(Integer recoverID) {
		this.recoverID = recoverID;
	}

	public Integer getBackupID() {
		return backupID;
	}

	public void setBackupID(Integer backupID) {
		this.backupID = backupID;
	}

	public Backup getBackup() {
		return backup;
	}

	public void setBackup(Backup backup) {
		this.backup = backup;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}