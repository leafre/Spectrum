package bluedot.spectrum.bean;

import java.io.Serializable;

public class Peak implements Serializable {
	private static final long serialVersionUID = -7526658256287988911L;
	private Long peakID;// 峰编号
	private String spectrumID;// 光谱编号
	private String peaks;// 峰值json字符串

	public Peak() {
	}

	public Peak(Long peakID, String spectrumID, String peaks) {
		this.peakID = peakID;
		this.spectrumID = spectrumID;
		this.peaks = peaks;
	}

	public Long getPeakID() {
		return peakID;
	}

	public void setPeakID(Long peakID) {
		this.peakID = peakID;
	}

	public String getSpectrumID() {
		return spectrumID;
	}

	public void setSpectrumID(String spectrumID) {
		this.spectrumID = spectrumID;
	}

	public String getPeaks() {
		return peaks;
	}

	public void setPeaks(String peaks) {
		this.peaks = peaks;
	}

	@Override
	public String toString() {
		return "Peak [peakID=" + peakID + ", spectrumID=" + spectrumID + ", peaks=" + peaks + "]";
	}

}
