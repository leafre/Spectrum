package bluedot.spectrum.bean;

import java.io.Serializable;
import java.util.List;

public class Role implements Serializable {
	private static final long serialVersionUID = -1769334232955924535L;
	public static final int SUPERADMIN = 1;// 超级管理员编号
	public static final int ADMIN = 2;// 普通管理员编号
	public static final int LAB = 3;// 实验员编号
	public static final int USER = 4;// 普通用户编号
	public static final int TOURIST = 5;// 游客编号
	private Integer roleID;// 角色编号
	private String roleName;// 角色名称
	private List<Function> functions;// 角色的功能

	public Role() {
	}

	public Role(Integer roleID, String roleName, List<Function> functions) {
		this.roleID = roleID;
		this.roleName = roleName;
		this.functions = functions;
	}

	public Integer getRoleID() {
		return roleID;
	}

	public void setRoleID(Integer roleID) {
		this.roleID = roleID;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<Function> getFunctions() {
		return functions;
	}

	public void setFunctions(List<Function> functions) {
		this.functions = functions;
	}

}
