package bluedot.spectrum.bean;

public class SpectrumSubmit extends Application {
	private static final long serialVersionUID = 2428770206942469213L;
	// private int submitID; //光谱提交编号
	private String spectrumID; // 光谱编号
	// private long submitTime; //光谱提交时间
	// private String state = ApplicationState.WAIT;// 申请状态

	public Integer getSubmitID() {
		return applyID;
	}

	public void setSubmitID(Integer submitID) {
		this.applyID = submitID;
	}

	public String getSpectrumID() {
		return spectrumID;
	}

	public void setSpectrumID(String spectrumID) {
		this.spectrumID = spectrumID;
	}

	public Long getSubmitTime() {
		return applyTime;
	}

	public void setSubmitTime(Long submitTime) {
		this.applyTime = submitTime;
	}

}
