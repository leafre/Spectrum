package bluedot.spectrum.bean;

import java.io.Serializable;

public class Log implements Serializable {
	private static final long serialVersionUID = -7204123407524232343L;
	private Long logID;// 日志编号
	private String level;// 日志等级
	private String type;// 日志类型
	private Long time;// 日志时间
	private Integer userID;// 用户编号
	private User user;// 被记录日志的用户（若是系统日志此项为空）
	private String ip;// 用户IP地址
	private Integer functionID;// 功能编号
	private Function function;// 日志的操作类型
	private String content;// 此项操作的具体内容

	public Log(){}
	/**
	 * 记录用户日志，servlet中调用
	 * 
	 * @param level
	 * @param type
	 * @param time
	 * @param userID
	 * @param ip
	 * @param functionID
	 * @param content
	 */
	public Log(String level, String type, Long time, Integer userID, String ip, Integer functionID, String content) {
		this.level = level;
		this.type = type;
		this.time = time;
		this.userID = userID;
		this.ip = ip;
		this.functionID = functionID;
		this.content = content;
	}

	/**
	 * 记录系统日志，service中调用
	 * 
	 * @param level
	 * @param type
	 * @param functionID
	 * @param content
	 */
	public Log(String level, String type, Integer functionID, String content) {
		this.level = level;
		this.type = type;
		this.functionID = functionID;
		this.content = content;
	}

	/**
	 * 日志等级
	 */
	public interface LogLevel {
		public static final String FATAL = "FATAL"; // 严重错误
		public static final String ERROR = "ERROR"; // 错误
		public static final String WARN = "WARN"; // 警告
		public static final String INFO = "INFO"; // 普通
	}

	/**
	 * 日志类型
	 */
	public interface LogType {
		// 用户日志(普通用户、实验人员),管理员日志(普通管理员、超级管理员)，系统日志
		public static final String USER = "USER";
		public static final String ADMIN = "ADMIN";
		public static final String SYSTEM = "SYSTEM";
	}

	public Long getLogID() {
		return logID;
	}

	public void setLogID(Long logID) {
		this.logID = logID;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Integer getFunctionID() {
		return functionID;
	}

	public void setFunctionID(Integer functionID) {
		this.functionID = functionID;
	}

	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}