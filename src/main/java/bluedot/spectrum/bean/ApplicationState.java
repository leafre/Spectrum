package bluedot.spectrum.bean;

public interface ApplicationState {
	// 申请的状态
	public static final String PASS = "通过";
	public static final String WAIT = "等待";
	public static final String REFUSE = "拒绝";
}