package bluedot.spectrum.bean;

public interface Gender {
	public static final String MAN = "男";
	public static final String WOMAN = "女";
	public static final String SECRECY = "保密";
}