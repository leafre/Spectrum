package bluedot.spectrum.bean;

public interface AccountState {
	String NORMAL = "正常";
	String FROZEN = "冻结";
}