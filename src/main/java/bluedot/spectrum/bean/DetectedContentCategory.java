package bluedot.spectrum.bean;

import java.io.Serializable;

/**
 * 被检测物分类
 */
public class DetectedContentCategory implements Serializable {
	private static final long serialVersionUID = -8649188775738491788L;
	private Integer categoryID; // 检测内容分类编号
	private String categoryName; // 检测内容分类名称

	public DetectedContentCategory() {
	}

	public DetectedContentCategory(Integer categoryID, String categoryName) {
		this.categoryID = categoryID;
		this.categoryName = categoryName;
	}

	public Integer getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

}
