package bluedot.spectrum.bean;

public class RoleApplication extends Application {
	private static final long serialVersionUID = 7952613653638154443L;
	private Integer roleID;// 申请的角色编号
	private Role applyRole;// 申请的角色
	private String applyFile;// 申请文件

	public Integer getRoleID() {
		return roleID;
	}

	public void setRoleID(Integer roleID) {
		this.roleID = roleID;
	}

	public Role getApplyRole() {
		return applyRole;
	}

	public void setApplyRole(Role applyRole) {
		this.applyRole = applyRole;
	}

	public String getApplyFile() {
		return applyFile;
	}

	public void setApplyFile(String applyFile) {
		this.applyFile = applyFile;
	}

}
