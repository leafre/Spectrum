package bluedot.spectrum.bean;

public class ThawApplication extends Application {
	private static final long serialVersionUID = -540163872629945526L;
	private String reason;// 解冻理由

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
