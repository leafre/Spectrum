package bluedot.spectrum.bean;

import java.io.Serializable;

public class Function implements Serializable {
	private static final long serialVersionUID = 6153009623385800065L;
	private Integer functionID;// 功能编号
	private String functionName;// 功能名称
	private String functionURL;// 功能URL地址

	// 默认构造函数
	public Function() {
	}

	public Function(Integer functionID, String functionName, String functionURL) {
		this.functionID = functionID;
		this.functionName = functionName;
		this.functionURL = functionURL;
	}

	public Integer getFunctionID() {
		return functionID;
	}

	public void setFunctionID(Integer functionID) {
		this.functionID = functionID;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getFunctionURL() {
		return functionURL;
	}

	public void setFunctionURL(String functionURL) {
		this.functionURL = functionURL;
	}

	@Override
	public String toString() {
		return "Function [functionID=" + functionID + ", functionName=" + functionName + ", functionURL=" + functionURL
				+ "]";
	}

}
