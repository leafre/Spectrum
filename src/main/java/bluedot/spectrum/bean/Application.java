package bluedot.spectrum.bean;

import java.io.Serializable;

public abstract class Application implements Serializable {
	private static final long serialVersionUID = -5797234392460853392L;
	protected Integer applyID;// 申请编号
	protected Integer userID;// 申请人编号
	protected User user;// 申请人
	protected Long applyTime;// 申请时间
	protected String state = ApplicationState.WAIT;// 申请状态

	protected Application() {
	}

	protected Application(Integer applyID, Integer userID, User user, Long applyTime, String state) {
		this.applyID = applyID;
		this.userID = userID;
		this.user = user;
		this.applyTime = applyTime;
		this.state = state;
	}

	public Integer getApplyID() {
		return applyID;
	}

	public void setApplyID(Integer applyID) {
		this.applyID = applyID;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Long applyTime) {
		this.applyTime = applyTime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
