package bluedot.spectrum.bean;

import java.io.Serializable;
import java.util.List;

import bluedot.spectrum.api.XUnit;
import bluedot.spectrum.api.YUnit;

public class Spectrum implements Serializable {
	private static final long serialVersionUID = 820937901438616729L;
	// 光谱编号，主键，格式：18位 [XX光谱类型][XXX硬件型号][XXXXXX被检测物][XXXXXXX自动编号]
	private String spectrumID;
	private String spectrumName;// 光谱的名称，不能为空
	private Integer userID;// 用户编号
	private User user;// 用户对象，包含用户的基本信息
	private String originalSpecrumID;
	private Spectrum originalSpecrum;// 原始光谱，指的是进行一些操作之前保留的备份光谱，以便撤销操作
	private Integer hardwareID; // 设备编号
	private Hardware hardware;// 硬件对象，包含了该光谱所对应的硬件信息
	private Integer spectrumTypeID; // 光谱类型编号
	private SpectrumType spectrumType;// 光谱类型对象，指定光谱的类型
	private Integer spectrumFileTypeID; // 光谱文件格式编号
	private SpectrumFileType spectrumFileType;// 光谱文件格式对象，指定光谱的文件格式
	private DetectedObject detectedObject;// 被检测物对象，包含被检测物的信息
	private String detectedID; // 被检测物对象编号
	private List<DetectedContent> contents;// 检测内容，是一个检测内容类型的列表
	private Integer[] contentsIDs; // 检测内容编号数组
	private Float resolutionRate;// 分辨率
	private String xUnit = XUnit.WAVE_NUMBER;// 光谱x轴单位,不为空,默认 波数
	private String yUnit = YUnit.ABSORBANCE;// 光谱y轴单位,不为空,默认 吸光度
	private String spectrumFile;// 光谱文件的存在路径，不为空
	private Long saveTime;// 光谱的保存时间，日期类型
	private String description;// 光谱的描述
	private boolean isStandard;// 是否是标准库，是布尔型数据，不为空
	private List<Peak> peaks;// 峰值类型的列表

	public String getSpectrumID() {
		return spectrumID;
	}

	public void setSpectrumID(String spectrumID) {
		this.spectrumID = spectrumID;
	}

	public String getSpectrumName() {
		return spectrumName;
	}

	public void setSpectrumName(String spectrumName) {
		this.spectrumName = spectrumName;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getOriginalSpecrumID() {
		return originalSpecrumID;
	}

	public void setOriginalSpecrumID(String originalSpecrumID) {
		this.originalSpecrumID = originalSpecrumID;
	}

	public Spectrum getOriginalSpecrum() {
		return originalSpecrum;
	}

	public void setOriginalSpecrum(Spectrum originalSpecrum) {
		this.originalSpecrum = originalSpecrum;
	}

	public Integer getHardwareID() {
		return hardwareID;
	}

	public void setHardwareID(Integer hardwareID) {
		this.hardwareID = hardwareID;
	}

	public Hardware getHardware() {
		return hardware;
	}

	public void setHardware(Hardware hardware) {
		this.hardware = hardware;
	}

	public Integer getSpectrumTypeID() {
		return spectrumTypeID;
	}

	public void setSpectrumTypeID(Integer spectrumTypeID) {
		this.spectrumTypeID = spectrumTypeID;
	}

	public SpectrumType getSpectrumType() {
		return spectrumType;
	}

	public void setSpectrumType(SpectrumType spectrumType) {
		this.spectrumType = spectrumType;
	}

	public Integer getSpectrumFileTypeID() {
		return spectrumFileTypeID;
	}

	public void setSpectrumFileTypeID(Integer spectrumFileTypeID) {
		this.spectrumFileTypeID = spectrumFileTypeID;
	}

	public SpectrumFileType getSpectrumFileType() {
		return spectrumFileType;
	}

	public void setSpectrumFileType(SpectrumFileType spectrumFileType) {
		this.spectrumFileType = spectrumFileType;
	}

	public DetectedObject getDetectedObject() {
		return detectedObject;
	}

	public void setDetectedObject(DetectedObject detectedObject) {
		this.detectedObject = detectedObject;
	}

	public String getDetectedID() {
		return detectedID;
	}

	public void setDetectedID(String detectedID) {
		this.detectedID = detectedID;
	}

	public List<DetectedContent> getContents() {
		return contents;
	}

	public void setContents(List<DetectedContent> contents) {
		this.contents = contents;
	}

	public Integer[] getContentsIDs() {
		return contentsIDs;
	}

	public void setContentsIDs(Integer[] contentsIDs) {
		this.contentsIDs = contentsIDs;
	}

	public Float getResolutionRate() {
		return resolutionRate;
	}

	public void setResolutionRate(Float resolutionRate) {
		this.resolutionRate = resolutionRate;
	}

	public String getxUnit() {
		return xUnit;
	}

	public void setxUnit(String xUnit) {
		this.xUnit = xUnit;
	}

	public String getyUnit() {
		return yUnit;
	}

	public void setyUnit(String yUnit) {
		this.yUnit = yUnit;
	}

	public String getSpectrumFile() {
		return spectrumFile;
	}

	public void setSpectrumFile(String spectrumFile) {
		this.spectrumFile = spectrumFile;
	}

	public Long getSaveTime() {
		return saveTime;
	}

	public void setSaveTime(Long saveTime) {
		this.saveTime = saveTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isStandard() {
		return isStandard;
	}

	public void setStandard(boolean isStandard) {
		this.isStandard = isStandard;
	}
	
	/**
	 * 使用BeanUtils.maptoBean时，由于数据库存储的是整型，而这里是布尔型，会导致冲突而使isStandard获得正确是值
	 * @author 付大石
	 * @param isStandard
	 */
	public void setStandard(int isStandard){
		if(isStandard==0){
			this.isStandard=false;
		} else {
			this.isStandard=true;
		}
	}
	
	public List<Peak> getPeaks() {
		return peaks;
	}

	public void setPeaks(List<Peak> peaks) {
		this.peaks = peaks;
	}

}