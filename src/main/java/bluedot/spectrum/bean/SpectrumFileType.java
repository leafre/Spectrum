package bluedot.spectrum.bean;

public class SpectrumFileType {
	private Integer spectrumFileTypeID; // 光谱文件格式编号
	private String spectrumFileType; // 光谱文件格式名称

	/**
	 * 默认构造函数
	 */
	public SpectrumFileType() {
	}

	/**
	 * 方法: 光谱文件格式编号和光谱文件格式名称进行构造
	 * @param spectrumFileTypeID
	 * @param spectrumFileType
	 */
	public SpectrumFileType(Integer spectrumFileTypeID, String spectrumFileType) {
	}

	public Integer getSpectrumFileTypeID() {
		return spectrumFileTypeID;
	}

	public void setSpectrumFileTypeID(Integer spectrumFileTypeID) {
		this.spectrumFileTypeID = spectrumFileTypeID;
	}

	public String getSpectrumFileType() {
		return spectrumFileType;
	}

	public void setSpectrumFileType(String spectrumFileType) {
		this.spectrumFileType = spectrumFileType;
	}

}
