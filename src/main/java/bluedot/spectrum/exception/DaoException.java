package bluedot.spectrum.exception;

/**
 * 数据库操作异常 
 * @author longshu 2016年8月10日
 */
public class DaoException extends Exception {
	private static final long serialVersionUID = 1L;

	public DaoException() {
		super();
	}

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public DaoException(String message) {
		super(message);
	}

	public DaoException(Throwable cause) {
		super(cause);
	}
}
