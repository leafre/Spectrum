<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>光谱操作</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">光谱操作</h1>
						<p class="description">您可以在此页面上对选择的光谱进行操作（若想体验更多操作可申请成为实验人员）。</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="row">
				<div class="col-md-10">
					<!-- Flat panel -->
					<div class="panel panel-flat">
						<!-- Add class "collapsed" to minimize the panel -->
						<div id="echarts" class="viewport" style="height:500px;"></div>
					</div>
				</div>
				<div class="col-md-2">
					<!-- Inverted colors panel -->
					<div class="panel panel-default panel-border panel-shadow">
						<!-- Add class "collapsed" to minimize the panel -->
						<div class="panel-heading">
							<h3 class="panel-title">
								<strong>操作区域</strong>
							</h3>
						</div>
						<div class="panel-body">
							<div class="gallery-sidebar">
								<ul class="list-unstyled">
									<li>
										<a href="javascript:showAddSpectrum();">
											<span>添加谱图</span>
										</a>
									</li>
									<hr />
									<li>
										<a href="javascript:;">
											<span>分层显示</span>
										</a>
									</li>
									<hr />
									<li>
										<a href="javascript:;">
											<span>重叠显示</span>
										</a>
									</li>
									<hr />
									<li>
										<a href="javascript:;">
											<span>选择工具</span>
										</a>
									</li>
									<hr />
									<li>
										<a href="javascript:;">
											<span>峰高工具</span>
										</a>
									</li>
									<hr />
									<li>
										<a href="javascript:;">
											<span>峰面积工具</span>
										</a>
									</li>
									<hr />
									<li>
										<a href="javascript:;">
											<span>平均值测量</span>
										</a>
									</li>
									<hr />
									<li>
										<a href="javascript:;">
											<span>噪声测量</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<form role="form" class="form-horizontal">
							<div class="form-group">

								<label class="col-sm-2 control-label">选择谱线颜色：</label>
								<div class="col-sm-2">
									<div class="input-group">
										<input type="text" class="form-control colorpicker" data-format="hex"
											value="#5a3d3d" />

										<div class="input-group-addon">
											<i class="color-preview"></i>
										</div>
									</div>
								</div>
								<a type="button" class="btn btn-success btn-xs pull-right">查看该光谱分析报告</a>
							</div>
						</form>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<!-- addSpectrum -->
	<div class="modal fade" id="addSpectrum_modal">
		<div class="modal-dialog">
			<div class="panel panel-default">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="panel-title">添加光谱</h4>
					<div class="panel-options">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#favorite_tab" data-toggle="tab">收藏夹选择</a>
							</li>
							<li>
								<a href="<c:url value="/uploadSpectrum.jsp"/>">本地上传</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="panel-body">
					<div class="tab-content">
						<div class="tab-pane active" id="favorite_tab">
							<div class="row">
								<div class="col-md-12">
									<form role="form" class="form-horizontal">
										<div class="form-group">
											<label class="control-label">请选择收藏夹</label>
											<select class="form-control" id="favorite_select">
											</select>
										</div>
									</form>
								</div>
							</div>
							<section class="mailbox-env">
								<div class="mail-env">
									<table class="table table-bordered spectrums_table">
										<!-- table header -->
										<thead>
											<tr>
												<th>编号</th>
												<th>名称</th>
												<th>类型</th>
												<th>描述</th>
												<th>时间</th>
											</tr>
										</thead>
										<!-- favorite spectrum list -->
										<tbody>
											<!-- <tr>
												<td class="spectrumID">编号</td>
												<td class="spectrumName spectrum_name">
													<a href="javascript:;"
														onclick="jQuery('#modal-1').modal('show', {backdrop: 'fade'});"
														class="col-name"> 名称 </a>
												</td>
												<td class="spectrumType">光谱类型</td>
												<td class="description">光谱描述</td>
												<td class="saveTime">收藏时间</td>
											</tr> -->
										</tbody>
									</table>
								</div>
							</section>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
								<button type="submit" class="btn btn-info">确定</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
	<script src="assets/js/colorpicker/bootstrap-colorpicker.min.js"></script>
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/select2/select2_locale_zh-CN.js"></script>
	<script src="assets/js/echarts/echarts.js "></script>
	<!-- <script src="assets/js/echarts/echarts.common.min.js "></script> -->
	<script src="<c:url value='assets/js/echarts/echarts_function.js'/>"></script>
	<!--echarts_demo-->
	<!-- <script src="assets/js/echarts/echarts_demo.js "></script> -->
	<script src="assets/js/custom/spectrum.js"></script>
	<script src="assets/js/custom/SpectrumFile.js"></script>
	<script src="assets/js/custom/spectrumOperate.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
	var none = getUrlParam('none');// 无操作
	if(null==none){
		var tempSpectrumFile,spectrumID,sequence;
		// 保存转发过来的光谱信息
		<c:if test="${tempSpectrumFile != null}">
			tempSpectrumFile = "${tempSpectrumFile}";
		</c:if>
		<c:if test="${spectrumID != null}">
			spectrumID = "${spectrumID}";
		</c:if>
		<c:if test="${sequence != null}">
			spectrumID = "${sequence}";
		</c:if>
		
		// 转发的数据
		var param = {};
		if(tempSpectrumFile!=undefined){
			param.tempSpectrumFile = tempSpectrumFile;
		}
		if(spectrumID!=undefined){
			param.spectrumID = spectrumID;
		}
		if(sequence!=undefined){
			param.sequence = sequence;
		}
		//URL的数据
		if(param.tempSpectrumFile==undefined){
			param.tempSpectrumFile = getUrlParam('tempSpectrumFile');
		}
		if(param.spectrumID==undefined){
			param.spectrumID = getUrlParam('spectrumID');
		}
		if(param.sequence==undefined){
			param.sequence = getUrlParam('sequence');
		}
		//console.log(param);
		
		setTimeout('showSpectrumFile(param)',50);
	}
	</script>
</body>

</html>