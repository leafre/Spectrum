<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>查看报告</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">报告</h1>
						<p class="description">描述</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<%-- 内容 --%>
			<div class="row">
				<div class="col-md-12">
					<!-- Default panel -->
					<div class="panel panel-default">
						<div class="panel-heading">
							报告名称
							<p class="description pull-right">2014-11-11 13:23:32</p>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<!-- Flat panel -->
									<div class="panel panel-flat">
										<!-- Add class "collapsed" to minimize the panel -->
										<div id="echarts" class="viewport " style="height: 500px;"></div>
									</div>
								</div>
							</div>
							<hr />
							<div class="row">
								<section class="invoice-env">
									<!-- Invoice header -->
									<div class="invoice-header">
										<!-- Invoice Data Header -->
										<div class="invoice-logo">
											<ul class="list-unstyled">
												<li class="upper">
													<strong>光谱类型：</strong>红外光谱
												</li>
												<li>
													<strong>光谱文件类型：</strong>.cov
												</li>
												<li>
													<strong>是否标准库：</strong>NO
												</li>
												<li>
													<strong>分辨率：</strong>4.0
												</li>
												<li>
													<strong>X轴单位：</strong>波数
												</li>
												<li>
													<strong>Y轴单位：</strong>吸光度
												</li>
												<li>
													<strong>检测设备：</strong>WQF-510A傅立叶变换红外光谱仪
												</li>
												<li>
													<strong>使用的处理算法：</strong>纵坐标归一化
												</li>
												<li>
													<strong>保存时间：</strong>2011年11月11日
												</li>
											</ul>
										</div>
									</div>
									<!-- Client and Payment Details -->
									<div class="invoice-details">
										<div class="invoice-client-info">
											<strong>特征峰值：</strong>
											<ul class="list-unstyled">
												<li>211、345、567.8、321.9、890.0、1111.1</li>
												<li>1232.5、1543.9、1765.2、2121.2</li>
											</ul>
										</div>
										<div class="invoice-payment-info"></div>
									</div>
									<hr />
									<div class="invoice-details">
										<div class="invoice-client-info">
											<strong>检测数据：（被检测物：沙糖桔）</strong>
										</div>
									</div>
									<!-- Invoice Entries -->
									<table class="table table-bordered">
										<thead>
											<tr class="no-borders">
												<th class="text-center hidden-xs">#</th>
												<th width="60%" class="text-center">检测内容</th>
												<th class="text-center hidden-xs">浓度</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="text-center hidden-xs">1</td>
												<td>On am we offices expense thought</td>
												<td class="text-center hidden-xs">1</td>
											</tr>

											<tr>
												<td class="text-center hidden-xs">2</td>
												<td>Up do view time they shot</td>
												<td class="text-center hidden-xs">1</td>
											</tr>

											<tr>
												<td class="text-center hidden-xs">3</td>
												<td>Way ham unwilling not breakfast</td>
												<td class="text-center hidden-xs">1</td>
											</tr>

											<tr>
												<td class="text-center hidden-xs">4</td>
												<td>Songs to an blush woman be sorry</td>
												<td class="text-center hidden-xs">1</td>
											</tr>

											<tr>
												<td class="text-center hidden-xs">5</td>
												<td>Luckily offered article led lasting</td>
												<td class="text-center hidden-xs">1</td>
											</tr>

											<tr>
												<td class="text-center hidden-xs">6</td>
												<td>Of as by belonging therefore suspicion</td>
												<td class="text-center hidden-xs">1</td>
											</tr>
										</tbody>
									</table>
								</section>
							</div>
						</div>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<!-- Imported scripts on this page -->
	<!--echarts_demo-->
	<script src="assets/js/echarts/echarts.common.min.js "></script>
	<script src="assets/js/echarts/echarts_demo.js "></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
	</script>
</body>

</html>