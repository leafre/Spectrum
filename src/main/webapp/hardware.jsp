<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="date" uri="/WEB-INF/datetag.tld"%>
<!DOCTYPE html>
<html>
<head>
<title>硬件管理</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
<!-- Imported styles on this page -->
<link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="assets/js/select2/select2.css">
<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="assets/js/multiselect/css/multi-select.css">
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">硬件管理</h1>
						<p class="description">您可以在此页面对系统支持的硬件进行添加、修改和查看</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<!-- 查看 -->
			<div class="panel panel-default info-block" id="list-view">
				<div class="panel-heading">
					<h2 class="panel-title">检测设备详情</h2>
					<div class="header-buttons">
						<button type="button" class="btn btn-info btn-single pull-right"
							id="btn-view-edit">修改</button>
					</div>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<h5 class="col-sm-4 control-label">硬件编号:&nbsp&nbsp
							<font class="prop-value" id="hardwareID-view" color="black" size="3"></font>
						</h5>
						<h5 class="col-sm-4 control-label">硬件名称:&nbsp&nbsp
							<font class="prop-value" id="hardwareName-view" color="black" size="3"></font>
						</h5>
						<h5 class="col-sm-4 control-label">添加时间:&nbsp&nbsp
							<font class="prop-value" id="addTime-view" color="black" size="3"></font>
						</h5>
					</div>
					<div class="form-group-separator"></div>
					<div class="form-group">
						<h5 class="col-sm-4 control-label">检测光谱类型:&nbsp&nbsp
							<font class="prop-value" id="spectrumType-view" color="black" size="3"></font>
						</h5>
						<h5 class="col-sm-4 control-label">检测光谱文件类型:&nbsp&nbsp
							<font class="prop-value" id="spectrumFileType-view" color="black" size="3"></font>
						</h5>
					</div>
				</div>
			</div>
			<!-- 添加 -->
			<div class="panel panel-default info-block" id="list-add" style="display:none;" >
				<div class="panel-heading">
					<h2 class="panel-title">添加设备</h2>
					<div class="form-group pull-right">
						<button type="button" class="btn btn-info" id="btn-save-add">确定添加</button>
						<button type="button" class="btn btn-cancel" onclick="">取消</button>
					</div>
				</div>
				<div class="panel-body">
					<form action="addDetectedObject.do" role="form" class="form-horizontal" method="post">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="hardwareName" disabled="true ">硬件名称:</label>
							<div class=" col-sm-2">
								<input type="text" class="form-control" id="hardwareName"
									placeholder="硬件名称" />
							</div>
							<label class="col-sm-2 control-label" for="hardwareType">硬件型号:</label>
							<div class=" col-sm-2">
								<input type="text" class="form-control col-sm-2"
									id="hardwareType" placeholder="硬件型号" />
							</div>
						</div>
						<div class="form-group-separator"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="addSpectrumType">检测光谱类型:</label>
							<div class=" col-sm-2">
								<select class="form-control" name="spectrumTypeID" id="addSpectrumType">
								</select>
							</div>
							<label class="col-sm-2 control-label" for="addSpectrumFileType">检测光谱文件类型:</label>
							<div class=" col-sm-2">
								<select class="form-control" name="spectrumFileTypeID" id="addSpectrumFileType">
								</select>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- 修改 -->
			<div class="panel panel-default info-block" id="list-edit" style="display:none;">
				<div class="panel-heading">
					<h2 class="panel-title">修改设备</h2>
					<div class="form-group pull-right">
						<button type="button" class="btn btn-info" id="btn-save-edit">保存更改</button>
						<button type="button" class="btn btn-cancel">取消</button>
					</div>
				</div>
				<div class="panel-body">
					<form action="addDetectedObject.do" role="form"
						class="form-horizontal" method="post">
						<div class="form-group">
							<label class="col-sm-2 control-label">硬件编号:</label>
							<div class=" col-sm-2">
								<input type="text" class="form-control" id="hardwareID-edit" disabled="disabled"/>
							</div>
							<label class="col-sm-2 control-label">硬件名称:</label>
							<div class=" col-sm-3">
								<input type="text" class="form-control col-sm-2" id="hardwareName-edit" />
							</div>
						</div>
						<div class="form-group-separator"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label">检测光谱文件类型:</label>
							<div class=" col-sm-2">
								<select class="form-control" name="spectrumFileTypeID" id="SpectrumFileType-edit">
								</select>
							</div>
							<label class="col-sm-2 control-label">检测光谱类型:</label>
							<div class=" col-sm-3">
								<select class="form-control" name="spectrumTypeID" id="SpectrumType-edit">
								</select>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- 硬件列表 -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h2 class="panel-title">已有设备列表</h2>
					<div class="form-group pull-right">
						<button class="btn btn-info btn-icon btn-icon-standalone " id="btn-add">
							<i class="fa-plus"></i> <span>添加</span>
						</button>
						<button class="btn btn-danger btn-icon btn-icon-standalone" id="btn-del">
							<i class="fa-remove"></i> <span>批量删除</span>
						</button>
						<button class="btn btn-gray btn-icon btn-icon-standalone" id="btn-refresh">
							<i class="fa-refresh"></i> <span>刷新</span>
						</button>
					</div>
				</div>
				<div class="panel-body">
					<table class="table table-striped table-bordered dataTable no-footer" id="hardware-table">
						<thead>
							<tr role="row">
								<th>
									<div class="cbr-replaced" id="check-all">
										<div class="cbr-input">
											<input type="checkbox" class="cbr cbr-done">
										</div>
										<div class="cbr-state">
											<span></span>
										</div>
									</div>
								</th>
								<th class="sorting">设备编号</th>
								<th>设备名称</th>
								<th>添加时间</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<div class="modal fade" id="message-modal" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">提示</h4>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>
	<!-- Imported scripts on this page -->
	<script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/select2/select2_locale_zh-CN.js"></script>
	<script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script
		src="assets/js/datatables/tabletools/dataTables.tableTools.min.js"></script>
	<script src="assets/js/dateformat.js"></script>
	<script src="assets/js/custom/spectrum.js"></script>
	<script src="assets/js/custom/hardware.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
	
	
	</script>
</body>

</html>