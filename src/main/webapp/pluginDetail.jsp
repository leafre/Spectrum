<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>插件详细信息</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">插件详细信息</h1>
						<p class="description">您可以此页面查看算法插件的详细信息</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading pluginDet">
							<div class="col-sm-6">
								<h3>
									<strong>${querydata[0].pluginName}</strong>
								</h3>
							</div>
							<div class="panel-options">
								<a title="21" href="javascript:;" data-action="deletePlugin"class="btn btn-gray btn-icon">
									<i class="fa-trash"></i>
								</a>
								<button type="button" class="btn btn-danger dropdown-toggle"
									data-toggle="dropdown">
									<i class="fa-download"></i>
								</button>
							</div>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-6">
									<h3>
										<strong>基本信息</strong>
									</h3>
									<div class="panel panel-flat">
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>名称</th>
													<th>内容</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>算法类型</td>
													<td class="middle-align">${querydata[0].pluginType}</td>
												</tr>
												<tr>
													<td>算法版本号</td>
													<td class="middle-align">${querydata[0].version}</td>
												</tr>
												<tr>
													<td>适用硬件型号</td>
													<td class="middle-align">
														<c:forEach var="result" items="${data}">
															${result.hardwareName}
														</c:forEach>
													</td>
												</tr>
												<tr>
													<td>适用光谱类型</td>
													<td class="middle-align">
														<c:forEach var="result" items="${data}">
															${result.spectrumType}
														</c:forEach>
													</td>
												</tr>
												<tr>
													<td>可检测物质</td>
													<td class="middle-align">苹果</td>
												</tr>
												<tr>
													<td>可检测内容</td>
													<td class="middle-align">糖份浓度</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">
								<strong>算法描述</strong>
							</h3>
							<div class="panel-options">
								<a href="#" data-toggle="panel">
									<span class="collapse-icon">&ndash;</span> <span class="expand-icon">+</span>
								</a>
							</div>
						</div>
						<div class="panel-body">
							<div class="scrollable" data-max-height="400">
								${querydata[0].description}
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">
								<strong>算法内容（代码）</strong>
							</h3>
							<div class="panel-options">
								<a href="#" data-toggle="panel">
									<span class="collapse-icon">&ndash;</span> <span class="expand-icon">+</span>
								</a>
							</div>
						</div>
						<div class="panel-body">
							<div class="scrollable" data-max-height="400">
								<p></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="modal-footer">
						<a title="${querydata[0].pluginID}"href="javascript:;" onclick="showRefuseReason(this)"
							class="btn btn-primary btn-single btn-sm pull-right">拒绝申请</a>
						<a href="auditPlugin.do?state=通过&pluginID=${querydata[0].pluginID}" class="btn btn-primary btn-single btn-sm pull-right">通过审核</a>
					</div>
				</div>
			</div>
			
			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<!-- Modal 1 (Custom Width)-->
	<div class="modal fade" id="deletePlugin_modal" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">确认提示（警告！）</h4>
				</div>
				<div class="modal-body">
					您确定要删除么？（删除后将无法找回！）
					<label type="hidden" title=""></label>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">取消</button>
					<button type="submit" class="btn btn-danger">删除</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal 2 (Custom Width)-->
	<div class="modal fade custom-width" id="refuseReason_modal">
		<div class="modal-dialog" style="width: 60%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">拒绝通过的原因</h4>
				</div>
				<div class="modal-body">
					<form method="post" action="auditPlugin.do" class="profile-post-form">
						<textarea name="reason" class="form-control input-unstyled input-lg autogrow"
							placeholder="请您填写拒绝通过的原因。。。"></textarea>
						<input type="hidden" value="" name="pluginID"/>
						<i class="el-edit block-icon"></i>
						<hr/>
						<button type="submit" class="btn btn-single btn-xs btn-success post-story-button pull-right">提交</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Imported scripts on this page -->
	<!--Specific JS for this page-->
	<script type="text/javascript">
		//删除插件
		$('.pluginDet a[data-action="deletePlugin"]').on('click', function(ev) {
			ev.preventDefault();
			var pluginID = $(this).attr('title');
			//$('#deletePlugin_modal label').attr('title',pluginID);
			jQuery('#deletePlugin_modal').modal('show', {backdrop: 'fade'});
			
			$('#deletePlugin_modal button[type="submit"]').on('click',function(){
				//var pluginID = $('#deletePlugin_modal label').attr('title');
				$.ajax({
					url: 'deletePlugin.do',
					type: 'post',
					dataType: 'json',
					data: {
						pluginID: pluginID,
					},
					success: function(jsondata) {
						alert(jsondata.deletePlugin_msg);
						/* if('删除成功'==jsondata.deletePlugin_msg){
							$('#deletePlugin_modal').modal('hide');
						} */
						history.go(-1);
					},error: function(){
						alert('服务器错误,删除失败!');
					}
				});
			});
			
		});
			
		//审核插件
		function showRefuseReason(obj){
			var pluginID = $(obj).attr('title');
			alert(pluginID);
			$('#auditPlugin_modal input').attr('value',pluginID);
			jQuery('#refuseReason_modal').modal('show');
		}
	</script>
</body>

</html>