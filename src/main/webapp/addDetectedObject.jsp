<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<title>添加被检测物</title>
	<%-- 全站样式 --%>
	<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">添加被检测物</h1>
						<p class="description">您可以在此页面上添加被检测物（我们已经录入了被检测物的标准分类）</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" /> </nav>

			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">请填写添加信息<i class="text-danger">（*为必填项）</i></h3>
						</div>
						<div class="panel-body">
							<form action="addDetectedObject.do" role="form" class="form-horizontal" method="post">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">被检测物分类<i class="text-danger">&nbsp;*</i></label>
									<div class="col-sm-9">
										<div class="col-sm-3">
											<strong>一级分类：</strong>
											<select class="form-control" id="fristDetectedCategoryIDs">
											<option value="0">一级分类</option>
											</select>
										</div>
										<div class="col-sm-3">
											<strong>二级分类：</strong>
											<select class="form-control" id="secondDetectedCategoryIDs">
											<option value="0">二级分类</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="detectedName">被检测物名称<i class="text-danger">&nbsp;*</i></label>
									<div class="col-sm-7">
										<input type="text" class="form-control" name="detectedName" id="detectedName" placeholder="请谨慎填写，名称一经填写不允许修改">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="detectedName">被检测物英文名<i class="text-danger">&nbsp;*</i></label>
									<div class="col-sm-7">
										<input type="text" class="form-control" name="version" id="version" placeholder="请谨慎填写，英文名一经填写不允许修改">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="detectedName">被检测物化学式<i class="text-danger">&nbsp;*</i></label>
									<div class="col-sm-7">
										<input type="text" class="form-control" name="version" id="version" placeholder="请谨慎填写，化学式一经填写不允许修改">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label">可检测内容</label>
									<div class="col-sm-7">
										<select class="form-control" name="contentIDs" id="contentIDs" multiple>
										</select>
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>检测内容</th>
													<th>检测标准</th>
													<th>标准线</th>
													<th>波动范围</th>
													<th>标准线单位</th>
												</tr>
											</thead>
											<tbody class="middle-align" id="detectedStandardIDs">
<!--  
												<tr>
													<td>脂肪</td>
													<td>GB/T 30942-2014</td>
													<td>60.5</td>
													<td>5</td>
													<td>毫克</td>
												</tr>
												<tr>
													<td>${Contents.contentName}</td>
													<td>${detectedStandard.standards}</td>
													<td>${detectedStandard.standardLine}</td>
													<td>${detectedStandard.value}</td>
													<td>${detectedStandard.unit}</td>
												</tr>
-->
											</tbody>
										</table>
									</div>
								</div>
<!-- 
								<div class="form-group-separator"></div>
								<div class="form-group">
									<div class="panel" style="margin-left:7%; margin-right:7%;">
									<c:forEach begin="0" end="4">
										<div class="panel panel-default panel-border panel-shadow col-sm-3" style="margin: 3%;">
											<div class="panel-heading">
												<h3 class="panel-title">可溶性固形物</h3>
												<div class="panel-options">
													<a href="#" data-toggle="panel">
														<span class="collapse-icon">&ndash;</span>
														<span class="expand-icon">+</span>
													</a>
													<a href="#" data-toggle="remove">
														&times;
													</a>
												</div>
											</div>
											<div class="panel-body">
												<div class="row">
													<label class="col-sm-6 control-label">检测标准<i class="text-danger">&nbsp;*</i></label>
													<div class="col-sm-6">
														<input type="text" class="form-control" name="standards" placeholder="下拉选择">
													</div>
												</div>
												<div class="row">
													<label class="col-sm-6 control-label">标准线<i class="text-danger">&nbsp;*</i></label>
													<div class="col-sm-6">
														<input type="text" class="form-control" name="standards" placeholder="数值精确到小数点后两位">
													</div>
												</div>
												<div class="row">
													<label class="col-sm-6 control-label">波动范围<i class="text-danger">&nbsp;*</i></label>
													<div class="col-sm-6">
														<input type="text" class="form-control" name="standards" placeholder="数值拖动框">
													</div>
												</div>
												<div class="row">
													<label class="col-sm-6 control-label">标准线单位<i class="text-danger">&nbsp;*</i></label>
													<div class="col-sm-6">
														<input type="text" class="form-control" name="standards" placeholder="下拉选择">
													</div>
												</div>
											</div>
										</div>
										</c:forEach>
									</div>
								</div>
-->
								<div class="form-group-separator"></div>
								<div class="form-group col-sm-6">
									<button type="submit" class="btn btn-info pull-right">确认添加</button>
								</div>
								<h3 style="color: red;"> ${addDetected_msg}</h3>
							</form>
						</div>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<div class="modal fade" id="addDetectedObject_msg_modal" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">系统提示信息</h4>
				</div>
				<div class="modal-body">
					<h3>${addDetectedObject_msg}</h3>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${!empty addDetectedObject_msg }">
	<script type="text/javascript">
		$(function() {
			$("#addDetectedObject_msg_modal").modal('show');
		});
	</script>
	</c:if>

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="assets/js/multiselect/css/multi-select.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/select2/select2_locale_zh-CN.js"></script>
	<script src="assets/js/custom/addDetected.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
	</script>
</body>

</html>