<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>查看用户</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- user Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">查看用户列表</h1>
						<p class="description">您可以在此页面上查看用户列表及其用户的详细信息以及冻结账户。</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>
			
			<div class="row">
				<!-- Tabbed panel 2 -->
				<div class="panel panel-default panel-tabs">
					<!-- Add class "collapsed" to minimize the panel -->
					<div class="panel-heading">
						<h3 class="panel-title">用户列表</h3>
						<div class="panel-options">
							<ul class="nav nav-tabs" id="nav_tabs">
								<li class="user">
									<a href="<c:url value="/users.do?roleID=4" />">普通用户</a>
								</li>
								<li class="lab">
									<a href="<c:url value="/users.do?roleID=3" />">实验员</a>
								</li>
								<li class="frozen">
									<a href="<c:url value="/users.do?state=冻结" />">已冻结用户</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div class="tab-pane active" id="tab_user">
								<div class="panel panel-default">
									<div class="panel-body">
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>用户名</th>
													<th>真实姓名</th>
													<th>邮箱地址</th>
													<th>操作</th>
												</tr>
											</thead>
											<tbody class="middle-align" id="middle-align">
												<c:forEach var="user" items="${querydata}"  varStatus="s">
													<tr>
														<td>${user.username}</td>
														<td>${user.realName}</td>
														<td>${user.email}</td>
														<td><a href="javascript:;"
															onclick="userDetailEvent(${user.userID})"
															class="btn btn-info btn-sm btn-icon icon-left" > 详细信息</a> 
														<c:if test="${user.state eq '正常'}">
														<a id="${user.userID }" href="#" onclick="thrawUser('${user.userID}','${user.email}')"
															class="btn btn-danger btn-sm btn-icon icon-left">
															冻结 </a>
														</c:if>
														</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<div class="modal fade" id="modal-3">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">请输入冻结理由</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<form class="validate">
								<div class="form-group">
									<textarea id="reason" class="form-control autogrow" cols="50" rows="70"></textarea>
								</div>
								<div class="col-md-6">
								</div>
								<div id="message" class="col-md-6">
								</div>
								<div class="form-group">
									<label class="col-sm-8 control-label">输入解冻日期</label>
									<div class="col-sm-9">
										<input type="text" id="year" size="10">年
										<input type="text" id="month" size="10">月
										<input type="text" id="day" size="10">日
									</div>
									<div class="col-sm-3">
									   <button type="button" id="sure" u="1" e="1"  class="btn btn-danger" onclick="sureThraw();">确定</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="modal-2">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">详细信息</h4>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-6">

									<div class="form-group">
										<label for="field-1" class="control-label">用户名</label>

										<input type="text" class="form-control" id="field-1"
											placeholder="" disabled />
									</div>
								</div>
								<div class="col-md-6">

									<div class="form-group">
										<label for="field-1" class="control-label">用户类型</label>

										<input type="text" class="form-control" id="field-2"
											placeholder="" disabled />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="field-1" class="control-label" >性别</label>

										<div class="radio">
											<label id="field-3">
												
											</label>
										</div>
										
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<label for="field-1" class="control-label">出生年月</label>

										<input type="text" class="form-control" id="field-4"
											placeholder="" disabled />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="field-1" class="control-label">邮箱地址</label>
										<input type="text" class="form-control" id="field-5"
											placeholder="" disabled />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="field-1" class="control-label">手机号码</label>
										<input type="text" class="form-control" id="field-6"
											placeholder="John" disabled />
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<img src="assets/images/user-4.png" alt="user-img"
								class="img-cirlce img-responsive img-thumbnail" id="field-7" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">详细地址</label>
								<input type="text" class="form-control" id="field-8" placeholder=""
									disabled />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">

							<div class="form-group">
								<label for="field-1" class="control-label">真实姓名</label>
								<input type="text" class="form-control" id="field-9" placeholder=""
									disabled />
							</div>

						</div>
						<div class="col-md-8">

							<div class="form-group">
								<label for="field-1" class="control-label">身份证号</label>
								<input type="text" class="form-control" id="field-10" placeholder=""
									disabled />
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label for="field-1" class="control-label">工作单位</label>

								<input type="text" class="form-control" id="field-11" placeholder=""
									disabled />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="field-1" class="control-label">职位</label>

								<input type="text" class="form-control" id="field-12" placeholder=""
									disabled />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">个人描述</label>

								<textarea class="form-control autogrow" cols="5" id="field-13"
									placeholder="I will grow as you enter new lines." disabled></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<div class="form-group">
								<label for="field-1" class="control-label">账号状态</label>

								<div class="radio">
									<label  id="field-14">
									<input  type="radio" checked disabled>
									
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
	<script src="assets/js/dateformat.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$("#table_user,#table_lab,#table_frozenuser").dataTable({
				aLengthMenu: [
					[5, 10, 25, 50, 100, -1],
					[5, 10, 25, 50, 100, "所有"]
				]
			});
		});
		jQuery(document).ready(function($) {
			var roleID = getUrlParam('roleID');
			var $nav_tabs = $('#nav_tabs');
			// 修改高亮
			if(null == roleID) {
				$nav_tabs.find('.frozen').addClass('active');
			} else if('4' == roleID) {
				$nav_tabs.find('.user').addClass('active');
			} else if('3' == roleID) {
				$nav_tabs.find('.lab').addClass('active');
			}
		});
		function userDetailEvent(userID){
			 $.ajax({  
	             url: 'queryUserDetail.do',  
	             data: "userID="+userID,
	             type: 'post',  
	             dataType: 'json',  
	             success: succFunction //成功执行方法    
	         })  
	         function succFunction(jsondata) {  
	             //eval将字符串转成对象数组  
	             //var json = { "id": "10086", "uname": "zhangsan", "email": "zhangsan@qq.com" };  
	             //json = eval(json);  
	             //alert("===json:id=" + json.id + ",uname=" + json.uname + ",email=" + json.email);  
	              var username; var tel; var address;
	              var rolename; var gender; var birthday;
	              var email; var realname; var IDNumber;
	              var institute; var position; var detail;
	              var state; var photo;
	             var json = eval(jsondata.querydata); //数组         
	             $.each(json, function (index, item) {  
	                 //循环获取数据    
	                  username = json[index].username;  
	                  tel = json[index].tel;  
	                  address = json[index].address;
	                  rolename=json[index].roleName;
	                  gender=json[index].gender;
	                  birthday=new Date(json[index].birthday).format("yyyy/mm/dd");
	                  email=json[index].email;
	                  realname=json[index].realName;
	                  IDNumber=json[index].IDNumber;
	                  institute=json[index].institute;
	                  position=json[index].position;
	                  detail=json[index].detail;
	                  state=json[index].state;
	                  photo=json[index].photo;
	                  $("#field-1").attr("placeholder",username);$("#field-2").attr("placeholder",rolename);
	 	             $("#field-3").html("<input type='radio' checked disabled>"+gender);$("#field-4").attr("placeholder",birthday);
	 	             $("#field-5").attr("placeholder",email);$("#field-6").attr("placeholder",tel);
	 	             $("#field-7").attr("{src:"+photo );$("#field-8").attr("placeholder",address);
	 	             $("#field-9").attr("placeholder",realname);$("#field-10").attr("placeholder",IDNumber);
	 	             $("#field-11").attr("placeholder",institute);$("#field-12").attr("placeholder",position);
	 	             $("#field-13").attr("placeholder",detail);  $("#field-14").html("<input type='radio' checked disabled>"+state);
	         	});  
	         }
			jQuery('#modal-2').modal('show', {backdrop: 'fade'});
		}
			 
		function thrawUser(userID,email){
			jQuery('#modal-3').modal('show', {backdrop: 'fade'});
			$("#sure").attr("u",userID);
			$("#sure").attr("e",email);
		}
		
		function sureThraw(){
			var reason=$("#reason").val();
			if(reason.length>256){
				reason=reason.substring(0,256);
				$("#reason").val(reason);
				$("#message").html("冻结理由最多256个字符!");
				return false;
			}
			if(reason.length==0){
				$("#message").html("冻结理由是必填的!");
				return false;
			}
			var userID=$("#sure").attr("u");
			var email=$("#sure").attr("e");
			if($("#year").val().length==0||$("#month").val().length==0||$("#day").val().length==0){
				$("#message").html("请输入正确的时间");
				return false;
			}
			var currentTime=new Date().getTime();
			var thawTime=$("#year").val()+"/"+$("#month").val()+"/"+$("#day").val();
			thawTime=new Date(thawTime).getTime();
			if(currentTime>thawTime){
				alert("解冻时间不合理");
				return false;
			}
			 $.ajax({  
	             url: 'frozen.do',  
	             data: {
	            	 userID: userID,
	            	 email: email,
	            	 reason: reason,
	            	 thawTime: thawTime
	             },
	             type: 'post',  
	             dataType: 'json',  
	             success: successFunction //成功执行方法    
	         })
	        
	      	function successFunction(jsondata){
				alert(jsondata.forzen_msg);
				$('#modal-3').modal('hide');
			    var obj=document.getElementById(jsondata.userID);
			    $(obj).remove();
			}
		}
	</script>
</body>

</html>