<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<title>上传光谱</title>
	<%-- 全站样式 --%>
	<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">上传光谱</h1>
						<p class="description">描述</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">请填写上传信息</h3>
						</div>
						<div class="panel-body">
							<form action="uploadSpectrum.do" role="form" class="form-horizontal" method="post" enctype="multipart/form-data">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="spectrumFile">光谱文件 </label>
									<div class="col-sm-8">
										<input id="spectrumFile" type="file" name="spectrumFile" class="control-label" />
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="spectrumName">光谱名称</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="spectrumName" id="spectrumName" placeholder="请填写您的光谱名称">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label">检测硬件 </label>
									<div class="col-sm-8">
										<select class="form-control" name="hardwareID" id="hardwareID">
											<option value="0">请选择检测硬件</option>
										</select>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label">光谱类型</label>
									<div class="col-sm-8">
										<select class="form-control" name="spectrumTypeID" id="spectrumTypeID">
											<option value="0">请选择光谱类型</option>
										</select>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label">光谱文件类型</label>
									<div class="col-sm-8">
										<select class="form-control" name="spectrumFileTypeID" id="spectrumFileTypeID">
											<option value="0">请选择光谱文件类型</option>
										</select>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label">被检测物</label>
									<div class="col-sm-8">
										<select class="form-control" name="detectedID" id="detectedID">
											<option value="0">请选择被检测物</option>
										</select>
									</div>
									<!--<div class="col-sm-8">
								<input type="hidden" name="detectedID" id="detectedID" />
							</div>-->
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label">检测内容</label>
									<div class="col-sm-8">
										<select class="form-control" name="contentsIDs" id="contentsIDs">
											<option value="0">请选择检测内容</option>
										</select>
									</div>
									<!--  
							<div class="col-sm-8">
								<input type="hidden" name="contentIDs" id="contentIDs" />
							</div>-->
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group col-sm-6">
									<button type="button" class="btn btn-gray btn-single pull-right">取消</button>
									<button type="submit" class="btn btn-info btn-single pull-right">上传</button>
								</div>
								<h2 style="color: red;"> ${uploadSpectrum_msg}</h2>
							</form>
						</div>
					</div>
				</div>
			</div>
			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>

	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<div class="modal fade" id="uploadSpectrum_msg_modal" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">系统提示信息</h4>
				</div>
				<div class="modal-body">
					<h3>${uploadSpectrum_msg}</h3>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${!empty uploadSpectrum_msg }">
		<script type="text/javascript">
			$(function() {
				$("#uploadSpectrum_msg_modal").modal('show');
			});
		</script>
	</c:if>
	
	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="assets/js/multiselect/css/multi-select.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/select2/select2_locale_zh-CN.js"></script>
	<script src="assets/js/custom/uploadSpectrum.js"></script>
	<!--Specific JS for this page-->
</body>

</html>