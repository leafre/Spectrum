<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>首页</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">首页</h1>
						<p class="description">描述</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="row">
				<h3>光谱&hellip;</h3>
				<div class="col-sm-3">
					
					<div class="xe-widget xe-vertical-counter xe-vertical-counter-danger" onclick="modifyPassword()" data-count=".num" data-from="0" data-to="67.9" data-decimal="," data-suffix="%" data-duration="3">
						<div class="xe-icon">
							<i class="linecons-doc"></i>
						</div>
						
						<div class="xe-label">
							<strong class="num">上传光谱</strong>
							<span>自动分析生成报告</span>
						</div>
					</div>
					
				</div>
			</div>
			<!-- <div id="echarts" class="viewport" style="height: 600px;"></div> -->

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />
	
	<!--Specific JS for this page-->
	<!-- <script src="assets/js/echarts/echarts.js"></script> -->
	<!-- <script src="assets/js/echarts/echarts.common.min.js"></script> -->
	<%-- <script src="<c:url value='assets/js/echarts/echarts_function.js'/>"></script> --%>
	<!--echarts_demo-->
	<!-- <script src="assets/js/echarts/echarts_demo.js"></script>
	<script src="assets/js/custom/SpectrumFile.js"></script> -->
	<script src="assets/js/custom/uploadSpectrum.js"></script>
	<script src="assets/js/custom/addDetected.js"></script>
	
	<script type="text/javascript">
	function modifyPassword(){
		//loadMyFavorite($('#session_userID').val());
		jQuery('#modifyPassword_modal').modal('show', {backdrop: 'static'});
		init();
	};
	function init(){
		$("#modifyPassword-1").attr("class","ms-hover active");
	 	$("#modifyPassword-2").removeClass("completed");;
	 	$("#modifyPassword-2").removeClass("active");
	 	$("#modifyPassword-3").removeClass("completed");;
	 	$("#modifyPassword-3").removeClass("active");
	 	$("#modifyPassword-4").removeClass("active");
	 	$("#modifyPassword-progress").width("0%");
	 	$("#fwv-1").addClass("active");
	 	$("#fwv-2").removeClass("active");
	 	$("#fwv-3").removeClass("active");
	 	$("#fwv-4").removeClass("active");
	}
	function f(i){
	 	$("#modifyPassword-"+i).attr("class","ms-hover completed");
	 	$("#modifyPassword-"+(i+1)).attr("class","ms-hover active");
	 	$("#modifyPassword-progress").width(i*25+"%");
	 	$("#fwv-"+i).removeClass("active");
	 	$("#fwv-"+(i+1)).addClass("active");
	}
	</script>
	
	<!-- modifyPassword -->
	<div class="modal fade" id="modifyPassword_modal">
		<div class="modal-dialog">
			<div class="panel panel-default">	
				<h3 class="text-gray">
					导入光谱 <br />
					 <hr />
					<small class="text-muted">按提示逐步填写信息，<strong>请正确填写光谱信息，以防影响分析结果</strong></small>
				</h3>
				<br />
				<form role="forl" id="rootwizard" class="form-wizard validate">
					
					<ul class="tabs">
						<li class="ms-hover active" id="modifyPassword-1">
							<a>
								上传光谱文件
								<span>1</span>
							</a>
						</li>
						<li id="modifyPassword-2">
							<a>
								填写光谱信息
								<span>2</span>
							</a>
						</li>
						<li id="modifyPassword-3">
							<a>
								选择检测内容
								<span>3</span>
							</a>
						</li>
						<li id="modifyPassword-4">
							<a>
								完成
								<span>4</span>
							</a>
						</li>
					</ul>
					
					<div class="progress-indicator" style="width: 0" id="modifyPassword-progress">
						<span></span>
					</div>
					
					<div class="tab-content no-margin">
						
						<!-- Tabs Content -->
						<div class="tab-pane with-bg active" id="fwv-1">
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label" for="full_name">上传文件</label>
										<input class="form-control" type="file" name="spectrumFile"/>
									</div>
								</div>
							</div>
							<div class="row">
							 <div class="col-md-5"></div>
								<div class="col-md-2">
									<a  class="btn btn-gray" href="javascript:f(1);">上传</a>
								</div>
							</div>
						</div>
						
						<div class="tab-pane with-bg" id="fwv-2">
							<form action="uploadSpectrum.do" role="form" class="form-horizontal" method="post" enctype="multipart/form-data">
								<div class="row">
									<div class="col-sm-1 control-label"></div>
									<label class="col-sm-2 control-label" for="spectrumName">光谱名称</label>
									<div class="col-sm-8">
										<div class="form-group">
											<input type="text" class="form-control" name="spectrumName" id="spectrumName" placeholder="请填写您的光谱名称">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-1 control-label"></div>
									<label class="col-sm-2 control-label">光谱类型</label>
									<div class="col-sm-8">
										<div class="form-group">
											<select class="form-control" name="spectrumTypeID" id="spectrumTypeID">
												<option value="0">请选择光谱类型</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-1 control-label"></div>
									<label class="col-sm-2 control-label">文件类型</label>
									<div class="col-sm-8">
										<div class="form-group">
											<select class="form-control" name="spectrumFileTypeID" id="spectrumFileTypeID">
												<option value="0">请选择光谱文件类型</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-1 control-label"></div>
									<label class="col-sm-2 control-label">检测硬件 </label>
									<div class="col-sm-8">
										<div class="form-group">
											<select class="form-control" name="hardwareID" id="hardwareID">
												<option value="0">请选择检测硬件</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-1 control-label"></div>
									<label class="col-sm-2 control-label">被检测物</label>
									<div class="col-sm-9 ">
										<div class="form-group">
											<div class="col-sm-5">
												<strong>一级分类：</strong>
												<select class="form-control" id="fristDetectedCategoryIDs">
												<option value="0">一级分类</option>
												</select>
											</div>
											<div class="col-sm-6">
												<strong>二级分类：</strong>
												<select class="form-control" id="secondDetectedCategoryIDs">
												<option value="0">二级分类</option>
												</select>
											</div>
											<div class="col-sm-8">
												<strong>被检测物：</strong>
												<select class="form-control" name="detectedID" id="detectedID">
													<option value="0">请选择被检测物</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="row">
								 	<div class="col-md-3"></div>
								 	<div class="col-md-3">
										<a  class="btn btn-gray" href="spectrumAnalyze.jsp">查看光谱</a>
									</div>
									<div class="col-md-3">
										<a  class="btn btn-gray" href="javascript:f(2);">生成报告</a>
									</div>
								</div>
							</form>
						</div>
						
						<div class="tab-pane with-bg" id="fwv-3">
							<div class="input-group">
		      					<div class="input-group-btn">
		       						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">品质检测类型<span class="caret"></span></button>
		        					<ul class="dropdown-menu" role="menu">
			          					<li><a onclick="addValue(this,'<%="jiance_Type"%>')" href="#">果蔬品质检测</a></li>
			          					<li><a onclick="addValue(this,'<%="jiance_Type"%>')" href="#">农药残留检测</a></li>
			          					<li  class="divider"></li>
			          					<li><a onclick="addValue(this,'<%="jiance_Type"%>')" href="#">肉类质量检测</a></li>
			         					 <li  class="divider"></li>
			          					<li><a onclick="addValue(this,'<%="jiance_Type"%>')" href="#">矿石质量检测</a></li>
		        					</ul>
		      				   </div>
		      					<input type="text" id="jiance_Type" class="form-control">
		    			    </div>
		    			    
							<div class="input-group">
		      					<div class="input-group-btn">
		       						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">检测内容选择<span class="caret"></span></button>
		        					<ul class="dropdown-menu" role="menu">
			          					<li><a onclick="addValue(this,'<%="content_Type"%>')" href="#">葡萄糖</a></li>
			          					<li><a onclick="addValue(this,'<%="content_Type"%>')" href="#">维生素C</a></li>
			          					<li class="divider"></li>
			          					<li><a onclick="addValue(this,'<%="content_Type"%>')" href="#">蛋白质</a></li>
			         					 <li class="divider"></li>
			          					<li><a onclick="addValue(this,'<%="content_Type"%>')" href="#">铁元素</a></li>
		        					</ul>
		      				   </div>
		      					<input type="text" id="content_Type" class="form-control">
		    			   </div>
						   <div class="input-group">
		      					<div class="input-group-btn">
		       						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">&nbsp;&nbsp;检测标准&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span></button>
		        					<ul class="dropdown-menu" role="menu">
			          					<li><a onclick="addValue(this,'<%="standard"%>')" href="#">肉类卫生标准</a></li>
			          					<li><a onclick="addValue(this,'<%="standard"%>')" href="#">农产品安全无公害蔬菜产品标准</a></li>
			          					<li class="divider"></li>
			          					<li><a onclick="addValue(this,'<%="standard"%>')" href="#">食品中农药残留最大限量标准</a></li>
			         					 <li class="divider"></li>
			          					<li><a onclick="addValue(this,'<%="standard"%>')" href="#">矿石检测标准</a></li>
		        					</ul>
		      				   </div>
		      					<input type="text" id="standard" class="form-control">
		    			    </div>
							<div class="form-group-separator"></div>
							<div class="row">
							 	<div class="col-md-3"></div>
								<div class="col-md-3">
									<a  class="btn btn-gray" href="javascript:f(3);">跳过</a>
								</div>
								<div class="col-md-3">
									<a  class="btn btn-gray" href="javascript:f(3);">确定</a>
								</div>
							</div>
						</div>
						
						<div class="tab-pane with-bg" id="fwv-4">
									<table class="table table-bordered">
								  <thead>
								    <tr>
								      <th>检测内容</th>
								      <th>相似度</th>
								      <th>有效峰谱数</th>
								      <th>含量</th>
								      <th>达标标准</th>
								    </tr>
								  </thead>
								  <tbody>
								    <tr>
								      <td>Fe</td>
								      <td>0.88</td>
								      <td>10</td>
								      <td>53.5%</td>
								      <td>>50%</td>
								    </tr>
								    <tr>
								      <td>Mg</td>
								      <td>0.79</td>
								      <td>10</td>
								      <td>23%</td>
								      <td>>50%</td>
								    </tr>
								    <tr>
								      <td>Al</td>
								      <td>0.60</td>
								      <td>10</td>
								      <td>9%</td>
								      <td>>30%</td>
								    </tr>
								  </tbody>
								</table>
								<table class="table table-bordered">
								  <thead>
								    <tr>
								      <th>光谱属性</th>
								      <th>值</th>
								    </tr>
								  </thead>
								  <tbody>
								    <tr>
								      <td>光谱类型</td>
								      <td>红外光谱</td>
								    </tr>
								    <tr>
								      <td>检测物硬件信息</td>
								      <td>SPZ-1</td>
								    </tr>
								    <tr>
								      <td>被检测物类型</td>
								      <td>矿石</td>
								    </tr>
								    <tr>
								      <td>检测内容数</td>
								      <td>7</td>
								    </tr>
								    <tr>
								      <td>组成元素种类</td>
								      <td>100</td>
								    </tr>
								    <tr>
								      <td>检测物分析类型</td>
								      <td>铁矿石含量检测</td>
								    </tr>
								    <tr>
								      <td>被检测物品质</td>
								      <td>达标</td>
								    </tr>
								  </tbody>
								</table>
						<div style="border-bottom:1px solid ;">
							<h2 style="text-align:left;">操作记录</h2>
						</div>
						<div>
							<div class="tab-content">
								 <div class="tab-pane" id="tab1">
								 	<p>1.乘谱</p>
								 	<p>2.差谱</p>
								 	<p>3.修改坐标 [20000,12000]</p>
								 	<p>4.乘谱</p>
								 	<p>5.乘谱</p>
								 	<p>6.乘谱</p>
								 </div>
								 <div class="tab-pane" id="tab2">
								 	<p>归一化</p>
								 </div>
								 <div class="active tab-pane" id="tab3">
								 	<p>铁谱法</p>
								 </div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<ul class="nav nav-tabs">
										 <li><a href="#tab1" data-toggle="tab">谱图操作</a></li>
										 <li><a href="#tab2" data-toggle="tab">预处理算法</a></li>
										 <li class="active"><a href="#tab3" data-toggle="tab">光谱分析算法</a></li>
									</ul>
								</div>
							</div>
						</div>
							<div class="form-group-separator"></div>
							<div class="row">
								<div class="col-md-3"></div>
								<div class="col-md-3">
									<a  class="btn btn-gray" href="javascript:f(4);">打印报告</a>
								</div>
								<div class="col-md-3">
									<a  class="btn btn-gray" href="javascript:f(4);">下载报告</a>
								</div>
							</div>
						</div>
						
						
						
						<!-- Tabs Pager -->
						
						
						
					</div>
					
				</form>
				
			</div>
		</div>
	</div>
	
</body>

</html>