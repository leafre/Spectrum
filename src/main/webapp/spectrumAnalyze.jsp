<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>光谱分析</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">

	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">光谱分析</h1>
						<p class="description">您可以在此页面上对选择的光谱进行操作、修改、分析。</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="row">
				<div class="col-md-12 col-sm-12" id="operate_tool">
					<nav class="navbar navbar-default" role="navigation">
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
										文件 <b class="caret"></b>
									</a>
									<ul class="dropdown-menu">
										<li>
											<a href="javascript:showAddSpectrum();" class="addFile">添加光谱</a>
										</li>
										<li>
											<a href="javascript:;" class="removeFile">清除所有光谱</a>
										</li>
										<li>
											<a href="javascript:;" class="removeFile">保存</a>
										</li>
									</ul>
								</li>
							</ul>
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
										谱图标准库 <b class="caret"></b>
									</a>
									<ul class="dropdown-menu">
										<li>
											<a href="http://www.chemcpd.csdb.cn/cmpref/irs/irs_pro_query.asp">谱图检索</a>
										</li>
										<li>
											<a href="uploadSpectrum.jsp">添加光谱到标准库</a>
										</li>
									</ul>
								</li>
							</ul>
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
										预处理算法 <b class="caret"></b>
									</a>
									<ul class="dropdown-menu">
										<li class="has-sub">
											<a href="javascript:;" class="smoothSpectrum">平滑处理</a>
											<ul>
												<li>
												<a href="javascript:;" class="smoothSpectrum">线性拟合平滑</a>
												</li>
												<li>
												<a href="javascript:;" class="smoothSpectrum">二次函数拟合平滑</a>
												</li>
												<li>
												<a href="javascript:;" class="smoothSpectrum">拉普拉斯平滑</a>
												</li>
												<li>
												<a href="javascript:;" class="smoothSpectrum">古德图灵平滑</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="javascript:;" class="calibrationSpectrum">基线校正</a>
										</li>
										<li>
											<a href="javascript:;" class="backgroundSubtraction">背景扣除</a>
										</li>
										<li>
											<a href="javascript:;" class="frequencyStandardization">频率标准化</a>
										</li>
										<li class="divider"></li>
										<li>
											<a href="javascript:;" class="swapCoordinate">谱图单位转换</a>
										</li>
									</ul>
								</li>
							</ul>
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
										数据处理 <b class="caret"></b>
									</a>
									<ul class="dropdown-menu">
										<li>
											<a href="javascript:;" class="substractSpectrum">差谱</a>
										</li>
										<li>
											<a href="javascript:;" class="multiplySpectrum">乘谱</a>
										</li>
										<li>
											<a href="javascript:;" class="derivativeSpectrum">导数谱图</a>
										</li>
										<li class="divider"></li>
										<li>
											<a href="javascript:;" class="arithmeticSpectrum">谱图运算</a>
										</li>
									</ul>
								</li>
							</ul>
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
										光谱分析 <b class="caret"></b>
									</a>
									<ul class="dropdown-menu">
										<li>
											<a href="javascript:;" class="getPeaks">标峰</a>
										</li>
										<li class="divider"></li>
										<li>
											<a href="javascript:;" class="measureAverageValue">平均值测量</a>
										</li>
										<li>
											<a href="javascript:;" class="measureNoise">噪声测量</a>
										</li>
										
									</ul>
								</li>
							</ul>
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
										定性分析 <b class="caret"></b>
									</a>
									<ul class="dropdown-menu">
										<li>
											<a href="javascript:analyzeSpectrum_dingXing();">傅里叶变换</a>
										</li>
										<li>
											<a href="javascript:analyzeSpectrum_dingXing();">小波变换</a>
										</li>
										<li>
											<a href="javascript:analyzeSpectrum_dingXing();">偏最小二乘法</a>
										</li>
									</ul>
								</li>
							</ul>
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
										定量分析 <b class="caret"></b>
									</a>
									<ul class="dropdown-menu">
										<li>
											<a href="javascript:analyzeSpectrum_dingLiang();">多元线性回归</a>
										</li>
										<li>
											<a href="javascript:analyzeSpectrum_dingLiang();">主成分回归</a>
										</li>
										<li>
											<a href="javascript:analyzeSpectrum_dingLiang();">人工神经网络</a>
										</li>
									</ul>
								</li>
							</ul>
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
										品质分析<b class="caret"></b>
									</a>
									<ul class="dropdown-menu">
										<li>
											<a href="javascript:analyzeSpectrum_pinZhiSelect();">品质检测</a>
										</li>
										<li>
											<a href="javascript:analyzeSpectrum_chengFen();">成分检测</a>
										</li>
									</ul>
								</li>
							</ul>
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a href="javascript:;">
										修改光谱基本信息 
									</a>
								</li>
							</ul>
						</div>
						<!-- /.navbar-collapse -->
					</nav>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<!-- Flat panel -->
					<div class="panel panel-flat">
						<!-- Add class "collapsed" to minimize the panel -->
						<div id="echarts" class="viewport " style="height: 500px;"></div>
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
					<div class="col-md-8">
						<table class="table table-condensed">
							<tr>
								<td><a href="javascript:;">
										<span>分层显示</span>
									</a></td>
									<td><a href="javascript:;">
										<span>重叠显示</span>
									</a></td>
									<td><a href="javascript:;">
										<span>选择工具</span>
									</a></td>
									<td><a href="javascript:;">
										<span>峰高工具</span>
									</a></td>
									<td><a href="javascript:;">
										<span>峰面积工具</span>
									</a></td>
									<td><a href="javascript:;">
										<span>坐标修改</span>
									</a></td>
							</tr>
						</table>
					</div>
						<form role="form" class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-2 control-label">选择谱线颜色：</label>
								<div class="col-sm-2">
									<div class="input-group">
										<input type="text" class="form-control colorpicker" data-format="hex"
											value="#5a3d3d" />
										<div class="input-group-addon">
											<i class="color-preview"></i>
										</div>
									</div>
								</div>
							<!--  	<a type="button" class="btn btn-success btn-xs pull-right">查看该光谱分析报告</a>-->
							</div>
						</form>
	
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<!-- addSpectrum -->
	<div class="modal fade" id="addSpectrum_modal">
		<div class="modal-dialog">
			<div class="panel panel-default">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="panel-title">添加光谱</h4>
					<div class="panel-options">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#favorite_tab" data-toggle="tab">收藏夹选择</a>
							</li>
							<li>
								<a href="<c:url value="/uploadSpectrum.jsp"/>">本地上传</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="panel-body">
					<div class="tab-content">
						<div class="tab-pane active" id="favorite_tab">
							<div class="row">
								<div class="col-md-12">
									<form role="form" class="form-horizontal">
										<div class="form-group">
											<label class="control-label">请选择收藏夹</label>
											<select class="form-control" id="favorite_select">
											</select>
										</div>
									</form>
								</div>
							</div>
							<section class="mailbox-env">
								<div class="mail-env">
									<table class="table table-bordered spectrums_table">
										<!-- table header -->
										<thead>
											<tr>
												<th>编号</th>
												<th>名称</th>
												<th>类型</th>
												<th>描述</th>
												<th>时间</th>
											</tr>
										</thead>
										<!-- favorite spectrum list -->
										<tbody>
											<!-- <tr>
												<td class="spectrumID">编号</td>
												<td class="spectrumName spectrum_name">
													<a href="javascript:;"
														onclick="jQuery('#modal-1').modal('show', {backdrop: 'fade'});"
														class="col-name"> 名称 </a>
												</td>
												<td class="spectrumType">光谱类型</td>
												<td class="description">光谱描述</td>
												<td class="saveTime">收藏时间</td>
											</tr> -->
										</tbody>
									</table>
								</div>
							</section>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
								<button type="submit" class="btn btn-info">确定</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 这里是单位转换选择  start -->
	<div>
		<div class="modal fade" data-backdrop="static" id="swapCoordinate">
		<div class="modal-dialog" >
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">谱图单位转换</h4>
				</div>
				<div class="panel-body ">
				<form action="" role="form" class="form-horizontal" method="post">
					<div class="form-group">
						<label class="col-sm-3 control-label">转换类型</label>
						<div class="col-sm-8">
							<select class="form-control" name=" " id=" ">
								<option>请选择转换类型</option>
								<option>吸光度</option>
								<option>%透过率</option>
								<option>Kubelkea-Munk</option>
								<option>光声</option>
								<option>%反射率</option>
								<option>Log(1/R)</option>
								<option>吸光度差值</option>
								<option>波数</option>
								<option>微米</option>
								<option>纳米</option>
							</select>
						</div>
					</div>
					<div class="form-group-separator"></div>
	
					<div class="row">
    			    	<div class="col-md-10 col-md-offset-8">
		    			  	<div class="btn-group col-md-offset-2">
		  						<button onclick="" type="button" class="btn btn-success" data-dismiss="modal">确定</button>
							</div>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
		</div>
	</div>
	<!-- 这里是单位转换选择  end -->

	<!-- 这里是乘谱选择  start -->
	<div>
		<div class="modal fade" data-backdrop="static" id="multiplySpectrum">
		<div class="modal-dialog" >
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">乘谱</h4>
				</div>
				<div class="panel-body ">
				<form action="" role="form" class="form-horizontal" method="post">
					<div class="form-group">
						<label class="col-sm-3 control-label">输入系数</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="coefficient" id="coefficient" placeholder="请填写您想输入的乘谱系数">
						</div>
					</div>
					<div class="form-group-separator"></div>
	
					<div class="row">
    			    	<div class="col-md-10 col-md-offset-8">
		    			  	<div class="btn-group col-md-offset-2">
		  						<button onclick="" type="button" class="btn btn-success" data-dismiss="modal">确定</button>
							</div>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
		</div>
	</div>
	<!-- 这里是乘谱选择  end -->

	<!-- 这里是导数谱图选择  start -->
	<div>
		<div class="modal fade" data-backdrop="static" id="derivativeSpectrum">
		<div class="modal-dialog" >
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">导数谱图</h4>
				</div>
				<div class="panel-body ">
				<form action="" role="form" class="form-horizontal" method="post">
					<div class="form-group">
						<label class="col-sm-3 control-label">导数</label>
						<div class="col-sm-6">
							<select class="form-control" name="" id="">
								<option>一阶</option>
								<option>二阶</option>
							</select>
						</div>
					</div>
					<div class="form-group-separator"></div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-1"></div>
							<label class="col-sm-3"><input name="derivative" type="radio" class="col-sm-3" value="" />一阶差异导数 </label> 
						</div>
					</div>
					<div class="form-group-separator"></div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-1"></div>
							<label class="col-sm-4"><input name="derivative" type="radio" class="col-sm-2" value="" />Savitsky-Golay导数</label>
						</div>
						<label class="col-sm-4 control-label">点数</label>
						<div class="col-sm-2">
							<select class="form-control" name="" id="">
								<option>1</option>
								<option>3</option>
							</select>
						</div>
						<label class="col-sm-3 control-label">多项式项数</label>
						<div class="col-sm-2">
							<select class="form-control" name="" id="">
								<option>1</option>
								<option>3</option>
							</select>
						</div>
					</div>
					<div class="form-group-separator"></div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-1"></div>
							<label class="col-sm-3"><input class="col-sm-3" name="derivative" type="radio" value="" />
							Norris导数</label>
						</div>
						<label class="col-sm-4 control-label">有效位数</label>
						<div class="col-sm-2">
							<select class="form-control" name="" id="">
								<option>1</option>
								<option>3</option>
							</select>
						</div>
						<label class="col-sm-3 control-label">有效位间隔</label>
						<div class="col-sm-2">
							<select class="form-control" name="" id="">
								<option>1</option>
								<option>3</option>
							</select>
						</div>
					</div>
					<div class="form-group-separator"></div>
	
					<div class="row">
    			    	<div class="col-md-10 col-md-offset-8">
		    			  	<div class="btn-group col-md-offset-2">
		  						<button onclick="" type="button" class="btn btn-success" data-dismiss="modal">确定</button>
							</div>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
		</div>
	</div>
	<!-- 这里是乘谱选择  end -->

	<!-- 这里是平均值测量结果  start -->
	<div>
		<div class="modal fade" data-backdrop="static" id="measureAverageValue">
		<div class="modal-dialog" >
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">平均值测量</h4>
				</div>
				<div class="panel-body ">
					<table class="table table-hover">
						<tr>
							<td class="col-sm-5 text-right"><strong>范围：</strong></td>
							<td class="col-sm-7">4000.000&nbsp;~&nbsp;400.000</td>
						</tr>
						<tr>
							<td class="col-sm-5 text-right"><strong>平均值：</strong></td>
							<td class="col-sm-7">0.16437</td>
						</tr>
					</table>
				
					<div class="row">
    			    	<div class="col-md-10 col-md-offset-8">
		    			  	<div class="btn-group col-md-offset-2">
		  						<button onclick="" type="button" class="btn btn-success" data-dismiss="modal">确定</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
	<!-- 这里是平均值测量结果  end -->
	
	<!-- 这里是噪声测量结果  start -->
	<div>
		<div class="modal fade" data-backdrop="static" id="measureNoise">
		<div class="modal-dialog" >
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">噪声测量</h4>
				</div>
				<div class="panel-body ">
					<table class="table table-hover">
						<tr>
							<td class="col-sm-5 text-right"><strong>范围：</strong></td>
							<td class="col-sm-7">4000.000&nbsp;~&nbsp;400.000</td>
						</tr>
						<tr>
							<td class="col-sm-5 text-right"><strong>峰&nbsp;-&nbsp;峰值：</strong></td>
							<td class="col-sm-7">5.9950228</td>
						</tr>
						<tr>
							<td class="col-sm-5 text-right"><strong>RMS值：</strong></td>
							<td class="col-sm-7">0.3668374</td>
						</tr>
					</table>
				
					<div class="row">
    			    	<div class="col-md-10 col-md-offset-8">
		    			  	<div class="btn-group col-md-offset-2">
		  						<button onclick="" type="button" class="btn btn-success" data-dismiss="modal">确定</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
	<!-- 这里是噪声测量结果  end -->
	
	<!-- 这里是定性分析报告弹窗  start -->
	<div>
	<div class="modal fade" id="dingXing">
		<div class="modal-dialog" >
			<div class="panel panel-default">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="panel-title">定性分析</h4>
				</div>
				<div class="panel-body">
				<div>
					<table class="table table-hover">
						<th><strong>匹配到的元素</strong></th>
						<th><strong>相似度</strong></th>
						<th><strong>标准库谱图</strong></th>
						<tr>
							<td>Fe</td>
							<td>50%</td>
							<td><ins><a href="#">查看谱图</a></ins></td>
						</tr>
					</table>
					<table class="table table-hover">
						<th><strong>特征峰序号</strong></th>
						<th><strong>峰位</strong></th>
						<tr>
							<td>1</td>
							<td>875</td>
						</tr>
						<tr>
							<td>2</td>
							<td>900</td>
						</tr>
						<tr>
							<td>3</td>
							<td>950</td>
						</tr>
						<tr>
							<td>4</td>
							<td>1000</td>
						</tr>
						<tr>
							<td>5</td>
							<td>1888</td>
						</tr>
					</table>
				</div>
				<div class="row">
					<div class="col-md-6 col-md-offset-8">
						<div class="row">
							<div class="col-md-4" style="left:50px;">
								<a href="#">保存</a>
							</div>
							<div class="col-md-4">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">操作记录</a>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
  					<div class="panel panel-default">
   			   <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
     			 <div class="panel-body">操作记录:
        				Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
     			 </div>
   			   </div>
 				 </div>
				</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<!-- 这里是定性分析结果弹窗  end -->
	
	<!-- 这里是定量分析选择界面  start -->
	<div>
		<div class="modal fade" id="dingLiang">
		<div class="modal-dialog" >
			<div class="panel panel-default">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="panel-title">定量分析<small>-------需要上传多张光谱</small></h4>
				</div>
				<div class="panel-body">
					<div class="input-group">
      					<div class="input-group-btn">
       						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">特征值个数<span class="caret"></span></button>
        					<ul class="dropdown-menu" role="menu">
	          					<li><a onclick="addValue(this,'<%="tezhengNumber"%>')" href="#">1</a></li>
	          					<li><a onclick="addValue(this,'<%="tezhengNumber"%>')" href="#">2</a></li>
	          					<li><a onclick="addValue(this,'<%="tezhengNumber"%>')" href="#">3</a></li>
	          					<li><a onclick="addValue(this,'<%="tezhengNumber"%>')" href="#">4</a></li>
	          					<li><a onclick="addValue(this,'<%="tezhengNumber"%>')" href="#">5</a></li>
	          					<li><a onclick="addValue(this,'<%="tezhengNumber"%>')" href="#">6</a></li>
	          					<li><a onclick="addValue(this,'<%="tezhengNumber"%>')" href="#">7</a></li>
	          					<li><a onclick="addValue(this,'<%="tezhengNumber"%>')" href="#">8</a></li>
	          					<li><a onclick="addValue(this,'<%="tezhengNumber"%>')" href="#">9</a></li>
        					</ul>
      				   </div>
      					<input type="text" id="tezhengNumber" class="form-control">
    			    </div>
    			    <div class="input-group">
      					<div class="input-group-btn">
       						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">&nbsp;&nbsp;上传样本&nbsp;&nbsp;<span class="caret"></span></button>
        					<ul class="dropdown-menu" role="menu">
	          					<li><a onclick="addValue(this,'<%="yangbenFile"%>')" href="#">样本1</a></li>
	          					<li><a onclick="addValue(this,'<%="yangbenFile"%>')" href="#">样本2</a></li>
	          					<li><a onclick="addValue(this,'<%="yangbenFile"%>')" href="#">样本3</a></li>
	          					<li><a onclick="addValue(this,'<%="yangbenFile"%>')" href="#">样本4</a></li>
	          					<li><a onclick="addValue(this,'<%="yangbenFile"%>')" href="#">样本5</a></li>
	          					<li><a onclick="addValue(this,'<%="yangbenFile"%>')" href="#">样本6</a></li>
	          					<li><a onclick="addValue(this,'<%="yangbenFile"%>')" href="#">样本7</a></li>
	          					<li><a onclick="addValue(this,'<%="yangbenFile"%>')" href="#">样本8</a></li>
	          					<li><a onclick="addValue(this,'<%="yangbenFile"%>')" href="#">样本9</a></li>
        					</ul>
      				   </div>
      					<input type="text" id="yangbenFile" class="form-control">
    			    </div>
    			    <div class="row">
    			    	<div class="col-md-6 col-md-offset-6">
    			    		<div class="col-md-4 col-md-offset-6">
    			    			<button onclick="analyzeSpectrum_DingLiangResult()" class="btn btn-success" >定量分析</button>
    			    		</div>
    			    	</div>
    			    </div>
				</div>
			</div>
		</div>
		</div>
	</div>
	<!-- 这里是定量分析选择界面  end -->

	<!-- 这里是定量分析结果界面 start -->
	<div>
	<div class="modal fade" id="dingLiang_Result">
		<div class="modal-dialog" >
			<div class="panel panel-default">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="panel-title">定量分析报告</h4>
				</div>
				<div class="panel-body">
				<div>
					<table class="table table-hover">
					
					</table>
					<table class="table table-hover">
						
					</table>
				</div>
				<div>
					<h4>定量模型</h4>Y=-2582.755+0.022067X1+0.702104X2+23.98506X3
				</div>
				<div class="row">
					<div class="col-md-6 col-md-offset-8">
						<div class="row">
							<div class="col-md-4" style="left:50px;">
								<a href="#">保存</a>
							</div>
							<div class="col-md-4">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseTwo">操作记录</a>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="false">
  					<div class="panel panel-default">
   			   <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
     			 <div class="panel-body"><h4>操作记录</h4><br>
        			预处理：矢量归一化+15点平滑		
        		 </div>
   			   </div>
 				 </div>
				</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<!-- 这里是定量分析结果界面 end -->
	
		
	<!-- 这里是品质分析选择界面  start -->
	<div>
		<div class="modal fade" id="pinZhi_select">
		<div class="modal-dialog" >
			<div class="panel panel-default">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="panel-title">品质检测</h4>
				</div>
				<div class="panel-body">
					<div class="input-group">
      					<div class="input-group-btn">
       						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">品质检测类型<span class="caret"></span></button>
        					<ul class="dropdown-menu" role="menu">
	          					<li><a onclick="addValue(this,'<%="jiance_Type"%>')" href="#">果蔬品质检测</a></li>
	          					<li><a onclick="addValue(this,'<%="jiance_Type"%>')" href="#">农药残留检测</a></li>
	          					<li  class="divider"></li>
	          					<li><a onclick="addValue(this,'<%="jiance_Type"%>')" href="#">肉类质量检测</a></li>
	         					 <li  class="divider"></li>
	          					<li><a onclick="addValue(this,'<%="jiance_Type"%>')" href="#">矿石质量检测</a></li>
        					</ul>
      				   </div>
      					<input type="text" id="jiance_Type" class="form-control">
    			    </div>
    			    <div class="input-group">
      					<div class="input-group-btn">
       						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">&nbsp;&nbsp;被检测物&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span></button>
        					<ul class="dropdown-menu" role="menu">
	          					<li><a onclick="addValue(this,'<%="material_Type"%>')" href="#">甜橙</a></li>
	          					<li><a onclick="addValue(this,'<%="material_Type"%>')" href="#">白菜</a></li>
	          					<li class="divider"></li>
	          					<li><a onclick="addValue(this,'<%="material_Type"%>')" href="#">牛肉</a></li>
	         					 <li class="divider"></li>
	          					<li><a onclick="addValue(this,'<%="material_Type"%>')" href="#">铁矿石</a></li>
        					</ul>
      				   </div>
      					<input type="text" id="material_Type" class="form-control">
    			    </div>
					<div class="input-group">
      					<div class="input-group-btn">
       						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">检测内容选择<span class="caret"></span></button>
        					<ul class="dropdown-menu" role="menu">
	          					<li><a onclick="addValue(this,'<%="content_Type"%>')" href="#">葡萄糖</a></li>
	          					<li><a onclick="addValue(this,'<%="content_Type"%>')" href="#">维生素C</a></li>
	          					<li class="divider"></li>
	          					<li><a onclick="addValue(this,'<%="content_Type"%>')" href="#">蛋白质</a></li>
	         					 <li class="divider"></li>
	          					<li><a onclick="addValue(this,'<%="content_Type"%>')" href="#">铁元素</a></li>
        					</ul>
      				   </div>
      					<input type="text" id="content_Type" class="form-control">
    			   </div>
				   <div class="input-group">
      					<div class="input-group-btn">
       						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">&nbsp;&nbsp;检测标准&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span></button>
        					<ul class="dropdown-menu" role="menu">
	          					<li><a onclick="addValue(this,'<%="standard"%>')" href="#">肉类卫生标准</a></li>
	          					<li><a onclick="addValue(this,'<%="standard"%>')" href="#">农产品安全无公害蔬菜产品标准</a></li>
	          					<li class="divider"></li>
	          					<li><a onclick="addValue(this,'<%="standard"%>')" href="#">食品中农药残留最大限量标准</a></li>
	         					 <li class="divider"></li>
	          					<li><a onclick="addValue(this,'<%="standard"%>')" href="#">矿石检测标准</a></li>
        					</ul>
      				   </div>
      					<input type="text" id="standard" class="form-control">
    			    </div>
    			    <div class="row">
    			    	<div class="col-md-10 col-md-offset-8">
		    			  	<div class="btn-group col-md-offset-2">
		  						<button onclick="analyzeSpectrum_pinZhiResult()" type="button" class="btn btn-success">品质检测</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<!-- 这里是品质分析选择界面  end -->
	
	<!-- 这里是定性分析报告弹窗  start -->
	<div class="modal fade" id="pinZhi_result">
		<div class="modal-dialog">
			<div class="panel panel-default">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="panel-title">品质分析报告</h4>
				</div>
				<div class="panel-body">
					<table class="table table-bordered">
					  <thead>
					    <tr>
					      <th>光谱属性</th>
					      <th>值</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>光谱类型</td>
					      <td>红外光谱</td>
					    </tr>
					    <tr>
					      <td>检测物硬件信息</td>
					      <td>SPZ-1</td>
					    </tr>
					    <tr>
					      <td>被检测物类型</td>
					      <td>矿石</td>
					    </tr>
					    <tr>
					      <td>检测内容数</td>
					      <td>7</td>
					    </tr>
					    <tr>
					      <td>组成元素种类</td>
					      <td>100</td>
					    </tr>
					    <tr>
					      <td>检测物分析类型</td>
					      <td>铁矿石含量检测</td>
					    </tr>
					    <tr>
					      <td>被检测物品质</td>
					      <td>达标</td>
					    </tr>
					  </tbody>
					</table>
						
					<table class="table table-bordered">
					  <thead>
					    <tr>
					      <th>检测内容</th>
					      <th>相似度</th>
					      <th>有效峰谱数</th>
					      <th>含量</th>
					      <th>达标标准</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>Fe</td>
					      <td>0.88</td>
					      <td>10</td>
					      <td>53.5%</td>
					      <td>>50%</td>
					    </tr>
					    <tr>
					      <td>Mg</td>
					      <td>0.79</td>
					      <td>10</td>
					      <td>23%</td>
					      <td>>50%</td>
					    </tr>
					    <tr>
					      <td>Al</td>
					      <td>0.60</td>
					      <td>10</td>
					      <td>9%</td>
					      <td>>30%</td>
					    </tr>
					  </tbody>
					</table>
					<div style="border-bottom:1px solid ;">
						<h4 style="text-align:left;">操作记录</h4>
					</div>
					<div class="tabs-vertical-env tabs-vertical-bordered">
						<ul class="nav tabs-vertical">
							<li><a href="#v-home" data-toggle="tab">谱图操作</a></li>
							<li class="active"><a href="#v-profile" data-toggle="tab">预处理算法</a></li>
							<li><a href="#v-messages" data-toggle="tab">光谱分析算法</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane" id="v-home">
							 	<p>1.乘谱</p>
							 	<p>2.差谱</p>
							 	<p>3.修改坐标 [20000,12000]</p>
							 	<p>4.乘谱</p>
							 	<p>5.乘谱</p>
							 	<p>6.乘谱</p>
							</div>
							<div class="tab-pane" id="v-profile">
					 			<p>归一化</p>
							</div>
							<div class="tab-pane active" id="v-messages">
								<p>铁谱法</p>
							</div>
						</div>
					</div>	
					<div class="row">
	   			    	<div class="col-md-10 col-md-offset-8">
		    			  	<div class="btn-group col-md-offset-2">
		  						<button onclick="" type="button" class="btn btn-success" data-dismiss="modal">保存</button>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
	<!-- 这里是定性分析报告弹窗  end -->	

	
	<!-- 这里是成分检测结果  start -->
	<div>
		<div class="modal fade" id="chengFen">
		<div class="modal-dialog" >
			<div class="panel panel-default">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="panel-title">成分检测</h4>
				</div>
				<div class="panel-body">
					<div class="input-group">
      					<div class="input-group-btn">
       						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">检测内容选择<span class="caret"></span></button>
        					<ul class="dropdown-menu" role="menu">
	          					<li><a onclick="addValue(this,'<%="content"%>')" href="#">葡萄糖</a></li>
	          					<li><a onclick="addValue(this,'<%="content"%>')" href="#">氟乐灵</a></li>
	          					<li><a onclick="addValue(this,'<%="content"%>')" href="#">维生素C</a></li>
	          					<li class="divider"></li>
	          					<li><a onclick="addValue(this,'<%="content"%>')" href="#">蛋白质</a></li>
	         					 <li class="divider"></li>
	          					<li><a onclick="addValue(this,'<%="content"%>')" href="#">铁元素</a></li>
        					</ul>
      				   </div>
      					<input type="text" id="content" class="form-control">
    			   </div>
    			   <div class="row">
    			    	<div class="col-md-8 col-md-offset-8">
		    			  	<div class="btn-group col-md-offset-4">
		  						<button onclick="showContent_Result()" type="button" class="btn btn-success">检测</button>
							</div>
						</div>
				   </div>
				   <div id="showContent_Result" class="Container" style="display:none;">
				   		<p><span class="active">葡萄糖</span>：含有</p>
				   		<p><span>氟乐灵(有毒)</span>：含有</p>
				   </div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<!-- 这里是成分检测结果  end -->
	
	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
	<script src="assets/js/colorpicker/bootstrap-colorpicker.min.js"></script>
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/select2/select2_locale_zh-CN.js"></script>
	<script src="assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="assets/js/echarts/echarts.js "></script>
	<!-- <script src="assets/js/echarts/echarts.common.min.js "></script> -->
	<script src="<c:url value='assets/js/echarts/echarts_function.js'/>"></script>
	<!--echarts_demo-->
	<!-- <script src="assets/js/echarts/echarts_demo.js "></script> -->
	<script src="assets/js/custom/spectrum.js"></script>
	<script src="assets/js/custom/SpectrumFile.js"></script>
	<script src="assets/js/custom/spectrumOperate.js"></script>
	<script src="assets/js/custom/spectrumAnalyze.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
	var none = getUrlParam('none');// 无操作
	if(null==none){
		var tempSpectrumFile,spectrumID,sequence;
		// 保存转发过来的光谱信息
		<c:if test="${tempSpectrumFile != null}">
			tempSpectrumFile = "${tempSpectrumFile}";
		</c:if>
		<c:if test="${spectrumID != null}">
			spectrumID = "${spectrumID}";
		</c:if>
		<c:if test="${sequence != null}">
			spectrumID = "${sequence}";
		</c:if>
		
		// 转发的数据
		var param = {};
		if(tempSpectrumFile!=undefined){
			param.tempSpectrumFile = tempSpectrumFile;
		}
		if(spectrumID!=undefined){
			param.spectrumID = spectrumID;
		}
		if(sequence!=undefined){
			param.sequence = sequence;
		}
		//URL的数据
		if(param.tempSpectrumFile==undefined){
			param.tempSpectrumFile = getUrlParam('tempSpectrumFile');
		}
		if(param.spectrumID==undefined){
			param.spectrumID = getUrlParam('spectrumID');
		}
		if(param.sequence==undefined){
			param.sequence = getUrlParam('sequence');
		}
		//console.log(param);
		
		setTimeout('showSpectrumFile(param)',50);
	}
	
	//定性分析新建页面
	function analyzeSpectrum_dingXing(){
		jQuery('#dingXing').modal('show', {backdrop: 'static'});
	}
	//定量分析选择新建页面
	function analyzeSpectrum_dingLiang(){
		jQuery('#dingLiang').modal('show', {backdrop: 'static'});
	}
	//定量分析结果新建页面
	function analyzeSpectrum_DingLiangResult(){
		jQuery('#dingLiang_Result').modal('show', {backdrop: 'static'});
	}

	//品质检测选择新建页面
	function analyzeSpectrum_pinZhiSelect(){
		jQuery('#pinZhi_select').modal('show', {backdrop: 'static'});
	}
	//品质检测结果新建页面
	function analyzeSpectrum_pinZhiResult(){
		jQuery('#pinZhi_result').modal('show', {backdrop: 'static'});
	}
	//成分检测新建页面
	function analyzeSpectrum_chengFen(){
		jQuery('#chengFen').modal('show', {backdrop: 'static'});
	}
	//品质检测页面-将选中内容增添至文本框
	function addValue(obj,id){
		var show = document.getElementById(id);//显示文本的对象
		if(id=="content_Type"||id=="content"){
			show.value=show.value+obj.innerText+" ";
		}
		else{
			show.value=obj.innerText;
		}
	}
	//检测成分结果显示
	function showContent_Result(){
		var obj=document.getElementById('showContent_Result');
		if(obj.style.display=="block"){
			obj.style.display="none";
		}
		else{
			obj.style.display="block";
		}
	}
</script>
</body>

</html>