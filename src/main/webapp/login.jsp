﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--[if IEMobile 7]><html class="no-js iem7 oldie linen"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js ie7 oldie linen" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js ie8 oldie linen" lang="en"><![endif]-->
<!--[if (IE 9)&!(IEMobile)]><html class="no-js ie9 linen" lang="en"><![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)]><!-->
<html class="no-js linen" lang="en">
<!--<![endif]-->

<head>
<title>登录</title>
<%-- 登录注册样式 --%>
<jsp:include page="/WEB-INF/jsps/login_head.jsp"></jsp:include>
</head>

<body>
	<c:if test="${!empty sessionScope.session_user}">
		<c:redirect url="" />
	</c:if>
	<div id="container">
		<hgroup id="login-title" class="large-margin-bottom">
		<h1 class="login-title-image">光谱数据库</h1>
		<h5>&copy; 江西农业大学-蓝点工作室</h5>
		</hgroup>
		<div id="form-block" class="scratch-metal">
			<form method="post" id="form-login"
				class="input-wrapper blue-gradient glossy">
				<ul class="inputs black-input large">
					<!-- The autocomplete="off" attributes is the only way to prevent webkit browsers from filling the inputs with yellow -->
					<li>
						<span class="icon-user mid-margin-right"></span>
						<input type="text" name="account" id="account"
							value="${cookie.account.value}" class="input-unstyled"
							placeholder="用户名/邮箱" autocomplete="off">
					</li>
					<li>
						<span class="icon-lock mid-margin-right"></span>
						<input type="password" name="password" id="password"
							value="${cookie.password.value}" class="input-unstyled" placeholder="密码"
							autocomplete="off">
					</li>
					<li class="vCode">
						<span class="icon-frame mid-margin-right"></span>
						<input type="text" name="verifyCode" id="verifyCode"
							class="input-unstyled" placeholder="验证码" autocomplete="off">
						<a href="javascript:changeVCode('vCode')">
							<img id="vCode" src="" /><b class="float-right">看不清</b>
						</a>
					</li>
				</ul>
				<p class="button-height">
					<button type="submit" class="button glossy float-right" id="login">登录</button>
					<input type="checkbox" name="remind" id="remind" value="1"
						<c:if test="${not empty cookie.account}">checked="checked"</c:if>
						class="switch tiny mid-margin-right with-tooltip" title="打开记住用户">
					<label for="remind">记住我</label>
				</p>
			</form>
		</div>
	</div>
	<!-- JavaScript at the bottom for fast page loading -->
	<script src="assets/js/custom/login.js"></script>
	<script src="assets/js/custom/verifycode.js"></script>
</body>
</html>