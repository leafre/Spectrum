<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>光谱上传申请列表</title>
	<%-- 全站样式 --%>
	<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">光谱上传审核</h1>
						<p class="description">此页面显示的申请为实验人员上传光谱至标准库的申请列表</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="panel panel-default panel-tabs">
				<div class="panel-heading">
					<h3 class="panel-title">申请列表</h3>
					<div class="panel-options">
						<ul class="nav nav-tabs" id="nav_tabs">
							<li class="wait"><a
								href="<c:url value='/querySubmitSpectrum.do?state=等待'/>">待审核</a>
							</li>
							<li class="pass"><a
								href="<c:url value='/querySubmitSpectrum.do?state=通过'/>">通过</a>
							</li>
							<li class="refuse"><a
								href="<c:url value='/querySubmitSpectrum.do?state=拒绝'/>">拒绝</a>
							</li>
							<li class="all"><a
								href="<c:url value='/querySubmitSpectrum.do'/>">所有申请</a></li>
						</ul>
					</div>
				</div>
				<div class="panel-body">
					<div class="tab-content">
						<div class="tab-pane active">
							<table class="table table-striped table-bordered">
								<thead>
									<tr class="replace-inputs">
										<th>光谱类型</th>
												<th>光谱名称</th>
												<th>检测硬件</th>
												<th>被检测物</th>
												<th>申请人</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach var="spectrum" items="${querydata}">
												<tr>
													<td>${spectrum.spectrumType}</td>
													<td><a
														href="<c:url value='/querySubmitSpectrumDetail.do?spectrumID=${spectrum.spectrumID}'/>" style="text-decoration:underline;">${spectrum.spectrumName}</a></td>
													<td>${spectrum.hardwareName}</td>
													<td class="center">${spectrum.chineseName}</td>
													<td class="center">${spectrum.userId}</td>
												</tr>
											</c:forEach>
								</tbody>
								<tfoot>
									<tr>
										<th>光谱类型</th>
												<th>光谱名称</th>
												<th>检测硬件</th>
												<th>被检测物</th>
												<th>申请人</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
		</div>


			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">

	<!-- Imported scripts on this page -->
	<script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
	<script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script src="assets/js/datatables/tabletools/dataTables.tableTools.min.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			var state = getUrlParam('state');
		
			var $nav_tabs = $('#nav_tabs');
			// 修改高亮
			if(null == state) {
				$nav_tabs.find('.all').addClass('active');
			} else if('通过' == state) {
				$nav_tabs.find('.pass').addClass('active');
			} else if('等待' == state) {
				$nav_tabs.find('.wait').addClass('active');
			} else if('拒绝' == state) {
				$nav_tabs.find('.refuse').addClass('active');
			}
			
			$("table").dataTable({
				aLengthMenu: [
					[5, 10, 25, 50, 100, -1],
					[5, 10, 25, 50, 100, "所有"]
				]
			}).yadcf([{
				column_number: 0,
				filter_type: 'text'
			}, {
				column_number: 1,
				filter_type: 'text'
			}, {
				column_number: 2,
				filter_type: 'text'
			},{
				column_number: 3,
				filter_type: 'text'
			} ]);
			
		});
	</script>
</body>

</html>