<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="date" uri="/WEB-INF/datetag.tld"%>
<!DOCTYPE html>
<html>
<head>
	<title>算法审核列表</title>
	<%-- 全站样式 --%>
	<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">算法插件审核</h1>
						<p class="description">您可以此页面查看实验人员申请添加到系统的算法插件，点击算法名查看算法详细信息。</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="panel panel-default panel-tabs">
				<div class="panel-heading">
					<h3 class="panel-title">申请列表</h3>
					<div class="panel-options">
						<ul class="nav nav-tabs" id="nav_tabs">
							<li class="wait">
								<a href="<c:url value='/queryPluginList.do?state=等待'/>">待审核</a>
							</li>
							<li class="pass">
								<a href="<c:url value='/queryPluginList.do?state=通过'/>">通过</a>
							</li>
							<li class="refuse">
								<a href="<c:url value='/queryPluginList.do?state=拒绝'/>">拒绝</a>
							</li>
							<li class="all">
								<a href="<c:url value='/queryPluginList.do'/>">所有申请</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="panel-body">
					<div class="tab-content">
						<div class="tab-pane active">
							<table class="table table-striped table-bordered">
								<thead>
									<tr class="replace-inputs">
										<th>算法名称</th>
										<th>算法类型</th>
										<th>添加时间</th>
										<th>申请人</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach var="plugin" items="${querydata}">
									<tr>
										<td>
											<a href="queryPluginDetail.do?pluginID=${plugin.pluginID}" style="text-decoration:underline;">
											${plugin.pluginName}
											</a>
										</td>
										<td>${plugin.pluginType}</td>
										<td><date:date value="${plugin.addTime}" parttern="yyyy/MM/dd hh:mm:ss"></date:date></td>
										<td class="center">${plugin.userName}</td>
									</tr>
								</c:forEach>
								</tbody>
								<tfoot>
									<tr>
										<th>算法名称</th>
										<th>算法类型</th>
										<th>添加时间</th>
										<th>提交人</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">

	<!-- Imported scripts on this page -->
	<script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
	<script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script src="assets/js/datatables/tabletools/dataTables.tableTools.min.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			var state = getUrlParam('state');
		
			var $nav_tabs = $('#nav_tabs');
			// 修改高亮
			if(null == state) {
				$nav_tabs.find('.all').addClass('active');
			} else if('通过' == state) {
				$nav_tabs.find('.pass').addClass('active');
			} else if('等待' == state) {
				$nav_tabs.find('.wait').addClass('active');
			} else if('拒绝' == state) {
				$nav_tabs.find('.refuse').addClass('active');
			}
			
			$("table").dataTable({
				aLengthMenu: [
					[5, 10, 25, 50, 100, -1],
					[5, 10, 25, 50, 100, "所有"]
				]
			}).yadcf([{
				column_number: 0,
				filter_type: 'text'
			}, {
				column_number: 1,
				filter_type: 'text'
			}, {
				column_number: 2,
				filter_type: 'text'
			},{
				column_number: 3,
				filter_type: 'text'
			} ]);
			
		});
	</script>
</body>

</html>