<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="date" uri="/WEB-INF/datetag.tld"%>
<!DOCTYPE html>
<html>
<head>
<title>个人插件管理</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">个人算法插件管理</h1>
						<p class="description">您可以在此页面上，对自己上传的个人算法进行管理</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>
			
			<div class="row">
				<div class="panel panel-default panel-tabs">
					<div class="panel-heading">
						<!--<div class="panel-options">-->
						<ul class="nav nav-tabs" id="nav_tabs">
							<li class="pass">
								<a href="<c:url value='/queryMyPluginList.do?state=通过'/>">已通过</a>
							</li>
							<li class="wait">
								<a href="<c:url value='/queryMyPluginList.do?state=等待'/>">待审核</a>
							</li>
							<li class="refuse">
								<a href="<c:url value='/queryMyPluginList.do?state=拒绝'/>">未通过</a>
							</li>
							<li class="all">
								<a href="<c:url value='/queryMyPluginList.do'/>">所有</a>
							</li>
						</ul>
						<!--</div>-->
					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div class="tab-pane active">
								<div class="panel panel-default">
									<div class="panel-heading">
										<a href="uploadPlugin.jsp" class="btn btn-primary btn-single btn-sm">上传</a>
									</div>
									<div class="panel-body">
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>算法名称</th>
													<th>算法类型</th>
													<th>上传时间</th>
													<th>版本号</th>
													<th>操作</th>
												</tr>
											</thead>
											<tbody class="middle-align">
											<c:forEach var="plugin" items="${querydata}">
												<tr>
													<td class="pluginName">${plugin.pluginName}</td>
													<td>${plugin.pluginType}</td>
													<td>
													<date:date value="${plugin.addTime}" parttern="yyyy/MM/dd hh:mm:ss"></date:date>
													</td>
													<td>${plugin.version}</td>
													<td class="pluginID">
														<a href="<c:url value='/queryPluginDetail.do?pluginID=${plugin.pluginID}'/>" class="btn btn-info btn-sm btn-icon icon-left">
															详细信息 </a>
														<c:if test="${plugin.state!='通过'}">
														<a href="javascript:;" onclick="showEditPlugin(${plugin.pluginID})" class="btn btn-secondary btn-sm btn-icon icon-left">
															修改 </a>
														<a title="${plugin.pluginID}" href="javascript:;" data-action="deletePlugin" class="btn btn-danger btn-sm btn-icon icon-left">
															删除 </a>
														</c:if>
													</td>
												</tr>
											</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<div class="modal fade" id="deletePlugin_modal" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">警告!</h4>
				</div>
				<div class="modal-body">
					您确定要删除此算法插件(<label title=""></label>)?<br />此操作不能恢复
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">取消</button>
					<button type="submit" class="btn btn-danger">删除</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="editPlugin_modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">插件可修改基本信息</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">算法插件名称</label>
								<input type="text" class="form-control" id="field-1" placeholder="John" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">算法插件类型</label>
								<input type="text" class="form-control" id="field-1" placeholder="John"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">算法插件类型</label>
								<select class="form-control" name="spectrumTypeIDs" id="alterSpectrumType">
									<optgroup label="可选择的插件类型">
										<option>预处理算法</option>
										<option>数据处理算法</option>
										<option>光谱解析算法</option>
										<option>光谱分析算法</option>
									</optgroup>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="version" class="control-label">算法版本号</label>
								<input type="text" class="form-control" name="version" id="version" placeholder=""/>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">上传时间</label>
								<input type="text" class="form-control" id="field-1" placeholder="2011年11月11日" disabled/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">算法描述</label>
								<textarea class="form-control autogrow" cols="5" id="field-5" placeholder="I will grow as you enter new lines."></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-info">修改</button>
				</div>
			</div>
		</div>
	</div>
		
	<link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
	<script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script src="assets/js/dateformat.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			var state = getUrlParam('state');
			var $nav_tabs = $('#nav_tabs');
			// 修改高亮
			if(null == state) {
				$nav_tabs.find('.all').addClass('active');
			} else if('通过' == state) {
				$nav_tabs.find('.pass').addClass('active');
			} else if('等待' == state) {
				$nav_tabs.find('.wait').addClass('active');
			} else if('拒绝' == state) {
				$nav_tabs.find('.refuse').addClass('active');
			}
			
			$("table").dataTable({
				aLengthMenu: [
					[5, 10, 25, 50, 100, -1],
					[5, 10, 25, 50, 100, "所有"]
				]
			}).yadcf([ {
				column_number : 0,
				filter_type : 'text'
			}, {
				column_number : 1,
				filter_type : 'text'
			}, {
				column_number : 2,
				filter_type : 'text'
			}, ]);
			
			$('table a[data-action="deletePlugin"]').on('click', function(ev) {
				ev.preventDefault();
				var $a_del = $(this);
				var pluginID = $a_del.attr('title');
				var pluginName = $a_del.parent().parent().find('.pluginName').text();
				$('#deletePlugin_modal label').text(pluginName).attr('title',pluginID);
				$("#deletePlugin_modal").modal('show');
				
				$('#deletePlugin_modal button[type="submit"]').on('click', function(ev) {
					ev.preventDefault();
					//var pluginID = $('#deletePlugin_modal label').attr('title');
					
					$.ajax({
						url: 'deletePlugin.do',
						type: 'post',
						dataType: 'json',
						data: {
							pluginID: pluginID,
						},
						success: function(jsondata) {
							if('删除成功'==jsondata.deletePlugin_msg){
								$("#deletePlugin_modal").modal('hide');
								$a_del.parent().parent().remove();
							}
							alert(jsondata.deletePlugin_msg);
						},error: function(){
							alert('服务器错误,删除失败!');
						}
					});
				});
				
			});
			
			
		});
		
		function showEditPlugin(pluginID){
			jQuery('#editPlugin_modal').modal('show', {backdrop: 'fade'});
		}
	</script>
</body>

</html>