<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="date" uri="/WEB-INF/datetag.tld"%>
<!DOCTYPE html>
<html>

<head>
	<title>角色申请</title>
	<%-- 全站样式 --%>
	<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">角色申请</h1>
						<p class="description">描述</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
						</div>
						<div class="panel-body">
							<form role="form"  class="validate form-horizontal" action="<c:url value='applyRole.do'/>" method="post" enctype="multipart/form-data">
								<div class="form-group">
									<label class="col-sm-2 control-label">角色选择</label>
									<div class="col-sm-7">
										<p>
											<!-- 如果用户角色是普通用户，则用户可以申请实验人员、普通管理员 -->
											<c:if test="${session_user.roleID==4}">
												<label class="radio-inline"> <input type="radio"
												name = "applyRoleID" value="3" checked>实验人员</label>
												<label class="radio-inline"> <input type="radio"
												name = "applyRoleID" value="2" >普通管理员</label>
											</c:if>
											<!-- 如果用户角色是实验人员，则用户可以申请普通管理员 -->
											<c:if test="${session_user.roleID==3}">
												<label class="radio-inline">
												<input type="radio" name="applyRoleID" value="2" checked>普通管理员
												</label>
											</c:if>
										</p>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">真实姓名</label>
									<div class="col-sm-7">
										<c:if test="${session_user.realName!=null }">
											<input type="text" class="form-control realName" id="realName" name="realName"
												value="${session_user.realName}" readonly />
										</c:if>
										<c:if test="${session_user.realName==null }">
											<input type="text" class="form-control realName" id="realName" name="realName"
												value="">
										</c:if>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">身份证号</label>
									<div class="col-sm-7">
										<c:if test="${session_user.IDNumber!= null }">
											<input type="text" class="form-control IDNumber" id="IDNumber" name="IDNumber"
												value="${session_user.IDNumber}" readonly />
										</c:if>
										<c:if test="${session_user.IDNumber== null }">
											<input type="text" class="form-control IDNumber" id="IDNumber" name="IDNumber"
												value="">
										</c:if>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">性别</label>
									<div class="col-sm-7">
										<p>
											<label class="radio-inline">
												<input type="radio" name="gender" value="保密" 
												<c:if test="${session_user.gender=='保密'}">checked</c:if> value="保密">保密
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="gender" value="男" 
												<c:if test="${session_user.gender=='男'}">checked</c:if> value="男">男
											</label>
										 	<label class="radio-inline"> 
										 		<input type="radio" name="gender" value="女" 
										 		<c:if test="${session_user.gender=='女'}">checked</c:if> value="女"> 女
											</label>
										</p>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">出生日期</label>
									<div class="col-sm-6">
										<input type="text" class="form-control datepicker" id="birthday" name="birthday"
											value="<date:date  value='${session_user.birthday}' parttern="MM/dd/yyyy"></date:date>" />
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="address">单位地址</label>
									<div class="col-sm-9">
										<div class="col-sm-3">
											<strong>省：</strong>
											<select class="form-control {required:true}" id="loc_province_select"></select>
										</div>
										<div class="col-sm-3">
											<strong>市：</strong>
											<select class="form-control" id="loc_city_select"></select>
										</div>
										<div class="col-sm-3">
											<strong>区/县：</strong>
											<select class="form-control" id="loc_town_select"></select>
										</div>
										<div class="form-group"></div>
										<div class="col-sm-7">
											<strong>街区：</strong>
											<input type="text" class="form-control" id="address" name="address" value="${session_user.address}"  placeholder="单位所在街区">
										</div>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">手机号</label>
									<div class="col-sm-7">
										<input id="tel" type="text" class="form-control tel" id="field-1" name = "tel" value = "${session_user.tel}" placeholder="您的手机号">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">工作单位</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="field-1" name = "institute" value = "${session_user.institute}" placeholder="您的工作单位">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">职位</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="field-1" name = "position" value = "${session_user.position}" placeholder="您的职位">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-5">个人描述</label>
									<div class="col-sm-7">
										<textarea id="detail" class="form-control autogrow detail" rows="5" cols="8"  id="field-5" 
										name = "detail" placeholder="您的个人描述，请与身份证号对应">${session_user.detail}</textarea>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="spectrumFile">申请文件 </label>
									<div class="col-sm-7">
										<input id="spectrumFile" type="file" name="applyFile" class="control-label" />
										<div class="form-group"></div>
										<a href="<c:url value='downloadFile.do?type=template&fileName=template.txt'/>">申请模板</a>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<button type="submit" class="btn btn-info btn-single pull-right">提交申请</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/jquery-validate/jquery.validate.js"></script>
	<script src="assets/js/jquery-validate/localization/messages_zh.js"></script>
	<script src="assets/js/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/province/area.js"></script>
	<script src="assets/js/province/location.js"></script>
	<script src="assets/js/custom/applyRole.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">

	window.onload = function() {
		if ('${applyRole_msg}') {
			alert('${applyRole_msg}');
		}
	}
	</script>

</body>
</html>