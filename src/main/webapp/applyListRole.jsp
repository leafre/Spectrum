<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="date" uri="/WEB-INF/datetag.tld"%>
<!DOCTYPE html>
<html>
<head>
<title>角色申请</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">查看申请列表</h1>
						<p class="description">您可以在此页面上查看用户的对角色的申请请求列表。</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="row">
				<!-- Tabbed panel 2 -->
				<div class="panel panel-default panel-tabs">
					<!-- Add class "collapsed" to minimize the panel -->
					<div class="panel-heading">
						<h3 class="panel-title">角色申请</h3>
						<div class="panel-options">
							<ul class="nav nav-tabs" id="nav_tabs">
								<li class="wait">
									<a href="<c:url value='/queryUserRoleApplication.do?app_state=等待'/>">待审核</a>
								</li>
								<li class="pass">
									<a href="<c:url value='/queryUserRoleApplication.do?app_state=通过'/>">通过</a>
								</li>
								<li class="refuse">
									<a href="<c:url value='/queryUserRoleApplication.do?app_state=拒绝'/>">拒绝</a>
								</li>
								<li class="all">
									<a href="<c:url value='/queryUserRoleApplication.do'/>">所有申请</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div class="tab-pane active">
								<div class="panel panel-default">
									<div class="panel-body">
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>用户名</th>
													<th>当前角色</th>
													<th>申请角色</th>
													<th>申请时间</th>
													<th>操作</th>
												</tr>
											</thead>
											<tbody class="middle-align">
												<c:forEach var="apply" items="${querydata}" varStatus="status">
												<tr>
													<td>${apply.username}</td>
													<td>${apply.roleName}</td>
													<td>${apply.app_roleName}</td>
													<td>
														<date:date value="${apply.applyTime}" parttern="yyyy/MM/dd hh:mm:ss"></date:date>
													</td>
												<!-- <td>
												<a href="<c:url value='/userProfile.jsp'/>" class="btn btn-secondary btn-sm btn-icon icon-left">
													Edit </a>
												<a href="#" class="btn btn-danger btn-sm btn-icon icon-left">
													Delete </a>
												<a href="#" class="btn btn-info btn-sm btn-icon icon-left">
													Profile </a>
												</td> -->
													<td>
								
														<a href="javascript:;" onclick="javascirpt:showApplayUserDetail('${apply.photo}','${apply.username}','${apply.gender}','${apply.birthday}','${apply.address}','${apply.realName }','${apply.IDNumber}','${apply.institute}','${apply.position}','${apply.tel}','${apply.detail}','${apply.applyfile}');" class="btn btn-info btn-sm btn-icon icon-left">
															申请人详细信息
														</a>
														<c:if test="${apply.app_state eq '等待'}">

															<a href="javascript:passRequest('${apply.applicationID}','${apply.userID}','${apply.app_roleID}','${apply.email}','${session_user.username}','${apply.userName}')"
																class="btn btn-secondary btn-sm btn-icon icon-left">通过</a>
														</c:if>
														<c:if test="${apply.app_state eq '等待'}">
											
															<a href="javascript:showRefuseDialog('${apply.applicationID}','${apply.app_roleID}','${apply.userID}','${apply.email}','${apply.userName}')" class="btn btn-danger btn-sm btn-icon icon-left"">拒绝</a>
														</c:if>
													</td>
												</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<div class="modal fade custom-width" id="modal-applyUserDetial">
		<div class="modal-dialog" style="width: 60%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">申请人详细信息</h4>
				</div>
				<div class="modal-body">
					<section class="profile-env">
						<div class="col-sm-3">
							<div class="user-info-sidebar" id="user_detail">
								<a href="#" class="user-img" >
									<img  id="user_img" src="assets/images/user-4.png" alt="user-img" class="img-cirlce img-responsive img-thumbnail" />
								</a>
								<a href="#" class="user-name" id="user_name">
								</a>
								<span class="user-title" id="user_gender"></span>
								<span class="user-title" id="user_birthday">
								</span>
								<hr />
								<ul class="list-unstyled user-info-list">
									<li>
										<i class="fa-home" id="user_home"></i>
									</li>
								</ul>
								<hr />
							</div>
						</div>
						<div class="col-sm-9">
							<section class="user-timeline-stories">
								<article class="timeline-story">
									<header>
										<div class="user-details">
											<h5>
												真实姓名 &nbsp;&nbsp;&nbsp;
												<font color="black" size=3 id="real_name"></font>
											</h5>
											<hr />
											<h5>
												身份证号 &nbsp;&nbsp;&nbsp;
												<font color="black" size=3 id="id"></font>
											</h5>
											<hr />
											<h5>
												工作单位 &nbsp;&nbsp;&nbsp;
												<font color="black" size=3 id="institute"></font>
											</h5>
											<hr />
											<h5>
												职&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;位 &nbsp;&nbsp;&nbsp;
												<font color="black" size=3 id="position"></font>
											</h5>
											<hr />
											<h5>
												手机号码 &nbsp;&nbsp;&nbsp;
												<font color="black" size=3 id="tel"></font>
											</h5>
											<hr />
											<h5>
												个人描述 &nbsp;&nbsp;&nbsp;
												<font color="black" size=3 id="detail"></font>
											</h5>
											<hr />
											<h5 id="file">
												
											</h5>
											<hr />
										</div>
										<div class="form-group-separator"></div>
										<div class="form-group">
										</div>
									</header>
								</article>
							</section>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade custom-width" id="modal-refuse">
		<div class="modal-dialog" style="width: 60%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">请输入拒绝理由</h4>
				</div>
				<div class="modal-body">
					<form id="refushForm" class="profile-post-form">
						<textarea class="form-control input-unstyled input-lg autogrow" placeholder="为什么拒绝呢？" name="reason"></textarea>
						<input type="hidden" name="applicationID"  id="i_applicationID" value="">
						<input type="hidden" name="applyRoleID" id="i_applyRoleID" value="">
						<input type="hidden" name="email" id="i_email" value="">
						<input type="hidden" name="session_username" value="${session_user.username}">
						<input type="hidden" name="username" id="i_username" value="">
						<input type="hidden" name="userID"  id="i_userID" value="">
						<input type="hidden" name="state" value="拒绝">
						<i class="el-edit block-icon"></i>
						<hr />
						<button type="submit" id="submit" class="btn btn-single btn-xs btn-success post-story-button pull-right">提交</button>
					</form>
				
				</div>
			</div>
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
	<!-- <script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script> -->
	<script src="assets/js/dateformat.js"></script>
	<!--Specific JS for this page-->
	<script src="assets/js/custom/applyListRole.js"></script>
</body>

</html>