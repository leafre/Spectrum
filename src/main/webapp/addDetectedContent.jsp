<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<title>上传算法插件</title>
	<%-- 全站样式 --%>
	<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">添加检测内容</h1>
						<p class="description">您可以在此页面上添加检测内容</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" /> </nav>

			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">请填写添加信息</h3>
						</div>
						<div class="panel-body">
							<form action="addDetectedContent.do" role="form" class="form-horizontal" method="post" enctype="multipart/form-data">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="detectedName">检测内容中文名<i class="text-danger">&nbsp;*</i></label>
									<div class="col-sm-7">
										<input type="text" class="form-control" name="version" id="version" placeholder="请谨慎填写，中文名一经填写不允许修改">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="detectedName">检测内容英文名<i class="text-danger">&nbsp;*</i></label>
									<div class="col-sm-7">
										<input type="text" class="form-control" name="version" id="version" placeholder="请谨慎填写，英文名一经填写不允许修改">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="detectedName">检测内容化学式<i class="text-danger">&nbsp;*</i></label>
									<div class="col-sm-7">
										<input type="text" class="form-control" name="version" id="version" placeholder="请谨慎填写，化学式一经填写不允许修改">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group col-sm-6">
									<button type="submit" class="btn btn-info pull-right">确认添加</button>
								</div>
								<h3 style="color: red;"> ${addDetectedContent_msg}</h3>
							</form>
						</div>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<div class="modal fade" id="addDetectedContent_msg_modal" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">系统提示信息</h4>
				</div>
				<div class="modal-body">
					<h3>${addDetectedContent_msg}</h3>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${!empty addDetectedContent_msg }">
	<script type="text/javascript">
		$(function() {
			$("#addDetectedContent_msg_modal").modal('show');
		});
	</script>
	</c:if>

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="assets/js/multiselect/css/multi-select.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/select2/select2_locale_zh-CN.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
	</script>
</body>

</html>