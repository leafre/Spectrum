<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="date" uri="/WEB-INF/datetag.tld"%>
<!DOCTYPE html>
<html>

<head>
	<title>系统维护管理</title>
	<%-- 全站样式 --%>
	<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">系统维护管理</h1>
						<p class="description">您可以在此页面对系统进行备份和恢复</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#home" data-toggle="tab">
							<span class="hidden-xs">系统恢复信息列表</span>
							</a>
						</li>
						<li>
							<a href="#profile" onclick="showBackupList()" data-toggle="tab">
							<span class="hidden-xs">系统备份信息列表</span>
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="home">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">系统恢复列表</h3>
								</div>
								<div class="panel-body">
									<table class="table table-striped table-bordered" id="recoverList">
										<thead>
											<tr class="replace-inputs">
												<th>编号</th>
												<th>恢复内容</th>
												<th>恢复时间</th>
												<th>操作</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="recover" items="${querydata}">
												<tr>
													<td class="recoverID">${recover.recoverID}</td>
													<td>${recover.content}</td>
													<td class="center">
														<date:date value="${recover.time}" parttern="yyyy/MM/dd hh:mm:ss"></date:date>
													</td>
													<td>
														<a href="javascript:;" onclick="showRecoverDetail(this)" class="btn btn-info btn-sm btn-icon icon-left">详细信息 </a>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div class="tab-pane" id="profile">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">系统备份列表</h3>
									<div class="fa-hover pull-right">
										<a href="javascript:;" onclick="showBackup_modal();">
											<i class="fa fa-files-o"></i> <span>备份文件</span>
										</a>
									</div>
								</div>
								<div class="panel-body">
									<table id="backupList" class="table table-striped table-bordered">
										<thead>
											<tr class="replace-inputs">
												<th>编号</th>
												<th>备份名称</th>
												<th>备份内容</th>
												<th>备份时间</th>
												<th>操作</th>
											</tr>
										</thead>
										<tbody id='backupTable'>
											<!-- <tr>
												<td>Trident</td>
												<td>Internet Explorer 4.0</td>
												<td>Win 95+</td>
												<td class="center">4</td>
												<td>
													<a href="javascript:;" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});" class="btn btn-secondary btn-sm btn-icon icon-left"> 恢复
													</a>
													<a href="javascript:;" onclick="showBackupDetail(this);" class="btn btn-info btn-sm btn-icon icon-left"> 详细信息 </a>
												</td>
											</tr> -->
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />
	<div class="modal fade" id="modal-1">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">提示信息</h4>
				</div>
				<div class="modal-body">您确定要删除这条备份么？</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-info">删除</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="backupDetail">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">备份详情</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="backupName" class="control-label">备份名称</label>
								<input id="backupName" type="text" value="备份名称" class="form-control"  disabled />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">备份时间</label>
								<input id="backupTime" type="text" class="form-control" disabled />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">备份内容</label>
								<input id="content" type="text" class="form-control" data-role="tagsinput" value="Sample tag, Another great tag, Awesome!" disabled />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">备份描述</label>
								<textarea id="description" class="form-control autogrow" cols="5" disabled></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="recoverDetail">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">恢复详情</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">恢复时间</label>
								<input id="recoverTime" type="text" class="form-control" disabled />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">恢复内容</label>
								<input name="content" id="content" type="text" class="form-control" data-role="tagsinput" value="Sample tag, Another great tag, Awesome!" disabled />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">恢复描述</label>
								<textarea name="description" id="description" class="form-control autogrow" cols="5" disabled></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="modal fade" id="recover_modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">填写恢复信息</h4>
				</div>
				<form action="" method="post" id="recoverForm">
				<input type="hidden" name="backupID" value="">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">选择需要恢复的内容</label>
								<script type="text/javascript">
									jQuery(document).ready(function($)
									{
										$("#recoverBackupContent").select2({
											placeholder: '请选择需要恢复的部分',
											allowClear: true
										}).on('select2-open', function()
										{
											// Adding Custom Scrollbar
											$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
										});
										
									});
								</script>								
								<select class="form-control" id="recoverBackupContent" multiple>
									<option></option>
									<optgroup label="可选择的恢复内容">
										<!-- <option selected>California</option> -->
									</optgroup>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">恢复描述</label>
								<textarea name="description" class="form-control autogrow" cols="5" id="field-5" placeholder="请您在此处填写对本次恢复操作的原因（128字以内）"></textarea>
							</div>
						</div>
					</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">取消</button>
					<button type="submit" class="btn btn-info" form="recoverForm">进行恢复</button>
				</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="backup_modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">填写备份信息</h4>
				</div>
				<form action="" method="post" id="backupForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="backupName" class="control-label">备份名称</label> 
								<input name="backupName" type="text" class="form-control" placeholder="备份名称">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">备份内容</label>
								<select name="content" class="form-control" id="backupContent" multiple>
									<option></option>
									<optgroup label="可备份内容">
										<option>数据库</option>
										<option>用户收藏夹</option>
										<option>申请文件文件夹</option>
										<option>系统算法文件夹</option>
										<option>用户算法文件夹</option>
										<option>标准库光谱文件夹</option>
									</optgroup>
								</select>

							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">备份描述</label>
								<textarea name="description" class="form-control autogrow" cols="5" placeholder="请在此处对备份的内容进行简单描述（128字以内）"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">取消</button>
					<button type="submit" class="btn btn-info" form="backupForm">进行备份</button>
				</div>
				</form>
			</div>
		</div>
	</div>

	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
	<script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script src="assets/js/datatables/tabletools/dataTables.tableTools.min.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/dateformat.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
		var backups = [];
		var recovers = [];
		jQuery(document).ready(function($) {
			//系统恢复列表
			$("#recoverList").dataTable({
				aLengthMenu: [
					[5, 10, 25, 50, 100, -1],
					[5, 10, 25, 50, 100, "所有"]
				]
			});
			$("#backupContent").select2({
				placeholder: '请您选择需要备份的内容',
				allowClear: true
			}).on('select2-open', function(){
				$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
			});
		});

		var $recoverBackupContent = $('#recover_modal #recoverBackupContent');
		
		function showRecover_modal(obj) {
			var backupID = $(obj).parent().parent().find("td").eq(0).text();
			for(var i in backups){
				var backup = backups[i];
				if(backup.backupID == backupID){
					$('#recover_modal input[name="backupID"]').val(backupID);
					//设置列表
					var contents = backup.content.split(",");
					$('#s2id_recoverBackupContent').children('ul').children('li.select2-search-choice').remove();
					$recoverBackupContent.find('optgroup').children().remove();
					var templateoption = "<option>content</option>";
					for(var j in contents){
						$recoverBackupContent.find('optgroup').append($(templateoption.replace('content',contents[j])));
					}
				}
			}
			jQuery('#recover_modal').modal('show', {
				backdrop: 'static'
			});
		}
		function showBackup_modal() {
			jQuery('#backup_modal').modal('show', {
				backdrop: 'static'
			});
		}

		function showBackupDetail(obj) {
			var backupID = $(obj).parent().parent().find("td").eq(0).text();
			if(backups.length==0){
				$.get("queryBackup.do", {
					'backupID': backupID
				}, function(data) {
					var backup = data.querydata[0];
					// 设置恢复详细信息
					$('#backupName').val(backup.backupName);
					$('#backupTime').val(new Date(backup.time).format("yyyy/MM/dd hh:mm:ss"));
					$('#content').val(backup.content);
					$('#description').val(backup.description);
				},"json");
			}else{
				for(var i in backups){
					var backup = backups[i];
					if(backup.backupID==backupID){
						// 设置恢复详细信息
						$('#backupName').val(backup.backupName);
						$('#backupTime').val(new Date(backup.time).format("yyyy/MM/dd hh:mm:ss"));
						$('#content').val(backup.content);
						$('#description').val(backup.description);
					}
				}
			}
			
			$('#backupDetail').modal('show', {
				backdrop: 'fade'
			});
		}
		
		function showRecoverDetail(obj) {
			var recoverID = $(obj).parent().parent().find("td").eq(0).text();
			$.get("queryRecover.do", {
				'recoverID': recoverID
			}, function(data) {
				var recover = data.querydata[0];
				// 设置恢复详细信息
				$('#recoverTime').val(new Date(recover.time).format("yyyy/MM/dd hh:mm:ss"));
				$('#recoverDetail input[name="content"]').val(recover.content);
				$('#recoverDetail textarea[name="description"]').val(recover.description);
			},"json");
			
			$('#recoverDetail').modal('show', {
				backdrop: 'fade'
			});
		}

		var buttonTemplate = "<td>" +
			"<a href='javascript:;'onclick='showRecover_modal(this);' class='btn btn-secondary btn-sm btn-icon icon-left'> 恢复</a>" +
			"<a href='javascript:;'onclick='showBackupDetail(this);'class='btn btn-info btn-sm btn-icon icon-left'> 详细信息 </a>";

		var $backupList_table = null;
		
		function showBackupList() {
			if(backups.length==0){
				$.ajax({
					url: 'queryBackup.do',
					type: 'post',
					dataType: 'json',
					success: function(jsondata) {
						var $backupTable = $('#backupTable');
						$backupTable.children().remove();
						backups = jsondata.querydata;
						
						for(var i in backups){
							backups[i].timeStr = new Date(backups[i].time).format("yyyy/MM/dd hh:mm:ss");
							backups[i].button = buttonTemplate;
						}
						if($backupList_table!=null){
							$backupList_table.destroy();
						}
						$backupList_table = $('#backupList').DataTable({
							aLengthMenu: [
								[5, 10, 25, 50, 100, -1],
								[5, 10, 25, 50, 100, "所有"]
							],
					        "data": backups,
					        "dataSrc": "",
					        "columns": [
					            { data: "backupID"},
					            { data: "backupName"},
					            { data: "content"},
					            { data: "timeStr"},
					            { data: "button"}
					        ]
					    });
						/* for(var i in backups) {
							var $tr = $("<tr></tr>")
								.append("<td class='backupID'>" + backups[i].backupID + "</td>")
								.append("<td>" + backups[i].backupName + "</td>")
								.append("<td>" + backups[i].content + "</td>")
								.append("<td>" + new Date(backups[i].time).format("yyyy/MM/dd hh:mm:ss") + "</td></tr>");
	
							$tr.append(buttonTemplate);
							$backupTable.append($tr);
						} */
						
						/* var $trs = $(backupTable).children("tr");
						$trs.each(function(i,tr){
							$(tr).find("td").eq(3).after(buttonTemplate);
						}); */
					}
				});
			}
		}
		// 备份操作
		$('#backup_modal #backupForm').submit(function(event) {
			event.preventDefault();
			var backupName = $.trim($('#backup_modal input[name="backupName"]').val()),
				content = $.trim($('#backup_modal #backupContent').val()),
				description = $.trim($('#backup_modal textarea[name="description"]').val());
            $.ajax({
            	url: "backup.do",
                type: "POST",
                dataType: "json",
                data: {
					backupName: backupName,
					content: content,
					description: description
				},
                success: function (jsondata) {
                	if('备份成功'==jsondata.backup_msg){
						$('#backup_modal').modal('hide');
                	 	// 成功
                	 	backups = [];
					}
					alert(jsondata.backup_msg);
                },
                error: function(jsondata) {
                	alert(jsondata.backup_msg);
                }
           });
    	});
		//恢复操作
		$('#recover_modal #recoverForm').submit(function(event) {
			event.preventDefault();
			var content = $.trim($('#recover_modal #recoverBackupContent').val()),
				backupID = $('#recover_modal input[name="backupID"]').val(),
				description = $.trim($('#recover_modal textarea[name="description"]').val());
            $.ajax({
            	url: "recover.do",
                type: "POST",
                data: {
                	backupID: backupID,
					content: content,
					description: description
				},
                dataType: "json",
                success: function (jsondata) {
                	if('恢复成功'==jsondata.recover_msg){
						$('#recover_modal').modal('hide');
					}
                	alert(jsondata.recover_msg);
                },
                error: function(jsondata) {
                	alert(jsondata.recover_msg);
                }
           });
    	});
	</script>
</body>

</html>