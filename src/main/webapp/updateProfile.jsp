<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="date" uri="/WEB-INF/datetag.tld"%>
<!DOCTYPE html>
<html>

<head>
<title>完善/修改个人信息</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">完善/修改个人信息</h1>
						<p class="description">用户可以在此页面上完善、修改个人信息（真实姓名和身份证号不可修改，请谨慎填写）</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading"></div>
						<div class="panel-body">
							<form role="update_form" action="modifyUserInfo.do?userID=${session_user.userID}" method="post" class="validate form-horizontal">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="realName">真实姓名:</label>
									<div class="col-sm-7">
										<c:if test="${session_user.realName!=null }">
											<input type="text" class="form-control realName" id="realName" name="realName"
												value="${session_user.realName}" readonly />
										</c:if>
										<c:if test="${session_user.realName==null }">
											<input type="text" class="form-control realName" id="realName" name="realName"
												value="">
										</c:if>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="IDNumber">身份证号:</label>
									<div class="col-sm-7">
										<c:if test="${session_user.IDNumber!= null }">
											<input type="text" class="form-control IDNumber" id="IDNumber" name="IDNumber"
												value="${session_user.IDNumber}" readonly />
										</c:if>
										<c:if test="${session_user.IDNumber== null }">
											<input type="text" class="for m-control IDNumber" id="IDNumber" name="IDNumber"
												value="">
										</c:if>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">性别:</label>
									<div class="col-sm-6">
										<p>
											<label class="radio-inline">
												<input type="radio" name="gender" class = "gender"
													<c:if test="${session_user.gender=='保密'}">checked="checked"</c:if>
													value="保密">
												保密
											</label>
											<label class="radio-inline">
												<input type="radio" name="gender" class = "gender"
													<c:if test="${session_user.gender=='男'}">checked="checked"</c:if>
													value="男">男
											</label>
											<label class="radio-inline">
												<input type="radio" name="gender" class = "gender"
													<c:if test="${session_user.gender=='女'}">checked="checked"</c:if>
													value="女">女
											</label>
										</p>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">出生日期</label>
									<div class="col-sm-6">
										<input type="text" class="form-control datepicker" id="birthday" name="birthday"
										value="<date:date  value='${session_user.birthday}' parttern="MM/dd/yyyy"></date:date>" />
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">详细地址</label>
									<div class="col-sm-9">
										<div class="col-sm-3">
											<strong>省：</strong>
											<select class="form-control" id="loc_province_select">
											</select>
										</div>
										<div class="col-sm-3">
											<strong>市：</strong>
											<select class="form-control" id="loc_city_select">
											</select>
										</div>
										<div class="col-sm-3">
											<strong>区/县：</strong>
											<select class="form-control" id="loc_town_select">
											</select>
										</div>
										<div class="form-group"></div>
										<div class="col-sm-7">
											<strong>街区：</strong>
											<input type="text" name="address" value="${session_user.address}"
												class="form-control" id="address" placeholder="街区">
										</div>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">手机号</label>
									<div class="col-sm-7">
										<input type="text" class="form-control tel" id="tel" name="tel"
											value="${session_user.tel}">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="institute">工作单位</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="institute"
											name="institute" value="${session_user.institute}">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="position">职位</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="position" name="position"
											value="${session_user.position}">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-5">个人描述</label>
									<div class="col-sm-7">
										<textarea class="form-control autogrow detail" rows="5" cols="8" id="detail"
											name="detail">${session_user.detail}</textarea>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<input type="submit" value="提交修改" class="btn btn-info btn-single pull-right"/>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/jquery-validate/jquery.validate.js"></script>
	<script src="assets/js/jquery-validate/localization/messages_zh.js"></script>
	<script src="assets/js/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/province/area.js"></script>
	<script src="assets/js/province/location.js"></script>
	<script src="assets/js/custom/updateProfile.js"></script>
	<script type="text/javascript">
	window.onload = function() {
		if ('${modifyInfo_msg}') {
			alert('${modifyInfo_msg}');
		}
	}
	</script>
</body>

</html>