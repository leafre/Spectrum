<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<title>审核光谱</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">审核光谱</h1>
						<p class="description">此页面上显示待审核光谱的详细信息。</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<%-- 内容 --%>
			<div class="row">
				<div class="col-md-12 gallery-right">
					<!-- Default panel -->
					<div class="panel panel-default">
						<!-- Add class "collapsed" to minimize the panel -->
						<div class="panel-heading">
							<h3 class="panel-title">光谱名称</h3>
							<div class="panel-options">
								<a href="#" data-toggle="panel"> <span class="collapse-icon">&ndash;</span>
									<span class="expand-icon">+</span>
								</a>
							</div>
						</div>
						<div class="panel-body">
							<p>
								<strong>光谱名称：</strong>${querydata[0].spectrumName}</p>
							<p>
								<strong>光谱描述：</strong>${querydata[0].description}</p>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<!-- Collapsed panel -->
					<div class="panel panel-default collapsed">
						<!-- Add class "collapsed" to minimize the panel -->
						<div class="panel-heading">
							<h3 class="panel-title">光谱详细信息</h3>
							<div class="panel-options">
								<a href="#" data-toggle="panel"> <span class="collapse-icon">&ndash;</span>
									<span class="expand-icon">+</span>
								</a>
							</div>
						</div>
						<div class="panel-body">
							<section class="invoice-env">
								<!-- Invoice header -->
								<div class="invoice-header">
									<!-- Invoice Options Buttons -->
									<div class="invoice-options hidden-print">
										<a href="#"
											class="btn btn-block btn-secondary btn-icon btn-icon-standalone btn-icon-standalone-right btn-single text-left">
											<i class="fa-print"></i> <span>Print</span>
										</a>
									</div>
									<!-- Invoice Data Header -->
									<div class="invoice-logo">
										<ul class="list-unstyled">
											<li class="upper"><strong>光谱类型：</strong>${querydata[0].spectrumType}
											</li>
											<li><strong>光谱文件类型：</strong>${querydata[0].spectrumFileType}</li>
											<li><strong>是否标准库：</strong> <c:choose>
													<c:when test="${querydata[0].isStandard == 0}">否</c:when>
													<c:otherwise>是</c:otherwise>
												</c:choose></li>
											<li><strong>分辨率：</strong>${querydata[0].resolutionRate}</li>
											<li><strong>X轴单位：</strong>${querydata[0].xUnit}</li>
											<li><strong>Y轴单位：</strong>${querydata[0].yUnit}</li>
											<li><strong>检测设备：</strong>${querydata[0].hardwareName}</li>
											<li><strong>使用的处理算法：</strong>纵坐标归一化</li>
											<li><strong>申请时间：</strong>${querydata[0].submitTime}</li>
											<!-- 将申请时间转型为yyyy-MM-dd HH:mm:ss -->
										</ul>
									</div>
								</div>
								<!-- Client and Payment Details -->
								<div class="invoice-details">
									<div class="invoice-client-info">
										<strong>被检测物</strong>
										<ul class="list-unstyled">
											<li>学名：</li>
											<li>产地：</li>
										</ul>
										<ul class="list-unstyled">
											<li>沙糖桔</li>
											<li>广东</li>
										</ul>
									</div>
									<div class="invoice-payment-info">
										<strong>可检测内容</strong>
										<ul class="list-unstyled">
											<li>水分、蛋白质、脂肪</li>
											<li>膳食纤维、胡萝卜素、视黄醇当量、硫胺素、核黄素、尼克酸</li>
											<li>维生素 C、维生素E</li>
											<li>钾、钠、钙、镁、铁、锰、锌、铜、磷、硒</li>
										</ul>
									</div>
								</div>
								<hr />
								<div class="invoice-details">
									<div class="invoice-client-info">
										<strong>特征峰值：</strong>
										<ul class="list-unstyled">
											<li>211、345、567.8</li>
											<li>321.9、890.0、1111.1</li>
											<li>1232.5、1543.9、1765.2</li>
											<li>2121.2</li>
										</ul>
									</div>
								</div>
								<hr />
								<div class="invoice-details">
									<div class="invoice-client-info">
										<strong>光谱分析报告：</strong>
									</div>
								</div>
								<!-- Invoice Entries -->
								<table class="table table-bordered">
									<thead>
										<tr class="no-borders">
											<th class="text-center hidden-xs">#</th>
											<th width="60%" class="text-center">报告名称</th>
											<th class="text-center hidden-xs">保存时间</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-center hidden-xs">1</td>
											<td><a href="#">On am we offices expense thought</a></td>
											<td class="text-center hidden-xs">1</td>
										</tr>
										<tr>
											<td class="text-center hidden-xs">2</td>
											<td><a href="#">Up do view time they shot</a></td>
											<td class="text-center hidden-xs">1</td>
										</tr>
										<tr>
											<td class="text-center hidden-xs">3</td>
											<td><a href="#">Way ham unwilling not breakfast</a></td>
											<td class="text-center hidden-xs">1</td>
										</tr>
										<tr>
											<td class="text-center hidden-xs">4</td>
											<td><a href="#">Songs to an blush woman be sorry</a></td>
											<td class="text-center hidden-xs">1</td>
										</tr>
										<tr>
											<td class="text-center hidden-xs">5</td>
											<td><a href="#">Luckily offered article led lasting</a></td>
											<td class="text-center hidden-xs">1</td>
										</tr>
										<tr>
											<td class="text-center hidden-xs">6</td>
											<td><a href="#">Of as by belonging therefore
													suspicion</a></td>
											<td class="text-center hidden-xs">1</td>
										</tr>
									</tbody>
								</table>
							</section>
						</div>
					</div>
				</div>
			</div>

			<section class="gallery-env">
				<div class="row">
					<div class="col-md-12">
						<div class="album-header">
							<h2>光谱图像</h2>
							<ul class="album-options list-unstyled list-inline">
								<li><a href="#"> <i class="fa-download"></i> 下载光谱
								</a></li>
								<li><a href="#" data-action="edit"> <i class="fa-edit"></i>
										使用光谱
								</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div id="echarts" class="viewport " style="height: 500px;"></div>
				</div>
			</section>

			<div class="row">
				<div class="col-sm-12">
					<div class="modal-footer">
						<a href="javascript:;" onclick="jQuery('#modal-2').modal('show');"
							class="btn btn-primary btn-single btn-sm pull-right">拒绝申请</a> <a
							href="#" class="btn btn-primary btn-single btn-sm pull-right" onclick="javascript:passRequest('${querydata[0].spectrumID}','${session_user.userID}')">通过审核</a>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>

	<!-- Modal 2 (Custom Width)-->
	<div class="modal fade custom-width" id="modal-2">
		<div class="modal-dialog" style="width: 60%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">拒绝通过的原因</h4>
				</div>
				<div class="modal-body">
					<form id ="refushForm" class="profile-post-form">
						<textarea class="form-control input-unstyled input-lg autogrow"
							placeholder="What's on your mind?" name="reason"></textarea>
							<input type="hidden" name="spectrumID" value="${querydata[0].spectrumID}">
							<input type="hidden" name="userID" value="${session_user.userID}">
							<input type="hidden" name="state" value="拒绝">
						<i class="el-edit block-icon"></i>
						<hr />
						<button type="submit"
							 id="submit" class="btn btn-single btn-xs btn-success post-story-button pull-right" >提交</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<!-- Imported scripts on this page -->
	<script src="assets/js/echarts/echarts.js "></script>
	<!-- <script src="assets/js/echarts/echarts.common.min.js "></script> -->
	<script src="<c:url value='assets/js/echarts/echarts_function.js'/>"></script>
	<!--echarts_demo-->
	<script src="assets/js/echarts/echarts_demo.js "></script>
	<script src="assets/js/custom/SpectrumFile.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('#refushForm').submit(function(){
				var spectrumID='${querydata[0].spectrumID}';
				var userID='${session_user.userID}';
                jQuery.ajax({
                    url: "auditSpectrum.do",   // 提交的页面
                    data: $('#refushForm').serialize(),// 从表单中获取数据
                    type: "POST",                   // 设置请求类型为"POST"，默认为"GET"
                    beforeSend: function()          // 设置表单提交前方法
                    {
                    	$("submit").attr('disable','disable');
                        
                    },
                    error: function(request) {      // 设置表单提交出错
                    	$("submit").removeAttr('disable')
                        alert("表单提交出错，请稍候再试");
                    },
                    success: function(data) {
                    	var msg= data.auditSpectrum_msg;
                    	if(msg==null){
                        alert("该申请已被拒绝，拒绝原因已发送");
                        dispatch_href();
                    	}else{
                    		alert(msg);
                    	}
                    }
                });
                return false;
            });
        });
		function  passRequest(spectrumID,userID){
		   var op= confirm("是否通过该请求");	
			if(op){
			$.post('auditSpectrum.do',{spectrumID:spectrumID,userID:userID,state:'通过'},function(json){
				var msg=json.auditSpectrum_msg;
				
				if(msg=="标准库更新成功"){
					alert(msg);
					dispatch_href();
				}else{
					alert(msg);
				}
			},'json').error(function(xhr,errorText,errorType){
				 alert("服务器错误，操作无效");
		    });
				
			}
			
		}
		function dispatch_href(){
			window.location.href="querySubmitSpectrum.do?state=等待";
		}
		 
			            
	</script>
</body>

</html>