<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<title>查看收藏夹</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
<script type="text/javascript">
function newFile(){
	if($('#newFavorite').length<=0){
		var input="<input type='text' class='form-control' value='新建文件夹' id='favoriteName' name='favoriteName'>";
		var fa_folder_o="<i class='fa-folder-o'></i>";
		var fa_remove="<i class='fa-remove' onclick='newFileOver();'></i>";
		var fa_check="<i class='fa-check' onclick='createFavorite();newFileOver();'></i>";
		$('#favoriteList').prepend(
				$("<li class='active' id='newFavorite'>").append(
						$("<table width='90%'>").append(
								$("<td>").append(
										$("<a>").append(fa_folder_o)
												.append($("<span>").append(input))//输入框的宽度问题
												.append(fa_check)
												.append(fa_remove)))));
	}
}
function newFileOver(){
	$('#newFavorite').remove();
}
</script>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">个人收藏</h1>
						<p class="description">
							您可以在此页面上对收藏的光谱进行操作&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong
								class="pull-right">（使用空间：1MB&nbsp;&nbsp;&nbsp;总空间：16MB）</strong>
						</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>
			<input type="hidden" id="roleID" value="${session_user.roleID}">
			<section class="gallery-env">
				<div class="row">
					<!-- Gallery Sidebar -->
					<div class="col-sm-4 gallery-left">
						<div class="icon-collection">
							<div class="fontawesome-icon-list">
								<div class="fa-hover">
									<i class="fa-cloud-upload" onclick="newFile();"></i> <span>新建文件夹</span>
								</div>
								<div class="fa-hover">
									<i class="fa-search-plus"
										onclick="jQuery('#modal_addFaverate').modal('show', {backdrop: 'fade'});"></i>
									<span>高级检索</span>
								</div>
							</div>
						</div>

						<div class="gallery-sidebar">
							<div class="breadcrumb-env">
								<ol class="breadcrumb bc-3">
									<li><i class="fa-reply"></i><a><span>...</span></a></li>
									<li><a href="ui-panels.html">全部文件</a></li>
									<li class="active"><strong>testSpectrum</strong></li>
								</ol>
							</div>
							<ul class="list-unstyled" id="favoriteList">
								<c:forEach var="favorite" items="${Favorite}">
									<li class="active" index="${favorite.favoriteID}">
										<table width="90%">
											<tr>
												<td><a href="javascript:;"
													onclick="showSpectrumF(${favorite.favoriteID});"> <i
														class="fa-folder-o"></i> <span>${favorite.favoriteName}</span>
												</a></td>
												<td align="right"><a href="javascript:;"
													data-action="trash_favorite"
													onclick="setID(${favorite.favoriteID});"> <i
														class="fa-trash"></i>
												</a></td>
											</tr>
										</table>
									</li>
								</c:forEach>
							</ul>
						</div>
					</div>
					<div class="panel panel-default col-sm-8">
						<div class="album-header">
							<h2>
								<i class="fa-folder-open-o"></i>testFavorite
							</h2>
							<ul class="album-options list-unstyled list-inline">
								<li><a href="javascript:;" onclick="refreshSpectrum()">
										<i class="fa-refresh"></i> 刷新
								</a></li>
								<li><a href="#"> <i class="fa-upload"></i> 上传光谱
								</a></li>
								<li><a href="#"> <i class="fa-download"></i> 下载光谱
								</a></li>
								<li><a href="javascript:;" onclick="useSpectrum()"> <i
										class="fa-edit"></i> 使用光谱
								</a></li>
								<li><a href="#" data-action="trash_favoriteSpectrum"> <i
										class="fa-trash"></i> 删除
								</a></li>
							</ul>
						</div>
						<section class="mailbox-env">
							<div class="mail-env">
								<table id="spectrumList" class="table mail-table">
									<!-- mail table header -->
									<thead>
										<tr>
											<td class="col-cb">
												<div class="checkbox checkbox-replace">
													<input type="checkbox" class="cbr" id="select-all" />
												</div>
											</td>
											<td class="col-name"><label for="select-all">选择全部</label>
											</td>
										</tr>
									</thead>
									<!-- email list -->
									<tbody id="spectrum">
										<!--  <tr>
										<td class="col-cb" id="checkbox_replace">
											<div class="checkbox checkbox-replace">
												<input type="checkbox" class="cbr" />
											</div>
										</td>
										<td class="col-name">
										<a href="javascript:;" onclick="showmodal();"
											class="col-name"> Google AdWords </a></td>
										<td class="col-subject">Google AdWords: Ads not serving</td>
										<td class="col-time" width="30%">08:40</td>
										<td class="col-options hidden-sm hidden-xs"><a href="#"
											data-action="trash_spectrum"> <i class="fa-trash"></i>
										</a></td>
									</tr>-->
									</tbody>
									<!-- mail table footer -->
									<tfoot>
										<tr>
											<th class="col-cb"></th>
											<th colspan="3" class="col-header-options">
												<div class="mail-pagination">
													该收藏夹一共有 <strong id="spectrumCount">0</strong> 张光谱
												</div>
											</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</section>
					</div>

				</div>
			</section>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	
	<!-- 删除收藏夹的弹窗 -->
	<div class="modal fade" id="trash_favorite_modal"
		data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">确认删除</h4>
				</div>
				<div class="modal-body">你想删除这个收藏夹吗？</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal"
						onclick="deleteFavorite();">删除</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Gallery Delete Image (Confirm)-2-->
	<!-- 删除单张光谱的弹窗 -->
	<div class="modal fade" id="trash_spectrum_modal"
		data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">确认删除</h4>
				</div>
				<div class="modal-body">你想删除这张光谱吗？连同他的报告也会一同删除</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal"
						onclick="deleteSpectrum();">删除</button>

				</div>
			</div>
		</div>
	</div>
	<!-- 	批量删除光谱的弹窗 -->
	<div class="modal fade" id="trash_favoriteSpectrum_modal"
		data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">确认删除</h4>
				</div>
				<div class="modal-body">你想删除这些张光谱吗？连同他们的报告也会一同删除</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal"
						onclick="batchDeleteSpectrum();">删除</button>
				</div>
			</div>
		</div>
	</div>
	<!-- 	批量删除光谱的弹窗(提示用户要选中若干张光谱) -->
	<div class="modal fade" id="trash_favoriteSpectrum_modal_prompt"
		data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">请选中若干张光谱</h4>
				</div>
				<div class="modal-body">请选中若干张光谱，这样我们才能猜出你想删哪些光谱</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">好的</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal_spectrum">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">光谱详细信息</h4>
				</div>

				<div class="modal-body">
					<a href="#"
						class="btn btn-info btn-sm btn-icon icon-left pull-right"
						id="spectrumHref">点击查看光谱</a>
					<hr />
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">光谱名称</label> <input
									type="text" class="form-control" id="spectrumName"
									placeholder="" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">光谱类型</label> <input
									type="text" class="form-control" id="spectrumType"
									placeholder="" disabled />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label for="field-1" class="control-label">标准库光谱</label>
								<div class="radio">
									<label> <input type="radio" id="noStandard" disabled />
										否
									</label> <label> <input type="radio" id="isStandard" disabled />
										是
									</label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="field-1" class="control-label">光谱保存时间</label> <input
									type="text" class="form-control" id="saveTime" placeholder=""
									disabled />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">光谱描述</label>
								<textarea class="form-control autogrow" cols="5"
									id="description" placeholder=""></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label for="field-1" class="control-label">产生光谱的硬件设备</label> <input
									type="text" class="form-control" id="hardware" placeholder=""
									disabled />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">被检测物</label> <input
									type="text" class="form-control" id="detectedObject"
									placeholder="" disabled />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">检测内容</label> <input
									type="text" class="form-control" id="spectrumContent"
									placeholder="" disabled />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-info" id="modifybutton"
						data-dismiss="modal" onclick="modifyFavoriteSpetrum();">修改</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal_addFaverate">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">新建收藏夹</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">请输入新收藏夹的名称（请勿重复；长度在16个字节以内）</label>
								<input type="text" class="form-control" placeholder="收藏夹名称"
									id="favoriteName" value="" name="favoriteName"
									autocomplete="off" />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-info"
						onclick="createFavorite();" data-dismiss="modal">新建</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Imported scripts on this page -->
	<!-- <script src="assets/js/jquery-ui.min.js"></script> -->
	<script src="assets/js/dateformat.js"></script>
	<!--Specific JS for this page-->
	<script src="assets/js/custom/favorite.js"></script>
</body>

</html>