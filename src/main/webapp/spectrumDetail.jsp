<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>详细光谱信息</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">查看光谱</h1>
						<p class="description">您可以在此页面上查看所选择的光谱的详细信息</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="row">
				<div class="col-md-12 gallery-right">
					<!-- Default panel -->
					<div class="panel panel-default">
						<!-- Add class "collapsed" to minimize the panel -->
						<div class="panel-heading">
							<h3 class="panel-title">光谱名称</h3>
							<div class="panel-options">
								<a href="#" data-toggle="panel">
									<span class="collapse-icon">&ndash;</span> <span class="expand-icon">+</span>
								</a>
							</div>
						</div>
						<div class="panel-body">
							<p>光谱描述</p>
							<p>Now indulgence dissimilar for his thoroughly has terminated.
								Agreement offending commanded my an. Change wholly say why eldest
								period. Are projection put celebrated particular unreserved joy
								unsatiable its. In then dare good am rose bred or. On am in nearer
								square wanted.</p>
							<p>She travelling acceptance men unpleasant her especially entreaties
								law. Law forth but end any arise chief arose. Old her say learn these
								large. Joy fond many ham high seen this. Few preferred continual sir led
								incommode neglected. Discovered too old insensible collecting unpleasant
								but invitation.</p>
						</div>
					</div>
				</div>
			</div>

			<section class="gallery-env">
			<div class="row">
				<div class="col-md-12">
					<div class="album-header">
						<h2>光谱图像</h2>
						<ul class="album-options list-unstyled list-inline">
							<li>
								<a href="#">
									<i class="fa-download"></i> 下载光谱
								</a>
							</li>
							<li>
								<a href="#" data-action="edit">
									<i class="fa-edit"></i> 使用光谱
								</a>
							</li>
							<li>
								<a href="#" data-action="trash">
									<i class="fa-trash"></i> 删除光谱
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div id="echarts" class="viewport" style="height:600px;"></div>
			</div>
			<button class="btn btn-white btn-block ">
				<i class="fa-bars "></i> 查看光谱的分析报告
			</button>
			</section>
			<div class="row">
				<div class="col-md-12">
					<!-- Collapsed panel -->
					<div class="panel panel-default collapsed">
						<!-- Add class "collapsed" to minimize the panel -->
						<div class="panel-heading">
							<h3 class="panel-title">光谱详细信息</h3>
							<div class="panel-options">
								<a href="#" data-toggle="panel">
									<span class="collapse-icon">&ndash;</span> <span class="expand-icon">+</span>
								</a>
							</div>
						</div>
						<div class="panel-body">
							<section class="invoice-env"> <!-- Invoice header -->
							<div class="invoice-header">
								<!-- Invoice Options Buttons -->
								<div class="invoice-options hidden-print">
									<a href="#"
										class="btn btn-block btn-secondary btn-icon btn-icon-standalone btn-icon-standalone-right btn-single text-left">
										<i class="fa-print"></i> <span>Print</span>
									</a>
								</div>
								<!-- Invoice Data Header -->
								<div class="invoice-logo">
									<ul class="list-unstyled">
										<li class="upper">
											<strong>光谱类型：</strong>红外光谱
										</li>
										<li>
											<strong>光谱文件类型：</strong>.cov
										</li>
										<li>
											<strong>是否标准库：</strong>NO
										</li>
										<li>
											<strong>分辨率：</strong>4.0
										</li>
										<li>
											<strong>X轴单位：</strong>波数
										</li>
										<li>
											<strong>Y轴单位：</strong>吸光度
										</li>
										<li>
											<strong>检测设备：</strong>WQF-510A傅立叶变换红外光谱仪
										</li>
										<li>
											<strong>使用的处理算法：</strong>纵坐标归一化
										</li>
										<li>
											<strong>保存时间：</strong>2011年11月11日
										</li>
									</ul>
								</div>
							</div>
							<!-- Client and Payment Details -->
							<div class="invoice-details">
								<div class="invoice-client-info">
									<strong>被检测物</strong>
									<ul class="list-unstyled">
										<li>学名：</li>
										<li>产地：</li>
									</ul>
									<ul class="list-unstyled">
										<li>沙糖桔</li>
										<li>广东</li>
									</ul>
								</div>
								<div class="invoice-payment-info">
									<strong>可检测内容</strong>
									<ul class="list-unstyled">
										<li>水分、蛋白质、脂肪</li>
										<li>膳食纤维、胡萝卜素、视黄醇当量、硫胺素、核黄素、尼克酸</li>
										<li>维生素 C、维生素E</li>
										<li>钾、钠、钙、镁、铁、锰、锌、铜、磷、硒</li>
									</ul>
								</div>
							</div>
							<hr />
							<div class="invoice-details">
								<div class="invoice-client-info">
									<strong>特征峰值：</strong>
									<ul class="list-unstyled">
										<li>211、345、567.8</li>
										<li>321.9、890.0、1111.1</li>
										<li>1232.5、1543.9、1765.2</li>
										<li>2121.2</li>
									</ul>
								</div>
							</div>
							<hr />
							<div class="invoice-details">
								<div class="invoice-client-info">
									<strong>关联光谱：</strong>
								</div>
							</div>
							<!-- Invoice Entries -->
							<table class="table table-bordered">
								<thead>
									<tr class="no-borders">
										<th class="text-center hidden-xs">#</th>
										<th width="60%" class="text-center">光谱名称</th>
										<th class="text-center hidden-xs">保存时间</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="text-center hidden-xs">1</td>
										<td>On am we offices expense thought</td>
										<td class="text-center hidden-xs">1</td>
									</tr>
									<tr>
										<td class="text-center hidden-xs">2</td>
										<td>Up do view time they shot</td>
										<td class="text-center hidden-xs">1</td>
									</tr>
									<tr>
										<td class="text-center hidden-xs">3</td>
										<td>Way ham unwilling not breakfast</td>
										<td class="text-center hidden-xs">1</td>
									</tr>
									<tr>
										<td class="text-center hidden-xs">4</td>
										<td>Songs to an blush woman be sorry</td>
										<td class="text-center hidden-xs">1</td>
									</tr>
									<tr>
										<td class="text-center hidden-xs">5</td>
										<td>Luckily offered article led lasting</td>
										<td class="text-center hidden-xs">1</td>
									</tr>
									<tr>
										<td class="text-center hidden-xs">6</td>
										<td>Of as by belonging therefore suspicion</td>
										<td class="text-center hidden-xs">1</td>
									</tr>
								</tbody>
							</table>
							</section>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<blockquote class="blockquote blockquote-red">
						<p>
							<strong>拒绝通过申请的原因</strong>
						</p>
						<p>
							<small>Inquietude simplicity terminated she compliment remarkably
								few her nay. The weeks are ham asked jokes. Neglected perceived shy nay
								concluded. Not mile draw plan snug next all. Houses latter an valley be
								indeed wished merely in my. Money doubt oh drawn every or an china.
								Visited out friends for expense message set eat. </small>
						</p>
					</blockquote>
				</div>
			</div>
			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />
	
	<!-- Gallery Delete Image (Confirm)-->
	<div class="modal fade" id="gallery-image-delete-modal" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Confirm Image Deletion</h4>
				</div>
				<div class="modal-body">Do you really want to delete this image?</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-danger">Delete</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Imported scripts on this page -->
	<script src="assets/js/echarts/echarts.js "></script>
	<!-- <script src="assets/js/echarts/echarts.common.min.js "></script> -->
	<script src="<c:url value='assets/js/echarts/echarts_function.js'/>"></script>
	<!--echarts_demo-->
	<script src="assets/js/echarts/echarts_demo.js "></script>
	<script src="assets/js/custom/SpectrumFile.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
		// Sample Javascript code for this page
		jQuery(document).ready(function($) {
			// Delete Modal-1
			$('.gallery-env a[data-action="trash"]').on('click', function(ev) {
				ev.preventDefault();
				$("#gallery-image-delete-modal").modal('show');
			});

		});
	</script>

</body>

</html>