<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hello</title>
</head>
<body>
	<c:set var="hello" value="Hello World!"></c:set>
	<h2>${hello }</h2>
	<p>用户信息:${data.user }</p>
	<%
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", "用户名");
		map.put("pwd", "密码");
		request.setAttribute("map", map);
	%>
	Map数据:<br /> ${map.name } , ${map['pwd'] }
</body>
</html>
