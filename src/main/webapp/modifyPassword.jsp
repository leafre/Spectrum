<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>修改密码</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title" align="center">修改密码</h1>
						<p class="description" align="right">
							<c:if test="${empty modifyPassword_msg}">
								&nbsp; <font color="red">&emsp;&emsp;&emsp;请谨慎输入原密码，连续超过5次将会冻结账号</font>
							</c:if>
							<c:if test="${!empty modifyPassword_msg}">
								&nbsp; <font color="red">&emsp;&emsp;&emsp;${modifyPassword_msg}</font>
							</c:if>
						</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="container">
				<div class="col-sm-8">
					<div class="panel panel-default">
						<div class="panel-heading"></div>
						<div class="panel-body">
							<form method="post" action="<c:url value='modifyPassword.do' />" id="form-testerregister"
								class="validate form-horizontal" title="修改密码">
								<div class="form-group">
									<label class="col-sm-2 control-label">原密码</label>
									<div class="col-sm-6">
										<input type="password" class="form-control oldPassword"
											id="oldPassword" name="oldPassword" placeholder="您正在使用的密码"
											required>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">新密码</label>
									<div class="col-sm-6">
										<input type="password" class="form-control newPassword" id="newPassword"
											name="newPassword" placeholder="您想要修改的密码；8-16个字符" required>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">确认密码</label>
									<div class="col-sm-6">
										<input type="password" class="form-control confirmPassword" id="confirmPassword"
											name="confirmPassword" placeholder="请再次确认密码" required>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">验证码</label>
									<div class="col-sm-4">
										<input type="text" class="form-control varifyCode" id="varifyCode"
											name="varifyCode" placeholder="请再次确认密码" required>
									</div>
									<a href="javascript:changeVCode('vCode')"> <img id="vCode"
										src="verifycode.do" /><b class="float-right">看不清</b>
									</a>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group col-sm-6">
									<button type="submit" class="btn btn-info btn-single pull-right" id="send-register">修改</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />
	<!-- Imported scripts on this page -->
	<script src="assets/js/jquery-validate/jquery.validate.min.js"></script>
	<script src="assets/js/jquery-validate/localization/messages_zh.js"></script>
	<script src="assets/js/custom/changePassword.js"></script>
	<script src="assets/js/custom/verifycode.js"></script>
</body>
</html>