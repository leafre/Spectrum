<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>管理员列表</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">管理员列表</h1>
						<p class="description">描述</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="row">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">管理员列表</h3>
						<div class="panel-options">
							<a href="#" data-toggle="panel">
								<span class="collapse-icon">&ndash;</span> <span class="expand-icon">+</span>
							</a>
						</div>
					</div>
					<div class="panel-body">
						<table class="table table-bordered table-striped" id="admins_table">
							<thead>
								<tr>
									<th>用户名</th>
									<th>邮箱</th>
									<th>手机号</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody class="middle-align">
							<c:forEach var="admin" items="${querydata}">
								<tr>
									<td class="username">${admin.username}</td>
									<td class="email">${admin.email}</td>
									<td class="tel">${admin.tel}</td>
									<td>
										<a title="${admin.userID}" href="#" class="btn btn-secondary btn-sm btn-icon icon-left">
											 操作日志</a>
										<a title="${admin.userID}" href="#" class="btn btn-danger btn-sm btn-icon icon-left">
											取消权限 </a>
										<a title="${admin.userID}" href="#" class="btn btn-info btn-sm btn-icon icon-left">
											个人信息 </a>
									</td>
								</tr>
							</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
	<script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
	jQuery(document).ready(function($) {
		$("#admins_table").dataTable({
			aLengthMenu: [
				[5, 10, 25, 50, 100, -1],
				[5, 10, 25, 50, 100, "所有"]
			]
		}).yadcf([{
			column_number : 0,
			filter_type : 'text'
		}, {
			column_number : 1,
			filter_type : 'text'
		}, {
			column_number : 2,
			filter_type : 'text'
		},]);
	});
	</script>
</body>

</html>