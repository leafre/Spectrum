<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
	xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word"
	xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core"
	xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
	xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
	xmlns:wsp="http://schemas.microsoft.com/office/word/2003/wordml/sp2"
	w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no"
	xml:space="preserve"><w:ignoreElements
	w:val="http://schemas.microsoft.com/office/word/2003/wordml/sp2" /><o:DocumentProperties><o:Title>光谱名称</o:Title><o:Author>F.n.</o:Author><o:LastAuthor>ji</o:LastAuthor><o:Revision>2</o:Revision><o:TotalTime>0</o:TotalTime><o:Created>2016-09-19T11:17:00Z</o:Created><o:LastSaved>2016-09-19T11:17:00Z</o:LastSaved><o:Pages>1</o:Pages><o:Words>40</o:Words><o:Characters>231</o:Characters><o:Lines>1</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>270</o:CharactersWithSpaces><o:Version>11.0000</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts
	w:ascii="Times New Roman" w:fareast="宋体" w:h-ansi="Times New Roman"
	w:cs="Times New Roman" /><w:font w:name="宋体"><w:altName
	w:val="SimSun" /><w:panose-1 w:val="02010600030101010101" /><w:charset
	w:val="86" /><w:family w:val="Auto" /><w:pitch w:val="variable" /><w:sig
	w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000"
	w:csb-0="00040001" w:csb-1="00000000" /></w:font><w:font w:name="@宋体"><w:panose-1
	w:val="02010600030101010101" /><w:charset w:val="86" /><w:family
	w:val="Auto" /><w:pitch w:val="variable" /><w:sig
	w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000"
	w:csb-0="00040001" w:csb-1="00000000" /></w:font></w:fonts><w:styles><w:versionOfBuiltInStylenames
	w:val="4" /><w:latentStyles w:defLockedState="off"
	w:latentStyleCount="156" /><w:style w:type="paragraph"
	w:default="on" w:styleId="a"><w:name w:val="Normal" /><wx:uiName
	wx:val="正文" /><w:rsid w:val="00DC0963" /><w:pPr><w:widowControl
	w:val="off" /><w:jc w:val="both" /></w:pPr><w:rPr><wx:font
	wx:val="Times New Roman" /><w:kern w:val="2" /><w:sz w:val="21" /><w:sz-cs
	w:val="24" /><w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" /></w:rPr></w:style><w:style
	w:type="character" w:default="on" w:styleId="a0"><w:name
	w:val="Default Paragraph Font" /><wx:uiName wx:val="默认段落字体" /><w:semiHidden /></w:style><w:style
	w:type="table" w:default="on" w:styleId="a1"><w:name w:val="Normal Table" /><wx:uiName
	wx:val="普通表格" /><w:semiHidden /><w:rPr><wx:font wx:val="Times New Roman" /></w:rPr><w:tblPr><w:tblInd
	w:w="0" w:type="dxa" /><w:tblCellMar><w:top w:w="0" w:type="dxa" /><w:left
	w:w="108" w:type="dxa" /><w:bottom w:w="0" w:type="dxa" /><w:right
	w:w="108" w:type="dxa" /></w:tblCellMar></w:tblPr></w:style><w:style
	w:type="list" w:default="on" w:styleId="a2"><w:name w:val="No List" /><wx:uiName
	wx:val="无列表" /><w:semiHidden /></w:style><w:style w:type="table"
	w:styleId="a3"><w:name w:val="Table Grid" /><wx:uiName
	wx:val="网格型" /><w:basedOn w:val="a1" /><w:rsid w:val="00DC0963" /><w:pPr><w:widowControl
	w:val="off" /><w:jc w:val="both" /></w:pPr><w:rPr><wx:font
	wx:val="Times New Roman" /></w:rPr><w:tblPr><w:tblInd w:w="0"
	w:type="dxa" /><w:tblBorders><w:top w:val="single" w:sz="4"
	wx:bdrwidth="10" w:space="0" w:color="auto" /><w:left w:val="single"
	w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto" /><w:bottom
	w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto" /><w:right
	w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto" /><w:insideH
	w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto" /><w:insideV
	w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto" /></w:tblBorders><w:tblCellMar><w:top
	w:w="0" w:type="dxa" /><w:left w:w="108" w:type="dxa" /><w:bottom
	w:w="0" w:type="dxa" /><w:right w:w="108" w:type="dxa" /></w:tblCellMar></w:tblPr></w:style><w:style
	w:type="paragraph" w:styleId="a4"><w:name w:val="Document Map" /><wx:uiName
	wx:val="文档结构图" /><w:basedOn w:val="a" /><w:semiHidden /><w:rsid
	w:val="0050047D" /><w:pPr><w:pStyle w:val="a4" /><w:shd
	w:val="clear" w:color="auto" w:fill="000080" /></w:pPr><w:rPr><wx:font
	wx:val="Times New Roman" /></w:rPr></w:style></w:styles><w:docPr><w:view
	w:val="print" /><w:zoom w:percent="100" /><w:bordersDontSurroundHeader /><w:bordersDontSurroundFooter /><w:proofState
	w:spelling="clean" w:grammar="clean" /><w:attachedTemplate
	w:val="" /><w:defaultTabStop w:val="420" /><w:drawingGridVerticalSpacing
	w:val="156" /><w:displayHorizontalDrawingGridEvery
	w:val="0" /><w:displayVerticalDrawingGridEvery
	w:val="2" /><w:punctuationKerning /><w:characterSpacingControl
	w:val="CompressPunctuation" /><w:optimizeForBrowser /><w:validateAgainstSchema /><w:saveInvalidXML
	w:val="off" /><w:ignoreMixedContent w:val="off" /><w:alwaysShowPlaceholderText
	w:val="off" /><w:compat><w:spaceForUL /><w:balanceSingleByteDoubleByteWidth /><w:doNotLeaveBackslashAlone /><w:ulTrailSpace /><w:doNotExpandShiftReturn /><w:adjustLineHeightInTable /><w:breakWrappedTables /><w:snapToGridInCell /><w:wrapTextWithPunct /><w:useAsianBreakRules /><w:dontGrowAutofit /><w:useFELayout /></w:compat><wsp:rsids><wsp:rsidRoot
	wsp:val="00DC0963" /><wsp:rsid wsp:val="000D51A9" /><wsp:rsid
	wsp:val="0050047D" /><wsp:rsid wsp:val="00A95BA9" /><wsp:rsid
	wsp:val="00BF45A5" /><wsp:rsid wsp:val="00C7042E" /><wsp:rsid
	wsp:val="00DC0963" /><wsp:rsid wsp:val="00F03C46" /><wsp:rsid
	wsp:val="00F64D2C" /></wsp:rsids></w:docPr><w:body><wx:sect><w:p
	wsp:rsidR="00DC0963" wsp:rsidRDefault="00DC0963" wsp:rsidP="00DC0963"><w:pPr><w:jc
	w:val="center" /><w:rPr><w:rFonts w:hint="fareast" /><w:b /><w:sz
	w:val="32" /><w:sz-cs w:val="32" /></w:rPr></w:pPr><w:r><w:rPr><w:rFonts
	w:hint="fareast" /><wx:font wx:val="宋体" /><w:b /><w:sz
	w:val="32" /><w:sz-cs w:val="32" /></w:rPr><w:t>光谱名称</w:t></w:r></w:p><w:p
	wsp:rsidR="00DC0963" wsp:rsidRDefault="00DC0963" wsp:rsidP="00DC0963"><w:pPr><w:jc
	w:val="center" /><w:rPr><w:rFonts w:hint="fareast" /><w:b /><w:sz
	w:val="32" /><w:sz-cs w:val="32" /></w:rPr></w:pPr><w:r><w:rPr><w:rFonts
	w:hint="fareast" /><wx:font wx:val="宋体" /><w:b /><w:sz
	w:val="32" /><w:sz-cs w:val="32" /></w:rPr><w:t>分析报告</w:t></w:r></w:p><wx:sub-section><w:p
	wsp:rsidR="00DC0963" wsp:rsidRPr="00DC0963" wsp:rsidRDefault="00DC0963"
	wsp:rsidP="0050047D"><w:pPr><w:jc w:val="right" /><w:outlineLvl
	w:val="0" /><w:rPr><w:rFonts w:hint="fareast" /><w:b /><w:color
	w:val="0000FF" /><w:sz-cs w:val="21" /></w:rPr></w:pPr><w:r
	wsp:rsidRPr="00DC0963"><w:rPr><w:b /><w:color w:val="0000FF" /><w:sz-cs
	w:val="21" /></w:rPr><w:t>T</w:t></w:r><w:r wsp:rsidRPr="00DC0963"><w:rPr><w:rFonts
	w:hint="fareast" /><w:b /><w:color w:val="0000FF" /><w:sz-cs
	w:val="21" /></w:rPr><w:t>${time}</w:t></w:r></w:p><w:tbl><w:tblPr><w:tblStyle
	w:val="a3" /><w:tblW w:w="0" w:type="auto" /><w:tblLook
	w:val="01E0" /></w:tblPr><w:tblGrid><w:gridCol w:w="1704" /><w:gridCol
	w:w="1704" /><w:gridCol w:w="1704" /><w:gridCol
	w:w="1705" /><w:gridCol w:w="1705" /></w:tblGrid><w:tr
	wsp:rsidR="00DC0963" wsp:rsidTr="00DC0963"><w:tc><w:tcPr><w:tcW w:w="1704"
	w:type="dxa" /><w:vmerge w:val="restart" /><w:vAlign
	w:val="center" /></w:tcPr><w:p wsp:rsidR="00DC0963" wsp:rsidRPr="00DC0963"
	wsp:rsidRDefault="00DC0963" wsp:rsidP="00DC0963"><w:pPr><w:jc w:val="center" /><w:rPr><w:b /></w:rPr></w:pPr><w:r
	wsp:rsidRPr="00DC0963"><w:rPr><wx:font wx:val="宋体" /><w:b /></w:rPr><w:t>光谱信息</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1704" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00DC0963"
	wsp:rsidRDefault="00DC0963" wsp:rsidP="00BF45A5"><w:pPr><w:jc w:val="center" /><w:rPr><w:rFonts
	w:hint="fareast" /></w:rPr></w:pPr><w:r><w:rPr><wx:font wx:val="宋体" /></w:rPr><w:t>光谱编号</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1704" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00DC0963"
	wsp:rsidRDefault="0050047D" wsp:rsidP="00DC0963"><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>${spectrumID}</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1705" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00DC0963"
	wsp:rsidRDefault="00DC0963" wsp:rsidP="00BF45A5"><w:pPr><w:jc w:val="center" /><w:rPr><w:rFonts
	w:hint="fareast" /></w:rPr></w:pPr><w:r><w:rPr><wx:font wx:val="宋体" /></w:rPr><w:t>光谱类型</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1705" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00DC0963"
	wsp:rsidRDefault="0050047D" wsp:rsidP="00DC0963"><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>${spectrumType}</w:t></w:r></w:p></w:tc></w:tr><w:tr
	wsp:rsidR="00DC0963" wsp:rsidTr="00DC0963"><w:tc><w:tcPr><w:tcW w:w="1704"
	w:type="dxa" /><w:vmerge /></w:tcPr><w:p wsp:rsidR="00DC0963"
	wsp:rsidRDefault="00DC0963" wsp:rsidP="00DC0963" /></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1704" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00DC0963"
	wsp:rsidRDefault="00DC0963" wsp:rsidP="00BF45A5"><w:pPr><w:jc w:val="center" /><w:rPr><w:rFonts
	w:hint="fareast" /></w:rPr></w:pPr><w:r><w:rPr><w:rFonts
	w:hint="fareast" /><wx:font wx:val="宋体" /></w:rPr><w:t>检测硬件</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1704" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00DC0963"
	wsp:rsidRDefault="0050047D" wsp:rsidP="00DC0963"><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>${hardwareName}</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1705" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00DC0963"
	wsp:rsidRDefault="00DC0963" wsp:rsidP="00BF45A5"><w:pPr><w:jc w:val="center" /><w:rPr><w:rFonts
	w:hint="fareast" /></w:rPr></w:pPr><w:r><w:rPr><w:rFonts
	w:hint="fareast" /><wx:font wx:val="宋体" /></w:rPr><w:t>分辨率</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1705" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00DC0963"
	wsp:rsidRDefault="0050047D" wsp:rsidP="00DC0963"><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>${resolutionRate}</w:t></w:r></w:p></w:tc></w:tr><w:tr
	wsp:rsidR="00DC0963" wsp:rsidTr="00BF45A5"><w:tc><w:tcPr><w:tcW w:w="8522"
	w:type="dxa" /><w:gridSpan w:val="5" /></w:tcPr><w:p
	wsp:rsidR="00DC0963" wsp:rsidRPr="00DC0963" wsp:rsidRDefault="00DC0963"
	wsp:rsidP="00DC0963"><w:pPr><w:jc w:val="center" /><w:rPr><w:color
	w:val="333399" /><w:u w:val="single" /></w:rPr></w:pPr><w:r
	wsp:rsidRPr="00DC0963"><w:rPr><wx:font wx:val="宋体" /><w:color
	w:val="333399" /><w:u w:val="single" /></w:rPr><w:t>分析结果</w:t></w:r></w:p></w:tc></w:tr><w:tr
	wsp:rsidR="00BF45A5" wsp:rsidTr="00BF45A5"><w:tc><w:tcPr><w:tcW w:w="1704"
	w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00BF45A5" wsp:rsidRDefault="00BF45A5"
	wsp:rsidP="00BF45A5"><w:pPr><w:jc w:val="center" /><w:rPr><w:rFonts
	w:hint="fareast" /></w:rPr></w:pPr><w:r><w:rPr><wx:font wx:val="宋体" /></w:rPr><w:t>被检测物</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="6818" w:type="dxa" /><w:gridSpan w:val="4" /></w:tcPr><w:p
	wsp:rsidR="00BF45A5" wsp:rsidRDefault="0050047D" wsp:rsidP="00DC0963"><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>${detectedName}</w:t></w:r></w:p></w:tc></w:tr><w:tr
	wsp:rsidR="00A95BA9" wsp:rsidTr="00A95BA9"><w:tc><w:tcPr><w:tcW w:w="1704"
	w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00A95BA9" wsp:rsidRPr="00A95BA9"
	wsp:rsidRDefault="00A95BA9" wsp:rsidP="00BF45A5"><w:pPr><w:jc w:val="center" /><w:rPr><w:color
	w:val="003300" /><w:u w:val="single" /></w:rPr></w:pPr><w:r
	wsp:rsidRPr="00A95BA9"><w:rPr><wx:font wx:val="宋体" /><w:color
	w:val="003300" /><w:u w:val="single" /></w:rPr><w:t>检测内容</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1704" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00A95BA9"
	wsp:rsidRPr="00A95BA9" wsp:rsidRDefault="00A95BA9" wsp:rsidP="00C7042E"><w:pPr><w:jc
	w:val="center" /><w:rPr><w:rFonts w:hint="fareast" /><w:color
	w:val="003300" /><w:u w:val="single" /></w:rPr></w:pPr><w:r
	wsp:rsidRPr="00A95BA9"><w:rPr><wx:font wx:val="宋体" /><w:color
	w:val="003300" /><w:u w:val="single" /></w:rPr><w:t>检测值</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1704" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00A95BA9"
	wsp:rsidRPr="00A95BA9" wsp:rsidRDefault="00A95BA9" wsp:rsidP="00C7042E"><w:pPr><w:jc
	w:val="center" /><w:rPr><w:color w:val="003300" /><w:u
	w:val="single" /></w:rPr></w:pPr><w:r wsp:rsidRPr="00A95BA9"><w:rPr><wx:font
	wx:val="宋体" /><w:color w:val="003300" /><w:u w:val="single" /></w:rPr><w:t>执行标准</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1705" w:type="dxa" /><w:shd w:val="clear" w:color="auto"
	w:fill="auto" /></w:tcPr><w:p wsp:rsidR="00A95BA9" wsp:rsidRPr="00A95BA9"
	wsp:rsidRDefault="00A95BA9" wsp:rsidP="00F64D2C"><w:pPr><w:jc w:val="center" /><w:rPr><w:rFonts
	w:hint="fareast" /><w:color w:val="003300" /><w:u w:val="single" /></w:rPr></w:pPr><w:r
	wsp:rsidRPr="00A95BA9"><w:rPr><wx:font wx:val="宋体" /><w:color
	w:val="003300" /><w:u w:val="single" /></w:rPr><w:t>标准线</w:t></w:r><w:r
	wsp:rsidRPr="00A95BA9"><w:rPr><w:rFonts w:hint="fareast" /><wx:font
	wx:val="宋体" /><w:color w:val="003300" /><w:u w:val="single" /></w:rPr><w:t>（${unit}</w:t></w:r><w:r
	wsp:rsidRPr="00A95BA9"><w:rPr><w:rFonts w:hint="fareast" /><w:color
	w:val="003300" /><w:u w:val="single" /></w:rPr><w:t>)</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1705" w:type="dxa" /><w:shd w:val="clear" w:color="auto"
	w:fill="auto" /></w:tcPr><w:p wsp:rsidR="00A95BA9" wsp:rsidRPr="00A95BA9"
	wsp:rsidRDefault="00A95BA9" wsp:rsidP="00F64D2C"><w:pPr><w:jc w:val="center" /><w:rPr><w:rFonts
	w:hint="fareast" /><w:color w:val="003300" /><w:u w:val="single" /></w:rPr></w:pPr><w:r><w:rPr><w:rFonts
	w:hint="fareast" /><wx:font wx:val="宋体" /><w:color
	w:val="003300" /><w:u w:val="single" /></w:rPr><w:t>波动值</w:t></w:r></w:p></w:tc></w:tr><w:tr
	wsp:rsidR="00A95BA9" wsp:rsidTr="00A95BA9"><w:tc><w:tcPr><w:tcW w:w="1704"
	w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00A95BA9" wsp:rsidRDefault="0050047D"
	wsp:rsidP="000D51A9"><w:pPr><w:jc w:val="center" /><w:rPr><w:rFonts
	w:hint="fareast" /></w:rPr></w:pPr><w:r><w:rPr><wx:font wx:val="宋体" /></w:rPr><w:t>${contentName}</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1704" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00A95BA9"
	wsp:rsidRDefault="0050047D" wsp:rsidP="00C7042E"><w:pPr><w:jc w:val="center" /></w:pPr><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>${concentration}</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1704" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00A95BA9"
	wsp:rsidRDefault="0050047D" wsp:rsidP="00C7042E"><w:pPr><w:jc w:val="center" /></w:pPr><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>${standards}</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1705" w:type="dxa" /><w:shd w:val="clear" w:color="auto"
	w:fill="auto" /></w:tcPr><w:p wsp:rsidR="00A95BA9"
	wsp:rsidRDefault="0050047D" wsp:rsidP="00F64D2C"><w:pPr><w:jc w:val="center" /></w:pPr><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>${standardLine}</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1705" w:type="dxa" /><w:shd w:val="clear" w:color="auto"
	w:fill="auto" /></w:tcPr><w:p wsp:rsidR="00A95BA9"
	wsp:rsidRDefault="0050047D" wsp:rsidP="00F64D2C"><w:pPr><w:jc w:val="center" /></w:pPr><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>${value}</w:t></w:r></w:p></w:tc></w:tr><w:tr
	wsp:rsidR="00A95BA9" wsp:rsidTr="00A95BA9"><w:tc><w:tcPr><w:tcW w:w="1704"
	w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00A95BA9" wsp:rsidRDefault="0050047D"
	wsp:rsidP="000D51A9"><w:pPr><w:jc w:val="center" /><w:rPr><w:rFonts
	w:hint="fareast" /></w:rPr></w:pPr><w:r><w:rPr><wx:font wx:val="宋体" /></w:rPr><w:t>地对地导弹</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1704" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00A95BA9"
	wsp:rsidRDefault="0050047D" wsp:rsidP="000D51A9"><w:pPr><w:jc w:val="center" /><w:rPr><w:rFonts
	w:hint="fareast" /></w:rPr></w:pPr><w:r><w:rPr><wx:font wx:val="宋体" /></w:rPr><w:t>地对地导弹</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1704" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00A95BA9"
	wsp:rsidRDefault="0050047D" wsp:rsidP="00C7042E"><w:pPr><w:jc w:val="center" /></w:pPr><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>地对地导弹</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1705" w:type="dxa" /><w:shd w:val="clear" w:color="auto"
	w:fill="auto" /></w:tcPr><w:p wsp:rsidR="00A95BA9"
	wsp:rsidRDefault="0050047D" wsp:rsidP="00F64D2C"><w:pPr><w:jc w:val="center" /></w:pPr><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>地对地导弹</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1705" w:type="dxa" /><w:shd w:val="clear" w:color="auto"
	w:fill="auto" /></w:tcPr><w:p wsp:rsidR="00A95BA9"
	wsp:rsidRDefault="0050047D" wsp:rsidP="00F64D2C"><w:pPr><w:jc w:val="center" /></w:pPr><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>地对地导弹</w:t></w:r></w:p></w:tc></w:tr><w:tr
	wsp:rsidR="00A95BA9" wsp:rsidTr="00A95BA9"><w:tc><w:tcPr><w:tcW w:w="1704"
	w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00A95BA9" wsp:rsidRDefault="0050047D"
	wsp:rsidP="000D51A9"><w:pPr><w:jc w:val="center" /><w:rPr><w:rFonts
	w:hint="fareast" /></w:rPr></w:pPr><w:r><w:rPr><wx:font wx:val="宋体" /></w:rPr><w:t>地对地导弹</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1704" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00A95BA9"
	wsp:rsidRDefault="0050047D" wsp:rsidP="000D51A9"><w:pPr><w:jc w:val="center" /><w:rPr><w:rFonts
	w:hint="fareast" /></w:rPr></w:pPr><w:r><w:rPr><wx:font wx:val="宋体" /></w:rPr><w:t>地对地导弹</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1704" w:type="dxa" /></w:tcPr><w:p wsp:rsidR="00A95BA9"
	wsp:rsidRDefault="0050047D" wsp:rsidP="00C7042E"><w:pPr><w:jc w:val="center" /></w:pPr><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>地对地导弹</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1705" w:type="dxa" /><w:shd w:val="clear" w:color="auto"
	w:fill="auto" /></w:tcPr><w:p wsp:rsidR="00A95BA9"
	wsp:rsidRDefault="0050047D" wsp:rsidP="00F64D2C"><w:pPr><w:jc w:val="center" /></w:pPr><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>地对地导弹</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW
	w:w="1705" w:type="dxa" /><w:shd w:val="clear" w:color="auto"
	w:fill="auto" /></w:tcPr><w:p wsp:rsidR="00A95BA9"
	wsp:rsidRDefault="0050047D" wsp:rsidP="00F64D2C"><w:pPr><w:jc w:val="center" /></w:pPr><w:r><w:rPr><wx:font
	wx:val="宋体" /></w:rPr><w:t>地对地导弹</w:t></w:r></w:p></w:tc></w:tr><w:tr
	wsp:rsidR="000D51A9" wsp:rsidTr="000D51A9"><w:trPr><w:trHeight w:val="640" /></w:trPr><w:tc><w:tcPr><w:tcW
	w:w="8522" w:type="dxa" /><w:gridSpan w:val="5" /></w:tcPr><w:p
	wsp:rsidR="000D51A9" wsp:rsidRDefault="000D51A9" wsp:rsidP="00F64D2C"><w:pPr><w:jc
	w:val="center" /></w:pPr><w:r><w:rPr><wx:font wx:val="宋体" /></w:rPr><w:t>图像</w:t></w:r></w:p></w:tc></w:tr></w:tbl><w:p
	wsp:rsidR="00DC0963" wsp:rsidRDefault="00DC0963" wsp:rsidP="00DC0963" /><w:sectPr
	wsp:rsidR="00DC0963"><w:pgSz w:w="11906" w:h="16838" /><w:pgMar
	w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851"
	w:footer="992" w:gutter="0" /><w:cols w:space="425" /><w:docGrid
	w:type="lines" w:line-pitch="312" /></w:sectPr></wx:sub-section></wx:sect></w:body></w:wordDocument>