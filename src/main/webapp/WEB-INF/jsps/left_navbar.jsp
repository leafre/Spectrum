<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- Left links for user info navbar -->
<ul class="user-info-menu left-links list-inline list-unstyled">
	<li class="hidden-sm hidden-xs">
		<a href="javascript:;" id="setSidebar_menu" data-toggle="sidebar">
			<i class="fa-bars"></i>
		</a>
	</li>
	<!-- 通知 -->
	<li class="dropdown hover-line">
		<a href="javascript:;" data-toggle="dropdown">
			<i class="fa-bell-o"></i> <span class="badge badge-purple">1</span>
		</a>
		<ul class="dropdown-menu notifications">
			<li class="top">
				<p class="small">
					<a href="javascript:;" class="pull-right">Mark all Read</a>
					You have <strong>3</strong> new notifications.
				</p>
			</li>

			<li>
				<ul class="dropdown-menu-list list-unstyled ps-scrollbar">
					<li class="active notification-success">
						<a href="javascript:;">
							<i class="fa-user"></i>
							<span class="line"> <strong>New user registered</strong> </span>
							<span class="line small time"> 30 seconds ago </span>
						</a>
					</li>
				</ul>
			</li>
			<li class="external">
				<a href="javascript:;">
					<span>View all notifications</span> <i class="fa-link-ext"></i>
				</a>
			</li>
		</ul>
	</li>
</ul>