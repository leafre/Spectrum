<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="author" content="LongShu">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- For all browsers -->
<link rel="stylesheet" href="assets/css/reset.css?v=1">
<link rel="stylesheet" href="assets/css/style.css?v=1">
<link rel="stylesheet" href="assets/css/colors.css?v=1">
<link rel="stylesheet" media="print" href="assets/css/print.css?v=1">
<!-- For progressively larger displays -->
<link rel="stylesheet" media="only all and (min-width: 480px)"
	href="assets/css/styles/480.css?v=1">
<link rel="stylesheet" media="only all and (min-width: 768px)"
	href="assets/css/styles/768.css?v=1">
<link rel="stylesheet" media="only all and (min-width: 1200px)"
	href="assets/css/styles/1200.css?v=1">
<!-- Additional styles -->
<link rel="stylesheet" href="assets/css/styles/form.css?v=1">
<!-- Login pages styles -->
<link rel="stylesheet" media="screen" href="assets/css/login.css?v=1">
<!-- loader-style -->
<link rel="stylesheet" href="assets/css/loader-style.css">
<script type="text/javascript" src="assets/js/load.js"></script>
<script type="text/javascript" src="assets/js/loader-style.js"></script>
<!-- For Modern Browsers -->
<link rel="shortcut icon" href="assets/img/favicon.png">
<!-- For retina screens -->
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="assets/img/favicons/apple-touch-icon-retina.png">
<!-- For iPad 1-->
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="assets/img/favicons/apple-touch-icon-ipad.png">
<!-- For iPhone 3G, iPod Touch and Android -->
<link rel="apple-touch-icon-precomposed"
	href="assets/img/favicons/apple-touch-icon.png">
<!-- iOS web-app metas -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<!-- Startup image for web apps -->
<link rel="apple-touch-startup-image"
	href="assets/img/splash/ipad-landscape.png"
	media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
<link rel="apple-touch-startup-image" href="assets/img/splash/ipad-portrait.png"
	media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
<link rel="apple-touch-startup-image" href="assets/img/splash/iphone.png"
	media="screen and (max-device-width: 320px)">
<!-- Microsoft clear type rendering -->
<meta http-equiv="cleartype" content="on">
<!-- IE9 Pinned Sites: http://msdn.microsoft.com/en-us/library/gg131029.aspx -->
<meta name="application-name" content="Developr Admin Skin">
<meta name="msapplication-tooltip" content="Cross-platform admin template.">
<meta name="msapplication-starturl"
	content="http://www.display-inline.fr/demo/developr">

<!-- JavaScript at the bottom for fast page loading -->
<script src="assets/js/jquery-1.11.1.min.js"></script>
<script src="assets/js/plugins/modernizr.custom.js"></script>
<script src="assets/js/developr/setup.js"></script>
<!--<script src="assets/js/developr/developr.input.js"></script>-->
<script src="assets/js/developr/developr.message.js"></script>
<script src="assets/js/developr/developr.notify.js"></script>
<!--<script src="assets/js/developr/developr.tooltip.js"></script>-->
<script src="<c:url value='/assets/js/js.cookie-2.1.2.min.js'/>"></script>