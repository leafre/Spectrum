<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="settings-pane">
	<a href="javascript:;" data-toggle="settings-pane" data-animate="true"> &times; </a>
	<div class="settings-pane-inner">
		<div class="row">
			<div class="col-md-4">
				<div class="user-info">
					<div class="user-image">
						<c:if test="${null==session_user.photo}">
							<img width="100px" src="<c:url value='/assets/img/user.png'/>" class="img-responsive img-circle" />
						</c:if>
						<c:if test="${null!=session_user.photo}">
							<img width="100px" src="<c:url value='/${session_user.photo}'/>" class="img-responsive img-circle" />
						</c:if>
					</div>
					<div class="user-details">
						<h3>
							<a href="javascript:;">${session_user.username}</a>
							<!-- Available statuses: is-online, is-idle, is-busy and is-offline -->
							<span class="user-status is-online"></span>
						</h3>
						<p class="user-title">${session_user.role.roleName}</p>
						<div class="user-links">
							<a href="userProfile.jsp" class="btn btn-primary">查看个人信息</a>
							<a href="updateProfile.jsp" class="btn btn-success">修改个人信息</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8 link-blocks-env">
				<div class="links-block left-sep">
					<h4>
						<span>面板方向</span>
					</h4>
					<ul class="list-unstyled">
						<li>
							<a href="javascript:;">
								<i class="fa-angle-right"></i> 纵向
							</a>
						</li>
						<li>
							<a href="javascript:;">
								<i class="fa-angle-right"></i> 横向
							</a>
						</li>
					</ul>
				</div>
				<div class="links-block left-sep">
					<h4>
						<span>Help Desk</span>
					</h4>
					<ul class="list-unstyled">
						<li>
							<a href="javascript:;">
								<i class="fa-angle-right"></i> Support Center
							</a>
						</li>
						<li>
							<a href="javascript:;">
								<i class="fa-angle-right"></i> Submit a Ticket
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>