<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- Right links for user info navbar -->
<ul class="user-info-menu right-links list-inline list-unstyled">
	<!-- <li class="search-form always-visible">
			You can add "always-visible" to show make the search input visible
			<form method="get" action="javascript:;">
				<input type="text" name="s" class="form-control search-field"
					placeholder="输入搜索内容..." />
				<button type="submit" class="btn btn-link">
					<i class="linecons-search"></i>
				</button>
			</form>
		</li> -->
	<li class="dropdown user-profile" id="session_userID" value="${session_user.userID}">
		<a href="javascript:;" data-toggle="dropdown">
			<img src="<c:url value='/assets/img/user.png'/>" alt="user-image" class="img-circle img-inline userpic-32"
				width="28" />
			<span>
				${session_user.username}<i class="fa-angle-down"></i>
			</span>
		</a>
		<ul class="dropdown-menu user-profile-menu list-unstyled">
			<c:if test="${!empty session_user}">
			<li>
				<a href="userProfile.jsp"> <i class="fa-user"></i> 个人信息
				</a>
			</li>
			<li>
				<a href="favorite.jsp"> <i class="fa-folder-o"></i> 个人收藏
				</a>
			</li>
			</c:if>
			<li>
				<a href="#help"> <i class="fa-info"></i> 帮助
				</a>
			</li>
			<c:if test="${null!=session_user}">
			<li>
				<a href="<c:url value='logout.do'/>"> <i class="fa-hand-o-right"></i> 注销
				</a>
			</li>
			</c:if>
			<c:if test="${null==session_user}">
			<li class="last">
				<a href='login.jsp'> <i class="fa-hand-o-right"></i> 登录
				</a>
			</li>
			</c:if>
		</ul>
	</li>
</ul>