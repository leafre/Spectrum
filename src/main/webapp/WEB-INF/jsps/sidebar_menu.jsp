<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 用户冻结则转发到冻结页面 --%>
<c:if test="${null!=sessionScope.session_user} && ${'冻结'==sessionScope.session_user.state}">
	<jsp:forward page="/frozen.jsp"></jsp:forward>
</c:if>
<!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
<!-- Add "fixed" class to make the sidebar fixed always to the browser viewport. -->
<!-- Adding class "toggle-others" will keep only one menu item open at a time. -->
<!-- Adding class "collapsed" collapse sidebar root elements and show only icons. -->
<div class="sidebar-menu toggle-others fixed">
	<div class="sidebar-menu-inner">
		<header class="logo-env">
			<!-- logo -->
			<div class="logo">
				<a href="index.jsp" class="logo-expanded">
					<img width="80" src="<c:url value='/assets/img/logo@2x.png'/>" alt="logo-expanded" />
				</a>
				<a href="index.jsp" class="logo-collapsed">
					<img width="40" src="<c:url value='/assets/img/logo-collapsed@2x.png'/>" alt="logo-collapsed" />
				</a>
			</div>
			<!-- This will toggle the mobile menu and will be visible only on mobile devices -->
			<div class="mobile-menu-toggle visible-xs">
				<a href="javascript:;" data-toggle="user-info-menu">
					<i class="fa-bell-o"></i>
					<span class="badge badge-success">7</span>
				</a>
				<a href="javascript:;" data-toggle="mobile-menu">
					<i class="fa-bars"></i>
				</a>
			</div>
			<!-- This will open the popup with user profile settings, you can use for any purpose, just be creative -->
			<div class="settings-icon">
				<a href="javascript:;" data-toggle="settings-pane" data-animate="true">
					<i class="linecons-cog"></i>
				</a>
			</div>
		</header>
		<ul id="main-menu" class="main-menu">
			<!-- add class "multiple-expanded" to allow multiple submenus to open -->
			<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
			<li>
				<a href="javascript:;">
					<i class="linecons-user"></i>
					<span class="title">个人信息管理</span>
				</a>
				<ul>
					<c:if test="${session_user!=null}">
					<li>
						<a href="<c:url value='/userProfile.jsp'/>">
							<span class="title">查看个人信息</span>
						</a>
					</li>
					<li>
						<a href="<c:url value='/updateProfile.jsp'/>">
							<span class="title">完善个人信息</span>
						</a>
					</li>
					<li>
						<a href="<c:url value='/modifyPassword.jsp'/>">
							<span class="title">修改密码</span>
						</a>
					</li>
					</c:if>
					<c:if test="${session_user.roleID!=1}">
					<li>
						<a href="<c:url value='/applyRole.jsp'/>">
							<span class="title">角色申请</span>
						</a>
					</li>
					</c:if>
				</ul>
			</li>
			<c:if test="${session_user.roleID!=1}">
			<li>
				<a href="<c:url value='/myFavorite.do'/>">
					<i class="linecons-star"></i>
					<span class="title">收藏夹管理</span>
				</a>
			</li>
			</c:if>
			<c:if test="${session_user.roleID <= 2}">
			<li>
				<a href="javascript:;">
					<i class="fa-bookmark-o"></i>
					<span class="title">审核申请</span>
				</a>
				<ul>
					<li>
						<a href="<c:url value='/querySubmitSpectrum.do?state=等待'/>">
							<span class="title">提交至标准库申请审核</span>
						</a>
					</li>
					<c:if test="${session_user.roleID==2}">
					<li>
						<a href="<c:url value='/queryUserThawApplication.do?state=等待'/>">
							<span class="title">解冻申请审核</span>
						</a>
					</li>
					</c:if>
					<c:if test="${session_user.roleID==2}">
					<li>
						<a href="<c:url value='/queryUserRoleApplication.do?state=等待'/>">
							<span class="title">角色申请审核</span>
						</a>
					</li>
					</c:if>
					<c:if test="${session_user.roleID==2}">
					<li>
						<a href="<c:url value='/queryPluginList.do?state=等待'/>">
							<span class="title">提交算法申请审核</span>
						</a>
					</li>
					</c:if>
				</ul>
			</li>
			</c:if>
			<c:if test="${session_user.roleID==2||session_user.roleID==3}">
			<li>
				<a href="javascript:;">
					<i class="fa-plug"></i>
					<span class="title">插件管理</span>
				</a>
				<ul>
					<li>
						<c:if test="${session_user.roleID==2}">
						<li>
							<a href="<c:url value='/queryPluginList.do?state=等待'/>">
								<span class="title">插件列表</span>
							</a>
						</li>
						</c:if>
						<c:if test="${session_user.roleID==3}">
						<li>
							<a href="<c:url value='/queryMyPluginList.do?state=通过'/>">
								<span class="title">我的插件列表</span>
							</a>
						</li>
						</c:if>
					</li>
					<li>
						<a href="<c:url value='/uploadPlugin.jsp'/>">
							<span class="title">上传插件</span>
						</a>
					</li>
					<c:if test="${session_user.roleID==3}">
						<li>
							<a href="<c:url value='/addDetectedObject.jsp'/>">
								<span class="title">添加被检测物</span>
							</a>
						</li>
					</c:if>
					<c:if test="${session_user.roleID==3}">
						<li>
							<a href="<c:url value='/addDetectedContent.jsp'/>">
								<span class="title">添加检测内容</span>
							</a>
						</li>
					</c:if>
					<c:if test="${session_user.roleID==3}">
						<li>
							<a href="<c:url value='/addHardware.jsp'/>">
								<span class="title">添加检测设备</span>
							</a>
						</li>
					</c:if>
				</ul>
			</li>
			</c:if>
			<c:if test="${session_user.roleID!=1}">
			<li>
				<a href="javascript:;">
					<i class="fa-area-chart"></i>
					<span class="title">光谱管理</span>
				</a>
				<ul>
					<li>
						<a href="<c:url value='/uploadSpectrum.jsp'/>">
							<span class="title">上传光谱</span>
						</a>
					</li>
					<c:if test="${session_user.roleID==4}">
					<li>
						<a href="<c:url value='/spectrumOperateUser.jsp?none=none'/>">
							<span class="title">光谱操作</span>
						</a>
					</li>
					</c:if>
					<c:if test="${session_user.roleID==2 || session_user.roleID==3}">
					<li>
						<a href="<c:url value='/spectrumAnalyze.jsp?none=none'/>">
							<span class="title">光谱分析</span>
						</a>
					</li>
					</c:if>
					<li>
						<a href="javascript:;">
							<span class="title">标准库</span>
						</a>
					</li>
				</ul>
			</li>
			</c:if>
			<c:if test="${session_user.roleID==2}">
			<li>
				<a href="<c:url value='/queryHardwareList.do'/>">
					<i class="fa-header"></i>
					<span class="title">硬件管理</span>
				</a>
			</li>
			</c:if>
			<c:if test="${session_user.roleID <= 2}">
			<li>
				<a href="<c:url value='/queryLog.do'/>">
					<i class="fa-minus-square-o"></i>
					<span class="title">日志管理</span>
				</a>
			</li>
			</c:if>
			<c:if test="${session_user.roleID==1}">
			<li>
				<a href="<c:url value='/queryRecoverList.do'/>">
					<i class="linecons-desktop"></i>
					<span class="title">系统维护管理</span>
				</a>
			</li>
			</c:if>
			<c:if test="${session_user.roleID <= 2}">
			<li>
				<a href="javascript:;">
					<i class="fa-users"></i>
					<span class="title">用户管理</span>
				</a>
				<ul>
					<c:if test="${session_user.roleID==1}">
					<li>
						<a href="<c:url value='/admins.do?roleID=2&state=正常'/>">
							<span class="title">查看管理员</span>
						</a>
					</li>
					</c:if>
					<c:if test="${session_user.roleID==2}">
					<li>
						<a href="<c:url value='/users.do?roleID=4&state=正常'/>">
							<span class="title">查看用户</span>
						</a>
					</li>
					</c:if>
				</ul>
			</li>
			</c:if>
			<c:if test="${session_user.roleID==2}">
			<li>
				<a href="<c:url value='/detected.jsp'/>">
					<i class="linecons-truck"></i>
					<span class="title">物质类别管理</span>
				</a>
			</li>
			</c:if>
			<li>
				<a href="javascript:;">
					<i class="linecons-t-shirt"></i>
					<span class="title">主题设置</span>
					<span class="label label-success pull-right">10</span>
				</a>
				<ul>
					<li>
						<a href="javascript:;">
							<span class="title">暗主题</span>
							<span class="label label-purple pull-right">5</span>
						</a>
						<ul>
							<li data-skin="">
								<a href="#" class="skin-name-link">默认主题</a>
							</li>
							<li data-skin="navy">
								<a href="#" class="skin-name-link">藏蓝色</a>
							</li>
							<li data-skin="facebook">
								<a href="#" class="skin-name-link">蓝色</a>
							</li>
							<li data-skin="green">
								<a href="#" class="skin-name-link">绿色</a>
							</li>
							<li data-skin="purple">
								<a href="#" class="skin-name-link">紫色</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:;">
							<span class="title">亮主题</span>
							<span class="label label-blue pull-right">5</span>
						</a>
						<ul>
							<li data-skin="aero">
								<a href="#" class="skin-name-link">藏青色</a>
							</li>
							<li data-skin="turquoise">
								<a href="#" class="skin-name-link">松绿色</a>
							</li>
							<li data-skin="lime">
								<a href="#" class="skin-name-link">青橙绿色</a>
							</li>
							<li data-skin="watermelon">
								<a href="#" class="skin-name-link">西瓜红</a>
							</li>
							<li data-skin="lemonade">
								<a href="#" class="skin-name-link">柠檬黄</a>
							</li>
						</ul>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</div>