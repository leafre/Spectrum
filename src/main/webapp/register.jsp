<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
	<title>用户注册</title>
	<%-- 全站样式 --%>
	<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title" align="center">用户注册</h1>
						<p class="description" align="right">&nbsp;
							<font color="red">&emsp;&emsp;&emsp;以下所有信息都是必填的，实验员注册审核结果会以邮件形式立即通知您！</font>
						</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default panel-tabs">
						<!-- Add class "collapsed" to minimize the panel -->
						<div class="panel-heading">
							<h3 class="panel-title">用户注册</h3>
							<div class="panel-options">
								<ul class="nav nav-tabs" id="nav_tabs">
									<li class="normal">
										<a href="<c:url value='/register.jsp?roleID=4'/>">普通用户注册</a>
									</li>
									<li class="tester">
										<a href="<c:url value='/register.jsp?roleID=3'/>">实验员注册</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="panel-body">
							<div class="tab-content" id="tab_content">
								<div class="tab-pane normal active">
									<div class="panel panel-default">
										<div class="panel-body">
											<form id="form-normalregister" class="validate form-horizontal" title="实验员注册">
												<input type="hidden" id="roleID" name="roleID" value="4">
												<div class="form-group">
													<label class="col-sm-2 control-label" for="username">用户名</label>
													<div class="col-sm-7">
														<input type="text" class="form-control username" id="username" name="username" placeholder="4-12个字符；您可以用户名登录"
															required>
													</div>
												</div>
												<div class="form-group-separator"></div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="email">邮箱</label>
													<div class="col-sm-7">
														<input type="email" class="form-control" id="email" name="email" required placeholder="邮箱地址需符合邮箱格式；系统会发送通知到指定邮箱中"
														/>
													</div>
												</div>
												<div class="form-group-separator"></div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="field-1">手机号</label>
													<div class="col-sm-7">
														<input type="tel" class="form-control" id="tel" name="tel" placeholder="您的手机号" required>
													</div>
												</div>
												<!-- 实验员填写start -->
												<div class="tab-pane tester">
													<div class="form-group-separator"></div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="field-1">真实姓名</label>
														<div class="col-sm-7">
															<input type="text" class="form-control realName" name="realName" placeholder="您的真实姓名" id="realName" required>
														</div>
													</div>
													<div class="form-group-separator"></div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="IDNumber">身份证号</label>
														<div class="col-sm-7">
															<input type="text" class="form-control IDNumber" name="IDNumber" id="IDNumber" required placeholder="您的身份证号；需与真实姓名对应">
														</div>
													</div>
													<div class="form-group-separator"></div>
													<div class="form-group">
														<label class="col-sm-2 control-label">性别</label>
														<div class="col-sm-7">
															<p>
																<label class="radio-inline"> 
																<input type="radio" name="gender" checked value="保密"> 保密 
															</label>
																<label class="radio-inline"> <input type="radio" name="gender" value="男"> 男
															</label>
																<label class="radio-inline"> 
																<input type="radio" name="gender" value="女"> 女 
															</label>
															</p>
														</div>
													</div>
													<div class="form-group-separator"></div>
													<div class="form-group">
														<label class="col-sm-2 control-label">出生日期</label>
														<div class="col-sm-7">
															<input id="birthday" class="form-control datepicker" type="text" name="birthday" />
														</div>
													</div>
													<div class="form-group-separator"></div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="address">单位地址：</label>
														<div class="col-sm-8">
															<div class="col-sm-3">
																<strong>省：</strong>
																<select class="form-control {required:true}" id="loc_province_select"></select>
															</div>
															<div class="col-sm-3">
																<strong>市：</strong>
																<select class="form-control" id="loc_city_select"></select>
															</div>
															<div class="col-sm-3">
																<strong>区/县：</strong>
																<select class="form-control" id="loc_town_select"></select>
															</div>
															<div class="col-sm-10">
																<strong>街区：</strong>
																<input type="text" class="form-control" id="address" name="address" placeholder="单位所在街区">
															</div>
														</div>
													</div>
													<div class="form-group-separator"></div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="institute">工作单位</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="institute" name="institute" placeholder="您单位工作单位的名称 " required>
														</div>
													</div>
													<div class="form-group-separator"></div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="position">职位</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="posiiton" name="position" placeholder="您的职位" required>
														</div>
													</div>
													<div class="form-group-separator"></div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="detail">个人描述</label>
														<div class="col-sm-7">
															<textarea class="form-control autogrow detail" cols="5" name="detail" id="detail" placeholder="个人描述，让我们更了解你"></textarea>
														</div>
													</div>
												</div>
												<!-- finish -->
												<div class="form-group-separator"></div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="password">密码</label>
													<div class="col-sm-7">
														<input type="password" class="form-control password" id="password" name="password" placeholder="8-16个字符"
															required>
													</div>
												</div>
												<div class="form-group-separator"></div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="confirmPassword">确认密码</label>
													<div class="col-sm-7">
														<input type="password" class="form-control confirmPassword" name="confirmPassword" placeholder="请再次确认密码"
															id="confirmPassword" required>
													</div>
												</div>
												<div class="form-group-separator"></div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="verifycode-nomalregister">验证码</label>
													<div class="col-sm-7">
														<span class="icon-frame mid-margin-right"></span>
														<input type="text" name="verifyCode" id="verifycode-nomalregister" class="form-control input-unstyled"
															placeholder="验证码" autocomplete="off">
														<a href="javascript:changeVCode('vCode2')">
															<img id="vCode2" src="verifycode.do" /><b class="float-right">看不清</b>
														</a>
													</div>
												</div>
												<div class="form-group col-sm-6">
													<input type="submit" class="btn btn-info btn-single pull-right" value="注册" />
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<div class="modal fade" id="register_msg_modal" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">系统提示信息</h4>
				</div>
				<div class="modal-body">
					<h3 class="register_msg"></h3>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/custom/verifycode.js"></script>
	<script src="assets/js/jquery-validate/jquery.validate.min.js"></script>
	<script src="assets/js/jquery-validate/localization/messages_zh.js"></script>
	<script src="assets/js/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/select2/select2_locale_zh-CN.js"></script>
	<script src="assets/js/province/area.js"></script>
	<script src="assets/js/province/location.js"></script>
	<script src="assets/js/custom/register.js"></script>
	<!--Specific JS for this page-->
</body>

</html>