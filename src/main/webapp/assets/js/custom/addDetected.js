$(function() {
	//初始化一级被检测物分类
	initFristDetectedCategory();
	//初始化二级被检测物分类
//	initSecondDetectedCategory();
	//初始检测内容
	initDectectedContent();
});

function initFristDetectedCategory() {
	var categorys = $("#fristDetectedCategoryIDs");
	$.post("queryDetectedCategoryDetail.do", function(json) {
		var detectedCategorys = json.querydata;
		$.each(detectedCategorys, function(j, detectedCategory) {
			categorys.append($("<option value='" + detectedCategory.categoryID + "'>" +
					detectedCategory.categoryName + "</option>"));
		});

	}, 'json').error(function(xhr, errorText, errorType) {
		alert('服务器错误,初始化被检测物失败!');
	});
	categorys.select2({
		placeholder: '点击选择被检测物',
		allowClear: true
	}).on('select2-open', function() {
		// 添加快速检索框
		$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
	});
};

$("#fristDetectedCategoryIDs").change(
	function DetectedCategory() {
		var categorys = $("#secondDetectedCategoryIDs");
		categorys.empty();
		categorys.append($("<option value='0'>二级分类</option>"));
		var parentID=$("#fristDetectedCategoryIDs").val();
		if(parentID!=0){
			$.post("queryDetectedCategoryDetail.do?parentID="+parentID, function(json) {
				var detectedCategorys = json.querydata;
				$.each(detectedCategorys, function(j, detectedCategory) {
					categorys.append($("<option value='" + detectedCategory.categoryID + "'>" +
							detectedCategory.categoryName + "</option>"));
				});
		
			}, 'json').error(function(xhr, errorText, errorType) {
				alert('服务器错误,初始化被检测物失败!');
			});
			categorys.select2({
				placeholder: '点击选择被检测物',
				allowClear: true
			}).on('select2-open', function() {
				// 添加快速检索框
				$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
			});
		}else{
			$("#fristDetectedCategoryIDs").val('0');
		}
	}
);


//TODO 令检测内容与二级分类关联，当二级分类为0时,令option为“请先选择物质分类”;当二级分类不为0时,查找其分类下对应的检测内容
function initDectectedContent() {
	var dectectedContentID = $("#contentIDs");
	$.post("queryDetectedContentDetail.do", function(json) {
		var dectectedContents = json.querydata;
		$.each(dectectedContents, function(j, dectectedContent) {
			dectectedContentID.append($("<option value='" + dectectedContent.contentID +
				"'>" + dectectedContent.chineseName + "</option>"));
		});
	}, 'json').error(function(xhr, errorText, errorType) {
		alert('服务器错误,初始化检测内容失败!');
	});
	dectectedContentID.select2({
		placeholder: '点击选择检测的内容',
		allowClear: true
	}).on('select2-open', function() {
		$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
	});

};

$("#contentIDs").change(
	function DetectedStandard(){
//		alert($("#contentIDs").val());
		var dectectedContents = $("#contentIDs").val();
		var detectedStandardID = $("#detectedStandardIDs");
		detectedStandardID.html("");
		//TODO 查询数据库获取检测标准
		$.each(dectectedContents, function(j,dectectedContent) {
//			alert($("#contentIDs option[value='"+dectectedContent+"']").text());
			detectedStandardID.append("<tr><td>"
					+$("#contentIDs option[value='"+dectectedContent+"']").text()+
					"</td><td>GB/T 30942-2014</td><td>60.5</td><td>5</td><td>毫克</td></tr>"
				);
		});
		
	}
);

