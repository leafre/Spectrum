$(document).ready(function() {
	$.validator.addMethod("newPassword", function(value, element) {
		
		return this.optional(element) || (/^\w{8,16}$/.test(value));
	}, "密码格式不正确,8-16位!");
	
	$.validator.addMethod("confirmPassword", function(value, element) {
		var newPassword = document.getElementById("newPassword").value;
		return this.optional(element) || (newPassword == value);
	}, "密码不一致!");
	
	$.validator.addMethod("varifyCode", function(value, element) {
		return this.optional(element) || (value.length==4);
	}, "验证码必须是4位!");
});