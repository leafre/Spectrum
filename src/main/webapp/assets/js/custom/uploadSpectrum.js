$(function() {
	//初始化硬件类型
	initHardWare();
	//初始化光谱类型
	initSpectrumType();
	//初始化光谱文件类型
	initSpectrumFileType();
	//初始化被检测物
	initDetected();
	//初始检测内容
	initDectectedContent();
});

function initHardWare() {
	var hardwareID = $("#hardwareID");
	$.post("queryHardwareDetail.do", function(json) {
		var hardwares = json.querydata;
		$.each(hardwares, function(j, hardware) {
			hardwareID.append($("<option value='" + hardware.hardwareID + "'>" +
				hardware.hardwareName + "</option>"));
		});
		hardwareID.select2({
			placeholder: '点击选择硬件类型',
			allowClear: true
		}).on('select2-open', function() {
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
	}, 'json').error(function(xhr, errorText, errorType) {
		alert('服务器错误,初始化硬件类型失败');
	});
};

function initSpectrumType() {
	var spectrumTypeID = $("#spectrumTypeID");

	$.post("querySpectrumType.do", function(json) {
		var spectrumTypes = json.querydata;
		$.each(spectrumTypes, function(j, spectrumType) {
			spectrumTypeID.append($("<option value='" + spectrumType.spectrumTypeID +
				"'>" + spectrumType.spectrumType + "</option>"));
		});
		spectrumTypeID.select2({
			placeholder: '点击选择光谱类型',
			allowClear: true
		}).on('select2-open', function() {
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
	}, 'json').error(function(xhr, errorText, errorType) {
		alert('服务器错误,初始光谱类型失败!');
	});
};

function initSpectrumFileType() {
	var spectrumFileTypeID = $("#spectrumFileTypeID");
	$.post("querySpectrumFileType.do", function(json) {
		var spectrumFileTypes = json.querydata;
		$.each(spectrumFileTypes, function(j, spectrumFileType) {
			spectrumFileTypeID.append($("<option value='" + spectrumFileType.spectrumFileTypeID +
				"'>" + spectrumFileType.spectrumFileType + "</option>"));
		});
	}, 'json').error(function(xhr, errorText, errorType) {
		alert('服务器错误,初始化光谱文件类型失败!');
	});
	spectrumFileTypeID.select2({
		placeholder: '点击选择光谱文件类型',
		allowClear: true
	}).on('select2-open', function() {
		$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
	});
};

function initDetected() {
	var detectedID = $("#detectedID");
	$.post("queryDetectedObjectDetail.do", function(json) {
		var detecteds = json.querydata;
		$.each(detecteds, function(j, detected) {
			detectedID.append($("<option value='" + detected.detectedID + "'>" +
				detected.chineseName + "</option>"));
		});
	}, 'json').error(function(xhr, errorText, errorType) {
		alert('服务器错误,初始化被检测物失败!');
	});
	detectedID.select2({
		placeholder: '点击选择被检测物',
		allowClear: true
	}).on('select2-open', function() {
		$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
	});
};

function initDectectedContent() {
	var dectectedContentID = $("#contentsIDs")
	$.post("queryDetectedContentDetail.do", function(json) {
		var dectectedContents = json.querydata;
		$.each(dectectedContents, function(j, dectectedContent) {
			dectectedContentID.append($("<option value='" + dectectedContent.contentID +
				"'>" + dectectedContent.chineseName + "</option>"));
		});
	}, 'json').error(function(xhr, errorText, errorType) {
		alert('服务器错误,初始化检测内容失败!');
	});
	dectectedContentID.select2({
		placeholder: '点击选择检测的内容',
		allowClear: true
	}).on('select2-open', function() {
		$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
	});
};