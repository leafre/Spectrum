/*
 * How do I hook my login script to this?
 * --------------------------------------
 *
 * This script is meant to be non-obtrusive: if the user has disabled javascript or if an error occurs, the login form
 * works fine without ajax.
 *
 * The only part you need to edit is the login script between the EDIT SECTION tags, which does inputs validation
 * and send data to server. For instance, you may keep the validation and add an AJAX call to the server with the
 * credentials, then redirect to the dashboard or display an error depending on server return.
 *
 * Or if you don't trust AJAX calls, just remove the event.preventDefault() part and let the form be submitted.
 */
$(document).ready(function() {
	/*
	 * JS login effect This script will enable effects for the login page
	 */
	// Elements
	var doc = $('html').addClass('js-login'),
		container = $('#container'),
		formLogin = $('#form-login'),
		formBlock = $('#form-block'),
		// If layout is centered
		centered;

	/** ***** EDIT THIS SECTION ****** */
	/*
	 * AJAX login This function will handle the login process through AJAX
	 */
	formLogin.submit(function(event) {
		// Values
		var account = $.trim($('#account').val()),
			password = $.trim($('#password').val()),
			verifyCode = $.trim($('#verifyCode').val()),
			remind = $('#remind').val();
		var accountError = '请输入用户用户名/邮箱',
			passwordError = '请输入用户密码',
			verifyCodeError = '请输入验证码';
		// Check inputs
		if(account.length == 0) {
			formLogin.clearMessages();
			displayError(accountError);
			return false;
		}
		if(password.length == 0) {
			// Remove empty login message if displayed
			formLogin.clearMessages();
			// Display message
			displayError(passwordError);
			return false;
		}
		if(errorCount > 0 && verifyCode == '') {
			formLogin.clearMessages();
			displayError(verifyCodeError);
			return false;
		} else {
			// Remove previous messages
			formLogin.clearMessages();
			// Show progress
			displayLoading("正在验证用户信息 ...");
			// Stop normal behavior
			event.preventDefault();
			/*
			 * This is where you may do your AJAX call, for instance:
			 */
			$.ajax({
				url: "login.do",
				type: "POST",
				dataType: "json",
				data: {
					account: account,
					password: password,
					// errorCount: errorCount,
					verifyCode: verifyCode
				},
				success: function(result) {
					if(result.login_msg != null) {
						errorCount = result.errorCount;
						if(errorCount > 0) {
							showVCode('vCode');
						}
						formLogin.clearMessages();
						displayError(result.login_msg);
					} else {
						saveAccount(remind, account, password);
						formLogin.clearMessages();
						setTimeout("document.location.href = 'index.jsp';", 200);
						displayLoading("登录成功,正在验证个人状态是否正常...");
					}
				},
				error: function() {
					formLogin.clearMessages();
					displayError("服务器出错,登陆失败 稍后再试!");
				}
			});
		}
	});
	/** ***** END OF EDIT SECTION ****** */

	// Handle resizing (mostly for debugging)
	function handleLoginResize() {
		// Detect mode
		centered = (container.css('position') === 'absolute');
		// Set min-height for mobile layout
		if(!centered) {
			container.css('margin-top', '');
		} else {
			if(parseInt(container.css('margin-top'), 10) === 0) {
				centerForm(false);
			}
		}
	};

	// Register and first call
	$(window).bind('normalized-resize', handleLoginResize);
	handleLoginResize();
	/*
	 * Center function @param boolean animate whether or not to animate the
	 * position change @param string|element|array any jQuery selector, DOM
	 * element or set of DOM elements which should be ignored @return void
	 */
	function centerForm(animate, ignore) {
		// If layout is centered
		if(centered) {
			var siblings = formLogin.siblings(),
				finalSize = formLogin.outerHeight();
			// Ignored elements
			if(ignore) {
				siblings = siblings.not(ignore);
			}
			// Get other elements height
			siblings.each(function(i) {
				finalSize += $(this).outerHeight(true);
			});
			// Setup
			container[animate ? 'animate' : 'css']({
				marginTop: -Math.round(finalSize / 2) + 'px'
			});
		}
	};
	// Initial vertical adjust
	centerForm(false);

	/**
	 * Function to display error messages
	 * @param string message the error to display
	 */
	function displayError(message) {
		// Show message
		var message = formLogin.message(message, {
			append: false,
			arrow: 'bottom',
			classes: ['red-gradient'],
			animate: false // We'll do animation later, we need to know the message height first
		});
		// Vertical centering (where we need the message height)
		centerForm(true, 'fast');
		// Watch for closing and show with effect
		message.bind('endfade', function(event) {
			// This will be called once the message has faded away and is removed
			centerForm(true, message.get(0));
		}).hide().slideDown('fast');
	}

	/**
	 * Function to display loading messages
	 * @param string  message the message to display
	 */
	function displayLoading(message) {
		// Show message
		var message = formLogin.message('<strong>' + message + '</strong>', {
			append: false,
			arrow: 'bottom',
			classes: ['blue-gradient', 'align-center'],
			stripes: true,
			darkStripes: false,
			closable: false,
			animate: false // We'll do animation later, we need to know the message height first
		});
		// Vertical centering (where we need the message height)
		centerForm(true, 'fast');
		// Watch for closing and show with effect
		message.bind('endfade', function(event) {
			// This will be called once the message has faded away and is removed
			centerForm(true, message.get(0));
		}).hide().slideDown('fast');
	}
});

// What about a notification?
notify('&nbsp;&nbsp; <a href="login-full.jsp">完整版</a>',
	'&nbsp;&nbsp; <a href="login-full.jsp#form-password"><b style="color: green;">忘记密码?</b></a> <a href="register.jsp"><b>注册?</b></a>', {
		autoClose: false,
		delay: 1500,
		icon: 'assets/img/notify.png'
	});

function saveAccount(remind, account, password) {
	if(remind == '1') {
		Cookies.set('account', account, {
			expires: 7,
			path: ''
		});
		Cookies.set('password', password, {
			expires: 7,
			path: ''
		});
	} else {
		Cookies.remove('account', {
			path: ''
		});
		Cookies.remove('password', {
			path: ''
		});
	}
}