/**
 * @author LongShu
 */
// 设置皮肤
function setSkin(){
	// 自动从Cookie中获取皮肤
	if (undefined != Cookies.get("current-skin")) {
		var skin = Cookies.get("current-skin"), skin_name = skin ? (' skin-' + skin): '';
		var $body = $("body");
		var body_classes = $body.attr('class').replace(/skin-[a-z]+/i, '');
		$body.attr('class', body_classes).addClass(skin_name);
	}
	$("[data-skin]").each(function(i, el) {
		var $el = $(el), skin = $el.data('skin');
		$el.find('a').attr('data-set-skin', skin).attr('href','#setSkin:' + skin);
	});
	// 点击设置
	$('[data-set-skin]').on('click',function(ev) {
		ev.preventDefault();
		var skin = $(this).data('set-skin'), skin_name = skin ? (' skin-' + skin): '';
		var body_classes = public_vars.$body.attr('class').replace(/skin-[a-z]+/i, '');
		public_vars.$body.attr('class', body_classes).addClass(skin_name);
		Cookies.remove('current-skin', { path: '' }); // removed!
		Cookies.set('current-skin', skin, { expires: 7, path: '' });
	});
}
// 设置侧栏
function setSidebar_menu(){
	var current_sidebar = Cookies.get('current_sidebar');
	var $sidebar_menu = $('.sidebar-menu');
	if(undefined != current_sidebar){
		$sidebar_menu.attr('class', current_sidebar);
	}
	$('#setSidebar_menu').on('click',function(){
		current_sidebar = $sidebar_menu.attr('class');
		Cookies.remove('current_sidebar', { path: '' });
		Cookies.set('current_sidebar', current_sidebar, { expires: 1, path: '' });
	});
}

jQuery(document).ready(function($) {
	setSidebar_menu();
	setSkin();
});