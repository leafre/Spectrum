/**
 * SpectrumFile对象
 * @author LongShu
 */

function SpectrumFile(spectrumObj) {
	this.pointsXY = []; // 数据点 , [[1,2],[1,3]]
	this.peaksXY = []; // 峰

	if(spectrumObj != undefined) {
		this.spectrumID = spectrumObj.spectrumID;
		this.spectrumName = spectrumObj.spectrumName;
		this.spectrumType = spectrumObj.spectrumType;
		this.spectrumName = spectrumObj.spectrumName;
		this.xUnit = spectrumObj.xUnit;
		this.yUnit = spectrumObj.yUnit;
		this.extInfo = spectrumObj.extInfo;
		this.points = spectrumObj.points;
		this.peaks = spectrumObj.peaks;

		if(this.points != undefined) {
			this.setPointsXY(this.points);
		}
		if(this.peaks != undefined) {
			this.setPeaksXY(this.peaks);
		}
	}
}

SpectrumFile.prototype.setPointsXY = function(points) {
	if(points != undefined) {
		this.pointsXY = [];
		for(i in points) {
			var point = points[i];
			this.pointsXY.push([point.x, point.y]);
		}
	}
}

SpectrumFile.prototype.setPeaksXY = function(peaks) {
	if(peaks != undefined) {
		this.peaksXY = [];
		for(i in peaks) {
			var point = peaks[i];
			this.peaksXY.push([point.x, point.y]);
		}
	}
}