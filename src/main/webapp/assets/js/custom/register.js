/**
 * 注册
 */
var roleID = getUrlParam('roleID');
var $nav_tabs = $('#nav_tabs');
var $tab_content = $('#tab_content');
// 修改高亮和详细内容显示
if(null == roleID) {
	$nav_tabs.find('.normal').addClass('active');
	$tab_content.find('.tester').hide();
} else if('4' == roleID) {
	$nav_tabs.find('.normal').addClass('active');
	$tab_content.find('.tester').hide();
} else if('3' == roleID) {
	$nav_tabs.find('.tester').addClass('active');
	$tab_content.find('.tester').addClass('active');
	$("#roleID").val(roleID);
}
//
$("#loc_province_select").select2({
	placeholder: '请选择省份',
	allowClear: true
}).on('select2-open', function() {
	$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});
//
$("#loc_city_select").select2({
	placeholder: '请选择市',
	allowClear: true
}).on('select2-open', function() {
	$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});
//
$("#loc_town_select").select2({
	placeholder: '选择县区',
	allowClear: true
}).on('select2-open', function() {
	$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});

$.validator.addMethod("username", function(value, element) {
	return this.optional(element) || (/^[\u4e00-\u9fa5A-Za-z0-9_]{6,16}$/.test(
		value));
}, "用户名格式错误,字母、数字、下划线和中文组成，以中文或字母开头!");

$.validator.addMethod("tel", function(value, element) {
	return this.optional(element) || (/^(13[0-9]|15[0-9]|18[0-9]|17[0-9])\d{8}$/
		.test(value));
}, "手机号码格式错误!");

$.validator.addMethod("IDNumber", function(value, element) {
	return this.optional(element) || (
		/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{4}$/.test(
			value));
}, "身份证号码格式错误!");

$.validator.addMethod("realName", function(value, element) {
	return this.optional(element) || (/^[\u4e00-\u9fa5]{2,8}$/.test(value));
}, "真实姓名格式错误,2-8位!");

$.validator.addMethod("detail", function(value, element) {
	return this.optional(element) || (value.length < 400);
}, "详细信息太长!");

$.validator.addMethod("password", function(value, element) {
	return this.optional(element) || (/^\w{8,16}$/.test(value));
}, "密码格式不正确,8-16位!");

$.validator.addMethod("confirmPassword", function(value, element) {
	var password = document.getElementById("password").value;
	return this.optional(element) || (password === value);
}, "密码不一致不正确!");

$('#form-normalregister').submit(function(event) {
	// Stop normal behavior
	event.preventDefault();
	jQuery.ajax({
		url: "register.do",
		data: $('#form-normalregister').serialize(),
		type: "POST",
		beforeSend: function() { // 设置表单提交前方法
			$("submit").attr('disable', 'disable');
		},
		erro: function(request) {
			$("submit").removeAttr('disable')
			showRegisterMsg("表单提交出错，请稍候再试");
		},
		success: function(data) {
			var msg = data.register_msg;
			if(msg != null) {
				if(msg == "success") {
					showRegisterMsg("你的帐号已激活，点击确定，将跳转至登入页面，快去登入吧");
					setTimeout(dispatch_href, 1500);
				} else {
					showRegisterMsg(msg);
				}
			}
		}
	});
	return false; //不自动刷新表单
});

function dispatch_href() {
	window.location.href = "login.jsp";
}

function showRegisterMsg(msg) {
	$("#register_msg_modal .register_msg").text(msg);
	$("#register_msg_modal").modal('show');
}