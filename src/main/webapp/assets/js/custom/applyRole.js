$.validator.addMethod("tel", function(value, element) {
	return this.optional(element) || (/^(13[0-9]|15[0-9]|18[0-9]|17[0-9])\d{8}$/.test(value));
}, "手机号码格式错误!");

$.validator.addMethod("IDNumber", function(value, element) {
	return this.optional(element) || (
		/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|(X|x))$/.test(value));
}, "身份证号码格式错误!");

$.validator.addMethod("realName", function(value, element) {
	return this.optional(element) || (/^[\u4e00-\u9fa5]{2,8}$/.test(value));
}, "真实姓名格式错误,2-8位!");

$.validator.addMethod("detail", function(value, element) {
	return this.optional(element) || (value.length < 400);
}, "详细信息太长!");
	
