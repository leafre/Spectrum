/**
 * 光谱分析工具栏
 */
var $operate_tool = $('#operate_tool');

// 预处理
$operate_tool.find('.smoothSpectrum').on('click', smoothSpectrum);
$operate_tool.find('.calibrationSpectrum').on('click', calibrationSpectrum);
$operate_tool.find('.swapCoordinate').on('click', swapCoordinate);
$operate_tool.find('.frequencyStandardization').on('click', frequencyStandardization);
// 数据处理
$operate_tool.find('.substractSpectrum').on('click', substractSpectrum);
$operate_tool.find('.multiplySpectrum').on('click', multiplySpectrum);
$operate_tool.find('.arithmeticSpectrum').on('click', arithmeticSpectrum);
$operate_tool.find('.derivativeSpectrum').on('click', derivativeSpectrum);
// 分析
$operate_tool.find('.getPeaks').on('click', getPeaks);
$operate_tool.find('.measureAverageValue').on('click',measureAverageValue);
$operate_tool.find('.measureNoise').on('click',measureNoise);
$operate_tool.find('.analyzeSpectrum').on('click',analyzeSpectrum);

/*
 * 预处理算法
 */
/**
 * 平滑处理
 */
function smoothSpectrum() {
	spectrumChart.showLoading();
	$.ajax({
		url: 'smoothSpectrum.do',
		type: 'post',
		dataType: 'json',
		data: search + "",
		success: function(jsondata) {
			alertMsg(jsondata,'smoothSpectrum_msg');
			if(jsondata.SpectrumFile!=undefined){
				spectrumFile = new SpectrumFile(jsondata.SpectrumFile);
				echarts_setOption(spectrumChart, echarts_Option(spectrumFile));
				alert("平滑处理["+jsondata.SpectrumFile.spectrumName+"]完成");
			}
			spectrumChart.hideLoading();
		},
		error: function() {
			alert('服务器错误,平滑处理失败!');
		}
	});
}
/**
 * 基线校正
 */
function calibrationSpectrum() {
	spectrumChart.showLoading();
	$.ajax({
		url: 'calibrationSpectrum.do',
		type: 'post',
		dataType: 'json',
		data: search + "",
		success: function(jsondata) {
			alertMsg(jsondata,'calibrationSpectrum_msg');
			if(jsondata.SpectrumFile!=undefined){
				spectrumFile = new SpectrumFile(jsondata.SpectrumFile);
				echarts_setOption(spectrumChart, echarts_Option(spectrumFile));
				alert("基线校正["+jsondata.SpectrumFile.spectrumName+"]完成");
			}
			spectrumChart.hideLoading();
		},
		error: function() {
			alert('服务器错误,基线校正失败!');
		}
	});
}
/**
 * 谱图单位转换
 */
function swapCoordinate() {
	//单位转换选择页面
	jQuery('#swapCoordinate').modal('show', {backdrop: 'static'});
	
//	$.ajax({
//		url: 'swapCoordinate.do',
//		type: 'post',
//		dataType: 'json',
//		data: search+"",
//		success: function(jsondata) {
//			alertMsg(jsondata,'swapCoordinate_msg');
//		},
//		error: function() {
//			alert('服务器错误,单位转换失败!');
//		}
//	});
}
/**
 * 频率标准化
 */
function frequencyStandardization() {
	$.ajax({
		url: 'frequencyStandardization.do',
		type: 'post',
		dataType: 'json',
		data: search + "",
		success: function(jsondata) {
			alertMsg(jsondata,'frequencyStandardization_msg');
		},
		error: function() {
			alert('服务器错误,频率标准化失败!');
		}
	});
}

/*
 * 数据处理
 */
/**
 * 差谱
 */
function substractSpectrum() {
	$.ajax({
		url: 'substractSpectrum.do',
		type: 'post',
		dataType: 'json',
		data: search+"",
		success: function(jsondata) {
			alertMsg(jsondata,'substractSpectrum_msg');
		},
		error: function() {
			alert('服务器错误,差谱运算失败!');
		}
	});
}
/**
 * 乘谱
 */
function multiplySpectrum() {
	//乘谱选择页面
	jQuery('#multiplySpectrum').modal('show', {backdrop: 'static'});
	
//	$.ajax({
//		url: 'multiplySpectrum.do',
//		type: 'post',
//		dataType: 'json',
//		data: search+"",
//		success: function(jsondata) {
//			alertMsg(jsondata,'multiplySpectrum_msg');
//		},
//		error: function() {
//			alert('服务器错误,乘谱运算失败!');
//		}
//	});
}
/**
 * 谱图运算
 */
function arithmeticSpectrum() {
	$.ajax({
		url: 'arithmeticSpectrum.do',
		type: 'post',
		dataType: 'json',
		data: search+"",
		success: function(jsondata) {
			alertMsg(jsondata,'arithmeticSpectrum_msg');
		},
		error: function() {
			alert('服务器错误,谱图运算失败!');
		}
	});
}
/**
 * 导数谱图
 */
function derivativeSpectrum() {

	//导数谱图选择页面
	jQuery('#derivativeSpectrum').modal('show', {backdrop: 'static'});
	
//	$.ajax({
//		url: 'derivativeSpectrum.do',
//		type: 'post',
//		dataType: 'json',
//		data: search+"",
//		success: function(jsondata) {
//			alertMsg(jsondata,'derivativeSpectrum_msg');
//		},
//		error: function() {
//			alert('服务器错误,导数谱图失败!');
//		}
//	});
}

/*
 * 光谱分析
 */
/**
 * 标峰
 */
function getPeaks() {
	var $a = $(this);
	// 清除峰
	if(echarts_isMarkedPeaks(spectrumChart)) {
		echarts_clearPeaks(spectrumChart);
		$a.text('标峰');
		return true;
	}
	// 获取峰数据,并标识
	$.ajax({
		url: 'getPeaks.do',
		type: 'post',
		dataType: 'json',
		data: search + "&valueY=10000", // 测试
		success: function(jsondata) {
			alertMsg(jsondata,'getPeaks_msg');
			if(jsondata.peaks==undefined){
				alert('无峰数据!');
				return false;
			}
			echarts_markPeaks(spectrumChart, jsondata.peaks);
			spectrumFile.peaks = jsondata.peaks;
			spectrumFile.setPeaksXY(spectrumFile.peaks);
			$a.text('清除峰');
		},
		error: function() {
			alert('服务器错误,获取峰值失败!');
		}
	});
}

/**
 * 平均值测量
 */
function measureAverageValue() {
	jQuery('#measureAverageValue').modal('show', {backdrop: 'static'});
//	$.ajax({
//		url: 'measureAverageValue.do',
//		type: 'post',
//		dataType: 'json',
//		data: search+"",
//		success: function(jsondata) {
//			alertMsg(jsondata,'measureAverageValue_msg');
//		},
//		error: function() {
//			alert('服务器错误,获取峰值失败!');
//		}
//	});
}

/**
 * 噪声测量
 */
//噪声检测新建页面
function measureNoise(){
	jQuery('#measureNoise').modal('show', {backdrop: 'static'});
}
//function measureNoise() {
//	$.ajax({
//		url: 'measureNoise.do',
//		type: 'post',
//		dataType: 'json',
//		data: search+"",
//		success: function(jsondata) {
//			alertMsg(jsondata,'measureNoise_msg');
//		},
//		error: function() {
//			alert('服务器错误,获取峰值失败!');
//		}
//	});
//}

/**
 * 官能团解析
 */
/**
 * 定性分析
 */
//定性分析新建页面
function analyzeSpectrum(){
	jQuery('#standXing').modal('show', {backdrop: 'static'});
}
/**
 * 定量分析
 */
