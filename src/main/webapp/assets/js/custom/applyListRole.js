jQuery(document).ready(function($) {
		var app_state = getUrlParam('app_state');
		var $nav_tabs = $('#nav_tabs');
		// 修改高亮
		if(null == app_state) {
			$nav_tabs.find('.all').addClass('active');
		} else if('通过' == app_state) {
			$nav_tabs.find('.pass').addClass('active');
		} else if('等待' == app_state) {
			$nav_tabs.find('.wait').addClass('active');
		} else if('拒绝' == app_state) {
			$nav_tabs.find('.refuse').addClass('active');
		}

		$("table").dataTable({
			aLengthMenu: [
				[5, 10, 25, 50, 100, -1],
				[5, 10, 25, 50, 100, "所有"]
			]
		});
	});

	function showApplayUserDetail(photo,username,gender,birthday,address,realName,IDNumber,institute,position,tel,detail,applyfile) {
	 if(photo!=""){
		var url="<c:url value='"+photo+"'/>"
		$('#user_img').attr('src',url);
	}
		$('#user_name').empty();
	 if(username!=""){
	var str=username+"<span class='user-status is-online'></span>";
	$('#user_name').html(str);
	 }
	 $('#user_gender').text("");
	if(gender!=""){
	$('#user_gender').text(gender);
	}
	$('#user_birthday').empty();
	if(birthday!=""){

    var  date=new Date(Number(birthday));
    var month=date.getMonth()+1;
    var str=date.getFullYear()+"/"+month+"/"+date.getDate();
	$('#user_birthday').html("<p>"+str+"<p>");
	}
	$('#user_home').text("");
	if(address!=""){
		$('#user_home').text(address);
	}
	$('#real_name').text("");
	if(realName!=""){
		$('#real_name').text(realName);
	}
	if(IDNumber!=null){
		$('#id').text(IDNumber);
	}
	if(institute!=null){
		$('#institute').text(institute);
	}
	if(position!=null){
		$('#position').text(position)
	}
	if(tel!=null){
		$('#tel').text(tel);
	}
	if(detail!=null){
		$('#detail').text(detail);
	}
	var str="申请文件下载 &nbsp;&nbsp;&nbsp;";
	var val="downloadFile.do?type=application&fileName="+applyfile;
	var url="<c:url value='"+val+"'/>";
	str+="<a href="+url+" class='btn btn-info btn-sm btn-icon'> 点击下载</a>";
	console.log(str);
	console.log(url);
     $('#file').html(str);
	 $('#modal-applyUserDetial').modal('show');
	}

	function showRefuseDialog(applicationID,applyRoleID,userID,email,username) {
	
		$('#i_applicationID').val(applicationID);
		$('#i_userID').val(userID);
		$('#i_applyRoleID').val(applyRoleID);
		$('#i_email').val(email);
		$('#i_username').val(username);
		$('#modal-refuse').modal('show');
	}
	
	$('#refushForm').submit(function() {
		jQuery.ajax({
			url: "examineRoleApplication.do", // 提交的页面
			data: $('#refushForm').serialize(), // 从表单中获取数据
			type: "POST", // 设置请求类型为"POST"，默认为"GET"
			beforeSend: function() { // 设置表单提交前方法
				$("submit").attr('disable', 'disable');
			},
			error: function(request) { // 设置表单提交出错
				console.log("error");
				$("submit").removeAttr('disable')
				alert("表单提交出错，请稍候再试");
			},
			success: function(data) {
				console.log("success");
				var msg = data.examineRoleApplication_msg;
				if(msg == "操作成功") {
					alert("操作成功");
					dispatch_href();
				} else {
					alert(msg);
				}
			}
		});
		return false;
	});

	function passRequest(applicationID, userID, applyRoleID, email, session_username, username) {
		var op = confirm("是否通过该请求");
		if(op) {
			$.post('examineRoleApplication.do', {
				applicationID: applicationID,
				userID: userID,
				state: '通过',
				applyRoleID: applyRoleID,
				email: email,
				session_username: session_username,
				username: username
			}, function(json) {
				var msg = json.examineRoleApplication_msg;

				if(msg == "操作成功") {
					alert(msg);
					dispatch_href();
				} else {
					alert(msg);
				}
			}, 'json').error(function(xhr, errorText, errorType) {
				alert("服务器错误，操作无效");
			});
		}
	}

	function dispatch_href() {
		window.location.href = "queryUserRoleApplication.do?state=等待";
	}