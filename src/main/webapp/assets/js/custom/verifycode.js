/**
 * 验证码
 */

var errorCount = 0;// 请求失败次数

$('.vCode').hide();

function showVCode(id){
	$vCode = $('.vCode');
	$vCode.attr('src','verifycode.do');
	changeVCode(id);
	$vCode.show();
}

function changeVCode(id) {
	var $imgEle = $('#'+id);
	$imgEle.attr('src','verifycode.do?time=' + new Date().getTime());
};