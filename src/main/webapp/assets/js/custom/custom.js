/**
 * @author LongShu
 */

/**
 * 非异步ajax获取json数据
 */
function getAjaxJsonData(url, senddata) {
//	console.log(url + "," + senddata);
	var result = null;
	var s = $.ajax({
		url : url,
		type : 'post',
		async : false,
		data : senddata,
		dataType : 'json',
		success : function(jsondata) {
			result = jsondata;
		}
	});
	console.log(s.responseJSON);
	return result;
};

/**
 * 获取url参数值
 * @param name
 * @returns
 */
function getUrlParam(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if (r != null)
		return decodeURI(r[2]);
	return null;
};


function alertMsg(json,key){
	if(json[key]!=undefined){
		if(null==key){
			alert(json);
		}else{
			alert(json[key]);
		}
	}
}