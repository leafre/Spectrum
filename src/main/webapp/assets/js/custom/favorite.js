/**
 * favorite.jsp
 */
var roleID = $("#roleID").val();//角色ID隐藏域中
var favoriteIDAll = 0;
var spectrums = []; // 收藏夹的光谱
var spectrum = null; // 详细的光谱
var $spectrum = $('#spectrum');
window.favoriteID = -1;//存储用户删除的favoriteID
window.nowFavoriteID = -1;	//存储<tbody>标签正在显示的收藏夹编号
window.spectrumID = -1;

function showSpectrumF(FavoriteID) {
	var spectrumData = "<td class='col-name'> <a href='javascript:;' onclick=showSpectrumDetail(spectrumID); class='col-name'>spectrumName</td>";
	var delete_td = "<td class='col-options hidden-sm hidden-xs'><a href='#' data-action='trash_spectrum'> <i class='fa-trash' onclick=setSpectrumID(spectrumID)></i> </a></td> ";
	var checkBox = "<td class='col-cb' id='checkbox_replace'><div class='checkbox checkbox-replace'><input type='checkbox' class='cbr' value='spectrumID'/></div></td>";
	
	function appendSpectrum(sp){
		var $tr = $("<tr index='spectrumID'></tr>".replace("spectrumID",sp.spectrumID))
			.append(checkBox.replace("spectrumID",sp.spectrumID))
			.append(spectrumData.replace("spectrumID",sp.spectrumID).replace("spectrumName",sp.spectrumName))
			.append("<td class='col-subject'>" + sp.description + "</td>")
			.append("<td class='col-time'>" + new Date(sp.saveTime).format("yyyy/MM/dd hh:mm:ss") + "</td>")
			.append(delete_td.replace("spectrumID",sp.spectrumID));
		$spectrum.append($tr);
	}
	if(spectrums.length == 0 || favoriteIDAll !=FavoriteID) {
		$.post("queryFavoriteSpectrumDetail.do", {
			'favoriteID': FavoriteID
		}, function(data) {
			$spectrum.children().remove();
			spectrums = data.querydata;
			favoriteIDAll = FavoriteID;
			for(var i in spectrums) {
				appendSpectrum(spectrums[i]);
			}
			$("#spectrumCount").html(spectrums.length);
			cbr_replace();
			initClick();
		}, "json");
	} else {
		$spectrum.children().remove();
		for(var i in spectrums) {
			appendSpectrum(spectrums[i]);
		}
		$("#spectrumCount").html(spectrums.length);
		cbr_replace();
		initClick();
	}
	window.nowFavoriteID = FavoriteID;//获取正在查看的收藏夹ID
}


function showSpectrumDetail(spectrumID) {
	var isStandard = $("#isStandard");
	var noStandard = $("#noStandard");
	var SpectrumContent = "";
	// 设置光谱详细信息
	if(spectrum!=null && spectrumID==spectrum.spectrumID){
		setSpectrumDetail(spectrum);
	}else{
		for(var i in spectrums) {
			if(spectrums[i].spectrumID == spectrumID) {
				spectrum = spectrums[i];
				//获取检测内容
				$.post("querySpectrumContent.do", {
					'spectrumID':spectrumID
				}, function(data) {
					for(var i in data.querydata) {
						SpectrumContent = SpectrumContent+"  "+data.querydata[i].chineseName;
					}
					spectrum.SpectrumContent=SpectrumContent;
					setSpectrumDetail(spectrum);
				}, "json");
			}
		}
	}
	$('#modal_spectrum').modal('show', {
		backdrop: 'fade'
	});
	//设置光谱弹窗详细信息
	function setSpectrumDetail(spectrum){
		
		console.log("83---"+roleID);
		if(roleID=="4"){
			$('#spectrumHref').attr('href',"/Spectrum/spectrumOperateUser.jsp?spectrumID="+spectrum.spectrumID); 			
		}else if(roleID=="3"){
			$('#spectrumHref').attr('href',"/Spectrum/spectrumAnalyze.jsp?spectrumID="+spectrum.spectrumID); 
		}else if(roleID=="2") {
			$('#spectrumHref').attr('href',"/Spectrum/spectrumAnalyze.jsp?spectrumID="+spectrum.spectrumID); 
		}
		$('#spectrumName').val(spectrum.spectrumName);
		$('#spectrumType').val(spectrum.spectrumType);
		$('#saveTime').val(new Date(spectrum.saveTime).format("yyyy/MM/dd hh:mm:ss"));
		$('#description').val(spectrum.description);
		$('#hardware').val(spectrum.hardwareName);
		$('#spectrumContent').val(spectrum.SpectrumContent);
		$('#detectedObject').val(spectrum.chineseName);
			if(spectrum.isStandard != 0) {
			isStandard.prop('checked', true);
			$('#spectrumName').attr("disabled",true);
			$('#description').attr("disabled",true);
			$("#modifybutton").attr("disabled",true);
		} else {
			noStandard.prop('checked', true);
		}
	}
}
//修改光谱信息
function modifyFavoriteSpetrum(){
	//获取修改数据
	spectrumName = $("#spectrumName").val();
	description = $("#description").val();
	if(spectrum.isStandard == 1){
		//如果为标准库不允许修改
		alert("该光谱是标准库光谱，不能进行信息修改！");
		return false;
	}else{
		//只当修改了才发请求进行修改
		if(spectrum.spectrumName != spectrumName || spectrum.description != description){
			//发送请求
			$.ajax({
				url:"modifySpectrum.do",
				type:"post",
				dataType:"json",
				data:{
					spectrumID: spectrum.spectrumID,
					spectrumName: spectrumName,
					spectruDescription: description
				},
				error:function(){
					alert("系统或网络异常，请稍后重试!");
				},
				success:function(data){
					if(data.modifySpectrum_msg=="光谱信息修改成功！") {
						alert(data.modifySpectrum_msg);
					}else{
						alert(data.modifySpectrum_msg);
						return;
					}
				}
			});
			//等待请求和修改完成执行刷新
			setTimeout(function () { 
				refreshSpectrum();
		    },100);
		}
	}
}
//刷新
function refreshSpectrum(){
	spectrums = [];
	spectrum = null;
	showSpectrumF(favoriteIDAll);
}

function initClick() {
	// Delete 收藏夹
	$('.gallery-env a[data-action="trash_favorite"]').on('click', function(ev) {
		ev.preventDefault();
		$("#trash_favorite_modal").modal('show');
	});
	//Delete 收藏夹的光谱
	$('.gallery-env a[data-action="trash_spectrum"]').on('click', function(ev) {
		ev.preventDefault();
		$("#trash_spectrum_modal").modal('show');
	});
	var $chcks = $(".mail-table tbody input[type='checkbox']");
	// Row Highlighting
	$chcks.each(function(i, el) {
		var $tr = $(el).closest('tr');
		$(this).on('change', function(ev) {
			$tr[$(this).is(':checked') ? 'addClass' : 'removeClass']('highlighted');
		});
	});
};

function  useSpectrum(){
	//获取选中的数量
	var length = $(".col-cb input:checked").length;
	if(length>0){
		if(length>1){
			alert("一次只能使用一张光谱！");
			return;
		}else{
			$(".col-cb input:checked").each(function(){ 
				if(roleID=="4"){
					window.location="/Spectrum/spectrumOperateUser.jsp?spectrumID="+$(this).val();
				}else if(roleID=="3"){
					window.location="/Spectrum/spectrumAnalyze.jsp?spectrumID="+$(this).val();
				}else if(roleID="2"){
					window.location="/Spectrum/spectrumAnalyze.jsp?spectrumID="+$(this).val();
				}
			}); 
		}
	}else{
		alert("请选中你要使用的一张光谱！");
		return;
	}
}

//获取用户想要删除的收藏夹ID，并存入全局变量中
function setID(favoriteID){
	window.favoriteID = favoriteID;
}
//获取用户想要删除的光谱的光谱ID，是否是标准库光谱，并存入全局变量中
//用于删除单张光谱，非批量删除
function setSpectrumID(spectrumID){
	window.spectrumID = spectrumID;
}
function insertFavoriteLi(favoriteName,favoriteID){
	
	var template = "<li class='active' index='#favoriteID#'>\
						<table width='100%'>\
							<tr>\
								<td>\
									<a href='javascript:;' onclick='showSpectrumF(#favoriteID#);' title='#favoriteID#'>\
										<i class='fa-folder-o'></i>\
										<span>#favoriteName#</span>\
									</a>\
								</td>\
								<td align='right'>\
									<a href='javascript:;' data-action='trash_favorite' onclick='setID(#favoriteID#);'>\
										<i class='fa-trash'></i>\
									</a>\
								</td>\
							</tr>\
					</table>\
				</li>";
	template = template.replace("#favoriteName#",favoriteName)
	template = template.replace("#favoriteID#",favoriteID);
	template = template.replace("#favoriteID#",favoriteID);
	template = template.replace("#favoriteID#",favoriteID);
	template = template.replace("#favoriteID#",favoriteID);
	$("#favoriteList").append(template);
}

function createFavorite(){
	//发送请求
	$.ajax({
		url:"createFavorite.do",
		type:"post",
		dataType:"json",
		data:{
			favoriteName:$("#favoriteName").val()
		},
		error:function(){
			
			alert("系统或网络异常，请稍后重试  :)  :)");
		},
		success:function(msg){
			if(msg.createFavorite_msg=="新建成功") {
				insertFavoriteLi(msg.Favorite.favoriteName,msg.Favorite.favoriteID);
				initClick();
			}
			alert(msg.createFavorite_msg);
		}
	});
	$("#favoriteName").val("");	//防止自动赋值， autocomplete="off"没用
}
function deleteFavoriteLi(){
	
	$("li[index="+window.favoriteID+"]").remove();
}
//删除单张光谱时调用这个删除光谱<tr>
function deleteOneSpectrumTr(index){
	
	$("tr[index="+index+"]").remove();
}
//删除收藏夹后，调用这个函数删除其显示的光谱<tr>
function deleteSpectrumTr(){
	
	$("#spectrum>tr").remove();
}
function deleteFavorite(favorite){
	
	//获取所以删除光谱的favoriteID
	var ID = $(favorite).attr('title');
	$.ajax({
		
		url:"deleteFavorite.do",
		type:"post",
		dataType:"json",
		data:{
			
			favoriteID:window.favoriteID
		},
		error:function(){
			
			alert("系统或网络异常，请稍后重试  :)");
		},
		success:function(msg){
			
			if(msg.deleteFavorite_msg=="删除成功") {
				
				deleteFavoriteLi();
				if(window.favoriteID==window.nowFavoriteID)	//如果删除正在查看的光谱，则把<tbody>标签中的数据删除掉
					deleteSpectrumTr();
			}
			alert(msg.deleteFavorite_msg);
		}
	});
}
//当批量删除光谱时，判断是否有光谱被勾选中
function hasSpectrum(){
	
	var length = $(".col-cb input:checked").length;
	return length>0;
}
//点击删除图标（垃圾桶）删除单张光谱
function deleteSpectrum(){
	
	$.ajax({
		
		url:"deleteSpectrum.do",
		type:"post",
		data:{
			
			spectrumID:window.spectrumID,
		},
		error:function(){
			
			alert("系统或网络异常，请稍后重试  :)");
		},
		success:function(msg){
			
			if(msg.deleteSpectrum_msg=="删除成功") {
				
				deleteOneSpectrumTr(window.spectrumID);
			}
			alert(msg.deleteSpectrum_msg);
		}
	});
}
//未完成
//批量删除光谱
function barchDeleteSpectrumTr(){
	
	$("#spectrum input:checked").parents(".highlighted").remove();
}
//批量删除光谱
function batchDeleteSpectrum(){
	
	//构造数据
	var length = $("#spectrum input:checked").length;
	var spectrumID = "";
	for(var i=0;i<length;i++) {
		
		spectrumID+="spectrumID="+$("#spectrum input:checked:eq("+i+")").val()+"&";
	}
	spectrumID = spectrumID.substring(0,spectrumID.length-1);
	//发送请求
	$.ajax({
		
		url:"deleteSpectrum.do",
		type:"post",
		data: spectrumID,
		error:function(){
			
			alert("系统或网络异常，请稍后重试  :)");
		},
		success:function(msg){
			
			if(msg.deleteSpectrum_msg=="删除成功") {
				
				barchDeleteSpectrumTr();
			}
			alert(msg.deleteSpectrum_msg);
		}
	});
}
jQuery(document).ready(function($) {
	initClick();
	// Stars
	/* $(".mail-table .star").on('click', function(ev) {
		ev.preventDefault();
		if(!$(this).hasClass('starred')) {
			$(this).addClass('starred').find('i').attr('class', 'fa-star');
		} else {
			$(this).removeClass('starred').find('i').attr('class', 'fa-star-empty');
		}
	}); */

	// Edit Modal
	$('.gallery-env a[data-action="edit"]').on('click',function(ev) {
			ev.preventDefault();
			$("#gallery-image-modal").modal('show');
	});
	//Delete 收藏夹的光谱
	$('.gallery-env a[data-action="trash_favoriteSpectrum"]').on('click', function(ev) {
		
		if(hasSpectrum()){	//当选中了若干张光谱时，就弹这个
			
			ev.preventDefault();
			$("#trash_favoriteSpectrum_modal").modal('show');
		} else {	//当一张光谱都没选中，就弹这个，提示用户要选中若干张光谱
			
			ev.preventDefault();
			$("#trash_favoriteSpectrum_modal_prompt").modal('show');
		}
	});

	$("#select-all").on('change', function(ev) {
		var is_checked = $(this).is(':checked');
		$(".mail-table tbody input[type='checkbox']").prop('checked', is_checked).trigger('change');
	});
});


