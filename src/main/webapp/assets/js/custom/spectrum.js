/**
 * @author LongShu
 */
var template_option = "<option value='id'>name</option>";
// 所有的光谱类型和光谱文件类型
var spectrumTypes = null;
var spectrumFileTypes = null;

/**
 * 光谱类型
 * @param $select jQuery的select对象
 */
function initSpectrumTypes($select) {
	if (spectrumTypes != null) {
		initSelect2SpectrumTypes($select);
		return;
	}
	$.getJSON('querySpectrumType.do', function(jsondata) {
		spectrumTypes = jsondata.querydata;
		initSelect2SpectrumTypes($select);
	});
}
function initSelect2SpectrumTypes($select) {
	$select.select2({
		language : 'zh-CN',
		placeholder: '请选择设备支持的光谱类型',
		allowClear: true
	}).on('select2-open', function(){
		// Adding Custom Scrollbar
		$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
	});
	if (spectrumTypes == null) {
		return;
	}
	for ( var j in spectrumTypes) {
		$select.append($(template_option.replace('name',
				spectrumTypes[j].spectrumType).replace('id',
				spectrumTypes[j].spectrumTypeID)));
	}
}
// 光谱文件类型
function initSpectrumFileTypes($select) {
	if (spectrumFileTypes != null) {
		initSelect2SpectrumFileTypes($select);
		return;
	}
	$.getJSON('querySpectrumFileType.do', function(jsondata) {
		spectrumFileTypes = jsondata.querydata;
		initSelect2SpectrumFileTypes($select);
	});
}
function initSelect2SpectrumFileTypes($select) {
	$select.select2({
		language : 'zh-CN',
		placeholder : '请选择设备支持的光谱文件类型',
		allowClear: true
	}).on('select2-open', function(){
		// Adding Custom Scrollbar
		$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
	});
	if (spectrumFileTypes == null) {
		return;
	}
	for ( var j in spectrumFileTypes) {
		$select.append($(template_option.replace('name',
				spectrumFileTypes[j].spectrumFileType).replace('id',
				spectrumFileTypes[j].spectrumFileTypeID)));
	}
}
