$(function() {

	initSpectrumType($("#addSpectrumType"));
	initSpectrumFileType($("#addSpectrumFileType"))

	var $table = $("#hardware-table");
	
	$.post("queryHardwareDetail.do", function(json) {
		var hardwareList = json.querydata;
		$.each(hardwareList, function(j, hardware) {
			setHardwareList(hardware);// TODO 分页问题
		})
	});

	// TODO 默认选中第一行
	$("tbody tr", $table).eq(1).click(function() {
	});

	// 查看详情面板中的修改
	$("#btn-view-edit").click(function() {
		HardwareManage.editItemInit();
	});

	// 添加面板中的确定添加
	$("#btn-save-add").click(function() {
		HardwareManage.showMessageMadel("确定添加该硬件?");
	});

	// 修改面板中的保存修改
	$("#btn-save-edit").click(function() {
		HardwareManage.showMessageMadel("确定保存该修改?");
	});

	// 添加/修改面板中的取消
	$(".btn-cancel").click(function() {
		HardwareManage.showItemDetail();
	});

	// 列表添加按钮
	$("#btn-add").click(function() {
		$("#list-add").show().siblings(".info-block").hide();
	});

	// 列表删除按钮
	$("#btn-del").click(function() {
		var $checked = $(".custom-checkbox.cbr-checked");//选中的单选框
		var checkedLen = $checked.length;
		switch (checkedLen) {
		case 0:
			HardwareManage.showDelMadel("请先选中要操作的行!");
			break;
		case 1:
			var hardwareID = $checked.closest("tr").attr("index");
			$.post("queryHardwareDetail.do?hardwareID=" + hardwareID,function(json) {
						HardwareManage.showDelMadel("确定要删除&nbsp'&nbsp"+ json.querydata[0].hardwareName+ "&nbsp'&nbsp硬件吗?");
					});
			break;
		default:
			HardwareManage.showDelMadel("确定要删除选中的&nbsp" + $checked.length + "&nbsp项记录吗?");
			break;
		}
	});

	// 列表刷新按钮
	$("#btn-refresh").click(function() {
		window.location.reload();
	});

	$table.on("click","#check-all",function() {// 表格标题全选框
		var $checkbox = $(".custom-checkbox", $table);
		$(this).toggleClass("cbr-checked");
		$(this).hasClass("cbr-checked") ? $checkbox.addClass("cbr-checked") : $checkbox.removeClass("cbr-checked");
	}).on("click", ".custom-checkbox", function() {// 表格行单选框
		$(this).toggleClass("cbr-checked");
	}).on("click","tbody tr",function(event) {// 表格行点击事件

		// 设置行选中样式
		$(this).addClass("active").siblings().removeClass("active");

		// 获取该行对应的数据
		var hardwareID = $(this).attr("index");
		$.post("queryHardwareDetail.do?hardwareID=" + hardwareID,
				function(json) {
					var hardware = json.querydata[0];
					HardwareManage.currentItem = hardware;// 设定当前选中对象
					// alert(event.target.nodeName);
					if ($(event.target).is("#btn-edit")) {// 点击行修改按钮
						HardwareManage.editItemInit();
					} else if ($(event.target).is("#btn-del")) {// 点击行删除按钮
						HardwareManage.showDelMadel("您确定要删除此硬件设备(" + hardware.hardwareName + ")?<br />此操作不能恢复");
					} else {
						HardwareManage.showItemDetail();
						// if($(event.target).closest("td").children().is(".custom-checkbox")){//单选
						// $(event.target).closest("td").children().toggleClass("cbr-checked");
						// }
					}
				});
	});
	// TODO 排序 查询
});

function initSpectrumType($select) {
	$.post("querySpectrumType.do", function(json) {
		var SpectrumType = json.querydata;
		$.each(SpectrumType, function(j, s) {
			var option = $("<option>").val(s.spectrumTypeID).text(s.spectrumType);
			$select.append(option);
		});

	});
}
function initSpectrumFileType($select) {
	$.post("querySpectrumFileType.do", function(json) {
		var SpectrumFileType = json.querydata;
		$.each(SpectrumFileType, function(j, s) {
			var option = $("<option>").val(s.spectrumFileTypeID).text(s.spectrumFileType);
			$select.append(option);
		});

	});
}

function setHardwareList(hardware) {
	var checkbox = "<div class='cbr-replaced custom-checkbox'>"
			+ "<div class='cbr-input'>"
			+ "<input type='checkbox' class='cbr cbr-done'>" + "</div>"
			+ "<div class='cbr-state'>" + "<span></span>" + "</div>" + "</div>";
	var editButton = "<button type='button' class='btn btn-info' id='btn-edit'>修改 </button>";
	var delButton = "<button type='button' class='btn btn-danger' id='btn-del'>删除</button>";
	$("tbody", "#hardware-table").append(
			"<tr index='" + hardware.hardwareID + "'>"// tr标签中写入id值
					+ "<td>" + checkbox + "</td>" + "<td>"
					+ hardware.hardwareID + "</td>" + "<td>"
					+ hardware.hardwareName + "</td>" + "<td>"
					+ new Date(hardware.addTime).format("yyyy/MM/dd hh:mm:ss")
					+ "</td>" + "<td>" + editButton + delButton + "</td>"
					+ "</tr>");
}

var HardwareManage = {
	currentItem : null,
	showItemDetail : function() {
		$("#list-view").show().siblings(".info-block").hide();
		if (!this.currentItem) {
			$("#list-view .prop-value").html("");
			return;
		}
		$("#hardwareID-view").html(this.currentItem.hardwareID);
		$("#hardwareName-view").html(this.currentItem.hardwareName);
		$("#addTime-view").html(
				new Date(this.currentItem.addTime)
						.format("yyyy/MM/dd hh:mm:ss"));
		$("#spectrumType-view").html(this.currentItem.spectrumType);
		$("#spectrumFileType-view").html(this.currentItem.spectrumFileType);
	},
	editItemInit : function() {
		if (!this.currentItem) {
			return;
		}
		$("#hardwareID-edit").val(this.currentItem.hardwareID);
		$("#hardwareName-edit").val(this.currentItem.hardwareName);
		//TODO 写入选中option
		initSpectrumFileType($("#SpectrumFileType-edit"));
//		$("#SpectrumFileType-edit").val(this.currentItem.spectrumFileTypeID);
		initSpectrumType($("#SpectrumType-edit"));
//		$("#SpectrumType-edit").val(this.currentItem.spectrumTypeID);
		
		$("#list-edit").show().siblings(".info-block").hide();
	},
	showMessageMadel : function(message) {
		var title = "提示";
		var confirm_btn = "<button type='button' class='btn btn-info'>确定</button>";
		var cancel_btn = "<button type='button' class='btn btn-white' data-dismiss='modal'>取消</button>";
		$(".modal-title", "#message-modal").html(title);
		$(".modal-body", "#message-modal").html(message);
		$(".modal-footer", "#message-modal").html(confirm_btn + cancel_btn);
		$("#message-modal").modal('show');
	},
	showDelMadel : function(message) {
		var title = "警告";
		var del_btn = "<button type='button' class='btn btn-danger'>删除</button>";
		var cancel_btn = "<button type='button' class='btn btn-white' data-dismiss='modal'>取消</button>";
		$(".modal-title", "#message-modal").html(title);
		$(".modal-body", "#message-modal").html(message);
		$(".modal-footer", "#message-modal").html(del_btn + cancel_btn);
		$("#message-modal").modal('show');
	}
}

//$("#hardware-table").dataTable({
//	aLengthMenu : [ [ 10, 25, 50, 100, -1 ], [ 10, 25, 50, 100, "所有" ] ]
//}).yadcf([ {
//	column_number : 1,
//	filter_type : 'text'
//}, {
//	column_number : 2,
//	filter_type : 'text'
//}, {
//	column_number : 3,
//	filter_type : 'text'
//}  ]);
