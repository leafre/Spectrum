/**
 * 光谱基本操作
 */

var currentSpectrumFile = {};
var search = window.location.search.substr(1); // 地址栏的查询参数

/**
 * 修改线颜色
 */
$('.colorpicker').colorpicker().on('changeColor', function(ev){
	if(spectrumChart!=undefined && spectrumChart!=null){
		var color = ev.color.toHex();
		echarts_changeColor(spectrumChart,color);
	}
});

//初始化echarts实例 
var spectrumChart = echarts_init('echarts');
var spectrumFile = null;

function showSpectrumFile(param){
	//从后台获取指定的光谱
	spectrumChart.showLoading();
	loadSpectrumFile(param,function(jsondata) {
		spectrumChart.hideLoading();
		if(null!=jsondata.loadSpectrumFile_msg){
			alert(jsondata.loadSpectrumFile_msg);
			return;
		}
		spectrumFile = new SpectrumFile(jsondata.SpectrumFile[0]);
		echarts_setOption(spectrumChart, echarts_Option(spectrumFile));
		currentSpectrumFile = spectrumFile;
	});
}

/**
 * 加载光谱
 * @param data 发送的数据
 * @param callback 成功时的回调
 */
function loadSpectrumFile(data,callback){
	$.ajax({
		url: 'loadSpectrumFile.do',
		type: 'post',
		dataType: 'json',
		data: data,
		success: callback,
		error: function() {
			alert('服务器错误,加载光谱失败!');
		}
	});
}

/**
 * 显示收藏夹的光谱
 */
var myFavorites = [];
var $favorite_select = $("#favorite_tab #favorite_select");

function showAddSpectrum(){
	loadMyFavorite($('#session_userID').val());
	jQuery('#addSpectrum_modal').modal('show', {backdrop: 'static'});
};

function loadMyFavorite(userID){
	$.ajax({
		url: 'queryUserFavorite.do',
		type: 'post',
		data: {userID: userID},
		dataType: 'json',
		success: function(jsondata) {
			myFavorites = jsondata.querydata;
			$favorite_select.empty();
			for(var i in myFavorites){
				var op  =$(template_option.replace('name', myFavorites[i].favoriteName)
				.replace('id',myFavorites[i].favoriteID))
				$favorite_select.append(op);
			}
		},error: function(){
			alert('加载个人收藏夹数据失败!');
		}
	});
};

var $spectrums_table = null;

function showFavoriteSpectrum(favoriteID){
	if(favoriteID=='undefined'){
		return;
	}
	var spectrums = [];
	$.ajax({
		url: 'queryFavoriteSpectrumDetail.do',
		type: 'post',
		data: {favoriteID: favoriteID},
		dataType: 'json',
		success: function(jsondata) {
			spectrums = jsondata.querydata;
			if(spectrums==null){
				return;
			}
			
			if($spectrums_table!=null){
				$spectrums_table.destroy();
			}
			$spectrums_table = $('.spectrums_table').DataTable({
		        "data": spectrums,
		        "dataSrc": "",
		        "columns": [
					{ data: "spectrumID"},
		            { data: "spectrumName"},
		            { data: "spectrumType"},
		            { data: "description"},
		            { data: "saveTime"}
		        ]
		    });
			bindClick();
		},error: function(){
			alert('加载收藏夹光谱失败!');
		}
	});
}
function bindClick(){
	//给行绑定选中事件
     $('.spectrums_table tbody').on('click', 'tr',function () {
    	 var spectrumID = $(this).find('td').eq(0).text();
    	 search = 'spectrumID='+spectrumID;
    	 if(currentSpectrumFile.spectrumID!=spectrumID){
        	 showSpectrumFile({spectrumID: spectrumID});
    	 }
         if ($(this).hasClass('highlighted')) {
            $(this).removeClass('highlighted');
        }else {
        	$spectrums_table.$('tr.highlighted').removeClass('highlighted');
            $(this).addClass('highlighted');
        }
        $('#addSpectrum_modal').modal('hide');
    });
}
//请选择收藏夹
$favorite_select.select2({
	language : 'zh-CN',
	placeholder: '请选择一个收藏夹',
	allowClear: true
}).on('select2-open', function(){
	$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
}).on('select2-close',function(){
	showFavoriteSpectrum($(this).val());
});

/* var $chcks = $(".spectrums_table tbody input[type='checkbox']");
// Row Highlighting
$chcks.each(function(i, el) {
	var $tr = $(el).closest('tr');
	$(this).on('change', function(ev) {
		$tr[$(this).is(':checked') ? 'addClass' : 'removeClass']('highlighted');
	});
}); */