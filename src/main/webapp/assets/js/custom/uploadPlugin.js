$(function() {
	//初始化硬件类型
	initHardWare();
	//初始化光谱类型
	initSpectrumType();
	//初始化被检测物
	initDetected();
	//初始检测内容
	initDectectedContent();
	//初始化算法上关联
	initPluginMain();
	//初始化算法下关联
	initPluginSub();
});

function initHardWare() {
	var hardwareID = $("#hardwareIDs");
	$.post("queryHardwareDetail.do", function(json) {
		var hardwares = json.querydata;
		$.each(hardwares, function(j, hardware) {
			hardwareID.append($("<option value='" + hardware.hardwareID + "'>" +
				hardware.hardwareName + "</option>"));
		});
	}, 'json').error(function(xhr, errorText, errorType) {
		alert('服务器错误,初始化硬件类型失败');
	});

};

function initSpectrumType() {
	var spectrumTypeID = $("#spectrumTypeIDs");
	$.post("querySpectrumType.do", function(json) {
		var spectrumTypes = json.querydata;
		$.each(spectrumTypes, function(j, spectrumType) {
			spectrumTypeID.append($("<option value='" + spectrumType.spectrumTypeID +
				"'>" + spectrumType.spectrumType + "</option>"));
		});
	}, 'json').error(function(xhr, errorText, errorType) {
		alert('服务器错误,初始光谱类型失败!');
	});

};

function initDetected() {
	var detectedID = $("#detectedObjectIDs");
	$.post("queryDetectedObjectDetail.do", function(json) {
		var detecteds = json.querydata;
		$.each(detecteds, function(j, detected) {
			detectedID.append($("<option value='" + detected.detectedID + "'>" +
				detected.chineseName + "</option>"));
		});

	}, 'json').error(function(xhr, errorText, errorType) {
		alert('服务器错误,初始化被检测物失败!');
	});
	detectedID.select2({
		placeholder: '点击选择被检测物',
		allowClear: true
	}).on('select2-open', function() {
		// 添加快速检索框
		$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
	});
};

function initDectectedContent() {
	var dectectedContentID = $("#contentIDs")
	$.post("queryDetectedContentDetail.do", function(json) {
		var dectectedContents = json.querydata;
		$.each(dectectedContents, function(j, dectectedContent) {
			dectectedContentID.append($("<option value='" + dectectedContent.contentID +
				"'>" + dectectedContent.chineseName + "</option>"));
		});
	}, 'json').error(function(xhr, errorText, errorType) {
		alert('服务器错误,初始化检测内容失败!');
	});
	dectectedContentID.select2({
		placeholder: '点击选择检测的内容',
		allowClear: true
	}).on('select2-open', function() {
		$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
	});

};

function initPluginMain() {
	var pluginMainID = $('#pluginMainIDs');
	$.post("queryPluginAjax.do", function(json) {
		var plugins = json.querydata;
		$.each(plugins, function(j, plugin) {
			pluginMainID.append($("<option value='" + plugin.pluginID + "'>" +
				plugin.pluginName + "</option>"));
		});
	}, 'json').error(function(xhr, errorText, errorType) {
		alert('服务器错误,初始化算法上关联失败');
	});
};

function initPluginSub() {
	var pluginSubID = $('#pluginSubIDs');
	$.post("queryPluginAjax.do", function(json) {
		var plugins = json.querydata;
		$.each(plugins, function(j, plugin) {
			pluginSubID.append($("<option value='" + plugin.pluginID + "'>" + plugin
				.pluginName + "</option>"));
		});
	}, 'json').error(function(xhr, errorText, errorType) {
		alert('服务器错误,初始化算法上关联失败');
	});
};