/**
 * echarts的操作功能
 * @author LongShu
 */

/*
 * 自动改变Chart的大小
 */
$('#setSidebar_menu').on('click', function() {
	echarts_resize('echarts');
});

/**
 * 改变Chart的大小
 * @param {Object}  id echarts的id
 */
function echarts_resize(id) {
	setTimeout(function() {
		echarts_get(id).resize();
	}, 50);
}

/**
 * echarts.init
 * @param {String}  id 标签的id
 */
function echarts_init(id) {
	return echarts.init(document.getElementById(id));
}

/**
 * 获取echarts实例
 * @param {String} id
 */
function echarts_get(id) {
	return echarts.getInstanceByDom(document.getElementById(id));
}

/**
 * echarts.setOption
 * @param {Object}  chart
 * @param {Object} option
 */
function echarts_setOption(chart, option) {
	chart.clear();
	chart.setOption(option);
	chart.resize();
	window.onresize = chart.resize;
}

/**
 * 将光谱文件的通用数据设置到option
 * @param {Object} fileData
 * @param {Object} option
 */
function echarts_Option(spectrumFile, option) {
	if(undefined == option) {
		option = {};
	}

	option.animation = true;
	option.backgroundColor = '#EEE'; // 背景
	// 标题
	option.title = {
		left: 'center',
		text: spectrumFile.spectrumName
	};
	// 图例组件
	option.legend = {
		left: '15%',
		data: [spectrumFile.spectrumName]
	};
	// 网格
	option.grid = {
		left: '5%',
		right: '8%',
		top: 50,
		bottom: 55
	};

	// 提示框组件
	option.tooltip = {
		trigger: 'axis',
		axisPointer: {
			type: 'line'
		}
	};
	// 工具栏
	option.toolbox = {
		show: true,
		feature: {
			dataZoom: {},
			dataView: {
				readOnly: false
			},
			magicType: {
				type: ['line', 'bar']
			},
			restore: {},
			saveAsImage: {}
		}
	};
	// 数据区域缩放
	option.dataZoom = [{
		type: 'slider',
		xAxisIndex: [0],
		end: 95
	}, {
		type: 'inside',
		xAxisIndex: [0]
	}, {
		type: 'slider',
		yAxisIndex: [0],
	}];
	// X轴
	option.xAxis = {
		type: 'value',
		scale: true,
		nameGap: 10,
		name: spectrumFile.xUnit
	};
	// Y轴
	option.yAxis = {
		type: 'value',
		scale: true,
		name: spectrumFile.yUnit
	};

	var defaultTtemStyle = {
		normal: {
			color: 'rgb(40,60,80)'
		}
	};

	// 系列列表
	option.series = [{
		type: 'line',
		smooth: true,
		symbol: 'pin',
		connectNulls: true,
		name: spectrumFile.spectrumName,
		data: spectrumFile.pointsXY,
		itemStyle: {
			normal: {
			}
		},
		markPoint: {
			data: [{
				name: '最大值',
				type: 'max',
				itemStyle: defaultTtemStyle
			}, {
				name: '最小值',
				type: 'min',
				itemStyle: defaultTtemStyle
			}, {
				name: '平均值',
				type: 'average',
				itemStyle: defaultTtemStyle
			}],
			tooltip: {
				formatter: function(param) {
					return param.name + ' ' + (param.data.coord || '');
				}
			}
		},
		markLine: {
			animation: false,
			data: [{
				name: '平均线',
				type: 'average'
			}]
		}
	}];
	option.markedPeaks = false; //标识是否标峰
	return option;
}
/**
 * 获取chart的图片
 * @param chart
 * @returns base64编码的图片
 */
function echarts_getImage(chart) {
	return chart.getDataURL();
}

/**
 * 标峰
 * @param {Object} chart
 * @param {Object} peaks
 */
function echarts_markPeaks(chart, peaks) {
	var option = chart.getOption();
	var _peaks = option.series[0].markPoint.data;
	for(i in peaks) {
		var data = {
			name: '峰',
			coord: [peaks[i].x, peaks[i].y]
		};
		_peaks.push(data);
	}
	option.series[0].markPoint.data = _peaks; // 数组
	option.markedPeaks = true; // 标峰标识
	echarts_setOption(chart, option);
}
/**
 * 判断是否标峰
 * @param {Object} chart
 */
function echarts_isMarkedPeaks(chart) {
	return chart.getOption().markedPeaks;
}
/**
 * 清除所有峰值显示
 * @param {Object}  chart
 */
function echarts_clearPeaks(chart) {
	var option = chart.getOption();
	for(i in option.series) {
		option.series[i].markPoint.data = [];
	}
	option.markedPeaks = false;
	echarts_setOption(chart, option);
}

/**
 * 修改光谱颜色
 * @param chart
 * @param color
 * @returns
 */
function echarts_changeColor(chart, color) {
	var option = chart.getOption();
	option.series[0].itemStyle.normal.color = color;
	echarts_setOption(chart, option);
}
