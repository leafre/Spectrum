// 初始化echarts实例 
var spectrumChart = echarts_init('echarts');
var spectrumFile = null;

spectrumChart.showLoading();
$.getJSON('test/spectrum.file.json', function(spectrumJson) {
	spectrumFile = new SpectrumFile(spectrumJson);
	spectrumChart.hideLoading();
	echarts_setOption(spectrumChart, echarts_Option(spectrumFile));
	
	// 标峰测试
	var peaks = [{"x":1473.88,"y":23577.7},{"x":1472.84,"y":20338.9},{"x":1274.02,"y":22086.0},{"x":1272.95,"y":26212.3},{"x":1271.89,"y":28992.2},{"x":1270.82,"y":29533.7},{"x":1269.75,"y":27188.7},{"x":1268.69,"y":22834.7},{"x":1263.35,"y":20928.1},{"x":1262.28,"y":22017.8},{"x":1261.21,"y":21501.9}];
	echarts_markPeaks(spectrumChart,peaks);
	// echarts_clearPeaks(spectrumChart);
	console.log(spectrumFile);
});