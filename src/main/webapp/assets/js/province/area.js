function showLocation(province, city, town) {

	var loc = new Location();
	var title = ['省份', '地级市', '县/区'];
	$.each(title, function(k, v) {
		title[k] = '<option value="">' + v + '</option>';
	})

	$('#loc_province_select').append(title[0]);
	$('#loc_city_select').append(title[1]);
	$('#loc_town_select').append(title[2]);

	$("#loc_province_select,#loc_city_select,#loc_town_select").select2({language : 'zh-CN'});
	$('#loc_province_select').change(function() {
		$('#loc_city_select').empty();
		$('#loc_city_select').append(title[1]);
		loc.fillOption('loc_city_select', '0,' + $('#loc_province_select').val());
		$('#loc_city_select').change();
	})

	$('#loc_city_select').change(function() {
		$('#loc_town_select').empty();
		$('#loc_town_select').append(title[2]);
		loc.fillOption('loc_town_select', '0,' +
			$('#loc_province_select').val() + ',' +
			$('#loc_city_select').val());
	})

	$('#loc_town_select').change(function() {
		$('#address').val($('#loc_province_select').select2('data').text +
			$('#loc_city_select').select2('data').text + $('#loc_town_select').select2(
				'data').text);
	})

	if(province) {
		loc.fillOption('loc_province_select', '0', province);
		if(city) {
			loc.fillOption('loc_city_select', '0,' + province, city);

			if(town) {
				loc.fillOption('loc_town_select', '0,' + province + ',' + city, town);
			}
		}
	} else {
		loc.fillOption('loc_province_select', '0');
	}
}

$(function() {
	showLocation();
})