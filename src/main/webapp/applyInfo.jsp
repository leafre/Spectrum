<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="date" uri="/WEB-INF/datetag.tld"%>
<!DOCTYPE html>
<html>
<head>
<title>申请详细信息</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">申请详细信息</h1>
						<p class="description">管理员再次页面上查看详细的申请信息。</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>
			
			<%-- 内容 --%>
			<div class="row">
				<section class="profile-env">
					<div class="row">
						<div class="col-sm-3">
							<!-- User Info Sidebar -->
							<div class="user-info-sidebar">
								<a href="#" class="user-img">
									<img src="assets/images/user-4.png" alt="user-img" class="img-cirlce img-responsive img-thumbnail" />
								</a>
								<a href="#" class="user-name">
									${username}
								</a>
								<span class="user-title">
									性别：${gender}
								</span>
								<span class="user-title">
									<span class="user-title"><date:date value="0${birthday}" parttern="yyyy/MM/dd"></date:date></span>
								</span>
								
								<hr />
								<ul class="list-unstyled user-info-list">
									<li>
										<i class="fa-home"></i>${address}
									</li>
								</ul>
								<hr />
							</div>
						</div>
						<div class="col-sm-9">
							<!-- User timeline stories -->
							<section class="user-timeline-stories">
								<!-- Timeline Story Type: Status -->
								<article class="timeline-story">
									<!-- User info -->
									<header>
										<div class="user-details">
											<h5>真实姓名：</h5>
											<hr />
											<h5>身份证号：</h5>
											<hr />
											<h5>工作单位：</h5>
											<hr />
											<h5>职位：</h5>
											<hr />
											<h5>手机号码：</h5>
											<hr />
											<h5>邮箱地址：</h5>
											<hr />
											<h5>个人描述：</h5>
											<hr />
										</div>
										<div class="form-group-separator"></div>
										<div class="form-group">
											<h5>申请文件：</h5>
											<div class="fa-hover">
												<a href="<c:url value="${applyFile}"/>"><i class="fa fa-file-text-o"></i> <span>申请文件.doc</span></a>
											</div>
										</div>
										<div class="form-group-separator"></div>
										<div class="form-group">
											<button type="button" class="btn btn-info btn-single pull-right" onclick="window.location='#'">拒绝申请</button>
											<button type="button" class="btn btn-info btn-single pull-right" onclick="window.location='#'">通过审核</button>
										</div>
									</header>
								</article>
							</section>

						</div>
					</div>
				</section>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<!-- Imported scripts on this page -->
	<script src="<c:url value='/assets/js/toastr/toastr.min.js'/>"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			// Notifications
			setTimeout(function() {
				var opts = {
					"closeButton" : true,
					"debug" : false,
					"positionClass" : "toast-top-right toast-default",
					"toastClass" : "black",
					"onclick" : null,
					"showDuration" : "100",
					"hideDuration" : "1000",
					"timeOut" : "5000",
					"extendedTimeOut" : "1000",
					"showEasing" : "swing",
					"hideEasing" : "linear",
					"showMethod" : "fadeIn",
					"hideMethod" : "fadeOut"
				};
				toastr.info("通知内容... ...", "通知标题...", opts);
			}, 2000);
		});
	</script>
</body>

</html>