<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<title>添加检测设备</title>
	<%-- 全站样式 --%>
	<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">添加检测设备</h1>
						<p class="description">您可以在此页面上添加硬件检测设备</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" /> </nav>

			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">请填写硬件信息</h3>
						</div>
						<div class="panel-body">
							<form action="uploadPlugin.do" role="form" class="form-horizontal" method="post" enctype="multipart/form-data">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="pluginName">硬件名称</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="pluginName" id="pluginName" placeholder="请填写您要添加的硬件名称">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label">硬件型号</label>
									<div class="col-sm-8">
										<select class="form-control" name="hardwareIDs" id="hardwareIDs" multiple>
										</select>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label">检测光谱类型</label>
									<div class="col-sm-8">
										<select class="form-control" name="spectrumTypeIDs" id="spectrumTypeIDs">
										</select>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label">检测光谱文件类型</label>
									<div class="col-sm-8">
										<select class="form-control" name="spectrumFileTypeID" id="spectrumFileTypeID">
											<option value="0">请选择光谱文件类型</option>
										</select>
									</div>
								</div>
								
								<div class="form-group-separator"></div>
								<div class="form-group col-sm-6">
									<button type="submit" class="btn btn-info pull-right">确认添加</button>
								</div>
								<h3 style="color: red;"> ${addHardware_msg}</h3>
							</form>
						</div>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<div class="modal fade" id="uploadPlugin_msg_modal" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">系统提示信息</h4>
				</div>
				<div class="modal-body">
					<h3>${uploadPlugin_msg}</h3>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${!empty uploadPlugin_msg }">
	<script type="text/javascript">
		$(function() {
			$("#uploadPlugin_msg_modal").modal('show');
		});
	</script>
	</c:if>

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="assets/js/multiselect/css/multi-select.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/select2/select2_locale_zh-CN.js"></script>
	<script src="assets/js/custom/uploadPlugin.js"></script>
	<script src="assets/js/custom/uploadSpectrum.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
		//算法类型
		$("#pluginType").select2();
		//适用的硬件类型
		$("#hardwareIDs").select2({
			placeholder: '点击选择此算法应适应的硬件类型',
			allowClear: true
		}).on('select2-open', function() {
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
		//适用的光谱类型
		$("#spectrumTypeIDs").select2({
			placeholder: '点击选择此算法应适应的光谱类型',
			allowClear: true
		}).on('select2-open', function() {
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
		//算法可以检测的物质
		$("#detectedObjectIDs").select2({
			placeholder: '点击选择此算法可以检测的物质',
			allowClear: true
		}).on('select2-open', function() {
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
		//算法可以检测的内容
		$("#contentIDs").select2({
			placeholder: '点击选择此算法可以检测的内容',
			allowClear: true
		}).on('select2-open', function() {
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
		//算法可以检测的物质
		$("#pluginMainIDs").select2({
			placeholder: '点击选择此算法可以检测的物质',
			allowClear: true
		}).on('select2-open', function() {
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
		//算法可以检测的内容
		$("#pluginSubIDs").select2({
			placeholder: '点击选择此算法可以检测的内容',
			allowClear: true
		}).on('select2-open', function() {
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
	</script>
</body>

</html>