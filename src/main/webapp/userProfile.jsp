<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="date" uri="/WEB-INF/datetag.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>查看个人信息</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation"> <jsp:include
				page="/WEB-INF/jsps/left_navbar.jsp" /> <%-- page-title --%>
			<ul class="user-info-menu left-links list-inline list-unstyled">
				<div>
					<h1 class="title">查看个人信息</h1>
					<p class="description">用户可以在此页面上，查看个人信息（用户近期动态显示）</p>
				</div>
			</ul>
			<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" /> </nav>

			<div class="row">
				<section class="profile-env">
				<div class="col-sm-4">
					<!-- User Info Sidebar -->
					<div class="user-info-sidebar">
					
						<a href="#" class="user-img">
							<c:if test="${null==session_user.photo}">
								<img src="assets/images/user-4.png" alt="user-img"
									class="img-cirlce img-responsive img-thumbnail" />
							</c:if>
							<c:if test="${null!=session_user.photo}">
								<img src="<c:url value='/${session_user.photo}'/>" alt="user-img"
									class="img-cirlce img-responsive img-thumbnail" />
							</c:if>
						</a>
						<a href="#" class="user-name">
							${session_user.username}<span class="user-status is-online"></span>
						</a>
						<span class="user-title">${session_user.gender}</span>
						<span class="user-title"><date:date value="${session_user.birthday}" parttern="yyyy/MM/dd"></date:date></span>
						<hr />
						<ul class="list-unstyled user-info-list">
							<li>
								<i class="fa-home"></i>${session_user.address}
							</li>
						</ul>
						<hr />
						<span class="user-title"></span>
						
					</div>
					
					<div class="col-sm-2"></div>
					<div class="col-sm-8">
				
						<!-- Colored panel, remember to add "panel-color" before applying the color -->
						<div class="panel panel-color panel-gray"><!-- Add class "collapsed" to minimize the panel -->
							<div class="panel-heading">
								<h3 class="panel-title">账号与安全</h3>
								<div class="panel-options">
									
									<a href="#" data-toggle="panel">
										<span class="collapse-icon">&ndash;</span>
										<span class="expand-icon">+</span>
									</a>
									
								</div>
							</div>
							
							<div class="panel-body">
								<button class="btn btn-pink" onclick="window.location.href='updateProfile.jsp';">申请角色</button>
								<button class="btn btn-pink" onclick="modifyPassword()">修改密码</button>
							</div>
						</div>
						
				</div>
				</div>
				
				<div class="col-sm-7">
					<!-- User timeline stories -->
					<section class="user-timeline-stories">
						<!-- Timeline Story Type: Status -->
						<article class="timeline-story">
							<!-- User info --> 
							<header>
							<div class="user-details">
								<h5>
									真实姓名 &nbsp;&nbsp;&nbsp; <font color="black" size=3>
										${session_user.realName}</font>
								</h5>
								<hr />
								<h5>
									身份证号 &nbsp;&nbsp;&nbsp;
									<font color="black" size=3>${session_user.IDNumber}</font>
								</h5>
								<hr />
								<h5>
									工作单位 &nbsp;&nbsp;&nbsp;
									<font color="black" size=3>${session_user.institute}</font>
								</h5>
								<hr />
								<h5>
									职&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;位 &nbsp;&nbsp;&nbsp;
									<font color="black" size=3>${session_user.position}</font>
								</h5>
								<hr />
								<h5>
									手机号码 &nbsp;&nbsp;&nbsp;
									<font color="black" size=3>${session_user.tel}</font>
								</h5>
								<hr />
								<h5>
									邮箱地址 &nbsp;&nbsp;&nbsp;
									<font color="black" size=3>${session_user.email}</font>
								</h5>
								<hr />
								<h5>
									个人描述 &nbsp;&nbsp;&nbsp;
									<font color="black" size=3>${session_user.detail}</font>
								</h5>
								<hr />
							</div>
							<div class="form-group-separator"></div>
							<div class="form-group">
								<button type="button" class="btn btn-info btn-single pull-right"
									onclick="window.location.href='updateProfile.jsp';">修改</button>
							</div>
							</header>
						</article>
					</section>
				</div>
				</section>
			</div>
			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />
	
	<!-- Imported scripts on this page -->
	<!--Specific JS for this page-->
	<script type="text/javascript">
	jQuery(document).ready(function($)
			{
				$(".multi-select").multiSelect({
					afterInit: function()
					{
						// Add alternative scrollbar to list
						this.$selectableContainer.add(this.$selectionContainer).find('.ms-list').perfectScrollbar();
					},
					afterSelect: function()
					{
						// Update scrollbar size
						this.$selectableContainer.add(this.$selectionContainer).find('.ms-list').perfectScrollbar('update');
					}
				});
				
				$(".selectboxit").selectBoxIt().on('open', function()
				{
					// Adding Custom Scrollbar
					$(this).data('selectBoxSelectBoxIt').list.perfectScrollbar();
				});
			});
		
		function modifyPassword(){
			//loadMyFavorite($('#session_userID').val());
			jQuery('#modifyPassword_modal').modal('show', {backdrop: 'static'});
			init();
		};
		function init(){
			$("#modifyPassword-1").attr("class","ms-hover active");
		 	$("#modifyPassword-2").removeClass("completed");;
		 	$("#modifyPassword-2").removeClass("active");
		 	$("#modifyPassword-3").removeClass("completed");;
		 	$("#modifyPassword-3").removeClass("active");
		 	$("#modifyPassword-4").removeClass("active");
		 	$("#modifyPassword-progress").width("0%");
		 	$("#fwv-1").addClass("active");
		 	$("#fwv-2").removeClass("active");
		 	$("#fwv-3").removeClass("active");
		 	$("#fwv-4").removeClass("active");
		}
		function f(i){
		 	$("#modifyPassword-"+i).attr("class","ms-hover completed");
		 	$("#modifyPassword-"+(i+1)).attr("class","ms-hover active");
		 	$("#modifyPassword-progress").width(i*25+"%");
		 	$("#fwv-"+i).removeClass("active");
		 	$("#fwv-"+(i+1)).addClass("active");
		}
	</script>
	
	<!-- modifyPassword -->
	<div class="modal fade" id="modifyPassword_modal">
		<div class="modal-dialog">
			<div class="panel panel-default">	
				<h3 class="text-gray">
					修改密码 <br />
					 <hr />
					<small class="text-muted">按提示逐步填写信息，<strong>请谨慎输入原密码，连续超过5次将会冻结账号</strong></small>
				</h3>
				<br />
				<form role="forl" id="rootwizard" class="form-wizard validate">
					
					<ul class="tabs">
						<li class="ms-hover active" id="modifyPassword-1" name="hhh">
							<a>
								原密码验证
								<span>1</span>
							</a>
						</li>
						<li id="modifyPassword-2">
							<a>
								邮箱验证
								<span>2</span>
							</a>
						</li>
						<li id="modifyPassword-3">
							<a>
								设置新密码
								<span>3</span>
							</a>
						</li>
						<li id="modifyPassword-4">
							<a>
								完成
								<span>4</span>
							</a>
						</li>
					</ul>
					
					<div class="progress-indicator" style="width: 0" id="modifyPassword-progress">
						<span></span>
					</div>
					
					<div class="tab-content no-margin">
						
						<!-- Tabs Content -->
						<div class="tab-pane with-bg active" id="fwv-1">
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label" for="full_name">原密码</label>
										<input class="form-control" name="full_name" id="full_name" data-validate="required" placeholder="请输入您的原密码" />
									</div>
								</div>
							</div>
							<div class="row">
							 <div class="col-md-5"></div>
								<div class="col-md-2">
									<a  class="btn btn-gray" href="javascript:f(1);">确定</a>
								</div>
							</div>
						</div>
						
						<div class="tab-pane with-bg" id="fwv-2">
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-6">
									<label class="control-label" for="street">验证码</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-6">
									<div class="form-group">
										<input class="form-control" name="street" id="street" data-validate="required" placeholder="请输入你邮箱收到的验证码" />
									</div>
								</div>
								
								<div class="col-md-4">
									<div class="form-group">
										<button class="btn btn-pink">点击获取验证码</button>
									</div>
								</div>
								
							</div>
							<div class="row">
							 <div class="col-md-5"></div>
								<div class="col-md-2">
									<a  class="btn btn-gray" href="javascript:f(2);">确定</a>
								</div>
							</div>
							
						</div>
						
						<div class="tab-pane with-bg" id="fwv-3">
							
							<strong>新密码必须为8-16个字符</strong>
							<br />
							<br />
				
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label" for="full_name">新密码</label>
										<input class="form-control" name="full_name" id="full_name" data-validate="required" placeholder="请输入您的新密码" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label" for="full_name">确认密码</label>
										<input class="form-control" name="full_name" id="full_name" data-validate="required" placeholder="请再次确认您的新密码" />
									</div>
								</div>
							</div>
							<div class="row">
							 <div class="col-md-5"></div>
								<div class="col-md-2">
									<a  class="btn btn-gray" href="javascript:f(3);">确定</a>
								</div>
							</div>
						</div>
						
						<div class="tab-pane with-bg" id="fwv-4">
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<h3 class="text-gray">
										恭喜您已成功修改密码！
									</h3>
								</div>
							</div>
						</div>
						
						
						
						<!-- Tabs Pager -->
						
						
						
					</div>
					
				</form>
				
			</div>
		</div>
	</div>
	
	
	
</body>

</html>