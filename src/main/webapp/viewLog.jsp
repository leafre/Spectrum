<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="date" uri="/WEB-INF/datetag.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>查看日志</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
				<div>
					<h1 class="title">日志信息</h1>
					<p class="description">您可以此页面查看用户的日志信息和系统的日志信息</p>
				</div>
			</ul>
			<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" /> </nav>

			<div class="row">
				<!-- Custom column filtering -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">日志列表</h3>
					</div>
					<div class="panel-body">
						<table id="logList" class="table table-striped table-bordered">
							<thead>
								<tr class="replace-inputs">
									<th>编号</th>
									<th>日志等级</th>
									<th>日志类型</th>
									<th>日志时间</th>
									<th>功能编号</th>
									<th>信息</th>
									<th>详细</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="log" items="${querydata}">
								<tr>
									<td width="5%" class="logID">${log.logID}</td>
									<td width="10%" class="level">${log.level}</td>
									<td width="10%" class="type">${log.type}</td>
									<td width="15%" class="time"><date:date value="${log.time}" parttern="yyyy/MM/dd hh:mm:ss"></date:date></td>
									<td width="6%" class="functionID">${log.functionID}</td>
									<td width="45%" class="content">${log.content}</td>
									<td width="5%" class="center">
										<a title="${log.logID}" href="javascript:;" onclick="showLogDetail(this);"
											class="btn btn-info btn-sm btn-icon icon-left"> 详细信息 </a>
									</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<div id="log_modal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">详细信息</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">日志等级</label>
								<input type="text" class="form-control" id="level" disabled />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">日志类型</label>
								<input type="text" class="form-control" id="type" disabled />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">日志时间</label>
								<input type="text" class="form-control" id="time" disabled />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">IP</label>
								<input type="text" class="form-control" id="ip" disabled />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">用户名</label>
								<input type="text" class="form-control" id="username" disabled />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">用户类型</label>
								<input type="text" class="form-control" id="roleName" disabled />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="field-1" class="control-label">操作类型</label>
								<input type="text" class="form-control" id="functionName" disabled />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">详细内容</label>
								<textarea class="form-control" id="content" disabled></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
	<script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script src="assets/js/dateformat.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$("#logList").dataTable().yadcf([ {
				column_number : 1,
				filter_type : 'text'
			}, {
				column_number : 2,
				filter_type : 'text'
			}, {
				column_number : 3,
				filter_type : 'text'
			},{
				column_number : 5,
				filter_type : 'text'
			} ]);
		});
		
		//显示日志详细信息
		
		function showLogDetail(obj) {

			$.getJSON('queryLogDetail.do', {
				logID : $(obj).attr('title')
			}, function(jsondata) {
				var log = jsondata.querydata[0];
				//设置日志详细信息
				$('#level').val(log.level);
				$('#type').val(log.type);
				$('#time').val(new Date(log.time).format("yyyy/MM/dd hh:mm:ss"));
				$('#ip').val(log.ip);
				$('#username').val(log.username);
				$('#roleName').val(log.roleName);
				$('#functionName').val(log.functionName);
				$('#content').val(log.content);
			});

			$('#log_modal').modal('show', {
				backdrop : 'fade'
			});
		}
	</script>
</body>

</html>