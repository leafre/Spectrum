<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<title>上传算法插件</title>
	<%-- 全站样式 --%>
	<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">上传算法插件</h1>
						<p class="description">您可以在此页面上传算法插件服务器，管理员会对您的上传进行申请，申请结果将会发送到您的邮箱</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" /> </nav>

			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">请填写上传信息</h3>
						</div>
						<div class="panel-body">
							<form action="uploadPlugin.do" role="form" class="form-horizontal" method="post" enctype="multipart/form-data">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="pluginName">算法名称</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="pluginName" id="pluginName" placeholder="请填写您的算法名称">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="pluginName">算法版本号</label>

									<div class="col-sm-8">
										<input type="text" class="form-control" name="version" id="version" placeholder="请填写您的算法版本号">
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label">算法类型</label>
									<div class="col-sm-8">
										<select class="form-control" name="pluginType" id="pluginType">
											<option>请选择算法类型</option>
											<option>预处理算法</option>
											<option>数据处理算法</option>
											<option>光谱解析算法</option>
											<option>光谱分析算法</option>
										</select>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label">适用的仪器类型(可选)</label>
									<div class="col-sm-8">
										<select class="form-control" name="hardwareIDs" id="hardwareIDs" multiple>
										</select>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label">适用的光谱类型(可选)</label>
									<div class="col-sm-8">
										<select class="form-control" name="spectrumTypeIDs" id="spectrumTypeIDs">
										</select>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="description">算法描述</label>
									<div class="col-sm-8">
										<textarea class="form-control autogrow" cols="5" name="description" id="description" placeholder="请填写您的算法描述"></textarea>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="detectedObjectIDs">算法可以检测的物质(可选)</label>
									<div class="col-sm-8">
										<select class="form-control" name="detectedObjectIDs" id="detectedObjectIDs">
										</select>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="contentIDs">算法可以检测的内容<br />(可选,若被检测物填写内容，此项必填)</label>
									<div class="col-sm-8">
										<select class="form-control" name="contentIDs" id="contentIDs" multiple>
										</select>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-5">适合的算法上关联(可选)</label>
									<div class="col-sm-8">
										<select class="form-control" name="pluginMainIDs" id="pluginMainIDs" multiple>
										</select>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-5">适合的算法下关联(可选)</label>
									<div class="col-sm-8">
										<select class="form-control" name="pluginSubIDs" id="pluginSubIDs" multiple>
										</select>
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group">
									<label class="col-sm-3 control-label">jar包: </label>
									<div class="col-sm-8">
										<input type="file" name="jarPath" value="" class="col-sm-8" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">源码: </label>
									<div class="col-sm-8">
										<input type="file" name="codePath" value="" class="col-sm-8" />
									</div>
								</div>
								<div class="form-group-separator"></div>
								<div class="form-group col-sm-6">
									<button type="submit" class="btn btn-info pull-right">上传</button>
								</div>
								<h3 style="color: red;"> ${uploadPlugin_msg}</h3>
							</form>
						</div>
					</div>
				</div>
			</div>

			<%-- 底部信息栏 --%>
			<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
		</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<div class="modal fade" id="uploadPlugin_msg_modal" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">系统提示信息</h4>
				</div>
				<div class="modal-body">
					<h3>${uploadPlugin_msg}</h3>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${!empty uploadPlugin_msg }">
	<script type="text/javascript">
		$(function() {
			$("#uploadPlugin_msg_modal").modal('show');
		});
	</script>
	</c:if>

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="assets/js/multiselect/css/multi-select.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/select2/select2_locale_zh-CN.js"></script>
	<script src="assets/js/custom/uploadPlugin.js"></script>
	<!--Specific JS for this page-->
	<script type="text/javascript">
		//算法类型
		$("#pluginType").select2();
		//适用的硬件类型
		$("#hardwareIDs").select2({
			placeholder: '点击选择此算法应适应的硬件类型',
			allowClear: true
		}).on('select2-open', function() {
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
		//适用的光谱类型
		$("#spectrumTypeIDs").select2({
			placeholder: '点击选择此算法应适应的光谱类型',
			allowClear: true
		}).on('select2-open', function() {
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
		//算法可以检测的物质
		$("#detectedObjectIDs").select2({
			placeholder: '点击选择此算法可以检测的物质',
			allowClear: true
		}).on('select2-open', function() {
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
		//算法可以检测的内容
		$("#contentIDs").select2({
			placeholder: '点击选择此算法可以检测的内容',
			allowClear: true
		}).on('select2-open', function() {
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
		//算法可以检测的物质
		$("#pluginMainIDs").select2({
			placeholder: '点击选择此算法可以检测的物质',
			allowClear: true
		}).on('select2-open', function() {
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
		//算法可以检测的内容
		$("#pluginSubIDs").select2({
			placeholder: '点击选择此算法可以检测的内容',
			allowClear: true
		}).on('select2-open', function() {
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
	</script>
</body>

</html>