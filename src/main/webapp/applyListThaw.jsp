<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="date" uri="/WEB-INF/datetag.tld"%>
<!DOCTYPE html>
<html>
<head>
<title>解冻申请管理</title>
<%-- 全站样式 --%>
<jsp:include page="/WEB-INF/jsps/template_style.jsp"></jsp:include>
</head>

<body class="page-body skin-navy">
	<%-- 顶部设置面板 --%>
	<jsp:include page="/WEB-INF/jsps/settings_pane.jsp"></jsp:include>
	<div class="page-container">
		<%-- 侧边菜单 --%>
		<jsp:include page="/WEB-INF/jsps/sidebar_menu.jsp" />
		<div class="main-content">
			<%-- 用户导航栏 --%>
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				<jsp:include page="/WEB-INF/jsps/left_navbar.jsp" />
				<%-- page-title --%>
				<ul class="user-info-menu left-links list-inline list-unstyled">
					<div>
						<h1 class="title">查看解冻</h1>
						<p class="description">您可以在此页面上查看用户的所有申请请求列表。</p>
					</div>
				</ul>
				<jsp:include page="/WEB-INF/jsps/right_navbar.jsp" />
			</nav>

			<div class="row">
				<!-- Tabbed panel 2 -->
				<div class="panel panel-default panel-tabs">
					<!-- Add class "collapsed" to minimize the panel -->
					<div class="panel-heading">
						<h3 class="panel-title">解冻申请</h3>
						<div class="panel-options">
							<ul class="nav nav-tabs" id="nav_tabs">
								<li class="wait"><a
									href="<c:url value='/queryUserThawApplication.do?state=冻结&app_state=等待'/>">待审核</a></li>
								<li class="pass"><a
									href="<c:url value='/queryUserThawApplication.do?state=正常&app_state=通过'/>">通过</a></li>
								<li class="refuse"><a
									href="<c:url value='/queryUserThawApplication.do?state=冻结&app_state=拒绝'/>">拒绝</a></li>
								<li class="all"><a
									href="<c:url value='/queryUserThawApplication.do'/>">所有申请</a></li>
							</ul>
						</div>
					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div class="tab-pane active">
								<div class="panel panel-default">
									<div class="panel-body">
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>用户名</th>
													<th>用户类型</th>
													<th>申请时间</th>
													<th>操作</th>
												</tr>
											</thead>
											<tbody class="middle-align">
												<c:forEach var="userinfo" items="${querydata}">
												<tr>
													<td>${userinfo.username}</td>
													<td>${userinfo.roleName}</td>
													<td>
													<date:date value="${userinfo.applyTime}" parttern="yyyy/MM/dd hh:mm:ss"></date:date>
													</td>
													<td>
														<a href="javascript:;"
															onclick="jQuery('#modal-1').modal('show', {backdrop: 'static'});"
															class="btn btn-info btn-sm btn-icon icon-left"> 申请理由
														</a>
														<c:if test="${userinfo.app_state eq '等待'}">
														<a href="#" class="btn btn-secondary btn-sm btn-icon icon-left">通过</a>
														</c:if>
														<c:if test="${userinfo.app_state eq '等待'}">
														<a href="#" class="btn btn-danger btn-sm btn-icon icon-left">拒绝</a>
														</c:if>
														<c:if test="${userinfo.app_state eq '拒绝'}">
														<a href="#" class="btn btn-danger btn-sm btn-icon icon-left">拒绝理由</a>
														</c:if>
													</td>
												</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%-- 底部信息栏 --%>
	<jsp:include page="/WEB-INF/jsps/main_footer.jsp" />
	</div>
	</div>
	<%-- 尾部内容 --%>
	<jsp:include page="/WEB-INF/jsps/template_tail.jsp" />

	<div class="modal fade" id="modal-1">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">申请理由</h4>
				</div>
				<div class="modal-body">
					<div class="scrollable" data-max-height="300">
						<p>Inclusive donate accelerate progress, empower equity
							life-saving maintain medical supplies. Working families campaign
							local, facilitate transform the world gun control. Kickstarter
							Martin Luther King Jr. vulnerable citizens; gender rights rural
							development Millennium Development Goals innovate measures
							meaningful work metrics philanthropy future making progress
							foundation.</p>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-info">Save changes</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
	<!-- Imported scripts on this page -->
	<script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
	<!-- <script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script> -->
	<!--Specific JS for this page-->
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			var app_state = getUrlParam('app_state');
			var $nav_tabs = $('#nav_tabs');
			// 修改高亮
			if(null == app_state) {
				$nav_tabs.find('.all').addClass('active');
			} else if('通过' == app_state) {
				$nav_tabs.find('.pass').addClass('active');
			} else if('等待' == app_state) {
				$nav_tabs.find('.wait').addClass('active');
			} else if('拒绝' == app_state) {
				$nav_tabs.find('.refuse').addClass('active');
			}
			
			$("table").dataTable({
				aLengthMenu : [
					[ 5, 10, 25, 50, 100, -1 ],
					[ 5, 10, 25, 50, 100, "所有" ] ]
			});
				
		});
	</script>
</body>

</html>