#Spectrum
##2014级暑期项目 **光谱数据库**  :v: 
##项目导入
1. 用SVN/Git客户端检出项目  :fa-plus-square: 

```
SVN: svn://git.oschina.net/BluedotStudio/Spectrum
Git: https://git.oschina.net/BluedotStudio/Spectrum.git
```

2. 根据你的是eclipse或myeclipse复制project下的项目配置文件到该工程根路径
3. 导入eclipse/myeclipse,如果有错误右键选择Maven->Update Project...
4. 数据库表在 project/spectrum.sql ,导入数据库即可。

如果有Maven直接运行package.bat/.sh即可打包war包发布到服务器上运行。
项目地址(测试) [http://bluedot.ittun.com/Spectrum/](http://bluedot.ittun.com/Spectrum/ "Spectrum")

----------

注意:
- 写代码前先用SVN/Git客户端更新
- 提交代码时记得加备注描述
- 不要随意改别人的代码相互讨论一下

按照以上规则一般不会出现冲突。

----------