1.没有使用多表连接的可以不用往下看，不会有影响

2.使用了多表连接的，这里在join内多加了一个【ftable】属性，这个属性不给出，则解析时连接表默认为table属性中的tablename。给出了则为所给出的表名。

3.join-->operation：  1：left join /// 2: right join /// 3：join

4.增加联合主键连接，用","隔开，如下示例

``` xml
	<operation name="test">
		<opetem tem="select" order="1">
			<table many="true" tablename="tableA">
				<join tablename="tableB" fkey="uID,AAA" pid="bID,BBB"
					operation="1" />
				<join tablename="tableC" fkey="fID" pid="cID" operation="3"
					ftable="tableB" />
				<join tablename="tableD" fkey="zID" pid="dID" operation="1"
					ftable="tableC" />
			</table>
			<column>*</column>
			<conditions connector="and" operator="=">a.email</conditions>
		</opetem>
	</operation>
```
```
上面的配置文件的结果：
select * from tableA 
left join tableB on tableA.uID = tableB.bID and tableA.AAA = tableB.BBB 
join tableC on tableB.fID = tableC.cID 
left join tableD on tableC.zID = tableD.dID 
where a.email = ? 
``` 